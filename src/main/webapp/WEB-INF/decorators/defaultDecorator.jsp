<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>

<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
	<meta http-equiv="charset" content="UTF-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>    
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1"/>    
	<title>Layout :: <decorator:title /></title>   
	<c:url value="/" var="contextPath"/>
	<c:url value="/resources" var="resources"/>
	<c:url value="/resources/css" var="stylesheetPath"/>
	<c:url value="/resources/js" var="javascriptPath"/>
	<%-- <link href="${stylesheetPath}/bootstrap.min.css" rel="stylesheet"></link> --%>
	<link href="${stylesheetPath}/jquery-ui.min.css" rel="stylesheet"></link>
	<link href="${stylesheetPath}/bootstrap-theme.min.css" rel="stylesheet"></link>
	<script src="${javascriptPath}/jquery-1.8.3.min.js"></script>
	<script src="${javascriptPath}/jquery-ui-1.10.4.min.js"></script>
	<script src="${javascriptPath}/jquery-ui-i18n.min.js"></script>
	<script src="${javascriptPath}/jquery.layout.min.js"></script>
   <decorator:head />   
</head>
<body>
	<decorator:body />	
	<%-- <script src="${javascriptPath}/bootstrap.min.js"></script> --%>
</body>
</html>