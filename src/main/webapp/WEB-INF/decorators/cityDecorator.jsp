<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>

<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/page" prefix="page" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%-- <jsp:scriptlet>out.print("\r\rdha");</jsp:scriptlet> --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
   <meta http-equiv="charset" content="UTF-8"/>
   <meta http-equiv="X-UA-Compatible" content="IE=edge"/>    
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
   <meta name="viewport" content="width=device-width, initial-scale=1"/>    
   <title>city :: <decorator:title /></title>
   
   <c:url value="/" var="contextPath"/>
   <c:url value="/resources" var="resources"/>
   <c:url value="/resources/css" var="stylesheetPath"/>
   <c:url value="/resources/js" var="javascriptPath"/>
   <link href="${stylesheetPath}/bootstrap.min.css" rel="stylesheet"></link>

   <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
   <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
   <!--[if lt IE 9]>
     <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
     <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
   <![endif]-->
   <decorator:head />
   
</head>
<body>
 	<decorator:getProperty property="page.formative"/>
   ${message} This is the decorator body in user:
	<decorator:body />	
	<decorator:getProperty property="page.formative"/>
	<page:applyDecorator name="city.footer">
		<page:param name="title">Inline content</page:param>
		Some inline stuff.	
	</page:applyDecorator>
	
	<page:applyDecorator name="city.footer">
		<page:param name="title">Inline content2</page:param>
		Some inline stuff2.	
	</page:applyDecorator>
			
   <script src="${javascriptPath}/jquery-1.8.3.min.js"></script>
   <script src="${javascriptPath}/jquery.layout.min.js"></script>
   <script src="${javascriptPath}/bootstrap.min.js"></script>
</body>
</html>