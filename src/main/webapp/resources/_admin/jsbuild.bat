@echo off
echo           ###############################################
echo          ###############################################
echo         ###                                         ###
echo        ###  ####### ###### ####### ###### ######## ###
echo       ###  ##      ##     ##   ## ##       ###    ###
echo      ###  #####   ###### ##   ## #####    ###    ###
echo     ###  ##          ## ##   ## ##       ###    ###
echo    ###  ##   ## ###### ####### ##       ###    ###
echo   ###                                         ###
echo  ###############################################
echo ###############################################
echo:

echo:
rem Software Developer use only
rem Environment Variable and Path Setting
set "EXTJS_APP=app"
set "CURRENT_DIR=%CD%"
set "BUILD_DIR=%CURRENT_DIR%"
set "APP_JSB3=%EXTJS_APP%.jsb3"

if "%SENCHA_HOME%" == "" goto failureSDK
if not "%SENCHA_HOME%" == "" goto setPath

:setPath
set "SENCHA_SDK_HOME=%SENCHA_HOME%\sdk"
set "Path=%Path%;%SENCHA_SDK_HOME%"

if not exist %APP_JSB3% goto failure
sencha build -p %APP_JSB3% -d %BUILD_DIR%
goto end

:failure
echo %APP_JSB3% file not found!
goto end

:end
@echo on
@pause