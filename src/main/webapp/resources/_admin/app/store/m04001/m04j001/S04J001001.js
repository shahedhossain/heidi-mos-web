Ext.define(Fsl.app.getAbsStore('S04J001001'), {
    extend      : 'Ext.data.Store',
    model       :  Fsl.app.getAbsModel('M04J001001'),
    idProperty  : 'id',
    autoLoad    :  false,
    autoSync    :  true,    
    remoteSort  :  false,
    pageSize    :  10,
    proxy       :  Fsl.proxy.getStoreAjaxUrlProxy('S04J001001'),
    sorters: [{
        property    : 'id', 
        direction   : 'asc'
    }]
});