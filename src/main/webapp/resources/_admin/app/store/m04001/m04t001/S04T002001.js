Ext.define(Fsl.app.getAbsStore('S04T002001'), {
    extend      : 'Ext.data.Store',
    model       :  Fsl.app.getAbsModel('M04T002001'),
    idProperty  : 'id',
    autoLoad    :  false,
    autoSync    :  true,    
    remoteSort  :  false,
    pageSize    :  10,
    proxy       :  Fsl.proxy.getStoreAjaxUrlProxy('S04T002001'),
    sorters: [{
        property    : 'id', 
        direction   : 'asc'
    }]
});