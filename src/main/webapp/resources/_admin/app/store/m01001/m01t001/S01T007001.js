Ext.define(Fsl.app.getAbsStore('S01T007001'), {
    extend      : 'Ext.data.Store',
    model       :  Fsl.app.getAbsModel('M01T007001'),
    idProperty  : 'id',
    autoLoad    :  false,
    autoSync    :  true,    
    remoteSort  :  false,
    pageSize    :  10,
    proxy       :  Fsl.proxy.getStoreAjaxUrlProxy('S01T007001'),
    sorters: [{
        property    : 'id', 
        direction   : 'asc'
    }]
});