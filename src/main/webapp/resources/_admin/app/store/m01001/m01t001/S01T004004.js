Ext.define(Fsl.app.getAbsStore('S01T004004'), {
    extend      : 'Ext.data.Store',
    model       :  Fsl.app.getAbsModel('M01T004003'),
    idProperty  : 'id',
    autoLoad    :  false,
    autoSync    :  true,    
    remoteSort  :  false,
    pageSize    :  10,
    proxy       :  Fsl.proxy.getStoreAjaxUrlProxy('C01T004003'),
    sorters: [{
        property    : 'id', 
        direction   : 'asc'
    }]
});