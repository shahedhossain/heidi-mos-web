Ext.define(Fsl.app.getAbsStore('S01T002002'), {   
    extend      : Fsl.app.getAbsStore('S01T002001'),
    model       : Fsl.app.getAbsModel('M01T002001'),
    proxy       : {
        type    : 'memory',
        reader  :{
            type  : 'json',
            root  : undefined
        }
    },
    sorters: [{
        property: 'id', 
        direction: 'asc'
    }]
});