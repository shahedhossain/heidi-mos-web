Ext.define(Fsl.app.getAbsStore('S01I009002'), {   
    extend      : Fsl.app.getAbsStore('S01I009001'),
    model       : Fsl.app.getAbsModel('M01I009001'),
    proxy       : {
        type    : 'memory',
        reader  :{
            type  : 'json',
            root  : undefined
        }
    },
    sorters: [{
        property: 'id', 
        direction: 'asc'
    }]
});