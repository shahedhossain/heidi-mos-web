
       /* api: {
        read    : '${createLink(controller:'C01t005002', action: 'pandingStore')}',
        destroy : '${createLink(controller:'C01t005002', action: 'removeall')}'
        },*/



Ext.define(Fsl.app.getAbsStore('S01T005004'), {
    extend      : 'Ext.data.Store',
    model       :  Fsl.app.getAbsModel('M01T005002'),
    idProperty  : 'id',
    autoLoad    :  false,
    autoSync    :  true,    
    remoteSort  :  false,
    pageSize    :  10,
    proxy       :  Fsl.proxy.getStoreAjaxUrlProxy('S01T005002'),
    sorters: [{
        property    : 'id', 
        direction   : 'asc'
    }]
});