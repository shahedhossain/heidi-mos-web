Ext.define(Fsl.app.getAbsStore('S01I006002'), {   
    extend      : Fsl.app.getAbsStore('S01I006001'),
    model       : Fsl.app.getAbsModel('M01I006001'),
    proxy       : {
        type    : 'memory',
        reader  :{
            type  : 'json',
            root  : undefined
        }
    },
    sorters: [{
        property: 'id', 
        direction: 'asc'
    }]
});