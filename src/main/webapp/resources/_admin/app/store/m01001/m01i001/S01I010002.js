Ext.define(Fsl.app.getAbsStore('S01I010002'), {   
    extend      : Fsl.app.getAbsStore('S01I010001'),
    model       : Fsl.app.getAbsModel('M01I010001'),
    proxy       : {
        type    : 'memory',
        reader  :{
            type  : 'json',
            root  : undefined
        }
    },
    sorters: [{
        property: 'id', 
        direction: 'asc'
    }]
});