Ext.define(Fsl.app.getAbsStore('S01T009001'), {
    extend      : 'Ext.data.Store',
    model       :  Fsl.app.getAbsModel('M01T009001'),
    idProperty  : 'id',
    autoLoad    :  false,
    autoSync    :  true,    
    remoteSort  :  false,
    pageSize    :  10,
    proxy       :  Fsl.proxy.getStoreAjaxUrlProxy('S01T009001'),
    sorters: [{
        property    : 'id', 
        direction   : 'asc'
    }]
});