Ext.define(Fsl.app.getAbsStore('S01I003002'), {   
    extend      : Fsl.app.getAbsStore('S01I003001'),
    model       : Fsl.app.getAbsModel('M01I003001'),
    proxy       : {
        type    : 'memory',
        reader  :{
            type  : 'json',
            root  : undefined
        }
    },
    sorters: [{
        property: 'id', 
        direction: 'asc'
    }]
});