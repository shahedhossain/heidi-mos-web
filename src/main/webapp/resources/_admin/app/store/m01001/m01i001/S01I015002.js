Ext.define(Fsl.app.getAbsStore('S01I015002'), {   
    extend      : Fsl.app.getAbsStore('S01I015001'),
    model       : Fsl.app.getAbsModel('M01I015001'),
    proxy       : {
        type    : 'memory',
        reader  :{
            type  : 'json',
            root  : undefined
        }
    },
    sorters: [{
        property: 'id', 
        direction: 'asc'
    }]
});