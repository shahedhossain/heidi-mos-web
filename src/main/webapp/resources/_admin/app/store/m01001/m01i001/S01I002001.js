Ext.define(Fsl.app.getAbsStore('S01I002001'), {
    extend      : 'Ext.data.Store',
    model       :  Fsl.app.getAbsModel('M01I002001'),
    idProperty  : 'id',
    autoLoad    : true,
    autoSync    : true,
    remoteSort  : true,
    pageSize    : 10,
    proxy       : Fsl.proxy.getStoreAjaxUrlProxy('S01I002001'),
    sorters: [{
        property: 'id', 
        direction: 'asc'
    }]
});