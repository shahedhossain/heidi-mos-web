Ext.define(Fsl.app.getAbsStore('S01I001004'), {
    extend      : 'Ext.data.Store',
    model       : Fsl.app.getAbsModel('M01I001001'),
    idProperty	: 'id',
    autoLoad  	: false,
    autoSync  	: true,    
    remoteSort	: true,
    pageSize  	: 10,
    proxy       : Fsl.proxy.getStoreAjaxUrlProxy('C01I001001'),
    sorters: [{
        property: 'id', 
        direction: 'asc'
    }]
});