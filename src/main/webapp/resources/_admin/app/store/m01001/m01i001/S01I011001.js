Ext.define(Fsl.app.getAbsStore('S01I011001'), {
    extend      : 'Ext.data.Store',
    model       :  Fsl.app.getAbsModel('M01I011001'),
    idProperty  : 'id',
    autoLoad    :  true,
    autoSync    :  true,    
    remoteSort  :  true,
    pageSize    :  10,
    proxy       :  Fsl.proxy.getStoreAjaxUrlProxy('S01I011001'),
    sorters: [{
        property: 'id', 
        direction: 'asc'
    }]
});
