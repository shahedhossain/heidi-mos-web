Ext.define(Fsl.app.getAbsStore('S01I004002'), {   
    extend      : Fsl.app.getAbsStore('S01I004001'),
    model       : Fsl.app.getAbsModel('M01I004001'),
    proxy       : {
        type    : 'memory',
        reader  :{
            type  : 'json',
            root  : undefined
        }
    },
    sorters: [{
        property: 'id', 
        direction: 'asc'
    }]
});