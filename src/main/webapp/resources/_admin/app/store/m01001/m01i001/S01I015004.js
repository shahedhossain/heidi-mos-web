Ext.define(Fsl.app.getAbsStore('S01I015004'), {
    extend      : 'Ext.data.Store',
    model       :  Fsl.app.getAbsModel('M01I015001'),
    idProperty  : 'id',
    autoLoad    :  false,
    autoSync    :  true,    
    remoteSort  :  false,
    pageSize    :  10,
    proxy       :  Fsl.proxy.getStoreAjaxUrlProxy('C01I015002'),
    sorters: [{
        property: 'id', 
        direction: 'asc'
    }]
});
