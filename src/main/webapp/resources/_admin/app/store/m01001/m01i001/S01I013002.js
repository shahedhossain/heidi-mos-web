Ext.define(Fsl.app.getAbsStore('S01I013002'), {   
    extend      : Fsl.app.getAbsStore('S01I013001'),
    model       : Fsl.app.getAbsModel('M01I013001'),
    proxy       : {
        type    : 'memory',
        reader  :{
            type  : 'json',
            root  : undefined
        }
    },
    sorters: [{
        property: 'id', 
        direction: 'asc'
    }]
});