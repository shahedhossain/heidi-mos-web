/*api: {
    read    : '${createLink(controller:'C01t008001', action: 'iItemStore')}',
    destroy : '${createLink(controller:'C01t008001', action: 'removeall')}'
},
*/

Ext.define(Fsl.app.getAbsStore('S01T008003'), {
    extend      : 'Ext.data.Store',
    model       :  Fsl.app.getAbsModel('M01T008002'),
    idProperty  : 'id',
    autoLoad    :  false,
    autoSync    :  true,    
    remoteSort  :  false,
    pageSize    :  10,
    proxy       :  Fsl.proxy.getStoreAjaxUrlProxy('m01001/m01t001/c01t008002'),
    sorters: [{
            property    : 'id', 
            direction   : 'asc'
        }]
});