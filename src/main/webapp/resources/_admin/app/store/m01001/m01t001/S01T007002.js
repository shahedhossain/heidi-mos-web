Ext.define(Fsl.app.getAbsStore('S01T007002'), {   
    extend      : Fsl.app.getAbsStore('S01T007001'),
    model       : Fsl.app.getAbsModel('M01T007001'),
    proxy       : {
        type    : 'memory',
        reader  :{
            type  : 'json',
            root  : undefined
        }
    },
    sorters: [{
        property: 'id', 
        direction: 'asc'
    }]
});