Ext.define(Fsl.app.getAbsStore('S01I001002'), {   
    extend      : Fsl.app.getAbsStore('S01I001001'),
    model       : Fsl.app.getAbsModel('M01I001001'),
    proxy       : {
        type    : 'memory',
        reader  :{
            type  : 'json',
            root  : undefined
        }
    },
    sorters: [{
        property: 'id', 
        direction: 'asc'
    }]
});