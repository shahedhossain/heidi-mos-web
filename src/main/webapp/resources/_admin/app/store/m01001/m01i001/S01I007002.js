Ext.define(Fsl.app.getAbsStore('S01I007002'), {
    extend      : 'Ext.data.Store',
    model       :  Fsl.app.getAbsModel('M01I007001'),
    idProperty  : 'id',
    data        : Fsl.db.M01I007001,
    sorters     : [{
        property: 'id', 
        direction: 'asc'
    }]
});
