Ext.define(Fsl.app.getAbsStore('S01T010002'), {   
    extend      : Fsl.app.getAbsStore('S01T010001'),
    model       : Fsl.app.getAbsModel('M01T010001'),
    proxy       : {
        type    : 'memory',
        reader  :{
            type  : 'json',
            root  : undefined
        }
    },
    sorters: [{
        property: 'id', 
        direction: 'asc'
    }]
});