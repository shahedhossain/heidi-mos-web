Ext.define(Fsl.app.getAbsStore('S01T006002'), {   
    extend      : Fsl.app.getAbsStore('S01T006001'),
    model       : Fsl.app.getAbsModel('M01T006001'),
    proxy       : {
        type    : 'memory',
        reader  :{
            type  : 'json',
            root  : undefined
        }
    },
    sorters: [{
        property: 'id', 
        direction: 'asc'
    }]
});