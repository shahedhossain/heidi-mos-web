Ext.define(Fsl.app.getAbsStore('S01I017002'), {   
    extend      : Fsl.app.getAbsStore('S01I017001'),
    model       : Fsl.app.getAbsModel('M01I017001'),
    proxy       : {
        type    : 'memory',
        reader  :{
            type  : 'json',
            root  : undefined
        }
    },
    sorters: [{
        property: 'id', 
        direction: 'asc'
    }]
});