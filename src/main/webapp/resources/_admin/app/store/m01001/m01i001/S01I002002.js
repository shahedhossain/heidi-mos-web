Ext.define(Fsl.app.getAbsStore('S01I002002'), {   
    extend      : Fsl.app.getAbsStore('S01I002001'),
    model       : Fsl.app.getAbsModel('M01I002001'),
    proxy: {
        type    : 'memory',
        reader:{
            type  : 'json',
            root  : undefined
        }
    },
    sorters: [{
        property: 'id', 
        direction: 'asc'
    }]
});