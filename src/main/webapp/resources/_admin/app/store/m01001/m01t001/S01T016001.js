Ext.define(Fsl.app.getAbsStore('S01T016001'), {
    extend      : 'Ext.data.Store',
    model       :  Fsl.app.getAbsModel('M01T016002'),
    idProperty  : 'id',
    autoLoad    :  false,
    autoSync    :  true,    
    remoteSort  :  false,
    pageSize    :  10,
    proxy       :  Fsl.proxy.getStoreAjaxUrlProxy('S01T016001'),
    sorters: [{
        property    : 'id', 
        direction   : 'asc'
    }]
});