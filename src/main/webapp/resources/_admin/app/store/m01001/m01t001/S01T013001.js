       
/*api: {
            create  : '${createLink(controller:'C01t013001', action: 'add')}',
            read    : '${createLink(controller:'C01t013001', action: 'store')}',
            update  : '${createLink(controller:'C01t013001', action: 'update')}',
            destroy : '${createLink(controller:'C01t014001', action: 'removeall')}'
        }*/


Ext.define(Fsl.app.getAbsStore('S01T013001'), {
    extend      : 'Ext.data.Store',
    model       :  Fsl.app.getAbsModel('M01T013002'),
    idProperty  : 'id',
    autoLoad    :  false,
    autoSync    :  true,    
    remoteSort  :  false,
    pageSize    :  10,
    proxy       :  Fsl.proxy.getStoreAjaxUrlProxy('S01T013001'),
    sorters: [{
        property    : 'id', 
        direction   : 'asc'
    }]
});