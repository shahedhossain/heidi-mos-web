Ext.define(Fsl.app.getAbsStore('S01I007001'), {
    extend      : 'Ext.data.Store',
    model       :  Fsl.app.getAbsModel('M01I007001'),
    idProperty  : 'id',
    autoLoad    :  true,
    autoSync    :  true,    
    remoteSort  :  true,
    pageSize    :  10,
    proxy       :  Fsl.proxy.getStoreAjaxUrlProxy('S01I007001'),
    sorters: [{
        property: 'id', 
        direction: 'asc'
    }]
});
