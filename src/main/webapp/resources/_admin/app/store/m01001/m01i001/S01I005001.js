Ext.define(Fsl.app.getAbsStore('S01I005001'), {
    extend      : 'Ext.data.Store',
    model       :  Fsl.app.getAbsModel('M01I005001'),
    idProperty  : 'id',
    autoLoad    :  false,
    autoSync    :  true,    
    remoteSort  :  false,
    pageSize    :  10,
    proxy       :  Fsl.proxy.getStoreAjaxUrlProxy('S01I005001'),
    sorters: [{
        property: 'id', 
        direction: 'asc'
    }]
});
