Ext.define(Fsl.app.getAbsStore('S01T009002'), {   
    extend      : Fsl.app.getAbsStore('S01T009001'),
    model       : Fsl.app.getAbsModel('M01T009001'),
    proxy       : {
        type    : 'memory',
        reader  :{
            type  : 'json',
            root  : undefined
        }
    },
    sorters: [{
        property: 'id', 
        direction: 'asc'
    }]
});