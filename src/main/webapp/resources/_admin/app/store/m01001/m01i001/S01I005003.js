Ext.define(Fsl.app.getAbsStore('S01I005003'), {
    extend      : 'Ext.data.Store',
    model       :  Fsl.app.getAbsModel('M01I005001'),
    idProperty  : 'id',
    autoLoad    :  false,
    autoSync    :  true,    
    remoteSort  :  false,
    pageSize    :  20,
    proxy       :  Fsl.proxy.getStoreAjaxUrlProxy('C01I005001'),
    sorters: [{
        property: 'id', 
        direction: 'asc'
    }]
});
