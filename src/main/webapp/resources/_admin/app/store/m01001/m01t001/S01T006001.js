
/*api: {
        create  : '${createLink(controller:'C01t006001', action: 'add')}',
        read    : '${createLink(controller:'C01t006001', action: 'store')}',
        update  : '${createLink(controller:'C01t006001', action: 'update')}',
        destroy : '${createLink(controller:'C01t006001', action: 'clearAndAddCart')}'
        },*/
        


Ext.define(Fsl.app.getAbsStore('S01T006001'), {
    extend      : 'Ext.data.Store',
    model       :  Fsl.app.getAbsModel('M01T006002'),
    idProperty  : 'id',
    autoLoad    :  false,
    autoSync    :  true,    
    remoteSort  :  false,
    pageSize    :  10,
    proxy       :  Fsl.proxy.getStoreAjaxUrlProxy('S01T006001'),
    sorters: [{
        property    : 'id', 
        direction   : 'asc'
    }]
});