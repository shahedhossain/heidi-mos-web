Ext.define(Fsl.app.getAbsStore('S01I008001'), {
    extend      : 'Ext.data.Store',
    model       :  Fsl.app.getAbsModel('M01I008001'),
    idProperty  : 'id',
    autoLoad    :  false,
    autoSync    :  true,    
    remoteSort  :  false,
    pageSize    :  10,
    proxy       :  Fsl.proxy.getStoreAjaxUrlProxy('S01I008001'),
    sorters: [{
        property: 'id', 
        direction: 'asc'
    }]
});
