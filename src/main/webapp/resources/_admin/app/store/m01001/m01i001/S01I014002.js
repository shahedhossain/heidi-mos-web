Ext.define(Fsl.app.getAbsStore('S01I014002'), {   
    extend      : Fsl.app.getAbsStore('S01I014001'),
    model       : Fsl.app.getAbsModel('M01I014001'),
    proxy       : {
        type    : 'memory',
        reader  :{
            type  : 'json',
            root  : undefined
        }
    },
    sorters: [{
        property: 'id', 
        direction: 'asc'
    }]
});