Ext.define(Fsl.app.getAbsModel('M04T003001'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M04T003001'),
    idProperty: 'id', 
    fields: [
    {name:'id',             mapping:'id',           type:'int' },
    {name:'role',           mapping:'role',         type:'int'}
    ],
    validations:[       
       { type:'length',        field:'role',         min:1,        max:60} 
    ],
    proxy       : Fsl.proxy.getAjaxApiProxy('M04T003001'),
    hasMany     : [  ],
    belongsTo   : [  ]
});