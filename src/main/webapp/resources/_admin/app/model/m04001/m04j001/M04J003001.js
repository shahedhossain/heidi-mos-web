Ext.define(Fsl.app.getAbsModel('M04J003001'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M04J003001'),
    idProperty: 'id', 
    fields: [
    {name:'id',             mapping:'id',           type:'int'},
    {name:'authorityId',    mapping:'authorityId',  type:'int'},
    {name:'roleId',         mapping:'roleId',       type:'int'},
    {name:'roleName',       mapping:'roleName',     type:'string'}
    ],
    
    proxy       : Fsl.proxy.getAjaxApiProxy('M04J003001'),
    hasMany     : [  ],
    belongsTo   : [ 
        Fsl.data.getBelongsTo('M04T002001', 'id', 'authorityId',    'm04t002001'),
        Fsl.data.getBelongsTo('M04T003001', 'id', 'roleId',         'm04t003001')
     ]
});