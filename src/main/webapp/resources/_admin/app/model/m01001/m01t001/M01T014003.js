Ext.define(Fsl.app.getAbsModel('M01T014003'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01T014003'),
    idProperty: 'id', 
    fields: [
        {name:'id',                   mapping:'id',                  type:'int'},
        {name:'branchId',             mapping:'branchId',            type:'int'},
        {name:'toBranchId',           mapping:'toBranchId',          type:'int'},
        {name:'entryDate',            mapping:'entryDate',           type:'date'},
        {name:'orderId',              mapping:'orderId',             type:'int'}
    ], 
    proxy       : Fsl.proxy.getAjaxApiProxy('M01T014001'),
    hasMany     :[ 		
        Fsl.data.getHasMany('M01T017002', 'id', 'orderId', 'm01t017002s')
    ],
     belongsTo : [ ]
});