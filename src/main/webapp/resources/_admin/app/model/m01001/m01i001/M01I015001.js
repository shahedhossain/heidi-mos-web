Ext.define(Fsl.app.getAbsModel('M01I015001'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01I015001'),
    idProperty: 'id', 
    fields: [
        {name:'id',              mapping:'id',                type:'int'},
        {name:'name',            mapping:'name',              type:'string'},
        {name:'location',        mapping:'location',          type:'string'},
        {name:'address',         mapping:'address',           type:'string'}
    ],
    validations:[        
        {type:'format ',    name:'name',              matcher:/^[\w\d,.#:\-\/ ]{2,45}$/},
        {type:'length ',    name:'location',          min:5,  max:100},    
        {type:'length',     name:'address',           min:5,  max:200}    
       ],
    proxy       : Fsl.proxy.getAjaxApiProxy('M01I015001'),
    hasMany     :[  		
        Fsl.data.getHasMany('M01T004001', 'id', 'branchId', 'm01t004001s'),		
        Fsl.data.getHasMany('M01T003001', 'id', 'branchId', 'm01t003001s'),
	Fsl.data.getHasMany('M01T002001', 'id', 'branchId', 'm01t002001s'),			
        Fsl.data.getHasMany('M01T008001', 'id', 'branchId', 'm01t008001s'),		
	Fsl.data.getHasMany('M01T006001', 'id', 'branchId', 'm01t006001s'),		
        Fsl.data.getHasMany('M01T013001', 'id', 'branchId', 'm01t013001s'),
	Fsl.data.getHasMany('M01I017001', 'id', 'branchId', 'm01i017001s'),
	Fsl.data.getHasMany('M01T009001', 'id', 'branchId', 'm01t009001s'),
        
	Fsl.data.getHasMany('M01T011001', 'id', 'branchId', 'm01t011001s'),
	Fsl.data.getHasMany('M01T011001', 'id', 'toBranchId','m01t011002s'),
        
	Fsl.data.getHasMany('M01I005001', 'id', 'branchId',  'm01i005001s'),
        /* for return Table*/
        Fsl.data.getHasMany('M01T015001', 'id', 'branchId',     'm01t015001s'),
	Fsl.data.getHasMany('M01T015001', 'id', 'fromBranchId', 'm01t015002s'),
        /*for return item*/
        Fsl.data.getHasMany('M01T016001', 'id', 'branchId', 'm01t016001s')
    ]
});

   
   