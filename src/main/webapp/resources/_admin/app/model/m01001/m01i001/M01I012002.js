Ext.define(Fsl.app.getAbsModel('M01I012002'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01I012002'),
    idProperty: 'id', 
    fields: [
        {name:'id',                   mapping:'id',                 type:'int'},
        {name:'price',                mapping:'price',              type:'float'},
        {name:'purchase_price',       mapping:'purchase_price',     type:'double'},  
        {name:'unit_cost',            mapping:'unit_cost',          type:'double'}, 
        {name:'unit_profit',          mapping:'unit_profit',        type:'double'},
        {name:'date',                 mapping:'date',               type:'date'}
    ],
    validations:[ 
        {type:'presence ',  name:'price'},
        {type:'format ',    name:'price',             matcher: /^\d*[0-9](\.\d*[0-9])?$/}
    ],  
    proxy       : Fsl.proxy.getAjaxApiProxy('M01I012002'),
    hasMany     :[ 
        Fsl.data.getHasMany('M01T005002', 'id', 'priceId', 'm01t005002s'),		
	Fsl.data.getHasMany('M01T008002', 'id', 'priceId', 'm01t008002s'),
        Fsl.data.getHasMany('M01T013002', 'id', 'priceId', 'm01t013002s')    
    ]
});
