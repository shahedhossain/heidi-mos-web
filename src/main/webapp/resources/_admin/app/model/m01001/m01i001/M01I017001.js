Ext.define(Fsl.app.getAbsModel('M01I017001'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01I017001'),
    idProperty: 'id', 
    fields: [
        {name:'id',                 mapping:'id',                   type:'int'},        
        {name:'articleId',          mapping:'articleId',            type:'int'},
        {name:'branchId',           mapping:'branchId',             type:'int'},
        {name:'maxQuantity',        mapping:'maxQuantity',          type:'float'},
        {name:'minQuantity',        mapping:'minQuantity',          type:'float'}
    ],
    validations:[       
        {type:'format',     name:'maxQuantity',       matcher: /^\d*[0-9](\.\d*[0-9])?$/},
        {type:'format',     name:'minQuantity',       matcher: /^\d*[0-9](\.\d*[0-9])?$/}
     ],  
    proxy       : Fsl.proxy.getAjaxApiProxy('M01I017001'),
    belongsTo   : [
	Fsl.data.getBelongsTo('M01I006001', 'id', 'articleId', 'm01i006001'),
	Fsl.data.getBelongsTo('M01I015001', 'id', 'brandId', 'm01i015001')
    ] 
});
