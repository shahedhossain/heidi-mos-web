Ext.define(Fsl.app.getAbsModel('M01T015001'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01T015001'),
    idProperty: 'id', 
    fields: [
        {name:'id',                   mapping:'id',                  type:'int'},
        {name:'branchId',             mapping:'branchId',            type:'int'},
        {name:'fromBranchId',         mapping:'fromBranchId',        type:'int'},
        {name:'entryDate',            mapping:'entryDate',           type:'date'},
        {name:'receiveId',            mapping:'receiveId',           type:'int'},
        {name:'transferId',           mapping:'transferId',          type:'int'},
        {name:'returnItem_status',    mapping:'returnItem_status',   type:'string'}
    ], 
    proxy       : Fsl.proxy.getAjaxApiProxy('M01T015001'),
    hasMany     :[ 		
        Fsl.data.getHasMany('M01T016001', 'id', 'returnId', 'm01t016001')
    ],
     belongsTo : [
        Fsl.data.getBelongsTo('M01I015001', 'id', 'branchId',     'm01i015001'),
        Fsl.data.getBelongsTo('M01I015001', 'id', 'fromBranchId', 'm01i015002')
     ]
});