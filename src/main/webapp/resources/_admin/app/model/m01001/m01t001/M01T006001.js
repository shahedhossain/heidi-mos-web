Ext.define(Fsl.app.getAbsModel('M01T006001'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01T006001'),
    idProperty: 'id', 
    fields: [
        {name:'id',             mapping:'id',               type:'int'}, 
        {name:'receiveId',      mapping:'receiveId',        type:'int'},
        {name:'articleId',      mapping:'articleId',        type:'int'},
        {name:'priceId',        mapping:'priceId',          type:'int'},
        {name:'vatId',          mapping:'vatId',            type:'int'},
        {name:'quantity',       mapping:'quantity',         type:'float'},
        {name:'receiveQuantity',mapping:'receiveQuantity',  type:'float'},
        {name:'discountId',     mapping:'discountId',       type:'int'},
        {name:'statusId',       mapping:'statusId',         type:'int'},
        {name:'branchId',       mapping:'branchId',         type:'int'}
    ],
    validations:[
        {type:'format',     name:'quantity',         matcher: /^\d*[0-9](\.\d*[0-9])?$/}
      ],  
    proxy       : Fsl.proxy.getAjaxApiProxy('M01T006001'),
    hasMany     :[],
    belongsTo : [
          Fsl.data.getBelongsTo('M01T012001', 'id', 'receiveId', 'm01t012001'),
		  Fsl.data.getBelongsTo('M01I006001', 'id', 'articleId', 'm01i006001'),
		  Fsl.data.getBelongsTo('M01I012001', 'id', 'priceId', 'm01i012001'),
		  Fsl.data.getBelongsTo('M01I013001', 'id', 'vatId', 'm01i013001'),
		  Fsl.data.getBelongsTo('M01T010001', 'id', 'discountId', 'm01t010001'),
		  Fsl.data.getBelongsTo('M01I015001', 'id', 'branchId', 'm01i015001'),
		  Fsl.data.getBelongsTo('M01I016001', 'id', 'statusId', 'm01i016001')
    ]
	 
});