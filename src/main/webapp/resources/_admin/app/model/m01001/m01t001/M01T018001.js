Ext.define(Fsl.app.getAbsModel('M01T018001'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01T018001'),
    idProperty: 'id', 
    fields: [
        {name:'id',                    mapping:'id',                  type:'int'},
        
        {name:'articleId',             mapping:'articleId',           type:'int'},
        {name:'priceId',               mapping:'priceId',             type:'int'},
        {name:'reqQty',                mapping:'reqQty',              type:'float'}, 
        {name:'appQty',                mapping:'appQty',              type:'float'}, 
        {name:'discountId',            mapping:'discountId',          type:'int'},        
        {name:'vatId',                 mapping:'vatId',               type:'int'},
        {name:'flag',                  mapping:'flag',                type:'boolean'},
        {name:'branchId',              mapping:'branchId',            type:'int'},
        {name:'orderApproveId',        mapping:'orderApproveId',      type:'int'},
    ],  
    proxy       : Fsl.proxy.getAjaxApiProxy('M01T018001'),
    hasMany     :[],
     belongsTo : [
        Fsl.data.getBelongsTo('M01T017001', 'id', 'orderApproveId','m01t017001'),
	Fsl.data.getBelongsTo('M01I006001', 'id', 'articleId',     'm01i006001'),
	Fsl.data.getBelongsTo('M01I013001', 'id', 'vatId',         'm01i013001'),
	Fsl.data.getBelongsTo('M01I012001', 'id', 'priceId',       'm01i012001'),		  
	Fsl.data.getBelongsTo('M01T010001', 'id', 'discountId',    'm01t010001'),
	Fsl.data.getBelongsTo('M01I015001', 'id', 'branchId',      'm01i015001')
     ]
});



