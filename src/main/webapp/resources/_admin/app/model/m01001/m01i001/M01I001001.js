Ext.define(Fsl.app.getAbsModel('M01I001001'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01I001001'),
    idProperty: 'id', 
    fields: [
    {name:'id',        mapping:'id',        type:'int' },
    {name:'name',      mapping:'name',      type:'string'},
    {name:'created',   mapping:'created',      type:'date'}    
    ],
    validations:[       
    { type:'format',        field:'name',         matcher:/^[\w\d,.#:\-\/ ]{2,60}$/ },
    { type:'length',        field:'name',         min:2,        max:60} 
    ],
    proxy      : Fsl.proxy.getAjaxApiProxy('M01I001001'),
    hasMany     :[ 
        Fsl.data.getHasMany('M01I002001', 'id', 'categoryId', 'm01i002001s'),
        Fsl.data.getHasMany('M01I003001', 'id', 'categoryId', 'm01i003001s')
    ]
});