
        /*api: {
            create  : '${createLink(controller:'C01t005002', action: 'create')}',
            read    : '${createLink(controller:'C01t005002', action: 'read')}',
            update  : '${createLink(controller:'C01t005002', action: 'update')}',
            destroy : '${createLink(controller:'C01t005002', action: 'destroy')}'
        }*/
Ext.define(Fsl.app.getAbsModel('M01T005003'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01T005003'),
    idProperty: 'id', 
    fields: [
        {name:'id',                    mapping:'id',                  type:'int'},
        {name:'transferId',            mapping:'transferId',          type:'int'},
        {name:'articleId',             mapping:'articleId',           type:'int'},
        {name:'quantity',              mapping:'quantity',            type:'float'},
        {name:'receiveQuantity',       mapping:'receiveQuantity',     type:'float'},
        {name:'statusId',              mapping:'statusId',            type:'int'},
        {name:'discountId',            mapping:'discountId',          type:'int'},
        {name:'priceId',               mapping:'priceId',             type:'int'},
        {name:'vatId',                 mapping:'vatId',               type:'int'},
        {name:'flag',                  mapping:'flag',                type:'boolean'},        
        
        {name:'branchId',              mapping:'branchId',            type:'int'},
        {name:'toBranchId',            mapping:'toBranchId',          type:'int'},
        {name:'entryDate',             mapping:'entryDate',           type:'date'},
        {name:'total',                 mapping:'total',               type:'float'},
    ],
    validations:[
        {type:'format ',    name:'quantity',      matcher: /^\d*[0-9](\.\d*[0-9])?$/},
        {type:'format ',    name:'receiveQuantity',    matcher: /^\d*[0-9](\.\d*[0-9])?$/}
    ],   
    proxy       : Fsl.proxy.getAjaxApiProxy('M01T005002'),
    hasMany     : [],
    belongsTo   : [
        Fsl.data.getBelongsTo('M01T011001', 'id', 'transferId', 'm01t011001'),
        Fsl.data.getBelongsTo('M01I006001', 'id', 'articleId', 'm01i006001'),
        Fsl.data.getBelongsTo('M01I012001', 'id', 'priceId', 'm01i012001'),
        Fsl.data.getBelongsTo('M01I013001', 'id', 'vatId', 'm01i013001'),
        Fsl.data.getBelongsTo('M01T010001', 'id', 'discountId', 'm01t010001'),
        Fsl.data.getBelongsTo('M01I016001', 'id', 'statusId', 'm01i016001')
    ] 
});

