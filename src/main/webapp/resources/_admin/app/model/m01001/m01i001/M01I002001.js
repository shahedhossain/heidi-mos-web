Ext.define(Fsl.app.getAbsModel('M01I002001'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01I002001'),
    idProperty: 'id', 
    fields: [
        {name:'id',              mapping:'id',                type:'int'},        
        {name:'name',            mapping:'name',              type:'string'},
        {name:'categoryId',      mapping:'categoryId',        type:'int'}
    ],
    validations:[       
	{type:'format', name:'name', matcher:/^[\w\d,.#:\-\/ ]{2,45}$/},
        {type:'length', name:'name', min:5, max:45}
    ],
    proxy      : Fsl.proxy.getAjaxApiProxy('M01I002001'),
    hasMany     :[ 
        Fsl.data.getHasMany('M01I001001', 'id', '', 'm01i001001s'),
        Fsl.data.getHasMany('M01I005001', 'id', 'manufacturerId', 'm01i001005s')
    ]
});