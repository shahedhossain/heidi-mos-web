Ext.define(Fsl.app.getAbsModel('M01T008001'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01T008001'),
    idProperty	: 'id', 
    fields: [
        {name:'id',              mapping:'id',                type:'int'},
        {name:'articleId',       mapping:'articleId',         type:'int'},
        {name:'invoiceId',       mapping:'invoiceId',         type:'int'},
        {name:'branchId',        mapping:'branchId',          type:'int'},
        {name:'priceId',         mapping:'priceId',           type:'int'},
        {name:'vatId',           mapping:'vatId',             type:'int'},        
        {name:'discountId',      mapping:'discountId',        type:'int'},
        {name:'quantity',        mapping:'quantity',          type:'float'},
        {name:'total',           mapping:'total',             type:'float'},
        {name:'flag',            mapping:'flag',              type:'boolean'},
        {name:'entryDate',       mapping:'entryDate',         type:'date'}
    ],  
    proxy       : Fsl.proxy.getAjaxApiProxy('M01T008001'),
    hasMany     :[],
     belongsTo : [
          Fsl.data.getBelongsTo('M01I006001', 'id', 'articleId', 'm01i006001'),
		  Fsl.data.getBelongsTo('M01I012001', 'id', 'priceId', 'm01i012001'),
		  Fsl.data.getBelongsTo('M01I013001', 'id', 'vatId', 'm01i013001'),
		  Fsl.data.getBelongsTo('M01T010001', 'id', 'discountId', 'm01t010001'),
		  Fsl.data.getBelongsTo('M01T007001', 'id', 'invoiceId', 'm01t007001'),
		  Fsl.data.getBelongsTo('M01I015001', 'id', 'branchId', 'm01i015001')
     ]
});	 