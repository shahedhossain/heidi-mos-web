        /*api: {
            create  : '${createLink(controller:'C01t011001', action: 'create')}',
            read    : '${createLink(controller:'C01t011001', action: 'read')}',
            update  : '${createLink(controller:'C01t011001', action: 'update')}',
            destroy : '${createLink(controller:'C01t011001', action: 'destroy')}'
        },*/
Ext.define(Fsl.app.getAbsModel('M01T011002'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01T011002'),
    idProperty	: 'id', 
     fields: [
        {name:'id',                   mapping:'id',                  type:'int'},
        {name:'branchId',             mapping:'branchId',            type:'int'},
        {name:'toBranchId',           mapping:'toBranchId',          type:'int'},
        {name:'statusId',             mapping:'statusId',            type:'int'},
        {name:'entryDate',            mapping:'entryDate',           type:'date'},
        {name:'receiveDate',          mapping:'receiveDate',         type:'date'},
        {name:'receiveId',            mapping:'receiveId',           type:'int'}
        ],
    proxy       : Fsl.proxy.getAjaxApiProxy('M01T011002'),
    hasMany     :[ 		
        Fsl.data.getHasMany('M01T005002', 'id', 'transferId', 'm01t005002s'),
	Fsl.data.getHasMany('M01T012001', 'id', 'transferId', 'm01t012001')
    ],
     belongsTo : [  
	Fsl.data.getBelongsTo('M01I015001', 'id', 'branchId', 'm01i015001'),
	Fsl.data.getBelongsTo('M01I015001', 'id', 'toBranchId', 'm01i015002')
     ]
}); 