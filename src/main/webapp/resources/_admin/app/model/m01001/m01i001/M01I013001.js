Ext.define(Fsl.app.getAbsModel('M01I013001'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01I013001'),
    idProperty: 'id', 
    fields: [
        {name:'id',               mapping:'id',               type:'int'},
        {name:'vat',              mapping:'vat',              type:'float'},   
        {name:'articleId',        mapping:'articleId',        type:'int'},
        {name:'date',             mapping:'date',             type:'date'}
    ],
    validations:[ 
        {type:'presence ',  name:'vat'},
        {type:'format ',    name:'vat',    matcher: /^\d*[0-9](\.\d*[0-9])?$/}
      ],   
    proxy       : Fsl.proxy.getAjaxApiProxy('M01I013001'),
    hasMany     :[ 
	Fsl.data.getHasMany('M01T001002', 'id', 'vatId', 'm01t001002s'),		
        Fsl.data.getHasMany('M01T004001', 'id', 'vatId', 'm01t004001s'),		
        Fsl.data.getHasMany('M01T003001', 'id', 'vatId', 'm01t003001s'),
        Fsl.data.getHasMany('M01T008001', 'id', 'vatId', 'm01t008001s'),		
	Fsl.data.getHasMany('M01T006001', 'id', 'vatId', 'm01t006001s'),
        Fsl.data.getHasMany('M01T005001', 'id', 'vatId', 'm01t005001s'),		
        Fsl.data.getHasMany('M01T013001', 'id', 'vatId', 'm01t013001s'),
        
        
        /*for return item*/
        Fsl.data.getHasMany('M01T016001', 'id', 'vatId', 'm01t016001s')
    ],
    belongsTo   : [
		Fsl.data.getBelongsTo('M01I006001', 'id', 'articleId', 'm01i006001')
    ] 
});
