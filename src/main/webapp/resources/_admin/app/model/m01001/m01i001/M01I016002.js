Ext.define(Fsl.app.getAbsModel('M01I016002'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01I016002'),
    idProperty: 'id', 
    fields: [
        {name:'id',                   mapping:'id',                 type:'int'},
        {name:'name',                 mapping:'name',               type:'string'}
    ],
    validations:[       
        {type:'format', field:'name', matcher:/^[\w\d,.#:\-\/ ]{2,245}$/},
        {type:'length', field:'name', min:2, max:245} 
    ],
    proxy       : Fsl.proxy.getAjaxApiProxy('M01I016002'),
    hasMany     :[ 		
        Fsl.data.getHasMany('M01T005002', 'id', 'statusId', 'm01t005002s'),
        Fsl.data.getHasMany('M01T016002', 'id', 'statusId', 'm01t016002s')
    ]
});