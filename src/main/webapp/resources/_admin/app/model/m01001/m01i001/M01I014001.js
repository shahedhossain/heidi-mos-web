Ext.define(Fsl.app.getAbsModel('M01I014001'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01I014001'),
    idProperty: 'id', 
    fields: [
        {name:'id',              mapping:'id',                type:'int'},
        {name:'titleId',         mapping:'titleId',           type:'int'},
        {name:'name',            mapping:'name',              type:'string'},
        {name:'address',         mapping:'address',           type:'string'},
        {name:'contact',         mapping:'contact',           type:'string'}
    ],
    validations:[       
        {type:'format', field:'name', matcher:/^[\w\d,.#:\-\/ ]{2,45}$/},
        {type:'length', field:'name', min:5, max:45} 
    ],   
    proxy       : Fsl.proxy.getAjaxApiProxy('M01I014001'),
    hasMany     :[ 		
        Fsl.data.getHasMany('M01T004001', 'id', 'vendorId', 'm01t004001s'),		
        Fsl.data.getHasMany('M01T003001', 'id', 'vendorId', 'm01t003001s'),
	Fsl.data.getHasMany('M01T002001', 'id', 'vendorId', 'm01t002001s')
    ],
    belongsTo   : [
		Fsl.data.getBelongsTo('M01I007001', 'id', 'titleId', 'm01i007001')
    ] 
});

   
   