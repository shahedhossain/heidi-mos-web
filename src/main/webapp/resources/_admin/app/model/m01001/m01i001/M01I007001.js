Ext.define(Fsl.app.getAbsModel('M01I007001'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01I007001'),
    idProperty: 'id', 
      fields: [
        {name:'id',              mapping:'id',                type:'int'},
        {name:'name',            mapping:'name',              type:'string'}
    ],
    validations:[       
        {type:'format', field:'name', matcher:/^[\w\d,.#:\-\/ ]{2,45}$/},
        {type:'length', field:'name', min:5, max:45} 
    ],
    proxy       : Fsl.proxy.getAjaxApiProxy('M01I007001'),
    hasMany     :[ 
        Fsl.data.getHasMany('M01T001001', 'id', 'titleId', 'm01t001001s'),
        Fsl.data.getHasMany('M01I014001', 'id', 'titleId', 'm01i014001s'),
        Fsl.data.getHasMany('M01I009001', 'id', 'titleId', 'm01i009001s')
    ]
});