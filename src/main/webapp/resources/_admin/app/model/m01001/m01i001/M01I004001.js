Ext.define(Fsl.app.getAbsModel('M01I004001'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01I004001'),
    idProperty: 'id', 
    fields: [
        {name:'id',              mapping:'id',                type:'int'},
        {name:'name',            mapping:'name',              type:'string'}
    ],
    validations:[       
        {type:'length', name:'name', min:2, max:45}
    ],
    proxy       : Fsl.proxy.getAjaxApiProxy('M01I004001'),
    hasMany     :[ 
        Fsl.data.getHasMany('M01I005001', 'id', 'brandTypeId', 'm01i005001s')
    ]
});