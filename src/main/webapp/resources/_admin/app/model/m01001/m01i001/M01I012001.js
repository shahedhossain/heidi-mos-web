Ext.define(Fsl.app.getAbsModel('M01I012001'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01I012001'),
    idProperty: 'id', 
    fields: [
        {name:'id',                   mapping:'id',                 type:'int'},
        {name:'price',                mapping:'price',              type:'float'},
        {name:'purchase_price',       mapping:'purchase_price',     type:'double'},  
        {name:'unit_cost',            mapping:'unit_cost',          type:'double'}, 
        {name:'unit_profit',          mapping:'unit_profit',        type:'double'},         
        {name:'articleId',            mapping:'articleId',          type:'int'},
        {name:'date',                 mapping:'date',               type:'date'}
    ],
    validations:[ 
        {type:'presence ',  name:'price'},
        {type:'format ',    name:'price',   matcher: /^\d*[0-9](\.\d*[0-9])?$/}
    ],  
    proxy       : Fsl.proxy.getAjaxApiProxy('M01I012001'),
    hasMany     :[ 

        Fsl.data.getHasMany('M01T004001', 'id', 'priceId', 'm01t004001s'),		
	Fsl.data.getHasMany('M01T002001', 'id', 'priceId', 'm01t002001s'),
        Fsl.data.getHasMany('M01T003001', 'id', 'priceId', 'm01t003001s'),
        Fsl.data.getHasMany('M01T008001', 'id', 'priceId', 'm01t008001s'),		
	Fsl.data.getHasMany('M01T006001', 'id', 'priceId', 'm01t006001s'),
        Fsl.data.getHasMany('M01T005001', 'id', 'priceId', 'm01t005001s'),		
        Fsl.data.getHasMany('M01T013001', 'id', 'priceId', 'm01t013001s'),

            /*for return item*/
        Fsl.data.getHasMany('M01T016001', 'id', 'priceId', 'm01t016001s')
    ],
    belongsTo   : [
	Fsl.data.getBelongsTo('M01I006001', 'id', 'articleId', 'm01i006001')
    ] 
});
