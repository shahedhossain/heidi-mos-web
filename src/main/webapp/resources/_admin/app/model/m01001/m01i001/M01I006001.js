Ext.define(Fsl.app.getAbsModel('M01I006001'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01I006001'),
    idProperty: 'id', 
     fields: [
        {name:'id',                 mapping:'id',                   type:'int'},        
        {name:'name',               mapping:'name',                 type:'string'},
        {name:'brandId',            mapping:'brandId',              type:'int'},
        {name:'mbcode',             mapping:'mbcode',               type:'string'},
        {name:'pbcode',             mapping:'pbcode',               type:'string'},
        {name:'details',            mapping:'details',              type:'string'}
    ],
    validations:[       
        {type:'format', name:'name',              matcher:/^[\w\d,.#:\-\/ ]{2,45}$/},
        {type:'length', name:'name',              min:5, max:45},        
        {type:'length', name:'mbcode',            min:4, max:45},
        {type:'length', name:'pbcode',            min:4, max:45}
    ],
    proxy       : Fsl.proxy.getAjaxApiProxy('M01I006001'),
    hasMany     :[ 
        Fsl.data.getHasMany('M01I012001', 'id', 'articleId', 'm01i012001s'),
        Fsl.data.getHasMany('M01I013001', 'id', 'articleId', 'm01i013001s'),
        Fsl.data.getHasMany('M01T004001', 'id', 'articleId', 'm01t004001s'),
	Fsl.data.getHasMany('M01T002001', 'id', 'articleId', 'm01t002001s'),
        Fsl.data.getHasMany('M01T003001', 'id', 'articleId', 'm01t003001s'),
        Fsl.data.getHasMany('M01T008001', 'id', 'articleId', 'm01t008001s'),		
	Fsl.data.getHasMany('M01T006001', 'id', 'articleId', 'm01t006001s'),
        Fsl.data.getHasMany('M01T005001', 'id', 'articleId', 'm01t005001s'),
        Fsl.data.getHasMany('M01T010001', 'id', 'articleId', 'm01t010001s'),
	Fsl.data.getHasMany('M01I017001', 'id', 'articleId', 'm01i017001s'),		
        Fsl.data.getHasMany('M01T013001', 'id', 'articleId', 'm01t013001s'),
        
        /*for return item*/
        Fsl.data.getHasMany('M01T016001', 'id', 'articleId', 'm01t016001s')
    ],
    belongsTo   : [
	Fsl.data.getBelongsTo('M01I005001', 'id', 'brandId', 'm01i005001')
    ] 
});