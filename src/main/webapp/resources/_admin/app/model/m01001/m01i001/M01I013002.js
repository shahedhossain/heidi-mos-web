Ext.define(Fsl.app.getAbsModel('M01I013002'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01I013002'),
    idProperty: 'id', 
    fields: [
        {name:'id',               mapping:'id',               type:'int'},
        {name:'vat',              mapping:'vat',              type:'float'},
        {name:'date',             mapping:'date',             type:'date'}
    ],
    validations:[ 
        {type:'presence ',  name:'vat'},
        {type:'format ',    name:'vat',   matcher: /^\d*[0-9](\.\d*[0-9])?$/}
      ],  
    proxy       : Fsl.proxy.getAjaxApiProxy('M01I013002'),
    hasMany     :[ 
	Fsl.data.getHasMany('M01T005002', 'id', 'vatId', 'm01t005002s'),		
        Fsl.data.getHasMany('M01T008002', 'id', 'vatId', 'm01t008002s'),		
        Fsl.data.getHasMany('M01T013002', 'id', 'vatId', 'm01t013002s')
    ]
});


