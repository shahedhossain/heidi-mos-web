Ext.define(Fsl.app.getAbsModel('M01T001001'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01T001001'),
    idProperty: 'id', 
    fields: [
        {name:'id',         mapping:'id',          type:'int'},
        {name:'titleId',    mapping:'titleId',     type:'int'},   
        {name:'name',       mapping:'name',        type:'string'},   
        {name:'address',    mapping:'address',     type:'string'},
        {name:'contact',    mapping:'contact',     type:'string'}
    ],  
    proxy       : Fsl.proxy.getAjaxApiProxy('M01T001001'),
    hasMany     :[ 		
        Fsl.data.getHasMany('M01T007001', 'id', 'customerId', 'm01t007001s')
    ],
     belongsTo : [
          Fsl.data.getBelongsTo('M01I007001', 'id', 'titleId', 'm01i007001')
     ]
});