Ext.define(Fsl.app.getAbsModel('M01T007002'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01T007002'),
    idProperty	: 'id', 
    fields: [
        {name:'id',              mapping:'id',               type:'int'},        
        {name:'customerId',      mapping:'customerId',       type:'int'},
        {name:'grossVatId',      mapping:'grossVatId',       type:'int'},
        {name:'rebateId',        mapping:'rebateId',         type:'int'},
        {name:'promotionId',     mapping:'promotionId',      type:'int'},
        {name:'payMethodId',     mapping:'paymethodId',      type:'int'},        
        {name:'branchId',        mapping:'branchId',         type:'int'},
        {name:'entryDate',       mapping:'entryDate',        type:'date'},
        {name:'payAmount',       mapping:'payamount',        type:'float'},
        {name:'total',           mapping:'total',            type:'float'},
        {name:'rebate',          mapping:'rebate',           type:'float'},
        {name:'grossVat',        mapping:'grossVat',         type:'float'},
        {name:'promotion',       mapping:'promotion',        type:'float'},
        {name:'gtotal',          mapping:'gtotal',           type:'float'}
    ],
    validations:[ 
        {type:'length',     name:'payAmount',           max:100,min:4}
    ], 
    proxy       : Fsl.proxy.getAjaxApiProxy('M01T007002'),
    hasMany     :[
	Fsl.data.getHasMany('M01T008002', 'id', 'invoiceId', 'm01t008002s')
	]
});	 

