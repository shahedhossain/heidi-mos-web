Ext.define('Fsl.admin.view.CenterPanel' , {
    extend      : 'Ext.tab.Panel',
    alias       : 'widget.centerPanel',
    id          : 'centerPanel',
    layout      : 'fit',
    overflowY	: 'auto',
    autoShow    : true,
    initComponent: function () {
        var me   = this,
        statusid = 'center-panel-status';     
        me.bbar  = Fsl.bbar.getBB01I001(statusid);
        me.items = [{ 
            xtype    	: 'panel',
            title	: 'Profile',
            overflowY	: 'auto',
            bodyCls	: 'fsl-content-bg'          
        }];
        me.callParent(arguments);
    }
});