Ext.define(Fsl.app.getAbsView('V01I001001X03'),{
    extend      : 'Ext.window.Window',
    alias     	: Fsl.app.getAlias('V01I001001X03'),
    id          : 'V01I001001X03'.toLowerCase(), 
    title       : 'CATEGORY :: V01I001001X03', 
    icon        : Fsl.route.getImage('APP01003.png'),	
    minimizable : false,
    maximizable : false,
    autoShow    : true,
    resizable   : false,
    border      : false,
    modal       : true,
    width       : 300,
    height      : 172,
    
    layout    : {
        type: 'vbox',
        align: 'stretch'
    },
    initComponent : function () {    
        var me = this;          
        me.items = [
        {
            xtype           : 'form',
            trackResetOnLoad:  true,
            bodyStyle       : {
                padding     : '10px',
                border      :  true
            },
            tbar: Ext.create('Ext.ux.StatusBar', {
                id                : 'win-statusbar-v01i001001x03',
                topBorder         :  true,
                text              : 'Status',
                defaultIconCls    : 'fsl-severity-info',
                defaultText       : 'Status'
            }),
            items : [{        
                xtype             : 'fieldcontainer',
                combineErrors     : true,
                height            : 65,
                defaults: {
                    layout            : 'fit',
                    width             :  265,
                    anchor            : '90%',
                    afterLabelTextTpl :  required
                },
                items: [{
                    xtype             : 'textfield',
                    name              : 'id',
                    fieldLabel        : 'ID',
                    emptyText         : 'Not Required...',
                    readOnly          :  true,
                    allowBlank        :  true
                },{
                    xtype             : 'textfield',
                    name              : 'name',
                    fieldLabel        : 'Category Name',
                    emptyText         : 'Write Category name',
                    allowBlank        :  false                   
                },{
                    xtype             : 'datefield',
                    name              : 'created',
                    fieldLabel        : 'Created'                 
                }]
            }]
        }]
        this.buttons = [
        {
            text    : 'Save',
            icon    : Fsl.route.getImage('SAV01004.png'),
            action  : 'save'
        },{
            text    : 'Close',
            icon    : Fsl.route.getImage('CLS01001.png'),     
            scope   : this,
            handler : function(button){
                var win = button.up('window');
                win.close();
            }
        }]
        this.callParent(arguments);
    }
});