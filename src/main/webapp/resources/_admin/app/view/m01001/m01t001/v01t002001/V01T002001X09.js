Ext.define(Fsl.app.getAbsView('V01T002001X09'), {
    extend    : 'Ext.grid.Panel',
    alias     : Fsl.app.getAlias('V01T002001X09'),
    id        : 'V01T002001X09'.toString(),
    border    : true,
    modal     : true,
    height    : 285,
    width     : 500,
    viewConfig: {
        stripeRows    : true,
        forceFit      : true,
        emptyText     : 'No Records to display',
        listeners     : {
            viewready : function(v) {
                var store = Fsl.store.getStore('S01I014001');
                store.clearFilter();
                store.loadPage(1);
            }
        }
    },
    initComponent: function() {
       var me     	= this;
       me.store   	= Fsl.app.getRelStore('S01I014001');                   
       me.tbar = Ext.create('Ext.ux.StatusBar', {            
            topBorder           : false,
            statusAlign         : 'right',
            items     : [{
                xtype       : 'button',
                text        : 'Clear',
                icon        : Fsl.route.getIcon('IC01001/CAN01003.png'),
                action      : 'clear'
            },'-',{
                xtype       : 'button',
                text        : 'search',
                icon        : Fsl.route.getIcon('IC01001/search.png'),
                action      : 'search'
            },'-',{  
                xtype       : 'statusbar',
                id          : 'win-statusbar-v01t002001x09',
                border      : false,
                defaultText : '',
                statusAlign : 'right'
            }]
        });
        
        this.bbar = new Ext.PagingToolbar({
            pageSize        :  20,
            store           : Fsl.app.getRelStore('S01I014001'),
            displayInfo     :  true
        });        

        this.columns = [{
            text        : 'SN',
            dataIndex   : 'id',
            width       : 40, 
            sortable    : false
        },{
            text        : 'Tittle',
            dataIndex   : 'titleId',
            renderer    : this.getTitle
        },{
            text        : 'Vendor Name',
            dataIndex   : 'name',
            flex        : 1
        },{
            text        : 'Contact',
            dataIndex   : 'contact'
        },{
            text        : 'Address',
            dataIndex   : 'address',
            flex        : 1
        }];       
        this.callParent(arguments);
    },
    getTitle :function(value, metadata, record, rowIdx, colIdx, store, vie) {
              return record.getM01i007001().data.name;
    }
});