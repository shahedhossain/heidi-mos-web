Ext.define(Fsl.app.getAbsView('V01T012001X09'), {
    extend    : 'Ext.grid.Panel',
    alias     : Fsl.app.getAlias('V01T012001X09'),
    id        : 'V01T012001X09'.toLowerCase(),
    border    : true,
    modal     : true,
    height    : 303,
    width     : 570,    
    viewConfig: {
        stripeRows    : true,
        forceFit      : true,
        loading       : true,
        emptyText     : 'No Records to display',
        listeners     : {
            viewready : function(v) { 
                var store = Fsl.store.getStore('S01T006001');
                store.clearFilter();
                store.loadPage(1);
            }
        }
    },
    features  : [{
        ftype       : 'summary'      
    }],
    initComponent: function() {
        var me     	= this;
        me.store   	= Fsl.app.getRelStore('S01T006001');  
        me.bbar= Ext.create('Ext.ux.StatusBar', {
            id            : 'win-statusbar-v01t012001x09',
            topBorder     : false,
            text          : 'Status',
            defaultIconCls: 'fsl-severity-info',
            defaultText   : 'Status'
        });     
       
        this.columns = [{
            text        : 'ID',
            dataIndex   : 'id',
            width       :  60, 
            sortable    :  false
        },{
            text        : 'Article',
            dataIndex   : 'articleId',            
            renderer    : this.getArticleName,
            flex        : 1
        },{
            text        : 'Rate',
            dataIndex   : 'priceId',
            align       : 'right',
            width       : 70,
            renderer    : this.getPrice
        },{
            text        : 'S.QTY',
            dataIndex   : 'quantity',
            align       : 'right',
            width       : 70,
            tdCls       : 'custom-column',
            summaryRenderer: function(value, summaryData, dataIndex) {                
                return parseFloat(value)
            },
            summaryType : 'count',
            renderer: function (value, meta) {
                meta.css = 'custom-column';
                return value;
            }
        },{
            text        : 'RECE.QTY',
            dataIndex   : 'receiveQuantity',
            align       : 'right',
            width       : 70,
            field       : {
                xtype             : 'numberfield',
                hideTrigger       : true,
                decimalPrecision  : 0,
                keyNavEnabled     : false,
                mouseWheelEnabled : false,
                enableKeyEvents   : true
            },
            summaryType : 'sum', 
            summaryRenderer: function(value, summaryData, dataIndex) {                
                return value
            }
        },{
            text        : 'Vat',
            dataIndex   : 'vatId',
            align       : 'right',
            width       : 50,
            renderer    : this.getVat
        },{
            text        : 'Disc',
            dataIndex   : 'discountId',
            align       : 'right',
            width       : 50,
            renderer    : this.getDiscount
        },{
            header      : 'Total',
            align       : 'right',
            width       : 80,
            sortable    : false,
            groupable   : false,           
            renderer    : function(value, metaData, record, rowIdx, colIdx, store, view) {
                metaData.css        = 'custom-column';
                var rate            = parseFloat(record.getM01i012001().data.price);                
                var quantity        = parseFloat(record.get('quantity'));               
                var vatrate         = parseFloat(record.getM01i013001().data.vat);                
                var vat             = parseFloat(rate *(vatrate/100));
                var total           = parseFloat((rate+vat)*quantity);
                
                var decimalcontrol  = Ext.util.Format.number(total,'0.00');                
                return decimalcontrol+'&nbsp' + 'ট';           
            }
        },{
            text        : 'Status',
            dataIndex   : 'statusId',
            align       : 'right',
            width       : 50,
            renderer    : this.getStatus
        }];   
        this.callParent(arguments);
    },    
    getArticleName :function(value, metadata, record, rowIdx, colIdx, store, vie) {
        return record.getM01i006001().data.name;        
    }, 
    getVendorName :function(value, metadata, record, rowIdx, colIdx, store, vie) {
        return record.getM01i014001().data.name;         
    }, 
    getBranchName :function(value, metadata, record, rowIdx, colIdx, store, vie) {
        return record.getM01i015001().data.name;
    },
    getFormatDate :function(value, metadata, record, rowIdx, colIdx, store, vie){
        return Ext.Date.format(value, 'M d, Y')
    },    
    getPrice :function(value, metaData, record, rowIdx, colIdx, store, view){
        var value=record.getM01i012001().data.price +'&nbsp';
        return value+'ট';       
    },
    getVat : function(value, metaData, record, rowIdx, colIdx, store, view){
        var value=record.getM01i013001().data.vat +'&nbsp';
        return value+'%';        
    },
    getDiscount :function(value, metaData, record, rowIdx, colIdx, store, view){
        var value=record.getM01t010001().data.discount +'&nbsp';
        return value+'%';        
    },
    getStatus : function(value, metaData, record, rowIdx, colIdx, store, view){        
        var value=record.getM01i016001().data.id;
        var icon;
        if(value == 1){
            icon = Fsl.route.getIcon('IC01001/waiting_room-3.png');         
        }else if(value == 2){        
            icon = Fsl.route.getIcon('IC01001/TIC01003.png');
        }else if(value == 3){
            icon = Fsl.route.getIcon('IC01001/partially-receive-icon.png');
        } 
        return "<img src='"+icon+"'/>";     
    }
});