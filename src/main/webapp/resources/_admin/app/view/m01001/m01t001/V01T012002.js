Ext.define(Fsl.app.getAbsView('V01T012002'), {
    extend      : 'Ext.window.Window',
    alias       : Fsl.app.getAlias('V01T012002'),
    icon        : Fsl.route.getImage('APP01009.png'),
    id          : 'V01T012002'.toLowerCase(),
    title       : 'RECEIVE LVM :: V01T012002',
    minimizable : false,
    maximizable : false,
    autoShow    : true,
    resizable   : false,
    modal       : true,
    width       : 570,   
      layout    : {
            type: 'vbox',
            align: 'stretch'
        },
    items  : [{ 
            xtype       : Fsl.app.getXtype('V01T012001X11'),           
            resizable   : false,
            border      : false,
            componentCls: 'my-border-bottom'
    },{ 
            xtype       : Fsl.app.getXtype('V01T012001X12'),             
            resizable   : false,
            border      : false
    }]
});