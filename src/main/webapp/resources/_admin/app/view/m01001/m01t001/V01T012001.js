Ext.define(Fsl.app.getAbsView('V01T012001'), {
    extend      : 'Ext.window.Window',
    alias       : Fsl.app.getAlias('V01T012001'),
    icon        : Fsl.route.getImage('APP01009.png'),
    id          : 'V01T012001'.toLowerCase(),
    title       : 'PENDING LVM :: V01T012001',
    minimizable : false,
    maximizable : false,
    autoShow    : true,
    resizable   : false,
    modal       : true,
    width       : 510,
    height      : 345,    
      layout    : {
            type: 'vbox',
            align: 'stretch'
        },
    items  : [{ 
            xtype       : Fsl.app.getXtype('V01T012001X01'),           
            resizable   : false,
            border      : false,
            componentCls: 'my-border-bottom'
    },{ 
            xtype       : Fsl.app.getXtype('V01T012001X02'),             
            resizable   : false,
            border      : false
    }]
});