Ext.define(Fsl.app.getAbsView('V01I001001'), {
    extend       : 'Ext.tree.Panel',
    alias        : 'widget.v01i000000',
    id           : 'v01i000000',
    title        : 'Navigation',
    rootVisible  : false,
    useArrows    : true,
    store        : 'S01I000000',
    border       : true,
    rootVisible  : false,
    lines        : false,
    singleExpand : true,
    useArrows    : true,

    initComponent: function() {
        required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
        (Ext.defer(function() {
            var hideMask = function () {
                Ext.get('loading').remove();
                Ext.fly('loading-mask').animate({
                    opacity :   0,
                    remove  :   true
                });
            };

            Ext.defer(hideMask, 250);       
        },500));
    
        Ext.apply(Ext.form.VTypes, {
            daterange : function(val, field) {
                var date = field.parseDate(val);

                if(!date){
                    return;
                }
                if (field.startDateField && (!this.dateRangeMax || (date.getTime() != this.dateRangeMax.getTime()))) {
                    var start = Ext.getCmp(field.startDateField);
                    start.setMaxValue(date);
                    start.validate();
                    this.dateRangeMax = date;
                } 
                else if (field.endDateField && (!this.dateRangeMin || (date.getTime() != this.dateRangeMin.getTime()))) {
                    var end = Ext.getCmp(field.endDateField);
                    end.setMinValue(date);
                    end.validate();
                    this.dateRangeMin = date;
                }
        
                return true;
            }
        });          
          
          
        var me  = this;
        me.tbar = [
        {
            xtype        : 'textfield',
            emptyText    : 'Type Here',
            fieldBodyCls :  'search',
            flex         : 1
        }
        ];
        me.bbar=[{ 
            xtype  : 'label',
            text   : 'Info',
            cls    : 'info'
        }]
        me.callParent(arguments);
    },
    statusbar :function(statusId, msg, icon, color){
        var status = Ext.getCmp('win-statusbar-'+statusId);
        status.setStatus({
            text    : '<p style="color: '+color+'">'+msg+'</p>',
            iconCls : icon,
            clear   : {
                wait: 6000,
                anim: false,
                useDefaults: true
            }
        });    
    },
    closeWindow :function(window){
        setTimeout(function(){            
            window.close();
        }, 6000);
    },    
    confirmDelete : function(itemId, modelName, storeName, statusId){
        Ext.MessageBox.show({
            title         : 'Delete',
            msg           : 'Really want to delete ?',
            icon          :  Ext.Msg.WARNING,
            buttons       :  Ext.MessageBox.YESNO,
            buttonText    :{ 
                yes: "Delete", 
                no : "No" 
            },
            scope         : this,
            fn: function(btn){
                if(btn == 'yes'){
                    this.onDeleteClick(itemId, modelName, storeName, statusId)
                }                         
            }         
        });   
    },  
    onDeleteClick : function(id, modelName, storeName, statusId) {
        var grossVat  = Ext.create('${appName}.model.'+modelName, {
            id : id
        });   
        var store     = Ext.data.StoreManager.get(storeName);                 
        grossVat.destroy({
            scope       :this,
            success: function(model, operation) {                 
                if(model != null){                    
                    var text    = operation.response.responseText,
                    json        = Ext.decode(text);
                    store.reload();
                    this.statusbar(statusId, json.message, 'warnInfo', 'red'); 
                }
            },
            failure: function(model, operation){
                this.statusbar(statusId, operation.error, 'warnInfo', 'red');
            }
        });       
    },
    storeFilter : function(storeName, property, value){
        var store = Ext.data.StoreManager.get(storeName);
        store = !store ? Ext.create(storeName) : store;
        if(!value){
            store.clearFilter();
            store.load();
        }else{                                          
            store.clearFilter();
            store.filter(property, value);
            store.load(1);
        }
    },
    storeFilterByMultipleProperty : function(storeName, array){
        var  store  = Ext.data.StoreManager.lookup(storeName);
        store.clearFilter();
        store.loadPage(1, {
            filters  : array                                
        });
    }
});