Ext.define(Fsl.app.getAbsView('V01T012001X04'), {
    extend        : 'Ext.form.Panel',
    alias         : Fsl.app.getAlias('V01T012001X04'),
    id            : 'V01T012001X04'.toLowerCase(),  
    background    : false,
    componentCls  : 'my-border-bottom',
    bodyStyle     : {
        background: 'none'                
    },
    defaults: {        
        activeRecord  : null,
        border        : true, 
        layout        : 'hbox',        
        fieldDefaults : {
            anchor    : '100%',
            labelAlign: 'right'
        }
    },
    
    initComponent     : function(){        
        Ext.QuickTips.init();
        Ext.form.Field.prototype.msgTarget = 'qtip';         
        var me = this;       
        me.items = [{
                xtype: 'fieldcontainer',
                combineErrors: true,                    
                defaults: {
                    layout      : 'fit',
                    flex        : 1,
                    margin      : '5 7 0 10',
                    hideLabel   : false,                
                    labelAlign  : 'left'
                },
                items: [{
                        xtype             : 'textfield',
                        name              : 'branchId',
                        fieldLabel        : 'Branch Id :',
                        allowBlank        : false,
                        value             : 1,
                        minValue          : 1,
                        readOnly          : true
                    },{
                        xtype             : 'textfield',
                        name              : 'fromBranchId',

                        fieldLabel        : 'From Branch Id :',
                        allowBlank        : false,
                        minValue          : 1,
                        readOnly          : true
                    }]
            },{
                xtype             : 'fieldcontainer',
                combineErrors     : true,             
                defaults: { 
                    layout        : 'fit',
                    flex          : 1,
                    margin        : '0 7 0 10',
                    hideLabel     : false,                
                    labelAlign    : 'left'
                },
                items: [
                    {
                        xtype             : 'textfield',
                        fieldLabel        : 'Branch Name :',
                        name              : 'branchName',
                        value             : 'This Branch',
                        readOnly          : true
                    },{
                        xtype             : 'textfield',
                        name              : 'fromBranchName',
                        fieldLabel        : 'From Branch :',
                        emptyText         : 'Double click here..',
                        allowBlank        : false,
                        readOnly          : true,
                        anchor            : '95%',
                        width             : 100,
                        margin            : '0 7 0 10'
                    }]
            },{
                xtype: 'fieldcontainer',
                combineErrors: true,                    
                defaults: {
                    layout      : 'fit',
                    flex        : 1,
                    margin      : '0 7 0 10',
                    hideLabel   : false,                
                    labelAlign  : 'left'
                },
                items: [
                    {
                        xtype             : 'numberfield',                
                        hideTrigger       : true,
                        decimalPrecision  : 0,
                        keyNavEnabled     : false,
                        mouseWheelEnabled : false,
                        fieldLabel        : 'Transfer Id :',                
                        name              : 'transferId',                    
                        anchor            : '95%',
                        readOnly          : true
                    },            
                    {
                        xtype             : 'datefield',
                        fieldLabel        : 'Date :',
                        name              : 'entryDate',
                        maxValue          : new Date(),
                        readOnly          : true,
                        margin            : '0 7 0 12'
                    }]
            },{
                xtype: 'fieldcontainer',
                combineErrors: true,                    
                defaults: {
                    layout      : 'fit',
                    flex        : 1,
                    margin      : '0 7 0 10',
                    hideLabel   : false,                
                    labelAlign  : 'left'
                },
                items: [{
                        xtype             : 'numberfield',                
                        hideTrigger       : true,
                        decimalPrecision  : 0,
                        keyNavEnabled     : false,
                        mouseWheelEnabled : false,
                        fieldLabel        : 'Receive Id :',                
                        name              : 'receiveId' 
                    },                                
                    {
                        xtype             : 'datefield',
                        fieldLabel        : 'Receive Date :',
                        name              : 'receiveDate',
                        format            : 'M d, Y',
                        altFormats        : 'd/m/Y|M d, Y h:i A',
                        value             : '2.4.2013',
                        readOnly          : true
                    }]
            }];       
        me.callParent(arguments);
    }
});

