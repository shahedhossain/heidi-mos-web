Ext.define(Fsl.app.getAbsView('V01R001001X02'), {
    extend      : 'Ext.form.Panel',
    alias     	: Fsl.app.getAlias('V01R001001X02'),
    id          : 'V01R001001X02'.toLowerCase(),  
    bodyStyle   : {
        background  : 'none'
    },
    
    initComponent: function() {
        var me     = this;
        Ext.QuickTips.init();
        Ext.form.Field.prototype.msgTarget = 'qtip';         
        me.items = [{
            xtype           : 'fieldcontainer',
            combineErrors   : true, 
            layout          : 'hbox',
            defaults        : {
                layout      : 'fit',
                margin      : '3 5 1 5',
                hideLabel   : false,                
                labelAlign  : 'top'
            },
            items: [{
                xtype           : 'datefield',
                name            : 'FROM_DATE',
                emptyText       : 'Date or From Date',  
                width           : 135,
                format          : 'Y-m-d H:i:s', 
                id              : 'startdt-v01r001001x02',
                vtype           : 'daterange',
                endDateField    : 'enddt-v01r001001x02'
            },{
                xtype           : 'displayfield',
                fieldLabel      : 'to'
            },{
                xtype           : 'datefield',
                name            : 'TO_DATE',
                emptyText       : 'To Date',  
                width           : 130,
                format          : 'Y-m-d H:i:s',
                id              : 'enddt-v01r001001x02',
                vtype           : 'daterange',
                startDateField  : 'startdt-v01r001001x02' 
            }]
        },{            
            xtype           : 'fieldcontainer', 
            layout          : 'hbox',
            defaults        : {
                layout      : 'fit',
                margin      : '0 5 1 5',
                hideLabel   :  false,                
                labelAlign  : 'top'
            },
            items: [{
                xtype           : 'textfield',
                name            : 'branchName',
                width           :  135,
                emptyText       : 'Double Click for Branch Name',
                readOnly        :  true                
            },{
                xtype           : 'displayfield',
                fieldLabel      : 'or'
            },{
                xtype           : 'textfield',
                name            : 'branchId',
                emptyText       : 'Branch ID',
                allowBlank      :  false,
                width           :  130
            }] 
        }];       
        me.callParent(arguments);
    }
});