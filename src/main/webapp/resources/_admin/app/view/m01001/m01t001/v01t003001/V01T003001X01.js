Ext.define(Fsl.app.getAbsView('V01T003001X01'), {
    extend      : 'Ext.form.Panel',
    alias     	: Fsl.app.getAlias('V01T003001X01'),
    id          : 'V01T003001X01'.toLowerCase(),  
    width       : 660,
    bodyStyle           : {
      background        : 'none'
    },
    defaults: {        
        activeRecord    : null,
        border          : true, 
        layout          : 'hbox',        
        fieldDefaults   : {
            anchor      : '100%',
            labelAlign  : 'right'
        }
    },
    
    initComponent       : function(){        
        Ext.QuickTips.init();
        Ext.form.Field.prototype.msgTarget = 'qtip';         
        me          = this; 
        me.items    = [{
            xtype                 : 'fieldcontainer',
            combineErrors         :  true,                    
            defaults              : {
                layout            : 'fit',
                margin            : '0 5 0 5',
                hideLabel         :  false,                
                labelAlign        : 'top',
                flex              : 1
            },
            items: [{
                xtype             : 'numberfield',
                hidden            :  true,
                name              : 'id',
                minValue          :  0,
                mouseWheelEnabled :  false,
                hideTrigger       :  true,
                decimalPrecision  :  0,
                keyNavEnabled     :  false
            },{
                xtype             : 'numberfield',
                fieldLabel        : 'Article ID',
                name              : 'articleId',
                minValue          :  0,
                mouseWheelEnabled :  false,
                hideTrigger       :  true,
                decimalPrecision  :  0,
                keyNavEnabled     :  false 
            },{
                xtype             : 'textfield',
                fieldLabel        : 'Article Name',
                name              : 'articleName'
            },{
                xtype             : 'textfield',
                fieldLabel        : 'MBCODE',
                name              : 'mbcode'
            },{
                xtype             : 'textfield',
                fieldLabel        : 'PBCODE',
                name              : 'pbcode'
            }]
        },{
            xtype                 : 'fieldcontainer',
            combineErrors         :  true,                    
            defaults              : {
                layout            : 'fit',
                margin            : '0 5 0 5',
                hideLabel         :  false,                
                labelAlign        : 'top',
                flex              : 1
            },
            items: [{
                xtype             : 'numberfield',
                fieldLabel        : 'Rate',
                name              : 'price',
                minValue          :  0,
                mouseWheelEnabled :  false,
                hideTrigger       :  true,
                decimalPrecision  :  0,
                keyNavEnabled     :  false 
            },{
                xtype             : 'numberfield',
                fieldLabel        : 'Vat',
                name              : 'vat',
                minValue          :  0,
                mouseWheelEnabled :  false,
                hideTrigger       :  true,
                decimalPrecision  :  0,
                keyNavEnabled     :  false 
            },{
                xtype             : 'numberfield',
                name              : 'vandorId',
                fieldLabel        : 'Vandor Id:',                       
                minValue          :  0,
                mouseWheelEnabled :  false,
                hideTrigger       :  true,
                decimalPrecision  :  0,
                keyNavEnabled     :  false                 
            },{
                xtype             : 'textfield',
                fieldLabel        : 'Vandor',
                name              : 'vandorName'
            }]
        }]
        me.callParent(arguments);
    }
});

