Ext.define(Fsl.app.getAbsView('V01T007001X03'), {
    extend      : 'Ext.window.Window',
    alias       : Fsl.app.getAlias('V01T007001X03'),
    icon        : Fsl.route.getImage('APP01009.png'),
    id          : 'V01T007001X03'.toLowerCase(),
    title       : 'INVOICEITEM :: V01T007001X03',
    minimizable : false,
    maximizable : false,
    autoShow    : true,
    resizable   : false,
    modal       : true,
      layout    : {
            type    : 'vbox',
            align   : 'stretch'
        },
    items  : [{ 
            xtype         :  Fsl.app.getXtype('V01T007001X04'),          
            resizable     :  false,
            border        :  false
    },{ 
            xtype         : Fsl.app.getXtype('V01T007001X05'),            
            resizable     :  false,
            border        :  false,
            componentCls  : 'my-border-bottom-left-right'
    },{ 
            xtype         : Fsl.app.getXtype('V01T007001X06'),          
            resizable     :  false,
            border        :  false,
            componentCls  : 'my-border-bottom-left-right'
    }],
      
    buttons: [
    {
        text    : 'NEW',
        icon    : Fsl.route.getImage('NEW01001.png'),
        action  : 'newInvoice'
    },{
        text    : 'SAVE',
        icon    :  Fsl.route.getImage('SAV01004.png'),
        action  : 'save'
    },{
        text    : 'PDF',
        icon    :  Fsl.route.getImage('PDF01001.png'),
        action  : 'pdf'
    }]
});