Ext.define(Fsl.app.getAbsView('V01I001001'), {
    extend      : 'Ext.window.Window',
    alias       : Fsl.app.getAlias('V01I001001'),
    icon        : Fsl.route.getImage('APP01009.png'),
    id          : 'V01I001001'.toLowerCase(),
    title       : 'CATEGORY LVM :: V01I001001',
    minimizable :  false,
    maximizable :  false,
    autoShow    :  true,
    resizable   :  false,
    modal       :  true,
    width       :  400,
    height      :  367, 
    layout      :  {
        type	: 'vbox',
        align	: 'stretch'
    },
    items  : [{ 
        xtype     	: Fsl.app.getXtype('V01I001001X01'), 
        resizable 	: false,
        border    	: false,
        componentCls    : 'my-border-bottom'
    },{ 
        xtype     	: Fsl.app.getXtype('V01I001001X02'),       
        resizable 	: false,
        border    	: false
    }]
});