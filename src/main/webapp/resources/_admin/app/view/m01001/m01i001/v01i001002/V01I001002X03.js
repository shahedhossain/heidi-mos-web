Ext.define(Fsl.app.getAbsView('V01I001002X03'),{
    extend            : 'Ext.window.Window',
    alias             : Fsl.app.getAlias('V01I001002X03'),
    id                : 'V01I001002X03'.toLowerCase(),
    title             : 'CATE. BY MANUFAC. :: V01I001002X03',
    icon              : Fsl.route.getImage('APP01009.png'),
    minimizable       : false,
    maximizable       : false,
    autoShow          : true,
    resizable         : false,
    modal             : true, 
    layout            : {
        type          : 'vbox',
        align         : 'stretch'
    },
    items: [{ 
        xtype         : Fsl.app.getXtype('V01I001002X04'),
        resizable     :  false,
        border        :  false,
        componentCls  : 'my-border-bottom'
    },{
        xtype         : Fsl.app.getXtype('V01I001002X05'),
        border        :  false
    }]
});