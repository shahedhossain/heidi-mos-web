﻿Ext.define(Fsl.app.getAbsView('V01I007001X01'), {
    extend      : 'Ext.form.Panel',
    alias     	: Fsl.app.getAlias('V01I007001X01'),
    id          : 'V01I007001X01'.toLowerCase(),  
    bodyStyle   : {
        background  : 'none'
    },
    defaults: {        
        activeRecord  : null,
        layout        : 'hbox',        
        fieldDefaults : {
            anchor    : '100%',
            labelAlign: 'right'
        }
    },
    initComponent: function() {
        var me     = this;
        Ext.QuickTips.init();
        Ext.form.Field.prototype.msgTarget = 'qtip';         
        me.items = [{
            xtype           : 'fieldcontainer',
            combineErrors   : true,                    
            defaults        : {
                layout      : 'fit',
                margin      : '3 5 1 5',
                hideLabel   : false,                
                labelAlign  : 'top'
            },
            items: [{
                xtype             : 'numberfield',
                name              : 'id',
                fieldLabel        : 'ID :',
                minValue          : 1,
                width             : 100,                        
                minValue          :  0,
                mouseWheelEnabled :  false,
                hideTrigger       :  true,
                decimalPrecision  :  0,
                keyNavEnabled     :  false                        
            },{
                xtype             : 'textfield',
                name              : 'name',
                fieldLabel        : 'NAME:',
                flex              : 1
            }]
        }];       
        me.callParent(arguments);
    }
});