Ext.define(Fsl.app.getAbsView('V01I016001'), {
    extend      : 'Ext.window.Window',
    alias       : Fsl.app.getAlias('V01I016001'),
    icon        : Fsl.route.getImage('APP01009.png'),
    id          : 'V01I016001'.toString(),
    title       : 'STATUS LVM :: V01I016001',
    minimizable :  false,
    maximizable :  false,
    autoShow    :  true,
    resizable   :  false,
    modal       :  true, 
    layout      :  {
        type	: 'vbox',
        align	: 'stretch'
    },
    items  : [{ 
        xtype     	: Fsl.app.getXtype('V01I016001X01'),       
        resizable 	: false,
        border    	: false
    }]
});