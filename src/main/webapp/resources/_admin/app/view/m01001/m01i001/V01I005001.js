Ext.define(Fsl.app.getAbsView('V01I005001'), {
    extend      : 'Ext.window.Window',
    alias       : Fsl.app.getAlias('V01I005001'),
    id          : 'V01I005001'.toLowerCase(),
    title       : 'BRAND LVM :: V01I005001',
    icon        : Fsl.route.getImage('APP01009.png'),
    minimizable : false,
    maximizable : false,
    autoShow    : true,
    resizable   : false,
    modal       : true,
    width       : 500,   
    layout    : {
        type: 'vbox',
        align: 'stretch'
    },
    items  : [{ 
        xtype       :  Fsl.app.getXtype('V01I005001X01'),
        resizable   :  false,
        border      :  false,
        componentCls: 'my-border-bottom'
    },{ 
        xtype       :  Fsl.app.getXtype('V01I005001X02'),
        resizable   :  false,
        border      :  false
    }]
});