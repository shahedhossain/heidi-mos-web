Ext.define(Fsl.app.getAbsView('V01I005001X07'), {
    extend      : 'Ext.window.Window',
    alias       : Fsl.app.getAlias('V01I005001X07'),
    id          : 'V01I005001X07'.toLowerCase(),
    title       : 'GENERIC PRODUCT LVM :: V01I005001X07',
    icon        : Fsl.route.getImage('APP01009.png'),
    minimizable : false,
    maximizable : false,
    autoShow    : true,
    resizable   : false,
    modal       : true,
    width       : 380,
    height      : 375,    
      layout    : {
            type: 'vbox',
            align: 'stretch'
        },
    items  : [{ 
        xtype       :  Fsl.app.getXtype('V01I005001X08'),
        resizable   :  false,
        border      :  false,
        componentCls: 'my-border-bottom'
    },{ 
        xtype       :  Fsl.app.getXtype('V01I005001X09'),
        resizable   :  false,
        border      :  false
    }]
});