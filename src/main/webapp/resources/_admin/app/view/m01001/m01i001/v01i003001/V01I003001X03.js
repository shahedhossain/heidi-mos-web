Ext.define(Fsl.app.getAbsView('V01I003001X03'), { 
    extend      : 'Ext.window.Window',
    alias       :  Fsl.app.getAlias('V01I003001X03'),
    id          : 'V01I003001X03'.toLowerCase(),   
    title       : 'GENERIC PRODUCT :: V01I003001X03',
    icon        :  Fsl.route.getImage('APP01003.png'),	
    minimizable :  false,
    maximizable :  false,
    autoShow    :  true,
    resizable   :  false,
    border      :  false,
    modal       :  true,
    width       :  300,
    height      :  183,
    layout      :  {
        type    : 'vbox',
        align   : 'stretch'
    },
    initComponent :  function () {       
        this.items = [
        {
            xtype                   : 'form',
            trackResetOnLoad        :  true,
            bodyStyle               : {
                padding             : '10px',
                border              : true
            },
            tbar: Ext.create('Ext.ux.StatusBar', {
                id                  : 'win-statusbar-v01i003001x03',
                topBorder           : true,
                text                : 'Status',
                defaultIconCls      : 'fsl-severity-info',
                defaultText         : 'Status'
            }),
            items: [{
                xtype               : 'numberfield',
                fieldLabel          : 'Generic Id',
                name                : 'id',
                width               :  265,
                minValue            :  0,
                mouseWheelEnabled   :  false,
                hideTrigger         :  true,
                decimalPrecision    :  0,
                keyNavEnabled       :  false,
                readOnly            :  true
            },{
                xtype               : 'textfield',
                name                : 'name',
                fieldLabel          : 'Generic Name',
                allowBlank          :  false,
                width               :  265,
                afterLabelTextTpl   :  required
            },
            {
                xtype               : 'textfield',
                name                : 'categoryName',
                fieldLabel          : 'Category',
                emptyText           : 'Double Click Here',
                width               :  265,
                allowBlank          :  false,
                editable            :  false,
                afterLabelTextTpl   :  required,
                readOnly            :  true,
                listeners: {
                    dblclick: {
                        element: 'el',
                        fn: function(){
                            Ext.widget('v01i003001x04').show();
                        }
                    }
                }
            },
            {
                xtype               : 'numberfield',
                hidden              :  true,
                name                : 'categoryId',
                minValue            :  0,
                mouseWheelEnabled   :  false,
                hideTrigger         :  true,
                decimalPrecision    :  0,
                keyNavEnabled       :  false,
                readOnly            :  true
            }]
        }];
        this.buttons = [{
            text        : 'Save',
            icon        : Fsl.route.getImage('SAV01004.png'),
            action      : 'save'
        },{
            text        : 'Close',
            icon        : Fsl.route.getImage('CLS01001.png'),
            scope       : this,
            handler     : this.close
        }];
        this.callParent(arguments);
    }
});