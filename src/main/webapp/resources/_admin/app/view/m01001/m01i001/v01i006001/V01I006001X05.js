Ext.define(Fsl.app.getAbsView('V01I006001X05'), {   
    extend          : 'Ext.form.Panel',
    alias           : Fsl.app.getAlias('V01I006001X05'),
    id              : 'V01I006001X05'.toLowerCase(), 
    height        	:  95,
    bodyStyle     	: {
		background  : 'none'
    },
    defaults: {        
        activeRecord  :  null,
        border        :  true, 
        layout        : 'hbox',        
        fieldDefaults :  {
            anchor    : '100%',
            labelAlign: 'right'
        }
    },
    
    initComponent     : function(){        
        Ext.QuickTips.init();
        Ext.form.Field.prototype.msgTarget = 'qtip';         
        var me        = this;      
        me.items  = [{
            xtype           : 'fieldcontainer',
            combineErrors   : true,                    
            defaults        : {
                layout      : 'fit',
                flex        :  1,
                margin      : '0 5 0 5',
                hideLabel   :  false,                
                labelAlign  : 'top'
            },
            items: [{
                xtype             : 'numberfield',
                name              : 'id',
                fieldLabel        : 'Brand Id :',
                minValue          :  1,
                width             :  100,                        
                minValue          :  0,
                mouseWheelEnabled :  false,
                hideTrigger       :  true,
                decimalPrecision  :  0,
                keyNavEnabled     :  false,
                scope             :  this,                
                listeners: {                    
                    dblclick    : {
                        element : 'el',
                        scope   : this,
                        fn: function(){
                        var form   = this.getForm(); //Must be this scoped 
                            form.findField('id').setValue('');                            
                        }
                    }
                }
            },{
                xtype             : 'textfield',
                name              : 'name',
                fieldLabel        : 'Brand Name:',                        
                scope             :  me,                
                listeners         : {
                    dblclick    : {
                        element : 'el',
                        scope   : this,
                        fn: function(){
                        var form   = this.getForm(); //Must be this scoped 
                            form.findField('name').setValue('');                            
                        }
                    }
                }                  
            },
            {
                xtype             : 'textfield',
                name              : 'brandTypeName',
                fieldLabel        : 'Brand Type',
                scope             :  this,                
                listeners     : {                    
                    'dblclick'  : {
                     scope   : this,
                     element : 'el',
                         fn   : function(evt, el, opt) {
                              var form   = this.getForm(); //Must be this scoped                                            
                              form.findField('brandTypeName').setValue('').focus();
                        }
                    } 
                }
            }]
        },
        {
            xtype           : 'fieldcontainer',
            combineErrors   : true,                    
            defaults        : {
                layout      : 'fit',
                flex        :  1,
                margin      : '0 5 0 5',
                hideLabel   :  false,                
                labelAlign  : 'top'
            },
            items: [{
                xtype             : 'textfield',
                name              : 'manufacturerName',
                fieldLabel        : 'Manufac. Name',
                scope             :  this,
                listeners: {
                    dblclick    : {
                        element : 'el',
                        scope   : this,
                        fn: function(){
                        var form   = this.getForm(); //Must be this scoped 
                            form.findField('manufacturerName').setValue('');
                        }
                    }
                }
            },{
                xtype             : 'textfield',
                name              : 'genericProductName',
                fieldLabel        : 'GP. Name',
                scope             :  me,
                listeners: {
                    dblclick: {
                        element: 'el',
                        scope   : this,
                        fn: function(){
                        var form   = this.getForm(); //Must be this scoped                             
                            form.findField('genericProductName').setValue('');
                        }
                    }
                }
            }]
        }];       
        me.callParent(arguments);
    }
});

