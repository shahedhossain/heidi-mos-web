Ext.define(Fsl.app.getAbsView('V01T014001X05'), {
    extend    : 'Ext.grid.Panel',
    alias     : Fsl.app.getAlias('V01T014001X05'),
    id        : 'V01T014001X05'.toLowerCase(),
    border    : true,
    modal     : true,
    height    : 303,
    width     : 570,    
    viewConfig: {
        stripeRows    : true,
        forceFit      : true,
        loading       : true,
        emptyText     : 'No Records to display',
        listeners     : {
            viewready : function(v) { 
                var store = Fsl.store.getStore('S01T013001');
                //                store.clearFilter();
                //                store.loadPage(1);
                
            }
        }
    },
    features  : [{
        ftype       : 'summary'       
    }],
    initComponent: function() {
        var me      = this;
        me.store    = Fsl.app.getRelStore('S01T013001');  
        me.bbar= Ext.create('Ext.ux.StatusBar', {
            id            : 'win-statusbar-v01t014001x05',
            topBorder     : false,
            text          : 'Status',
            defaultIconCls: 'fsl-severity-info',
            defaultText   : 'Status'
        });
        this.columns = [{
            text      : 'SN',
            xtype     : 'rownumberer',
            width     :  40, 
            sortable  :  false
        },{
            text        : 'ID',
            dataIndex   : 'id',
            width       :  60, 
            sortable    :  false,
            hidden      : true
        },{
            text        : 'Article',
            dataIndex   : 'articleId',            
            renderer    : this.getArticleName,
            flex        : 1,
            field: {
                xtype: 'numberfield'
            }
        },{
            text        : 'Rate',
            dataIndex   : 'priceId',
            align       : 'right',
            width       : 70,
            renderer    : this.getPrice,
            field: {
                xtype: 'numberfield'
            }
        },{
            text        : 'Req.QTY',
            dataIndex   : 'quantity',
            align       : 'right',
            width       : 70,
            field: {
                xtype: 'numberfield'
            },            
            summaryRenderer: function(value, summaryData, dataIndex) {                
                return parseFloat(value)
            },
            summaryType : 'count'
        },{
            text        : 'Approval.QTY',
            dataIndex   : 'approvalQty',
            align       : 'right',
            width       : 70,
            field: {
                xtype: 'numberfield'
            },            
            summaryRenderer: function(value, summaryData, dataIndex) {                
                return parseFloat(value)
            },
            summaryType : 'count'
        },{
            text        : 'Vat',
            dataIndex   : 'vatId',
            align       : 'right',
            width       : 50,
            renderer    : this.getVat
        },{
            text        : 'Disc',
            dataIndex   : 'discountId',
            align       : 'right',
            width       : 50,
            renderer    : this.getDiscount
        },{
            header      : 'Total',
            align       : 'right',
            width       : 80,
            sortable    : false,
            groupable   : false,           
            renderer    : function(value, metaData, record, rowIdx, colIdx, store, view) {
                var rate            = parseFloat(record.getM01i012001().data.price);                
                var quantity        = parseFloat(record.get('quantity'));               
                var vatrate         = parseFloat(record.getM01i013001().data.vat);                
                var vat             = parseFloat(rate *(vatrate/100));
                var total           = parseFloat((rate+vat)*quantity);
                var decimalcontrol  = Ext.util.Format.number(total,'0.00');                
                return decimalcontrol+'&nbsp' + '৳';           
            }
        },{
            menuDisabled    : true,
            sortable        : false,
            xtype           : 'actioncolumn',            
            width           : 22,            
            items           : [{
                icon        : Fsl.route.getImage('DEL01005.png'),
                tooltip     : 'Delete This?',
                scope       : this,
                handler     : function(grid, rowIdx, colIdx) {                                      
                    var record = grid.getStore().getAt(rowIdx);                     
                    var articleId = record.getM01i006001().data.id;
                    Ext.getCmp('WestPanel').confirmDelete(articleId, 'M01T013001', 'S01T013001', 'v01t014001x05');
                }
            }]
        }];       
        this.callParent(arguments);
    },    
    getArticleName :function(value, metadata, record, rowIdx, colIdx, store, vie) {
        return record.getM01i006001().data.name;
    }, 
    getVendorName :function(value, metadata, record, rowIdx, colIdx, store, vie) {
        return record.getM01i014001().data.name;
    }, 
    getBranchName :function(value, metadata, record, rowIdx, colIdx, store, vie) {
        return record.getM01i015001().data.name;
    },
    getFormatDate :function(value, metadata, record, rowIdx, colIdx, store, vie){
        return Ext.Date.format(value, 'M d, Y')
    },    
    getPrice :function(value, metaData, record, rowIdx, colIdx, store, view){
        var value=record.getM01i012001().data.price +'&nbsp';
        return value+'৳';
    },
    getVat : function(value, metaData, record, rowIdx, colIdx, store, view){
        var value=record.getM01i013001().data.vat +'&nbsp';
        return value+'%';
    },
    getDiscount :function(value, metaData, record, rowIdx, colIdx, store, view){
        var value=record.getM01t010001().data.discount +'&nbsp';
        return value+'%';
    }
});