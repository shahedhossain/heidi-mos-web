Ext.define(Fsl.app.getAbsView('V01I017001X07'), {
    extend      : 'Ext.window.Window',
    alias       : Fsl.app.getAlias('V01I017001X07'),
    icon        : Fsl.route.getImage('APP01009.png'),
    id          : 'V01I017001X07'.toString(),
    title       : 'BRANCH LVM :: V01I017001X07',
    minimizable :  false,
    maximizable :  false,
    autoShow    :  true,
    resizable   :  false,
    modal       :  true, 
    layout      :  {
        type	: 'vbox',
        align	: 'stretch'
    },
    items  : [{ 
        xtype     	: Fsl.app.getXtype('V01I017001X08'), 
        resizable 	: false,
        border    	: false,
        componentCls    : 'my-border-bottom'
    },{ 
        xtype     	: Fsl.app.getXtype('V01I017001X09'),       
        resizable 	: false,
        border    	: false
    }]
});