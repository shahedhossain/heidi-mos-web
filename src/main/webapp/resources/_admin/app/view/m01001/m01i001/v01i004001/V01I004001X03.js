Ext.define(Fsl.app.getAbsView('V01I004001X03'), { 
    extend      : 'Ext.window.Window',
    alias       :  Fsl.app.getAlias('V01I004001X03'),
    id          : 'V01I004001X03'.toLowerCase(),   
    title       : 'BRAND TYPE :: V01I004001X03',
    icon        :  Fsl.route.getImage('APP01003.png'),	
    autoShow  	:  true,
    modal     	:  true,
    width     	:  300,
    height    	:  157,
    layout      :  {
        type    : 'vbox',
        align   : 'stretch'
    },
    initComponent: function () {
        var me = this        
        me.items = [
        {
            xtype                 : 'form',
            trackResetOnLoad      :  true,
            border                :  false,
            bodyStyle             : {                    
                padding           : '10px'
            },            
            tbar : Ext.create('Ext.ux.StatusBar', {
                id            	  : 'win-statusbar-v01i004001x03',
                topBorder     	  :  false,
                text          	  : 'Status',
                defaultIconCls	  : 'fsl-severity-info',
                defaultText   	  : 'Status'
            }),
            
            items: [{
                xtype             : 'textfield',
                fieldLabel        : 'Id',
                emptyText         : 'Required false..',
                name              : 'id',
                width             :  265,
                readOnly          :  true
            },
            {
                xtype             : 'textfield',
                fieldLabel        : 'Brand Type Name',
                emptyText         : 'Write name....',
                name              : 'name',
                width             :  265,
                allowBlank        :  false
            }]
        }];
        me.buttons = [
        {
            text		: 'Save',
            icon                : Fsl.route.getImage('SAV01004.png'),
            action		: 'save'
        },
        {
            text		: 'Close',
            icon                : Fsl.route.getImage('CLS01001.png'),  
            scope		: this,
            handler		: this.close
        }
        ];
        me.callParent(arguments);
    }
});


