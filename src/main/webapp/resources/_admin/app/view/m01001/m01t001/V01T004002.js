Ext.define(Fsl.app.getAbsView('V01T004002'), {
    extend      : 'Ext.window.Window',
    alias       : Fsl.app.getAlias('V01T004002'),
    icon        : Fsl.route.getImage('APP01009.png'),
    id          : 'V01T004002'.toLowerCase(),
    title       : 'STOCK STATUS :: V01T004002',
    minimizable : false,
    maximizable : false,
    autoShow    : true,
    resizable   : false,
    modal       : true,
      layout    : {
            type: 'vbox',
            align: 'stretch'
        },
    items  : [{ 
        xtype     	: Fsl.app.getXtype('V01T004002X01'), 
        resizable 	: false,
        border    	: false,
        componentCls    : 'my-border-bottom'
    },{ 
        xtype     	: Fsl.app.getXtype('V01T004002X02'),       
        resizable 	: false,
        border    	: false
    }]
});