Ext.define(Fsl.app.getAbsView('V01T003001X05'), {
    extend        : 'Ext.form.Panel',
    alias         : Fsl.app.getAlias('V01T003001X05'),
    id            : 'V01T003001X05'.toLowerCase(),  
    width         :  285,
    height        :  60,
    bodyStyle     : {
    background    : 'none'
    },
    defaults: {        
        activeRecord  :  null,
        border        :  true, 
        layout        : 'hbox',        
        fieldDefaults :  {
            anchor    : '100%',
            labelAlign: 'right'
        }
    },
    
    initComponent     : function(){        
        Ext.QuickTips.init();
        Ext.form.Field.prototype.msgTarget = 'qtip';         
        var me    = this;     
        me.items  = [{
            xtype           : 'fieldcontainer',
            combineErrors   : true,                    
            defaults        : {
                layout      : 'fit',
                margin      : '5 5 0 5',
              
                labelAlign  : 'left',
                width       :  230
            },
            items: [{
                xtype             : 'numberfield',
                name              : 'id',
                fieldLabel        : 'Article Id :',
                minValue          :  1,                                        
                minValue          :  0,
                mouseWheelEnabled :  false,
                hideTrigger       :  true,
                decimalPrecision  :  0,
                keyNavEnabled     :  false,
                scope             :  this,                
                listeners: {                    
                    dblclick    : {
                        element : 'el',
                        scope   : this,
                        fn: function(){
                            var form   = this.getForm();
                            form.findField('id').setValue('');                            
                        }
                    }
                }
            },{
                xtype             : 'textfield',
                name              : 'name',
                fieldLabel        : 'Article Name:',                        
                scope             :  this,                
                listeners         : {                    
                    dblclick    : {
                        element : 'el',
                        scope   : this,
                        fn: function(){
                            var form   = this.getForm(); 
                            form.findField('name').setValue('');                            
                        }
                    }
                }
            }]
        },
        {
            xtype           : 'fieldcontainer',
            combineErrors   : true,                    
            defaults        : {
                layout      : 'fit',
                width       :  230, 
                margin      : '0 5 0 5',
                hideLabel   :  false,                
                labelAlign  : 'left'
            },
            items: [{
                xtype             : 'numberfield',
                name              : 'mbcode',
                fieldLabel        : 'MB CODE:',
                minValue          :  1,                        
                minValue          :  0,
                mouseWheelEnabled :  false,
                hideTrigger       :  true,
                decimalPrecision  :  0,
                keyNavEnabled     :  false,
                scope             :  this,                
                listeners: {                    
                    dblclick    : {
                        element : 'el',
                        scope   : this,
                        fn: function(){
                            var form   = this.getForm(); 
                            form.findField('mbcode').setValue('');                            
                        }
                    }
                }
            },{
                xtype             : 'numberfield',
                name              : 'pbcode',
                fieldLabel        : 'PB CODE:',
                minValue          :  1,
                                    
                minValue          :  0,
                mouseWheelEnabled :  false,
                hideTrigger       :  true,
                decimalPrecision  :  0,
                keyNavEnabled     :  false,
                scope             :  this,                
                listeners: {                    
                    dblclick    : {
                        element : 'el',
                        scope   : this,
                        fn: function(){
                            var form   = this.getForm(); 
                            form.findField('pbcode').setValue('');                            
                        }
                    }
                }
            }]
        }];       
        me.callParent(arguments);
    }
});

