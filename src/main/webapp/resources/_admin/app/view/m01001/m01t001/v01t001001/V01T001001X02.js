Ext.define(Fsl.app.getAbsView('V01T001001X02'), {
    extend    : 'Ext.grid.Panel',
    alias     : Fsl.app.getAlias('V01T001001X02'),
    id        : 'V01T001001X02'.toString(),
    border    : true,
    modal     : true,    
    width     : 500,
    height    : 285,
    viewConfig: {
        stripeRows    : true,
        forceFit      : true,
        emptyText     : 'No Records to display',
        listeners     : {
            viewready : function(v) {               
                var store = Fsl.store.getStore('S01T001001');
                store.clearFilter();
                store.loadPage(1);
            }
        }
    },
    initComponent: function() {
        var me     	= this;
        me.store   	= Fsl.app.getRelStore('S01T001001');      
        me.tbar = Ext.create('Ext.ux.StatusBar', {            
            topBorder           : false,
            statusAlign         : 'right',
            items     : [{
                xtype       : 'button',
                text        : 'ADD',
                icon        : Fsl.route.getIcon('TB01001/ADD01004.png'),
                action      : 'add'
            },'-',{
                xtype       : 'button',
                text        : 'Clear',
                icon        : Fsl.route.getIcon('IC01001/CAN01003.png'),
                action      : 'clear'
            },'-',{
                xtype       : 'button',
                text        : 'search',
                icon        : Fsl.route.getIcon('IC01001/search.png'),
                action      : 'search'
            },'-',{  
                xtype       : 'statusbar',
                id          : 'win-statusbar-v01t001001x02',
                border      : false,
                defaultText : '',
                statusAlign : 'right'
            }]
        });
        
        me.bbar = new Ext.PagingToolbar({
            pageSize    	: 10,
            store       	: Fsl.app.getRelStore('S01T001001'),
            displayInfo 	: true    
        });
        
        me.columns = [{
            text      		: 'SN',
            xtype     		: 'rownumberer',
            width     		:  40, 
            sortable  		:  false
        },{
            text        	: 'ID',
            dataIndex   	: 'id',
            width       	: 40, 
            sortable    	: false
        },{
            text        	: 'Name',
            dataIndex   	: 'name',
            renderer    	: me.getTitle,
            flex        	: 1
        },{
            text        	: 'Contact',
            dataIndex   	: 'contact'            
        },{
            text        	: 'Address',
            dataIndex   	: 'address'
        },{
            menuDisabled    : true,
            sortable        : false,
            xtype           : 'actioncolumn',            
            width           : 22,            
            items           : [{
                icon        : Fsl.route.getImage('DEL01005.png'),
                tooltip     : 'Delete This?',
                scope       : this,
                handler     : function(grid, rowIdx, colIdx) {
                    var record 		= grid.getStore().getAt(rowIdx);
                    var customerId 	= record.data.id
                    Ext.getCmp('WestPanel').confirmDelete(customerId, 'M01T001001', 'S01T001001', 'v01t001001x02');
                }
            }]
        }];       
        me.callParent(arguments);
    },
    getTitle:function(value, metadata, record, rowIdx, colIdx, store, vie) {
        var title = record.getM01i007001().data.name;
        return title+''+value;
    }
});