Ext.define(Fsl.app.getAbsView('V01T009001X01'), {
    extend              : 'Ext.form.Panel',
    alias               : Fsl.app.getAlias('V01T009001X01'),
    id                  : 'V01T009001X01'.toLowerCase(),  
    bodyStyle           : {
      background        : 'none'
    },
    defaults: {        
        activeRecord    : null,
        border          : true, 
        layout          : 'hbox',        
        fieldDefaults   : {
            anchor      : '100%',
            labelAlign  : 'right'
        }
    },
    
    initComponent       : function(){        
        Ext.QuickTips.init();
        Ext.form.Field.prototype.msgTarget = 'qtip';         
        var me      = this; 
        me.items    = [{
            xtype                 : 'fieldcontainer',
            combineErrors         :  true,                    
            defaults              : {
                layout            : 'hbox',
                margin            : '0 5 0 5',
                hideLabel         :  false,                
                labelAlign        : 'top',
                width             : 180
            },
            items: [{
                xtype             : 'numberfield',
                name              : 'id',
                fieldLabel        : 'User Id:',                       
                minValue          :  0,
                mouseWheelEnabled :  false,
                hideTrigger       :  true,
                decimalPrecision  :  0,
                keyNavEnabled     :  false                
            },{
                xtype             : 'textfield',
                fieldLabel        : 'Name',
                emptyText         : 'User name',
                allowBlank        :  true,
                name              : 'name'
            },{
                xtype             : 'textfield',
                fieldLabel        : 'Contact',
                emptyText         : 'contact here',
                name              : 'contact'
            }]
        },{
            xtype                 : 'fieldcontainer',
            combineErrors         :  true,                    
            defaults              : {
                layout            : 'hbox',
                margin            : '0 5 0 5',
                hideLabel         :  false,                
                labelAlign        : 'top',
                width             : 180
            },
            items: [{
                xtype             : 'numberfield',
                fieldLabel        : 'Branch ID',
                name              : 'branchId',
                minValue          :  0,
                mouseWheelEnabled :  false,
                hideTrigger       :  true,
                decimalPrecision  :  0,
                keyNavEnabled     :  false 
            },{
                xtype             : 'textfield',
                fieldLabel        : 'Branch Name',
                name              : 'branchName'
            },{
                xtype             : 'textfield',
                fieldLabel        : 'Location',
                name              : 'location'
            }]
        }]
        me.callParent(arguments);
    }
});

