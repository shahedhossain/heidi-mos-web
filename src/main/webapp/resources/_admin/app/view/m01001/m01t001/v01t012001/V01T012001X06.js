Ext.define(Fsl.app.getAbsView('V01T012001X06'),{
    extend      : 'Ext.window.Window',
    alias     	: Fsl.app.getAlias('V01T012001X06'),
    id          : 'V01T012001X06'.toLowerCase(), 
    title       : 'QUANTITY :: V01T012001X06', 
    icon        : Fsl.route.getImage('APP01003.png'),
    layout      : 'fit',
    autoShow    : true,
    modal       : true,    
    initComponent: function () {
        var me = this;
        me.items = [
        {
            xtype               : 'form',
            border    		:  false,
            trackResetOnLoad    :  true,
            bodyStyle : {                    
                padding   : '10px'
            },
            tbar : Ext.create('Ext.ux.StatusBar', {
                id            : 'win-statusbar-v01t012001x06',
                topBorder     :  false,
                text          : 'Status',
                defaultIconCls: 'fsl-severity-info',
                defaultText   : 'Status'
            }),
            
            items: [{
                xtype             : 'textfield',
                fieldLabel        : 'Id',
                emptyText         : 'Not need....',
                name              : 'id',
                width             :  265,
                anchor            : '100%',                
                readOnly          : true
            },{
                xtype             : 'textfield',
                fieldLabel        : 'Article',
                emptyText         : 'Not need....',
                name              : 'articleId',
                width             :  265,
                anchor            : '100%',
                readOnly          : true
            },{
                xtype             : 'textfield',
                fieldLabel        : 'Name',
                emptyText         : 'Not need....',
                name              : 'articleName',
                width             :  265,
                anchor            : '100%',
                readOnly          : true
            },{
                xtype             : 'textfield',
                fieldLabel        : 'S.QTY',
                emptyText         : 'Not need....',
                name              : 'quantity',
                width             :  265,
                anchor            : '100%',
                readOnly          : true
            },{            
                xtype             : 'textfield',
                fieldLabel        : 'RECE.QTY',
                name              : 'receiveQuantity',
                width             :  265,
                anchor            : '100%'
            }]
        }];
        this.buttons = [
        {
            text    : 'Save',
            icon    :  Fsl.route.getImage('SAV01004.png'),
            action  : 'save'
        },
        {
            text    : 'Close',
            icon    : Fsl.route.getImage('CLS01001.png'),     
            scope   : this,
            handler : this.close
        }];
        this.callParent(arguments);
    }
});


