Ext.define(Fsl.app.getAbsView('V01I008001X04'), { 
    extend      	: 'Ext.window.Window',
    alias       	:  Fsl.app.getAlias('V01I008001X04'),
    id          	: 'V01I008001X04'.toLowerCase(),   
    title       	: 'GROSS VAT UPDATE :: V01I008001X04',
    icon        	:  Fsl.route.getImage('APP01003.png'),	
    layout      	: 'fit',
    autoShow  		: true,
    modal     		: true,
    width         	: 285,
    height        	: 205,
    border        	: false,
    initComponent 	: function () {    
        var me = this
        me.items = [
        {
            xtype           : 'form',
            trackResetOnLoad:  true,
            bodyStyle       : {
                padding   : '10px',
                border    : true
            },
            tbar: Ext.create('Ext.ux.StatusBar', {
                id                : 'win-statusbar-v01i008001x04',
                topBorder         : true,
                text              : 'Status',
                defaultIconCls    : 'fsl-severity-info',
                defaultText       : 'Status'
            }),
            items : [{
                xtype             : 'textfield',
                name              : 'id',
                fieldLabel        : 'Id',
                emptyText         : 'Not need....',
                width             : 265,
                anchor            : '100%',
                allowBlank        : false,
                readOnly          : true
            },
            {
                xtype             : 'datefield',
                fieldLabel        : 'Start Date',
                emptyText         : 'Write Start Date....',
                name              : 'startDate',
                format            : 'M d, Y',
                altFormats        : 'd/m/Y|M d, Y h:i A',
                value             : '2.4.2013',
                width             : 265,
                anchor            : '100%',
                allowBlank        : false,
                readOnly          : true
            },
            {
                xtype             : 'datefield',
                name              : 'endDate',
                fieldLabel        : 'End Date',
                emptyText         : 'End Date...',
                format            : 'M d, Y',
                altFormats        : 'd/m/Y|M d, Y h:i A',
                value             : '2.4.2013',
                width             : 265,
                anchor            : '100%',
                allowBlank        : false,
                readOnly          : true
            },
            {
                xtype             : 'numberfield',
                name              : 'vatPercent',
                fieldLabel        : 'Vat Percent',
                emptyText         : 'Vat Percent...',
                width             : 265,
                anchor            : '100%',
                allowBlank        : false,
                maxValue          : 100,
                minValue          : 0
            }]
        }];
        this.buttons = [{
            text        : 'Save',
            icon        : Fsl.route.getImage('SAV01004.png'),
            action      : 'save',
            height      : 20
        },{
            text        : 'Close',
            icon        : Fsl.route.getImage('CLS01001.png'),   
            scope       : this,
            handler     : this.close,
            height      : 20
        }];
        this.callParent(arguments);
    }
});