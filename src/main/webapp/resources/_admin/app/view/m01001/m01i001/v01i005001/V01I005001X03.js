Ext.define(Fsl.app.getAbsView('V01I005001X03'), { 
    extend      : 'Ext.window.Window',
    alias       :  Fsl.app.getAlias('V01I005001X03'),
    id          : 'V01I005001X03'.toLowerCase(),   
    title       : 'BRAND :: V01I005001X03',
    icon        :  Fsl.route.getImage('APP01003.png'),	
    autoShow  	:  true,
    modal     	:  true,
    width     	:  300,
    height    	:  230,
    layout      :  {
        type    : 'vbox',
        align   : 'stretch'
    },
    initComponent: function () {    
        var me = this;        
        me.items = [
        {
            xtype     		: 'form',
            border              :  false,
            trackResetOnLoad  	:  true,
            bodyStyle 			: {                    
                padding   : '10px'
            },
            
            tbar : Ext.create('Ext.ux.StatusBar', {
                id            	  : 'win-statusbar-v01i005001x03',
                topBorder     	  :  false,
                text          	  : 'Status',
                defaultIconCls	  : 'fsl-severity-info',
                defaultText   	  : 'Status'
            }),
            
            items: [{
                xtype             : 'textfield',
                fieldLabel        : 'Id',
                emptyText         : 'Do need write anything..',
                name              : 'id',
                width             : 265,
                anchor            : '100%',
                readOnly          : true
            },
            {
                xtype             : 'textfield',
                fieldLabel        : 'Brand Name',
                emptyText         : 'Write name....',
                name              : 'name',
                width             : 265,
                anchor            : '100%',
                allowBlank        : false
            },
            {
                xtype             : 'combo',
                name              : 'brandTypeId',
                fieldLabel        : 'Brand Type',
                store             : Fsl.store.getStore('S01I004001'),
                displayField      : 'name',
                valueField        : 'id',
                queryMode         : 'remote',
                allowBlank        : false,
                editable          : false,
                width             : 265,
                scope             : me,
                tpl: Ext.create('Ext.XTemplate',
                    '<tpl for=".">',
                    '<div class="x-boundlist-item">{id} - {name}</div>',
                    '</tpl>'
                    ),
                displayTpl: Ext.create('Ext.XTemplate',
                    '<tpl for=".">',
                    '{id} - {name}',
                    '</tpl>'
                    )
            },
            {
                xtype             : 'textfield',
                name              : 'manufacturerName',
                fieldLabel        : 'Manufacture',
                emptyText         : 'Double click here....',
                allowBlank        :  false,
                editable          :  false,
                readOnly          :  true,
                width             : 265,
                listeners         : {
                    dblclick      : {
                        element   : 'el',
                        fn        : function(){
                            Fsl.app.getWidget('v01i005001x04').show()
                        }
                    }
                }
            },
            {
                xtype             : 'textfield',
                hidden            :  true,
                name              : 'manufacturerId',
                allowBlank        :  false,
                editable          :  false,
                readOnly          :  true                
            },
            {
                xtype             : 'textfield',
                name              : 'genericProductName',
                fieldLabel        : 'Generic Product',
                emptyText         : 'Double click here....',
                allowBlank        :  false,
                editable          :  false,
                readOnly          :  true,
                width             : 265,
                listeners         : {
                    dblclick      : {
                        element   : 'el',
                        fn        : function(){
                            Fsl.app.getWidget('v01i005001x07').show()
                        }
                    }
                }
            },
            {
                xtype             : 'textfield',
                hidden            :  true,
                name              : 'genericProductId',
                allowBlank        :  false,
                editable          :  false,
                readOnly          :  true
            }]
        }];
        this.buttons = [
        {
            text    	: 'Save',
            icon        : Fsl.route.getImage('SAV01004.png'),
            action  	: 'save'
        },
        {
            text    	: 'Close',
            icon        :  Fsl.route.getImage('CLS01001.png'),      
            scope   	:  this,
            handler 	:  this.close
        }];
        this.callParent(arguments);
    }
});


