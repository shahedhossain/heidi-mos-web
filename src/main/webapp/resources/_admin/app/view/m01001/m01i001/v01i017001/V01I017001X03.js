Ext.define(Fsl.app.getAbsView('V01I017001X03'), { 
    extend      	: 'Ext.window.Window',
    alias       	:  Fsl.app.getAlias('V01I017001X03'),
    id          	: 'V01I017001X03'.toLowerCase(),   
    title       	: 'ARTICLE LIMIT ENTRY :: V01I017001X03',
    icon        	:  Fsl.route.getImage('APP01003.png'),	
    layout              : 'fit',
    autoShow            : true,
    modal               : true,
    width               : 320,
    height              : 230, 
    initComponent: function () {    
        var me = this;        
        me.items = [
        {
            xtype               : 'form',
            border              :  false,
            trackResetOnLoad	:  true,
            bodyStyle : {                    
                padding   : '10px'               
            },
            
            tbar : Ext.create('Ext.ux.StatusBar', {
                id                : 'win-statusbar-v01i017001x03',
                topBorder         : false,
                text              : 'Status',
                defaultIconCls    : 'fsl-severity-info',
                defaultText       : 'Status'
            }),

            items: [{
                xtype             : 'textfield',
                name              : 'id',
                fieldLabel        : 'Id',
                emptyText         : 'Not need....',
                width             : 265,
                anchor            : '100%',
                readOnly          : true
            },
            {
                xtype             : 'textfield',
                name              : 'articleId',                
                allowBlank        :  false,
                readOnly          :  true,
                hidden            :  true
            },
            {
                xtype             : 'textfield',
                name              : 'articleName',                
                allowBlank        :  false,
                readOnly          :  true,
                fieldLabel        : 'Article',
                width             :  287
            },
            {
                xtype             : 'textfield',
                name              : 'branchId',                
                allowBlank        :  false,
                readOnly          :  true,
                hidden            :  true
            },
            {
                xtype             : 'textfield',
                name              : 'branchName',                
                allowBlank        :  false,
                readOnly          :  true,
                fieldLabel        : 'Branch',
                width             :  287
            },
            {
                xtype             : 'textfield',
                name              : 'maxQuantity',
                fieldLabel        : 'Max Quantity',
                emptyText         : 'Max Quantity Here...',
                width             : 265,
                anchor            : '100%',
                allowBlank        : false
            },
            {
                xtype             : 'textfield',
                name              : 'minQuantity',
                fieldLabel        : 'Min Quantity',
                emptyText         : 'Min Quantity Here...',
                width             : 265,
                anchor            : '100%',
                allowBlank        : false
            }]
        }];
        this.buttons = [{
            text        : 'Save',
            icon        :  Fsl.route.getImage('SAV01004.png'),
            action      : 'save',
            height      :  20
        },{
            text        : 'Close',
            icon        :  Fsl.route.getImage('CLS01001.png'),   
            scope       :  this,
            handler     :  this.close,
            height      :  20
        }];
        this.callParent(arguments);
    }
});

