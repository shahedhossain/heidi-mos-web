Ext.define(Fsl.app.getAbsView('V01T014001X02'), {
    extend    : 'Ext.grid.Panel',
    alias     : Fsl.app.getAlias('V01T014001X02'),
    id        : 'V01T014001X02'.toLowerCase(),
    border    : true,
    modal     : true,
    height    : 285,
    width     : 500,     
    viewConfig: {
        stripeRows    : true,
        forceFit      : true,
        emptyText     : 'No Records to display',
        loadMask      : false,
        listeners     : {
            viewready : function(v) {               
                var store = Fsl.store.getStore('S01T014001');
                store.clearFilter();
                store.loadPage(1);
            }
        }
    },
    columnLines     : true,
    initComponent   : function() {
        var me      = this;
        me.store    = Fsl.app.getRelStore('S01T014001');  
        me.tbar     = Ext.create('Ext.ux.StatusBar', {            
            topBorder           : false,
            statusAlign         : 'right',
            items     : [{
                xtype       :'button',
                text        : 'New',
                icon        : Fsl.route.getIcon('TB01001/ADD01004.png'),
                action      : 'new'
            },'-',{
                xtype       : 'button',
                text        : 'Clear',
                icon        : Fsl.route.getIcon('IC01001/CAN01003.png'),
                action      : 'clear'
            },'-',{
                xtype       : 'button',
                text        : 'search',
                icon        : Fsl.route.getIcon('IC01001/search.png'),
                action      : 'search'
            },'-',{  
                xtype       : 'statusbar',
                id          : 'win-statusbar-v01t014001x02',
                border      : false,
                defaultText : '',
                statusAlign : 'right'
            }]
        });
        
        this.bbar = new Ext.PagingToolbar({
            pageSize        :  10,
            store           :  Fsl.app.getRelStore('S01T014001'),
            displayInfo     :  true
        });  
                
        this.columns = [{
            text        : 'ORDER ID',
            dataIndex   : 'id',            
            sortable    :  false
        },{
            text        : 'BRANCH',
            hidden      :  true,
            dataIndex   : 'branchId',
            renderer    :  this.getBranchName
            
        },{
            text        : 'TO BRANCH',
            dataIndex   : 'toBranchId',
            flex        :  1,
            renderer    :  this.getToBranchName
            
        },{
            text        : 'Date',
            dataIndex   : 'entryDate',
            renderer    :  this.getFormatDate
        }];
        this.callParent(arguments);
    },
    sendNewOrder: function(btn, e, eOpts){
        var view=Ext.widget('sv01t01400103',{
            animateTarget  : 'sv01t01400103'
        });
        view.show();        
        setTimeout(function(){            
            Ext.getCmp('sv01t01400105').getEl().unmask();
        }, 500);
    },   
    getBranchName :function(value, metadata, record, rowIdx, colIdx, store, vie) {
       /* var form = Ext.getCmp('sv01t01400106').getForm();
        form.findField('branchId').setValue(record.getM01I015001().data.id);
        form.findField('branchName').setValue(record.getM01I015001().data.name);*/
        return record.getM01i015001().data.name;
    },   
    getToBranchName :function(value, metadata, record, rowIdx, colIdx, store, vie) {
        return record.getM01i015002().data.name;
    },
    getFormatDate :function(value, metadata, record, rowIdx, colIdx, store, vie){
        return Ext.Date.format(value, 'M d, Y')
    }
});