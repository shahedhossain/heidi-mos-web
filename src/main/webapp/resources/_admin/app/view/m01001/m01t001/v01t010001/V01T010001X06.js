Ext.define(Fsl.app.getAbsView('V01T010001X06'), {
    extend            : 'Ext.grid.Panel',
    alias             : Fsl.app.getAlias('V01T010001X06'),
    id                : 'V01T010001X06'.toString(),
    border            : true,
    modal             : true,
    width             : 550,
    height            : 290,
    viewConfig        : {
        stripeRows    : true,
        forceFit      : true,
        emptyText     : 'No Records to display',
        listeners     : {
            viewready : function(v) {               
                var store = Fsl.store.getStore('S01I006007');
                store.clearFilter();
                store.loadPage(1);
            }
        }
    },
    initComponent: function() {
        var me     	= this;
        me.store   	= Fsl.app.getRelStore('S01I006007');      
        me.tbar= Ext.create('Ext.ux.StatusBar', {            
            topBorder           : false,
            statusAlign         : 'right',
            items    : [{
                xtype       : 'button',
                text        : 'Clear',
                icon        : Fsl.route.getIcon('IC01001/CAN01003.png'),
                action      : 'clear'
            },'-',{
                xtype       : 'button',
                text        : 'search',
                icon        : Fsl.route.getIcon('IC01001/search.png'),
                action      : 'search'
            },'-',{  
                xtype       : 'statusbar',
                id          : 'win-statusbar-v01t010001x06',
                border      : false,
                defaultText : '',
                statusAlign : 'right'
            }]
        });
        
          

        this. bbar= new Ext.PagingToolbar({
            pageSize            : 10,
            store       	: Fsl.app.getRelStore('S01I006007'),
            displayInfo         : true         
        });
        
        
        this.columns = [{
            text                : 'SN',
            xtype               : 'rownumberer',
            width               :  40, 
            sortable            :  false
        },{
            text                : 'ID',
            dataIndex           : 'id',
            width               : 40, 
            sortable            : false,
            renderer            :  this.rowToolTip
        },{
            text                : 'Article',
            dataIndex           : 'name',
            flex                : 1,
            renderer            :  this.rowToolTip
        },{
            text                : 'MBCODE',
            dataIndex           : 'mbcode',
            renderer            :  this.rowToolTip

        },{
            text                : 'PBCODE',
            dataIndex           : 'pbcode',
            renderer            :  this.rowToolTip

        },{
            text                : 'Brand',
            dataIndex           : 'brandId',
            renderer            : this.getBrandName
        }];      
        this.callParent(arguments);
    },      
    getBrandName :function(value, metadata, record, rowIdx, colIdx, store, vie) {
        this.rowToolTip(value, metadata, record)
        return record.getM01i005001().data.name;
    },
    rowToolTip : function(value, metadata, record) {
        var smarty = "<b>Article Information</b>";
        smarty += "<br/>Article Id    : {id}&nbsp;Name : {name}";        
        smarty += "<br/>MBCode        : {mbcode} &nbsp; PBCode  :{pbcode}"
        smarty += "<br/>Brand Id      : {brandId} &nbsp;"+'Name:'+record.getM01i005001().data.name;        
        smarty += "<br/>Comment       : {details}";
       

        var tpl = new Ext.XTemplate(smarty);
        var qtip = tpl.apply(record.data)
        metadata.tdAttr = 'data-qtip="' + qtip + '"';               

        return value
    }   
});