Ext.define(Fsl.app.getAbsView('V01T014001X04'), {
    extend        : 'Ext.form.Panel',
    alias         : Fsl.app.getAlias('V01T014001X04'),
    id            : 'V01T014001X04'.toLowerCase(), 
    background    : false,
    bodyStyle     : {
        background: 'none'                
    },
    defaults: {        
        activeRecord  : null,
        border        : true, 
        layout        : 'hbox',        
        fieldDefaults : {
            anchor    : '100%',
            labelAlign: 'right'
        }
    },    
    initComponent     : function(){               
        var me = this;         
        me.items = [{
            xtype: 'fieldcontainer',
            combineErrors: true,                    
            defaults: {
                layout      : 'fit',
                flex        : 1,
                margin      : '5 7 0 10',
                hideLabel   : false,                
                labelAlign  : 'left'
            },
            items: [{
                xtype             : 'textfield',
                name              : 'branchId',
                fieldLabel        : 'Branch Id :',
                allowBlank        : false,
                minValue          : 1,
                readOnly          : true
            },{
                xtype             : 'textfield',
                name              : 'toBranchId',
                fieldLabel        : 'To Branch Id :',
                allowBlank        : false,
                minValue          : 1,
                readOnly          : true
            }]
        },{
            xtype             : 'fieldcontainer',
            combineErrors     : true,             
            defaults: { 
                layout        : 'fit',
                flex          :  1,
                margin        : '0 7 0 10',
                hideLabel     :  false,                
                labelAlign    : 'left'
            },
            items: [
            {
                xtype             : 'textfield',
                fieldLabel        : 'Branch Name :',
                name              : 'branchName',
                allowBlank        : false,
                readOnly          :  true
            },{
                xtype             : 'textfield',
                name              : 'toBranchName',
                fieldLabel        : 'To Branch :',
                allowBlank        :  false,
                readOnly          :  true,
                anchor            : '95%',
                margin            : '0 7 0 10'
            }]
        },{
            xtype             : 'fieldcontainer',
            combineErrors     : true,             
            defaults: { 
                layout        : 'fit',
                margin        : '0 7 0 10',
                hideLabel     : false,                
                labelAlign    : 'left',
                flex          : 1
            },
            items: [
            {
                xtype             : 'numberfield',                
                hideTrigger       :  true,
                decimalPrecision  :  0,
                keyNavEnabled     :  false,
                mouseWheelEnabled :  false,
                fieldLabel        : 'Order Id :',                
                name              : 'id',
                enableKeyEvents   :  true,               
                msgDisplay        : 'tooltip',
                width             :  267,
                anchor            : '100%'
            },            
            {
                xtype             : 'datefield',
                fieldLabel        : 'Date :',
                name              : 'entryDate',
                width             : 267,                
                maxValue          : new Date(),
                value             : new Date(),
                format            : 'M d, Y',
                altFormats        : 'd/m/Y|M d, Y h:i A'
            }]
        },{
            xtype                  : 'fieldcontainer',
            combineErrors          : true,  
            itemId                 : 'orderCt',
            defaults  : {  
                layout             : 'fit',
                margin             : '0 0 0 5',
                hideLabel          : false,
                msgTarget          : 'qtip',
                labelAlign         : 'top'
            },
            items: [{
                xtype              : 'numberfield',
                hideTrigger        : true,
                decimalPrecision   : 0,
                keyNavEnabled      : false,
                mouseWheelEnabled  : false,
                fieldLabel         : 'Article Id',
                name               : 'articleId',
                minValue           : 1,
                width              : 100,
                margin             : '0 0 0 10'
            },{
                xtype             : 'textfield',
                name              : 'articleName',
                fieldLabel        : 'Article Name',
                width             : 143,
                flex              : 1,
                readOnly          : true
            },{
                xtype             : 'textfield',
                name              : 'price',
                fieldLabel        : 'Rate',
                width             : 50,                
                readOnly          : true
            },{
                xtype             : 'hiddenfield',
                name              : 'priceId',
                readOnly          : true
            },{
                xtype             : 'numberfield',                
                hideTrigger       : true,
                decimalPrecision  : 0,
                keyNavEnabled     : false,
                mouseWheelEnabled : false,
                msgTarget         : 'qtip',
                name              : 'quantity', 
                fieldLabel        : 'Qty',
                minValue          : 0,                 
                width             : 70,
                enableKeyEvents   : true,
                margin            : '0 0 0 0'
            },{
                xtype             : 'numberfield',                
                hideTrigger       : true,                
                keyNavEnabled     : false,
                mouseWheelEnabled : false,
                msgTarget         : 'qtip',
                name              : 'discount',
                fieldLabel        : 'Disc[%]',
                width             : 50,
                readOnly          : true
            },{
                xtype             : 'hiddenfield',
                name              : 'discountId',
                readOnly          :  true
            },{
                xtype             : 'textfield',
                name              : 'vat',
                fieldLabel        : 'Vat[%]',
                width             : 50,
                readOnly          : true,
                margin            : '0 5 0 0'
            },{
                xtype             : 'textfield', 
                fieldLabel        : 'Total',
                name              : 'individualTotal',
                readOnly          : true,
                width             : 70,
                margin            : '0 -3 0 0'
            },{
                xtype             : 'hiddenfield',
                name              : 'vatId',
                readOnly          : true
            },{
                xtype             : 'hiddenfield',
                name              : 'statusId',
                value             : 1,
                readOnly          : true
            }]
        }];       
        me.callParent(arguments);
    }
});

