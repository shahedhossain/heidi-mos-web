Ext.define(Fsl.app.getAbsView('V01T010001X01'), {
    extend      : 'Ext.form.Panel',
    alias     	: Fsl.app.getAlias('V01T010001X01'),
    id          : 'V01T010001X01'.toLowerCase(), 
    width       : 455,
    bodyStyle           : {
        background        : 'none'
    },
    defaults: {        
        border          : true, 
        layout          : 'hbox',        
        fieldDefaults   : {
            anchor      : '100%',
            labelAlign  : 'right'
        }
    },
    
    initComponent       : function(){        
        Ext.QuickTips.init();
        Ext.form.Field.prototype.msgTarget = 'qtip';         
        var me          = this; 
        me.items    = [{
            xtype                 : 'fieldcontainer',
            combineErrors         :  true,                    
            defaults              : {
                layout            : 'fit',
                margin            : '0 5 0 5',
                hideLabel         :  false,                
                labelAlign        : 'top',
                flex              : 1
            },
            items: [{
                xtype             : 'numberfield',
                name              : 'id',
                fieldLabel        : 'Discount ID:',
                width             :  100,                        
                minValue          :  0,
                mouseWheelEnabled :  false,
                hideTrigger       :  true,
                decimalPrecision  :  0,
                keyNavEnabled     :  false                
            },{
                xtype             : 'numberfield',
                name              : 'articleId',
                fieldLabel        : 'Article ID:',
                width             :  100,                        
                minValue          :  0,
                mouseWheelEnabled :  false,
                hideTrigger       :  true,
                decimalPrecision  :  0,
                keyNavEnabled     :  false                
            },{
                xtype             : 'textfield',
                fieldLabel        : 'Article Name',
                emptyText         : 'Write article name',
                allowBlank        :  true,
                name              : 'articleName'
            },{
                xtype             : 'numberfield',
                name              : 'discount',
                fieldLabel        : 'Discount:',
                minValue          :  0,
                mouseWheelEnabled :  false,
                hideTrigger       :  true,
                decimalPrecision  :  0,
                keyNavEnabled     :  false
            }]
        },{ 
            xtype                 : 'fieldcontainer',
            combineErrors         :  true,                    
            defaults              : {
                layout            : 'fit',
                margin            : '0 5 0 5',
                hideLabel         :  false,                
                labelAlign        : 'top',
                flex              : 1
            },
            items: [{
                xtype             : 'numberfield',
                name              : 'mbcode',
                fieldLabel        : 'MBCODE:',
                minValue          :  0,
                mouseWheelEnabled :  false,
                hideTrigger       :  true,
                decimalPrecision  :  0,
                keyNavEnabled     :  false
            },{
                xtype             : 'numberfield',
                name              : 'pbcode',
                fieldLabel        : 'PBCODE:',
                minValue          :  0,
                mouseWheelEnabled :  false,
                hideTrigger       :  true,
                decimalPrecision  :  0,
                keyNavEnabled     :  false
            },{
                xtype             : 'datefield',
                name              : 'startDate',
                fieldLabel        : 'Start Date:',
                format            : 'M d, Y',
                altFormats        : 'd/m/Y|M d, Y h:i A',                
                id                : 'startdt-v01t010001x01',
                vtype             : 'daterange',
                endDateField      : 'enddt-v01t010001x01'
            },{
                xtype             : 'datefield',
                name              : 'endDate',
                fieldLabel        : 'End Date:',
                format            : 'M d, Y',
                altFormats        : 'd/m/Y|M d, Y h:i A',
                id                : 'enddt-v01t010001x01',
                vtype             : 'daterange',
                startDateField    : 'startdt-v01t010001x01'
            }]
        }];       
        me.callParent(arguments);
    }
});

