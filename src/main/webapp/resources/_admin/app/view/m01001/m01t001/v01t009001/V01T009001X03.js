Ext.define(Fsl.app.getAbsView('V01T009001X03'),{
    extend      : 'Ext.window.Window',
    alias     	: Fsl.app.getAlias('V01T009001X03'),
    id          : 'V01T009001X03'.toLowerCase(), 
    title       : 'USER :: V01T009001X03', 
    icon        : Fsl.route.getImage('APP01003.png'),
    layout      : 'fit',
    autoShow    : true,
    modal       : true,
    width       : 285,
    height      : 350,    
    
    initComponent: function () {
        var me = this;
        me.items = [
            {
                xtype     : 'form',
                border    :  false,
                bodyStyle : {                    
                    padding   : '10px'
                },
            
           tbar : Ext.create('Ext.ux.StatusBar', {
                id                : 'win-statusbar-v01t009001x03',
                topBorder         :  true,
                text              : 'Status',
                defaultIconCls    : 'fsl-severity-info',
                defaultText       : 'Status'
            }),
            

                items: [{
                        xtype             : 'numberfield',
                        name              : 'id',
                        fieldLabel        : 'Id',
                        emptyText         : 'Not need....',
                        width             :  265,
                        anchor            : '100%',
                        readOnly          :  true,
                        minValue          :  0,
                        mouseWheelEnabled :  false,
                        hideTrigger       :  true,
                        decimalPrecision  :  0,
                        keyNavEnabled     :  false
                    },
                    {
                        xtype             : 'combo',
                        name              : 'titleId',
                        fieldLabel        : 'User Title',
                        emptyText         : 'Select One...',
                        store             : Fsl.store.getStore('S01I007001'),
                        displayField      : 'name',
                        valueField        : 'id',
                        queryMode         : 'remote',
                        allowBlank        :  false,
                        editable          :  false,
                        width             :  265,
                        anchor            : '100%'
                    },
                    {
                        xtype             : 'textfield',
                        name              : 'name',
                        fieldLabel        : 'User Name',
                        emptyText         : 'User Name Here...',
                        width             :  265,
                        anchor            : '100%',
                        allowBlank        :  false
                    },
                    {
                        xtype             : 'textfield',
                        name              : 'branchName',
                        fieldLabel        : 'Branch',
                        emptyText         : 'Double Click ...',                        
                        allowBlank        :  false,
                        readOnly          :  true,
                        width             :  265,
                        anchor            : '100%'
                    },{
                        xtype             : 'combo',
                        name              : 'branchId',
                        allowBlank        :  false,
                        readOnly          :  true,
                        hidden            :  true
                    },{
                        xtype             : 'textfield',
                        name              : 'contact',
                        fieldLabel        : 'Contact',
                        emptyText         : 'Contact Here...',
                        width             :  265,
                        anchor            : '100%',
                        allowBlank        :  false
                    },
                    {
                        xtype             : 'textfield',
                        name              : 'location',
                        fieldLabel        : 'Location',
                        emptyText         : 'Location Here...',
                        width             :  265,
                        anchor            : '100%',
                        allowBlank        : false
                    },
                    {
                        xtype             : 'textarea',
                        name              : 'address',
                        fieldLabel        : 'Address',
                        emptyText         : 'Address Here...',
                        width             :  265,
                        anchor            : '100%',
                        allowBlank        :  false
                    }]
            }];
        me.buttons = [{
            text    : 'Save',
            icon    :  Fsl.route.getImage('SAV01004.png'),
            action  : 'save'
        },
        {
            text    : 'Close',
            icon    : Fsl.route.getImage('CLS01001.png'),     
            scope   : this,
            handler : this.close
        }];
        this.callParent(arguments);
    }
});

