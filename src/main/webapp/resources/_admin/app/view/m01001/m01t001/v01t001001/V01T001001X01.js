Ext.define(Fsl.app.getAbsView('V01T001001X01'), {
    extend      : 'Ext.form.Panel',
    alias     	: Fsl.app.getAlias('V01T001001X01'),
    id          : 'V01T001001X01'.toLowerCase(),  
    bodyStyle           : {
      background        : 'none'
    },
    defaults: {        
        activeRecord    : null,
        border          : true, 
        layout          : 'hbox',        
        fieldDefaults   : {
            anchor      : '100%',
            labelAlign  : 'right'
        }
    },
    
    initComponent       : function(){        
        Ext.QuickTips.init();
        Ext.form.Field.prototype.msgTarget = 'qtip';         
        var me      = this; 
        me.items    = [{
            xtype                 : 'fieldcontainer',
            combineErrors         :  true,                    
            defaults              : {
                layout            : 'fit',
                margin            : '0 5 0 5',
                hideLabel         :  false,                
                labelAlign        : 'top',
                flex              : 1
            },
            items: [{
                xtype             : 'numberfield',
                name              : 'id',
                fieldLabel        : 'Customer Id:',
                width             :  100,                        
                minValue          :  0,
                mouseWheelEnabled :  false,
                hideTrigger       :  true,
                decimalPrecision  :  0,
                keyNavEnabled     :  false                
            },{
                xtype             : 'textfield',
                fieldLabel        : 'Name',
                emptyText         : 'Customer name',
                allowBlank        :  true,
                name              : 'name'
            },{
                xtype             : 'textfield',
                fieldLabel        : 'Contact',
                emptyText         : 'contact here',
                name              : 'contact'
            }]
        }];       
        me.callParent(arguments);
    }
});

