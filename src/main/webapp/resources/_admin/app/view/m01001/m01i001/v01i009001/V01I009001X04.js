Ext.define(Fsl.app.getAbsView('V01I009001X04'), { 
    extend      	: 'Ext.window.Window',
    alias       	:  Fsl.app.getAlias('V01I009001X04'),
    id          	: 'V01I009001X04'.toLowerCase(),   
    title       	: 'REBATE UPDATE :: V01I009001X04',
    icon        	:  Fsl.route.getImage('APP01003.png'),
    layout        	: 'fit',
    autoShow      	:  true,    
    modal         	:  true,
    width         	: 300,
    height        	: 230,
    border        	:  false,
    initComponent 	:  function () {
    
        var me = this;
        me.items = [
        {
            xtype           	: 'form',
            trackResetOnLoad	:  true,
            bodyStyle : {
                padding   : '10px',
                border    : true
            },
             tbar: Ext.create('Ext.ux.StatusBar', {
                id                		  : 'win-statusbar-v01i009001x04',
                topBorder         		  : true,
                text              		  : 'Status',
                defaultIconCls    		  : 'fsl-severity-info',
                defaultText       		  : 'Status'
            }),
            items : [{
                        xtype             : 'textfield',
                        name              : 'id',
                        fieldLabel        : 'Id',
                        emptyText         : 'Not need....',
                        width             :  265,
                        anchor            : '100%',
                        readOnly          :  true
                    },
                    {
                        xtype             : 'datefield',
                        fieldLabel        : 'Start Date',
                        emptyText         : 'Write Start Date....',
                        name              : 'startDate',
                        width             :  265,
                        anchor            : '100%',
                        allowBlank        :  false,
                        format            : 'M d, Y',
                        altFormats        : 'd/m/Y|M d, Y h:i A',
                        value             : '2.4.2013',
                        readOnly          :  true
                    },
                    {
                        xtype             : 'datefield',
                        name              : 'endDate',
                        fieldLabel        : 'End Date',
                        emptyText         : 'End Date...',
                        width             :  265,
                        anchor            : '100%',
                        allowBlank        :  false,
                        format            : 'M d, Y',
                        altFormats        : 'd/m/Y|M d, Y h:i A',
                        value             : '2.4.2013',
                        readOnly          :  true
                    },
                    {
                        xtype             : 'numberfield',
                        name              : 'purchaseTarget',
                        fieldLabel        : 'Purchase Target',
                        emptyText         : 'Purchase Target...',
                        width             : 265,
                        anchor            : '100%',
                        allowBlank        : false,
                        mouseWheelEnabled : false,
                        hideTrigger       : true,
                        decimalPrecision  : 0,
                        keyNavEnabled     : false,
                        minValue          : 0
                    },
                    {
                        xtype             : 'numberfield',
                        name              : 'rebatePercent',
                        fieldLabel        : 'Rebate Percent',
                        emptyText         : 'Rebate Percent...',
                        width             :  265,
                        anchor            : '100%',
                        allowBlank        :  false,
                        minValue          :  0,
                        maxValue          :  100
                    }]
        }];
        me.buttons = [{
            text        : 'Save',
            icon        :  Fsl.route.getImage('SAV01004.png'),
            action      : 'save',
            height      :  20
        },{
            text        : 'Close',
            icon        : Fsl.route.getImage('CLS01001.png'),   
            scope       :  this,
            handler     :  this.close,
            height      :  20
        }];
        this.callParent(arguments);
    }
});