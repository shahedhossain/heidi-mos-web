Ext.define(Fsl.app.getAbsView('V01T007001X04'), {
    extend              : 'Ext.form.Panel',
    alias               : Fsl.app.getAlias('V01T007001X04'),
    id                  : 'V01T007001X04'.toLowerCase(),  
    margin    		: '0 0 0 0',
    height    		:  130,
    bodyStyle 	: {
        background: 'none'                
    },
    initComponent : function(){
        Ext.QuickTips.init();
        Ext.form.Field.prototype.msgTarget = 'qtip';         
        var me = this;       
        
        me.tbar = Ext.create('Ext.ux.StatusBar', {
            id                : 'win-statusbar-v01t007001x04',
            topBorder         :  true,
            text              : 'Status',
            defaultIconCls    : 'fsl-severity-info',
            defaultText       : 'Status'
        }),
            
        me.items  =[{                
            xtype               : 'fieldcontainer',
            layout              : 'vbox',           
            items :[{
                defaultType     : 'textfield',
                layout          : 'hbox',
                flex            : 1,
                border          : false,
                margin          : '0 0 -5 0',
                fieldDefaults   : {
                    anchor      : '100%',
                    labelAlign  : 'right'               
                },
                bodyStyle       : {
                    background  : 'none'                
                },
                items   : [{
                    xtype             : 'fieldcontainer',
                    layout            : 'vbox',
                    margin            : '5 5 0 5',                        
                    defaults          :  {
                        width           :  170,
                        labelAlign      : 'left',
                        labelWidth      : 60,
                        readOnly        : true
                    }, 
                    items : [                        
                    {
                        xtype               : 'numberfield',                
                        hideTrigger         : true,
                        decimalPrecision    : 0,
                        keyNavEnabled       : false,
                        mouseWheelEnabled   : false,
                        fieldLabel          : 'InvoiceNo',
                        name                : 'invoiceId',
                        enableKeyEvents     :  true,
                        msgDisplay          : 'tooltip',
                        anchor              : '95%',
                        readOnly            : false
                    },{
                        xtype               : 'numberfield',
                        fieldLabel          : 'ID',
                        name                : 'customerId',
                        hideTrigger         : true,                
                        keyNavEnabled       : false,
                        mouseWheelEnabled   : false,
                        allowNegative       : false,
                        allowBlank          : false,
                        readOnly            : false,
                        minValue            : 1,
                        enableKeyEvents     : true
                    }]
                },                         
                {
                    xtype               : 'fieldcontainer',
                    layout              : 'vbox',
                    margin              : '5 0 0 10',                        
                    defaults            :  {
                        width               :  150,
                        labelAlign          : 'left',
                        labelWidth          : 40,
                        readOnly            : true
                    }, 
                    items: [{
                        xtype               : 'datefield',
                        name                : 'date',
                        fieldLabel          : 'Date',                                               
                        value               : new Date(),
                        format              : 'M d, Y',
                        altFormats          : 'd/m/Y|M d, Y h:i A'
                    },{
                        xtype               : 'combo',
                        name                : 'titleId',
                        fieldLabel          : 'Title',
                        store               :  Fsl.store.getStore('S01I007001'),
                        displayField        : 'name',
                        valueField          : 'id',
                        queryMode           : 'remote',
                        allowBlank          : false,
                        editable            : false                        
                    }]
                },{
                    xtype               : 'fieldcontainer',
                    layout              : 'vbox',
                    margin              : '5 0 0 10',                        
                    defaults            :  {
                        width           :  205,
                        flex            :  1,
                        labelAlign      : 'left',
                        labelWidth      : 50,
                        readOnly        : true
                    },                 
                    items: [{
                        xtype               : 'textfield',
                        name                : 'branchId',
                        fieldLabel          : 'Branch Id',
                        allowNegative       :  false,     
                        readOnly            :  true,
                        allowBlank          :  false,
                        hidden              :  true
                    },{
                        xtype               : 'textfield',
                        name                : 'branchName',
                        fieldLabel          : 'Branch',    
                        readOnly            : true
                    },{
                        xtype               : 'textfield',
                        placeHolder         : 'Customer Name',
                        name                : 'name',
                        fieldLabel          : 'Name'
                    }]
                }]
            }]
        },{
          
            xtype               : 'fieldcontainer',
            combineErrors       : true,
            layout              : 'hbox', 
            itemId              : 'invoiceCt',
            defaults            : {  
                layout          : 'fit',
                margin          : '0 0 0 5',
                hideLabel       : false,
                msgTarget       : 'qtip',
                labelAlign      : 'top'
            },
            items: [{
                xtype               : 'numberfield',
                hideTrigger         : true,
                decimalPrecision    : 0,
                keyNavEnabled       : false,
                mouseWheelEnabled   : false,
                fieldLabel          : 'Article Id',
                name                : 'articleId',
                minValue            : 1,
                width               : 90,
                margin              : '0 0 0 5'
            },{
                xtype               : 'textfield',
                name                : 'articleName',
                fieldLabel          : 'Article Name',
                flex                : 1,
                readOnly            : true
            },{
                xtype               : 'numberfield',
                name                : 'price',
                hideTrigger         : true,
                decimalPrecision    : 0,
                keyNavEnabled       : false,
                mouseWheelEnabled   : false,
                msgTarget           : 'qtip',
                fieldLabel          : 'Rate',
                width               : 50,
                readOnly            : true
            },{
                xtype               : 'hiddenfield',
                name                : 'priceId',
                readOnly            : true
            },{
                xtype               : 'numberfield',                
                hideTrigger         : true,
                decimalPrecision    : 0,
                keyNavEnabled       : false,
                mouseWheelEnabled   : false,
                msgTarget           : 'qtip',
                name                : 'quantity', 
                fieldLabel          : 'Qty',
                minValue            : 0,                 
                width               : 65,
                enableKeyEvents     : true,
                margin              : '0 0 0 0'                
            },{
                xtype               : 'numberfield',                
                hideTrigger         : true,                
                keyNavEnabled       : false,
                mouseWheelEnabled   : false,
                msgTarget           : 'qtip',
                name                : 'discount',
                fieldLabel          : 'Dis.[%]',
                width               : 50,
                readOnly            : true
            },{
                xtype               : 'hiddenfield',
                name                : 'discountId',
                readOnly            :  true
            },{
                xtype               : 'textfield',
                name                : 'vat',
                fieldLabel          : 'V[%]',
                width               : 40,
                readOnly            : true,
                margin              : '0 5 0 0'
            },{
                xtype               : 'textfield', 
                fieldLabel          : 'Total',
                name                : 'individualTotal',
                readOnly            : true,
                width               : 70,
                margin              : '0 -10 0 0'
            },{
                xtype               : 'hiddenfield',
                name                : 'vatId',
                readOnly            : true
            },{
                xtype               : 'hiddenfield',
                name                : 'dbQuantity',
                readOnly            : true
            },{
                xtype               : 'hiddenfield',
                name                : 'tQuantity',
                readOnly            : true
            }]
        }];
        this.callParent(arguments);
    }
});