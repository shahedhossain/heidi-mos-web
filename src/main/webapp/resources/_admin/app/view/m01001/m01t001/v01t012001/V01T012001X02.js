Ext.define(Fsl.app.getAbsView('V01T012001X02'), {
    extend    : 'Ext.grid.Panel',
    alias     : Fsl.app.getAlias('V01T012001X02'),
    id        : 'V01T012001X02'.toLowerCase(),
    border    : true,
    modal     : true,
    height    : 260,
    width     : 500,     
    viewConfig: {
        stripeRows    : true,
        forceFit      : true,
        emptyText     : 'No Records to display',
        loadMask      : false,
        listeners     : {
            viewready : function(v) {               
                var store = Fsl.store.getStore('S01T012002');
                store.clearFilter();
                store.loadPage(1);
            }
        }
    },
    features  : [{
        ftype       : 'summary'     
    }],
    initComponent: function() {
        var me     	= this;
        me.store   	= Fsl.app.getRelStore('S01T012002');  
        me.tbar = Ext.create('Ext.ux.StatusBar', {            
            topBorder           : false,
            statusAlign         : 'right',
            items     : [{
                xtype       : 'button',
                text        : 'Clear',
                icon        : Fsl.route.getIcon('IC01001/CAN01003.png'),
                action      : 'clear'
            },'-',{
                xtype       : 'button',
                text        : 'search',
                icon        : Fsl.route.getIcon('IC01001/search.png'),
                action      : 'search'
            },'-',{  
                xtype       : 'statusbar',
                id          : 'win-statusbar-v01t012001x02',
                border      : false,
                defaultText : '',
                statusAlign : 'right'
            }]
        });
        
        this.bbar = new Ext.PagingToolbar({
            pageSize        :  10,
            store           :  Fsl.app.getRelStore('S01T012002'),
            displayInfo     :  true
        });  
       
    
            
        this.columns = [{
            text        : 'TRANSFER ID',
            dataIndex   : 'id',            
            sortable    :  false
        },{
            text        : 'FROM BRANCH',
            dataIndex   : 'branchId',
            flex        : 1,
            renderer    : this.getBranchName
            
        },{
            text        : 'DATE',
            dataIndex   : 'entryDate',
            renderer    : this.getFormatDate
        },{
            text        : 'ReceiveId',
            dataIndex   : 'receiveId'
        },{
            text        : 'Receive Date',
            dataIndex   : 'receiveDate',
            renderer    : this.getFormatDate
        }];
        this.callParent(arguments);
    },
    
    getBranchName :function(value, metadata, record, rowIdx, colIdx, store, vie) {
        /*var form = Ext.getCmp('sv01t01200114').getForm();
        form.findField('branchId').setValue(record.getM01I015002().data.id);
        form.findField('branchName').setValue(record.getM01I015002().data.name);*/
        return record.getM01i015001().data.name;
    },
    getFormatDate :function(value, metadata, record, rowIdx, colIdx, store, vie){
        return Ext.Date.format(value, 'M d, Y')
    }
});