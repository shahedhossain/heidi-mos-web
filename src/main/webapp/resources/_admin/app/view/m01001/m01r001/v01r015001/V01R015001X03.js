Ext.define(Fsl.app.getAbsView('V01R015001X03'), {   
    extend          : 'Ext.grid.Panel',
    alias           : Fsl.app.getAlias('V01R015001X03'),
    id              : 'V01R015001X03'.toLowerCase(), 
    border    : true,
    modal     : true,
    height    : 285,
    width     : 455,
    viewConfig: {
        stripeRows    : true,
        forceFit      : true,
        emptyText     : 'No Records to display',
        listeners     : {
            viewready : function(v) {               
                var store = Fsl.store.getStore('S01I015005');
                store.clearFilter();
                store.loadPage(1);
				}
        }
    },
    initComponent: function() {
	   var me      = this;
       me.store    = Fsl.app.getRelStore('S01I015005');    
       me.tbar = Ext.create('Ext.ux.StatusBar', {            
            topBorder           : false,
            statusAlign         : 'right',
            items     : [{
                xtype       :'button',
                text        : 'Clear',
                icon        : Fsl.route.getIcon('IC01001/CAN01003.png'),
                action      : 'clear'
            },'-',{
                xtype       : 'button',
                text        : 'search',
                icon        : Fsl.route.getIcon('IC01001/search.png'),
                action      : 'search'
            },'-',{  
                xtype       :'statusbar',
                id          : 'win-statusbar-v01r015001x03',
                border      : false,
                defaultText : '',
                statusAlign : 'right'
            }]
        });
        
        this. bbar= new Ext.PagingToolbar({
            pageSize        : 10,
            store           : Fsl.app.getRelStore('S01I015005'),
            displayInfo     : true
        });
         
        this.columns = [{
            text            : 'SN',
            xtype           : 'rownumberer',
            width           :  40, 
            sortable        :  false
        },{
            text            : 'SN',
            dataIndex       : 'id',
            width           :  40, 
            sortable        :  false
        },{
            text            : 'Branch name',
            dataIndex       : 'name'
        },{
            text            : 'Location',
            dataIndex       : 'location'
        },{
            text            : 'Address',
            dataIndex       : 'address',
            flex            : 1
        }];       
        this.callParent(arguments);
    },    
    getTitle :function(value, metadata, record, rowIdx, colIdx, store, vie) {
              return record.getM01i007001().data.name;
    }
});