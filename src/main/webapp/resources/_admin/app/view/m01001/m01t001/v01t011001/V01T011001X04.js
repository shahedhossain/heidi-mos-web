Ext.define(Fsl.app.getAbsView('V01T011001X04'), {
    extend    : 'Ext.grid.Panel',
    alias     : Fsl.app.getAlias('V01T011001X04'),
    id        : 'V01T011001X04'.toLowerCase(),
    border    : true,
    modal     : true,
    height    : 290,
    width     : 500,   
    
    viewConfig: {
        stripeRows    : true,
        forceFit      : true,
        emptyText     : 'No Records to display',
        loadMask      : false,
        listeners     : {
            viewready : function(v) {               
                var store = Fsl.store.getStore('S01T011001');
                store.clearFilter();
                store.loadPage(1);
            }
        }
    },
    columnLines 	: true,
    rowLines            : true,
    initComponent	: function() {
        var me     	= this;
        me.store   	= Fsl.app.getRelStore('S01T011001');    
        me.tbar = Ext.create('Ext.ux.StatusBar', {            
            topBorder           : false,
            statusAlign         : 'right',
            items     : [{
                xtype       : 'button',
                text        : 'NEW',
                icon        : Fsl.route.getIcon('TB01001/ADD01004.png'),
                action      : 'new'
            },'-',{
                xtype       : 'button',
                text        : 'Clear',
                icon        : Fsl.route.getIcon('IC01001/CAN01003.png'),
                action      : 'clear'
            },'-',{
                xtype       : 'button',
                text        : 'search',
                icon        : Fsl.route.getIcon('IC01001/search.png'),
                action      : 'search'
            },'-',{  
                xtype       : 'statusbar',
                id          : 'win-statusbar-v01t011001x04',
                border      : false,
                defaultText : '',
                statusAlign : 'right'
            }]
        });
        
        this.bbar = new Ext.PagingToolbar({
            pageSize        :  10,
            store           :  Fsl.app.getRelStore('S01T011001'),
            displayInfo     :  true
        });  
		
        this.columns = [{
            text        : 'TRANSFER ID',
            dataIndex   : 'id',            
            sortable    :  false
        },{
            text        : 'TO BRANCH',
            dataIndex   : 'toBranchId',
            flex        : 1,
            renderer    : this.getBranchName
            
        },{
            text        : 'DATE',
            dataIndex   : 'entryDate',
            flex        : 1,
            renderer    : this.getFormatDate
        }];
       
        this.callParent(arguments);
    },   
    getBranchName :function(value, metadata, record, rowIdx, colIdx, store, vie) {
       /* var form = Ext.getCmp('v01t011001x03').getForm();
        form.findField('branchId').setValue(record.getM01i015001().data.id);
        form.findField('branchName').setValue(record.getM01i015001().data.name); */   
        return record.getM01i015002().data.name;
    },
    getFormatDate :function(value, metadata, record, rowIdx, colIdx, store, vie){
        return Ext.Date.format(value, 'M d, Y')
    }
});