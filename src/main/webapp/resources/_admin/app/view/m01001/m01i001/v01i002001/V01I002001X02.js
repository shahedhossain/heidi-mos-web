Ext.define(Fsl.app.getAbsView('V01I002001X02'), {  
    extend      : 'Ext.grid.Panel',
    alias       : Fsl.app.getAlias('V01I002001X02'),
    id          : 'V01I002001X02'.toLowerCase(), 
    border      : true,
    modal       : true,
    height      : 285,
    width       : 370,
    viewConfig  : {
        stripeRows    : true,
        forceFit      : true,
        emptyText     : 'No Records to display',
        listeners     : {
            viewready : function(v) {               
                var store = Fsl.store.getStore('S01I002001');
                store.clearFilter();
                store.loadPage(1);
            }
        }
    },
    initComponent: function() {
        var me = this;
        me.store   	= Fsl.app.getRelStore('S01I002001');        
        me.tbar = Ext.create('Ext.ux.StatusBar', { 
            border          : false,
            statusAlign     : 'right',
            items           : [{
                xtype       :'button',
                text        : 'ADD',
                icon        : Fsl.route.getIcon('TB01001/ADD01004.png'),
                action      : 'add'
            },'-',{
                xtype       :'button',
                text        : 'Clear',
                icon        : Fsl.route.getIcon('IC01001/CAN01003.png'),
                action      : 'clear'
            },'-',{
                xtype       : 'button',
                text        : 'search',
                icon        : Fsl.route.getIcon('IC01001/search.png'),
                action      : 'search'
            },'-',{  
                xtype       :'statusbar',
                id          : 'win-statusbar-v01i002001x02',
                border      : false,
                defaultText : '',
                statusAlign : 'right'
            }]
        });
        
        this. bbar= new Ext.PagingToolbar({
            pageSize    : 10,
            store       : Fsl.app.getRelStore('S01I002001'),
            displayInfo : true
        });
        
        this.columns = [{
            text            : 'SN',
            xtype           : 'rownumberer',
            width           :  40, 
            sortable        :  false
        },{
            text            : 'ID',
            dataIndex       : 'id',
            width           : 40, 
            sortable        : false
        },{
            text            : 'Manufacture',
            dataIndex       : 'name',
            flex            : 1
        },{
            menuDisabled    : true,
            sortable        : false,
            xtype           : 'actioncolumn',            
            width           : 22,            
            items           : [{
                icon        : Fsl.route.getImage('DEL01005.png'),
                tooltip     : 'Delete This?',
                scope       : this,
                handler     : function(grid, rowIdx, colIdx) {
                    var record      = grid.getStore().getAt(rowIdx),
                    manufacId       = record.data.id;                  
                    Ext.getCmp('WestPanel').confirmDelete(manufacId, 'M01I002001', 'S01I002001', 'v01i002001x02');  
                }
            }]
        }];
        this.callParent(arguments);
    }
});
