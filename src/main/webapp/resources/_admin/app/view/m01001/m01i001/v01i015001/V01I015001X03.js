Ext.define(Fsl.app.getAbsView('V01I015001X03'), { 
    extend      	: 'Ext.window.Window',
    alias       	:  Fsl.app.getAlias('V01I015001X03'),
    id          	: 'V01I015001X03'.toLowerCase(),   
    title       	: 'BRANCH ENTRY :: V01I015001X03',
    icon        	:  Fsl.route.getImage('APP01003.png'),	
    layout    : 'fit',
    autoShow  : true,
    modal     : true,
    width     : 320,
    height    : 260,
    initComponent: function () {    
        var me = this;        
        me.items = [
        {
            xtype     : 'form',
            border    : false,
            trackResetOnLoad	:  true,
            bodyStyle : {                    
                padding   : '10px'
            },
            
            tbar : Ext.create('Ext.ux.StatusBar', {
                id            : 'win-statusbar-v01i015001x03',
                topBorder     : false,
                text          : 'Status',
                defaultIconCls: 'fsl-severity-info',
                defaultText   : 'Status'
            }),

            items: [{
                xtype             : 'textfield',
                name              : 'id',
                fieldLabel        : 'Id',
                emptyText         : 'Not need....',
                width             : 265,
                anchor            : '100%',
                readOnly          : true
            },
            {
                xtype             : 'textfield',
                name              : 'name',
                fieldLabel        : 'Branch Name',
                emptyText         : 'Branch Name Here...',
                width             : 265,
                anchor            : '100%',
                allowBlank        : false
            },
            {
                xtype             : 'textfield',
                name              : 'location',
                fieldLabel        : 'Location',
                emptyText         : 'Location Here...',
                width             : 265,
                anchor            : '100%',
                allowBlank        : false
            },
            {
                xtype             : 'textarea',
                name              : 'address',
                fieldLabel        : 'Address',
                emptyText         : 'Address Here...',
                width             : 265,
                anchor            : '100%',
                allowBlank        : false
            }]
        }];
        this.buttons = [{
            text        : 'Save',
            icon        :  Fsl.route.getImage('SAV01004.png'),
            action      : 'save',
            height      :  20
        },{
            text        : 'Close',
            icon        :  Fsl.route.getImage('CLS01001.png'),   
            scope       :  this,
            handler     :  this.close,
            height      :  20
        }];
        this.callParent(arguments);
    }
});

