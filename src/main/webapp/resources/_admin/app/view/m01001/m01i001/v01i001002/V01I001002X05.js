Ext.define(Fsl.app.getAbsView('V01I001002X05'),{
    extend          : 'Ext.grid.Panel',
    alias           : Fsl.app.getAlias('V01I001002X05'),
    id              : 'V01I001002X05'.toLowerCase(),
    sortableColumns : true,       
    modal           : true,
    height          : 250,
    width           : 350,
    viewConfig: {
        stripeRows    : true,
        forceFit      : true,
        emptyText     : 'No Records to display',
        listeners     : {
            viewready : function(v) {               
                var store = Fsl.store.getStore('S01I002002');
                store.clearFilter();
                store.loadPage(1);
            }
        }
    },
    initComponent: function() {
        var me      = this;
        me.store    = Fsl.app.getRelStore('S01I002002');        
        me.tbar     = Ext.create('Ext.ux.StatusBar', {           
            topBorder       : false,
            statusAlign     : 'right',
            items    : [{
                xtype       :'button',
                text        : 'ADD',
                icon        : Fsl.route.getIcon('TB01001/ADD01004.png'),
                action      : 'add'
            },'-',{  
                xtype       : 'statusbar',
                id          : 'win-statusbar-v01i001002x05',
                border      :  false,
                defaultText : '',
                statusAlign : 'right'
            }]
        });
        
        this.bbar = Ext.create('Ext.PagingToolbar', {
            store           : Fsl.app.getRelStore('S01I002002'),
            displayInfo     : true,
            displayMsg      : 'Display {0} - {1} of {2}',
            emptyMsg        : "No Items"   
        });
             
        this.columns = [{
            text      : 'SN',
            xtype     : 'rownumberer',
            width     :  40, 
            sortable  :  false
        },{
            text        : 'ID',
            dataIndex   : 'id',
            width       :  40, 
            sortable    :  true            
        },{
            text        : 'Manufacturer',
            dataIndex   : 'name',
            flex        : 1
        }];
        this.callParent(arguments);
    }
});