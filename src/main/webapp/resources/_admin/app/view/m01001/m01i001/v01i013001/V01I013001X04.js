Ext.define(Fsl.app.getAbsView('V01I013001X04'), {
    extend      : 'Ext.window.Window',
    alias       : Fsl.app.getAlias('V01I013001X04'),
    id          : 'V01I013001X04'.toLowerCase(),
    title       : 'ARTICLE LVM :: V01I013001X04',
    icon        : Fsl.route.getImage('APP01009.png'),
    minimizable : false,
    maximizable : false,
    autoShow    : true,
    resizable   : false,
    modal       : true,
      layout    : {
            type: 'vbox',
            align: 'stretch'
        },
    items  : [{ 
        xtype       :  Fsl.app.getXtype('V01I013001X05'),
        resizable   :  false,
        border      :  false,
        componentCls: 'my-border-bottom'
    },{ 
        xtype       :  Fsl.app.getXtype('V01I013001X06'),
        resizable   :  false,
        border      :  false
    }]
});