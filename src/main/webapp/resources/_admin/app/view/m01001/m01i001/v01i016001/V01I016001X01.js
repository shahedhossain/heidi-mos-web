Ext.define(Fsl.app.getAbsView('V01I016001X01'), {   
    extend          : 'Ext.grid.Panel',
    alias           : Fsl.app.getAlias('V01I016001X01'),
    id              : 'V01I016001X01'.toLowerCase(), 
    border    : true,
    modal     : true,
    height    : 285,
    width     : 300,
    viewConfig: {
        stripeRows    : true,
        forceFit      : true,
        emptyText     : 'No Records to display',
        listeners     : {
            viewready : function(v) {               
                var store = Fsl.store.getStore('S01I016001');
                store.clearFilter();
                store.loadPage(1);
            }
        }
    },
    initComponent: function() {
        var me      = this;
        me.store    = Fsl.app.getRelStore('S01I016001');    
        me.tbar = Ext.create('Ext.ux.StatusBar', {            
            topBorder           : false,
            statusAlign         : 'right',
            items     : [{
                xtype       :'button',
                text        : 'ADD',
                icon        : Fsl.route.getIcon('TB01001/ADD01004.png'),
                action      : 'add'
            }]
        });

        this. bbar= Ext.create('Ext.ux.StatusBar', {
            id            : 'win-statusbar-v01i016001x01',
            topBorder     : false,
            text          : 'Status',
            defaultIconCls: 'fsl-severity-info',
            defaultText   : 'Status'
        });

        this.columns = [{
            text      : 'SN',
            xtype     : 'rownumberer',
            width     :  40, 
            sortable  :  false
        },{
            text        : 'ID',
            dataIndex   : 'id',
            width       : 40, 
            sortable    : false
        },{
            text        : 'name',
            dataIndex   : 'name',
            flex        : 1
        },{
            menuDisabled    : true,
            sortable        : false,
            xtype           : 'actioncolumn',            
            width           : 22,            
            items           : [{
                icon        : Fsl.route.getImage('DEL01005.png'),
                tooltip     : 'Delete This?',
                scope       : this,
                handler     : function(grid, rowIdx, colIdx) {
                    var record = grid.getStore().getAt(rowIdx);
                    var statusId = record.data.id
                    Ext.getCmp('WestPanel').confirmDelete(statusId, 'M01I016001', 'S01I016001', 'v01i016001x01');
                }
            }]
        }];       
        this.callParent(arguments);
    }
});