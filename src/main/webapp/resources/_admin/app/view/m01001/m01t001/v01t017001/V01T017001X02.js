Ext.define(Fsl.app.getAbsView('V01T017001X02'), {
    extend    : 'Ext.grid.Panel',
    alias     : Fsl.app.getAlias('V01T017001X02'),
    id        : 'V01T017001X02'.toLowerCase(),
    border    : true,
    modal     : true,
    height    : 285,
    width     : 500,     
    viewConfig: {
        stripeRows    : true,
        forceFit      : true,
        emptyText     : 'No Records to display',
        loadMask      : false,
        listeners     : {
            viewready : function(v) {               
                var store = Fsl.store.getStore('S01T017001');
                store.clearFilter();
                store.loadPage(1);
            }
        }
    },
    columnLines     : true,
    initComponent   : function() {
        var me      = this;
        me.store    = Fsl.app.getRelStore('S01T017001');  
        me.tbar     = Ext.create('Ext.ux.StatusBar', {            
            topBorder           : false,
            statusAlign         : 'right',
            items     : [{
                xtype       : 'button',
                text        : 'Clear',
                icon        : Fsl.route.getIcon('IC01001/CAN01003.png'),
                action      : 'clear'
            },'-',{
                xtype       : 'button',
                text        : 'search',
                icon        : Fsl.route.getIcon('IC01001/search.png'),
                action      : 'search'
            },'-',{  
                xtype       : 'statusbar',
                id          : 'win-statusbar-v01t017001x02',
                border      : false,
                defaultText : '',
                statusAlign : 'right'
            }]
        });
        
        this.bbar = new Ext.PagingToolbar({
            pageSize        :  10,
            store           :  Fsl.app.getRelStore('S01T017001'),
            displayInfo     :  true
        });  
                
        this.columns = [{
            text        : 'ID',
            dataIndex   : 'id',            
            sortable    :  false
        },{
            text        : 'ORDER ID',
            dataIndex   : 'orderId',            
            sortable    :  false
        },{
            text        : 'BRANCH',
            hidden      :  true,
            dataIndex   : 'branchId',
            renderer    :  this.getBranchName
            
        },{
            text        : 'FROM BRANCH',
            dataIndex   : 'fromBranchId',
            flex        :  1,
            renderer    :  this.getFromBranchName
            
        },{
            text        : 'STATUS',
            dataIndex   : 'status'            
        },{
            text        : 'Date',
            dataIndex   : 'entryDate',
            renderer    :  this.getFormatDate
        },{
            text        : 'Transfer',
            dataIndex   : 'transferId'
        }];
        this.callParent(arguments);
    },      
    getBranchName :function(value, metadata, record, rowIdx, colIdx, store, vie) {
        return record.getM01i015001().data.name;
    },   
    getFromBranchName :function(value, metadata, record, rowIdx, colIdx, store, vie) {
        return record.getM01i015002().data.name;
    },
    getFormatDate :function(value, metadata, record, rowIdx, colIdx, store, vie){
        return Ext.Date.format(value, 'M d, Y')
    }
});