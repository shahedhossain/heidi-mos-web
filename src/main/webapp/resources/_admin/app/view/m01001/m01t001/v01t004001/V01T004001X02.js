Ext.define(Fsl.app.getAbsView('V01T004001X02'), {
    extend    : 'Ext.grid.Panel',
    alias     : Fsl.app.getAlias('V01T004001X02'),
    id        : 'V01T004001X02'.toLowerCase(),
    border    : true,
    modal     : true,
    height    : 285,
    width     :  660,
    viewConfig: {
        stripeRows    : true,
        forceFit      : true,
        emptyText     : 'No Records to display',
        listeners     : {
            viewready : function(v) {               
                var store = Fsl.store.getStore('S01T004001');
                store.clearFilter();
                store.loadPage(1);
            }
        }
    },
    columnLines: true,
    initComponent: function() {
        var me     	= this;
        me.store   	= Fsl.app.getRelStore('S01T004001');     
        me.tbar = Ext.create('Ext.ux.StatusBar', {            
            topBorder           : false,
            statusAlign         : 'right',
            items     : [{
                xtype       : 'button',
                text        : 'ADD',
                icon        : Fsl.route.getIcon('TB01001/ADD01004.png'),
                action      : 'add'
            },'-',{
                xtype       : 'button',
                text        : 'Clear',
                icon        : Fsl.route.getIcon('IC01001/CAN01003.png'),
                action      : 'clear'
            },'-',{
                xtype       : 'button',
                text        : 'search',
                icon        : Fsl.route.getIcon('IC01001/search.png'),
                action      : 'search'
            },'-',{  
                xtype       : 'statusbar',
                id          : 'win-statusbar-v01t004001x02',
                border      : false,
                defaultText : '',
                statusAlign : 'right'
            }]
        });
        
        this.bbar = new Ext.PagingToolbar({
            pageSize        :  10,
            store           : Fsl.app.getRelStore('S01T004001'),
            displayInfo     :  true
        });        
            
        this.columns = [{
            text      : 'SN',
            xtype     : 'rownumberer',
            width     :  40, 
            sortable  :  false
        },{
            text        : 'ID',
            dataIndex   : 'id',
            width       :  40, 
            sortable    :  false,
            renderer    :  this.rowToolTip
        },{
            text        : 'Article',
            dataIndex   : 'articleId', 
            scope       :  this,
            renderer    :  this.getArticleName,
            flex        :  1,
            field: {
                xtype: 'numberfield'
            }
        },{
            text        : 'Rate',
            dataIndex   : 'priceId',
            align       : 'right',
            width       : 60,
            scope       :  this,
            renderer    : this.getPrice,
            field: {
                xtype: 'numberfield'
            }
        },{
            text        : 'Quantity',
            dataIndex   : 'quantity',
            align       : 'right',
            width       : 80,
            renderer    : this.rowToolTip,
            field: {
                xtype: 'numberfield'
            }            
        },{
            text        : 'Vat',
            dataIndex   : 'vatId',
            align       : 'right',
            width       : 50,
            scope       :  this,
            renderer    : this.getVat
        },{
            header      : 'Total',
            align       : 'right',
            width       : 80,
            sortable    : false,
            groupable   : false, 
            scope       : this,
            renderer    : function(value, metadata, record, rowIdx, colIdx, store, view) {
                this.rowToolTip(value, metadata, record)
                var rate            = parseFloat(record.getM01i012001().data.price);                
                var quantity        = parseFloat(record.get('quantity'));               
                var vatrate         = parseFloat(record.getM01i013001().data.vat);                
                var vat             = parseFloat(rate *(vatrate/100));
                var total           = parseFloat((rate+vat)*quantity);
                var decimalcontrol  = Ext.util.Format.number(total,'0.00');                
                return decimalcontrol+'&nbsp' + '৳';            
            }
        },{
            text            : 'Date',
            dataIndex       : 'date',
            width           : 80,
            renderer        : this.getFormatDate
            
        },{
            menuDisabled    :  true,
            sortable        :  false,
            xtype           : 'actioncolumn',            
            width           :  22,            
            items           :  [{
                icon        : Fsl.route.getImage('DEL01005.png'),
                tooltip     : 'Delete This?',
                scope       :  this,
                handler     :  function(grid, rowIdx, colIdx) {
                    var record  = grid.getStore().getAt(rowIdx);
                    var catId   = record.data.id
                    var stockId = record.data.id
                    Ext.getCmp('WestPanel').confirmDelete(stockId, 'M01T004001', 'S01T004001', 'v01t004001x02');
                }
            }]
        }];       
        this.callParent(arguments);
    },    
    getArticleName :function(value, metadata, record, rowIdx, colIdx, store, vie) {
        this.rowToolTip(value, metadata, record)
        return record.getM01i006001().data.name;
    }, 
    getVendorName :function(value, metadata, record, rowIdx, colIdx, store, vie) {
        this.rowToolTip(value, metadata, record)
        return record.getM01i014001().data.name;
    }, 
    getBranchName :function(value, metadata, record, rowIdx, colIdx, store, vie) {
        this.rowToolTip(value, metadata, record)
        return record.getM01i015001().data.name;
    },
    getFormatDate :function(value, metadata, record, rowIdx, colIdx, store, vie){
        this.rowToolTip(value, metadata, record)
        return Ext.Date.format(value, 'M d, Y')
    },    
    getPrice :function(value, metadata, record, rowIdx, colIdx, store, view){
        this.rowToolTip(value, metadata, record)
        var value=record.getM01i012001().data.price +'&nbsp';
        return value+'৳';
    },
    getVat : function(value, metadata, record, rowIdx, colIdx, store, view){
        this.rowToolTip(value, metadata, record)
        var value=record.getM01i013001().data.vat +'&nbsp';
        return value+'%';
    },
    rowToolTip : function(value, metadata, record) {
        metadata.style = 'color:#D10000;background:#F7E4E4;' //font-weight:bold
        var smarty = "<b>Stock Item Information</b>";
        smarty += "<br/>Stock No    : {id}";
        smarty += "<br/>Product id  : {articleId}";
        smarty += "<br/>Product Name:"+record.getM01i006001().data.name;
        smarty += "<br/>Rate        :"+record.getM01i012001().data.price;
        smarty += "<br/>Vat         :"+record.getM01i013001().data.vat;
        smarty += "<br/>Quantity    : {quantity}";  
        smarty += "<br/>Branch      :"+record.getM01i015001().data.name;
        smarty += "<br/>Vendor      :"+record.getM01i014001().data.name;
        smarty += "<br/>Date        :"+Ext.Date.format(record.data.date, 'M d, Y'); 

        var tpl = new Ext.XTemplate(smarty);
        var qtip = tpl.apply(record.data)
        metadata.tdAttr = 'data-qtip="' + qtip + '"';           

        return value
    }
});