Ext.define(Fsl.app.getAbsView('V01I008001X01'), {
    extend      	: 'Ext.form.Panel',
    alias     		: Fsl.app.getAlias('V01I008001X01'),
    id          	: 'V01I008001X01'.toLowerCase(),      
    bodyStyle           : {
        background        : 'none'
    },
    defaults: {        
        activeRecord    : null,
        border          : true, 
        layout          : 'hbox',        
        fieldDefaults   : {
            anchor      : '100%',
            labelAlign  : 'right'
        }
    },
    
    initComponent       : function(){        
        Ext.QuickTips.init();
        Ext.form.Field.prototype.msgTarget = 'qtip';         
        var me          = this; 
        me.items    = [{
            xtype                 : 'fieldcontainer',
            combineErrors         :  true,                    
            defaults              : {
                layout            : 'fit',
                margin            : '0 5 0 5',
                hideLabel         :  false,                
                labelAlign        : 'top',
                flex              : 1
            },
            items: [{
                xtype             : 'numberfield',
                name              : 'id',
                fieldLabel        : 'ID:',
                width             :  100,                        
                minValue          :  0,
                mouseWheelEnabled :  false,
                hideTrigger       :  true,
                decimalPrecision  :  0,
                keyNavEnabled     :  false, 
                width             :  70
            },{
                xtype             : 'numberfield',
                name              : 'vatPercent',
                fieldLabel        : 'Percent:',
                minValue          :  0,
                maxValue          :  100,
                mouseWheelEnabled :  false,
                hideTrigger       :  true,
                decimalPrecision  :  4,
                keyNavEnabled     :  false, 
                width             :  70
            },{
                xtype             : 'datefield',
                name              : 'startDate',
                fieldLabel        : 'Start Date:',
                format            : 'M d, Y',
                altFormats        : 'd/m/Y|M d, Y h:i A',
                id                : 'startdt-v01i008001x01',
                vtype             : 'daterange',
                endDateField      : 'enddt-v01i008001x01',
                flex              : 1

            },{
                xtype             : 'datefield',
                name              : 'endDate',
                fieldLabel        : 'End Date:',
                format            : 'M d, Y',
                altFormats        : 'd/m/Y|M d, Y h:i A',
                id                : 'enddt-v01i008001x01',
                vtype             : 'daterange',
                startDateField    : 'startdt-v01i008001x01' ,
                flex              : 1
            }]
        }];       
        me.callParent(arguments);
    }
});

