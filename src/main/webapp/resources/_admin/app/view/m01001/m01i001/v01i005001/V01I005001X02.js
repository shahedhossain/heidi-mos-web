Ext.define(Fsl.app.getAbsView('V01I005001X02'), {   
    extend  		: 'Ext.grid.Panel',
    alias           : Fsl.app.getAlias('V01I005001X02'),
    id              : 'V01I005001X02'.toLowerCase(), 
    border    		: true,
    modal     		: true,    
    width     		: 500,
    height    		: 285,
    viewConfig		: {
        stripeRows    	: true,
        forceFit      	: true,
        emptyText     	: 'No Records to display',
        listeners     	: {
            viewready 	: function(v) {               
                var store = Fsl.store.getStore('S01I005001');
                store.clearFilter();
                store.loadPage(1);
            }
        }
    },
    initComponent: function() {
        var me = this;
        me.store   = Fsl.app.getRelStore('S01I005001');    
        this.tbar   = Ext.create('Ext.ux.StatusBar', {            
            topBorder       : false,
            statusAlign     : 'right',
            items    : [{
                xtype       :'button',
                text        : 'ADD',
                icon        : Fsl.route.getIcon('TB01001/ADD01004.png'),
                action      : 'add'
            },'-',{
                xtype       :'button',
                text        : 'Clear',
                icon        : Fsl.route.getIcon('IC01001/CAN01003.png'),
                action      : 'clear'
            },'-',{
                xtype       : 'button',
                text        : 'search',
                icon        : Fsl.route.getIcon('IC01001/search.png'),
                action      : 'search'
            },'-',{  
                xtype       :'statusbar',
                id          : 'win-statusbar-v01i005001x02',
                border      : false,
                defaultText : '',
                statusAlign : 'right'
            }]
        });
        
          

        this. bbar= new Ext.PagingToolbar({
            pageSize    	: 20,
            store       	: Fsl.app.getRelStore('S01I005001'),
            displayInfo 	: true
        });
        
            
        this.columns = [{
            text        	: 'SN',
            xtype       	: 'rownumberer',
            width       	:  40, 
            sortable    	:  false
        },{
            text        	: 'ID',
            dataIndex   	: 'id',
            width       	:  40, 
            sortable    	:  false
        },{
            text        	: 'Brand',
            dataIndex   	: 'name'
        },{
            text        	: 'brand Type',
            dataIndex   	: 'brandTypeId',
            renderer    	:  this.getBrandType,
            flex        	:  1
        },{
            text        	: 'Manufacture',
            dataIndex   	: 'manufacturerId',
            renderer    	:  this.getManufacturerName,
            flex        	:  1
        },{
            text        	: 'Generic Product',
            dataIndex   	: 'genericProductId',
            renderer   		:  this.getGenericProductName,
            flex        	:  1
        },{
            menuDisabled    : true,
            sortable        : false,
            xtype           : 'actioncolumn',            
            width           : 22,            
            items           : [{
                icon        : Fsl.route.getImage('DEL01005.png'),
                tooltip     : 'Delete This?',
                scope       : this,
                handler     : function(grid, rowIdx, colIdx) {
                    var record = grid.getStore().getAt(rowIdx);
                    var brandId = record.data.id
                    Ext.getCmp('WestPanel').confirmDelete(brandId, 'M01I005001', 'S01I005001', 'v01i005001x02');                              
                }
            }]
        }];       
        this.callParent(arguments);
    },
    getBrandType :function(value, metadata, record, rowIdx, colIdx, store, vie) {
        return record.getM01i004001().data.name;
    },
    getManufacturerName :function(value, metadata, record, rowIdx, colIdx, store, vie) {
        return record.getM01i002001().data.name;
    },
    getGenericProductName :function(value, metadata, record, rowIdx, colIdx, store, vie) {
        return record.getM01i003001().data.name;
    }
});