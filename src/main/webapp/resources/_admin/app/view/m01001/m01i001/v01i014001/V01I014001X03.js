Ext.define(Fsl.app.getAbsView('V01I014001X03'), { 
    extend      	: 'Ext.window.Window',
    alias       	:  Fsl.app.getAlias('V01I014001X03'),
    id          	: 'V01I014001X03'.toLowerCase(),   
    title       	: 'VENDOR ENTRY :: V01I014001X03',
    icon        	:  Fsl.route.getImage('APP01003.png'),	
    layout              : 'fit',
    autoShow            : true,
    modal               : true,
    width               : 320,
    height              : 290,   
    initComponent: function () {
    
        var me = this;        
        me.items = [
        {
            xtype     : 'form',
            border    :  false,
            trackResetOnLoad	:  true,
            bodyStyle : {                    
                padding   : '10px'
            },
            
            tbar : Ext.create('Ext.ux.StatusBar', {
                id            : 'win-statusbar-v01i014001x03',
                topBorder     : false,
                text          : 'Status',
                defaultIconCls: 'fsl-severity-info',
                defaultText   : 'Status'
            }),
            items: [{
                xtype             : 'numberfield',
                name              : 'id',
                fieldLabel        : 'Id',
                emptyText         : 'Not Required....',
                width             :  265,
                anchor            : '100%',
                readOnly          :  true,
                minValue          :  0,
                mouseWheelEnabled :  false,
                hideTrigger       :  true,
                decimalPrecision  :  0,
                keyNavEnabled     :  false
            },
            {
                xtype             : 'combo',
                name              : 'titleId',
                fieldLabel        : 'Vendor Title',
                emptyText         : 'Select One...',
                store             : Fsl.app.getRelStore('S01I007002'),
                displayField      : 'name',
                valueField        : 'id',
                queryMode         : 'remote',
                allowBlank        : false,
                editable          : false,
                width             : 265,
                anchor            : '100%'
            },
            {
                xtype             : 'textfield',
                name              : 'name',
                fieldLabel        : 'Vendor Name',
                emptyText         : 'Vendor Name Here...',
                width             : 265,
                anchor            : '100%',
                allowBlank        : false
            },
            {
                xtype             : 'textfield',
                name              : 'contact',
                fieldLabel        : 'Contact',
                emptyText         : 'Contact Here...',
                width             : 265,
                anchor            : '100%',
                allowBlank        : false
            },
            {
                xtype             : 'textarea',
                name              : 'address',
                fieldLabel        : 'Address',
                emptyText         : 'Address Here...',
                width             : 265,
                anchor            : '100%',
                allowBlank        : false
            }]
        }];
        this.buttons = [{
            text        : 'Save',
            icon        :  Fsl.route.getImage('SAV01004.png'),
            action      : 'save',
            height      :  20
        },{
            text        : 'Close',
            icon        :  Fsl.route.getImage('CLS01001.png'),   
            scope       :  this,
            handler     :  this.close,
            height      :  20
        }];
        this.callParent(arguments);
    }
});

