Ext.define(Fsl.app.getAbsView('V01I017001'), {
    extend      : 'Ext.window.Window',
    alias       : Fsl.app.getAlias('V01I017001'),
    icon        : Fsl.route.getImage('APP01009.png'),
    id          : 'V01I017001'.toString(),
    title       : 'ARTICLE LIMIT LVM :: V01I017001',
    minimizable :  false,
    maximizable :  false,
    autoShow    :  true,
    resizable   :  false,
    modal       :  true, 
    layout      :  {
        type	: 'vbox',
        align	: 'stretch'
    },
    items  : [{ 
        xtype     	: Fsl.app.getXtype('V01I017001X01'), 
        resizable 	: false,
        border    	: false,
        componentCls    : 'my-border-bottom'
    },{ 
        xtype     	: Fsl.app.getXtype('V01I017001X02'),       
        resizable 	: false,
        border    	: false
    }]
});