Ext.define(Fsl.app.getAbsView('V01I007001X03'), { 
    extend      : 'Ext.window.Window',
    alias       :  Fsl.app.getAlias('V01I007001X03'),
    id          : 'V01I006001X03'.toLowerCase(),   
    title       : 'SALUTATION :: V01I007001X03',
    icon        :  Fsl.route.getImage('APP01003.png'),	
	layout      : 'fit',
    autoShow  	:  true,
    modal     	:  true,
    width     	:  270,
    height    	:  157,
    initComponent: function () {
        var me = this;     
        me.items = [
        {
            xtype           : 'form',
            border          :  false,
            trackResetOnLoad:  true,
            bodyStyle : {                    
                padding   : '10px'
            },
            
            tbar : Ext.create('Ext.ux.StatusBar', {
                id            : 'win-statusbar-v01i007001x03',
                topBorder     : false,
                text          : 'Status',
                defaultIconCls: 'fsl-severity-info',
                defaultText   : 'Status'
            }),
            
            items: [{
                    xtype             : 'numberfield',
                    name              : 'id',
                    fieldLabel        : 'Id',
                    emptyText         : 'Not need....',
                    width             :  265,
                    anchor            : '100%',
                    readOnly          :  true,
                    minValue          :  0,
                    mouseWheelEnabled :  false,
                    hideTrigger       :  true,
                    decimalPrecision  :  0,
                    labelWidth        :  50,
                    keyNavEnabled     :  false
                }, 
                {
                    xtype             : 'textfield',
                    fieldLabel        : 'NAME',
                    emptyText         : 'Write name....',
                    name              : 'name',
                    width             : 265,
                    anchor            : '100%',
                    labelWidth        :  50,
                    allowBlank        : false
            }]
        }];
        this.buttons = [
        {
            text		: 'Save',
            icon        : Fsl.route.getImage('SAV01004.png'),
            action		: 'save'
        },
        {
            text		: 'Close',
            icon        : Fsl.route.getImage('CLS01001.png'),   
            scope		: this,
            handler		: this.close
        }
        ];
        this.callParent(arguments);
    }
});


