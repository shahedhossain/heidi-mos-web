Ext.define(Fsl.app.getAbsView('V01T012001X08'), {
    extend        : 'Ext.form.Panel',
    alias         : Fsl.app.getAlias('V01T012001X08'),
    id            : 'V01T012001X08'.toLowerCase(),  
    background    : false,
    componentCls  : 'my-border-bottom',
    bodyStyle     : {
        background: 'none'                
    },
    defaults: {        
        activeRecord  : null,
        border        : true, 
        layout        : 'hbox',        
        fieldDefaults : {
            anchor    : '100%',
            labelAlign: 'right'
        }
    },
    
    initComponent     : function(){        
        Ext.QuickTips.init();
        Ext.form.Field.prototype.msgTarget = 'qtip';         
        var me = this;        
        me.items = [{
            xtype: 'fieldcontainer',
            combineErrors: true,                    
            defaults: {
                layout      : 'fit',
                flex        : 1,
                margin      : '5 7 0 10',
                hideLabel   : false,                
                labelAlign  : 'left'
            },
            items: [{
                xtype             : 'textfield',
                name              : 'transferId', 
                fieldLabel        : 'Transfer Id:',                       
                readOnly          : true
            },{
                xtype             : 'datefield',
                name              : 'sendDate',
                fieldLabel        : 'Send Date:',                        
                readOnly          : true,
                format            : 'M d, Y',
                altFormats        : 'd/m/Y|M d, Y h:i A'
            }]
        },{
            xtype             : 'fieldcontainer',
            combineErrors     : true,             
            defaults: { 
                layout        : 'fit',
                flex          : 1,
                margin        : '0 7 0 10',
                hideLabel     : false,                
                labelAlign    : 'left'
            },
            items: [
            {
                xtype             : 'textfield',
                fieldLabel        : 'From Branch Id:',
                name              : 'fromBranchId', 
                readOnly          : true
            },{
                xtype             : 'textfield',
                name              : 'fromBranchName',  
                fieldLabel        : 'From Branch :',
                readOnly          : true,
                anchor            : '95%',
                width             : 100,
                margin            : '0 7 0 10'
            }]
        },{
            xtype             : 'fieldcontainer',
            combineErrors     : true,             
            defaults: { 
                layout        : 'fit',
                margin        : '0 7 0 10',
                hideLabel     : false,                
                labelAlign    : 'left'
            },
            items: [
            {
                xtype             : 'numberfield',                
                hideTrigger       : true,
                decimalPrecision  : 0,
                keyNavEnabled     : false,
                mouseWheelEnabled : false,
                fieldLabel        : 'Receive Id :',                
                name              : 'receiveId', 
                width             : 267
            },            
            {
                xtype             : 'datefield',
                fieldLabel        : 'Receive Date :',
                name              : 'entryDate',
                width             : 267,
                format            : 'M d, Y',
                altFormats        : 'd/m/Y|M d, Y h:i A',
                value             : '2.4.2013',
                readOnly          : true
            }]
        }];       
        me.callParent(arguments);
    }
});

