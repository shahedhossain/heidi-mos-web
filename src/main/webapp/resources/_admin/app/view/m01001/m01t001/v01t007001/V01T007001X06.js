Ext.define(Fsl.app.getAbsView('V01T007001X06'), {
    extend              : 'Ext.form.Panel',
    alias               : Fsl.app.getAlias('V01T007001X06'),
    id                  : 'V01T007001X06'.toLowerCase(),  
    bodyStyle : {
        background: 'none'                
    },
    initComponent : function(){
        var me      = this;     
        me.items  =[{
            defaultType   : 'textfield',
            layout        : 'hbox',
            flex          : 1,
            border        : false,
            fieldDefaults : {
                anchor      : '100%',
                labelAlign  : 'right'               
            },
            bodyStyle     : {
                background  : 'none'                
            },
            items   : [{
                xtype             : 'fieldcontainer',
                layout            : 'vbox',
                margin            : '5 0 0 5',                        
                defaults          :  {
                    width           :  150,
                    labelAlign      : 'left',
                    labelWidth      :  60,
                    readOnly        :  true
                }, 
                items : [
                {
                    xtype               : 'numberfield',
                    hideTrigger         :  true,
                    decimalPrecision    :  0,
                    keyNavEnabled       :  false,
                    mouseWheelEnabled   :  false,
                    fieldLabel          : 'Rebate',
                    name                : 'rebate'
                },{
                    xtype               : 'numberfield',
                    hideTrigger         :  true,
                    decimalPrecision    :  0,
                    keyNavEnabled       :  false,
                    mouseWheelEnabled   :  false,
                    fieldLabel          : 'G.VAT',
                    name                : 'grossVat' ,
                    allowBlank          :  false,
                    value               :  0.00
                },{
                    xtype               : 'numberfield',
                    hideTrigger         :  true,
                    decimalPrecision    :  0,
                    keyNavEnabled       :  false,
                    mouseWheelEnabled   :  false,
                    fieldLabel          : 'Promotion',
                    name                : 'promotion' ,
                    value               :  0.00
                }
                ]
            },                          
            {
                xtype           : 'fieldcontainer',
                layout          : 'vbox',
                margin          : '5 0 0 10',
                defaults        :  {
                    width       :  30,
                    readOnly    :  true
                },
                items: [
                {
                    xtype     : 'textfield',
                    name      : 'rebatePercent' 
                },{
                    xtype     : 'textfield',
                    name      : 'vatPercent'

                },{
                    xtype     : 'textfield',
                    name      : 'percent',
                    enable    :  true

                },{
                    xtype     : 'hiddenfield',
                    name      : 'rebateId'
                },{
                    xtype     : 'hiddenfield',
                    name      : 'grossVatId'
                },{
                    xtype     : 'hiddenfield',
                    name      : 'promotionId'
                }]
            },{
                xtype     : 'fieldcontainer',
                layout    : 'vbox',
                margin    : '3 0 0 7',
                items: [
                {
                    xtype     : 'displayfield',
                    value     : '%',
                    margin    : '5 0 5 0'
                },{
                    xtype     : 'displayfield',
                    value     : '%',
                    margin    : '5 0 0 0'
                }
                ]
            },{
                xtype       : 'fieldcontainer',
                layout      : 'vbox',
                margin      : '5 0 0 10',                        
                defaults          :  {
                    width           :  150,
                    labelAlign      : 'left',
                    labelWidth      : 60,
                    readOnly        : true
                }, 
                items: [{
                    xtype               : 'textfield',
                    name                : 'purchaseTarget',
                    fieldLabel          : 'Target'                                
                },{
                    xtype               : 'numberfield',
                    hideTrigger         :  true,
                    decimalPrecision    :  0,
                    keyNavEnabled       :  false,
                    mouseWheelEnabled   :  false,
                    fieldLabel          : 'Total',
                    name                : 'total',
                    allowBlank          :  false,
                    value               :  0.00
                },{
                    xtype               : 'numberfield',
                    fieldLabel          : 'G.Total',
                    name                : 'gtotal' , 
                    hideTrigger         :  true,
                    decimalPrecision    :  0,
                    keyNavEnabled       :  false,
                    mouseWheelEnabled   :  false,
                    allowBlank          :  false                                
                }
                ]
            },{
                xtype               : 'fieldcontainer',
                layout              : 'vbox',
                margin              : '5 0 0 10',                        
                defaults            :  {
                    width           :  160,
                    flex            :  1,
                    labelAlign      : 'left',
                    labelWidth      : 60
                },                 
                items: [
                {
                    xtype         : 'combobox',
                    fieldLabel    : 'Method',
                    triggerAction : 'all',
                    forceSelection: true,
                    store         : Fsl.store.getStore('S01I011001'),
                    editable      :  false,
                    emptyText     : 'Please Select',
                    name          : 'payMethodId',
                    typeAhead     :  true,
                    queryMode     : 'remote',
                    displayField  : 'name',
                    valueField    : 'id',
                    allowBlank    : false
                },{
                    xtype               : 'numberfield',
                    name                : 'payAmount',
                    fieldLabel          : 'Pay',
                    enableKeyEvents     : true,
                    hideTrigger         : true,
                    decimalPrecision    : 0,
                    keyNavEnabled       : false,
                    mouseWheelEnabled   : false,
                    allowBlank          : false
                },{
                    xtype     : 'textfield',
                    name      : 'return',
                    fieldLabel: 'Return',
                    readOnly: true
                }]
            }]
        }];
        this.callParent(arguments);
    }
});