Ext.define(Fsl.app.getAbsView('V01T011001X05'), {
    extend      : 'Ext.window.Window',
    alias       : Fsl.app.getAlias('V01T011001X05'),
    icon        : Fsl.route.getImage('APP01009.png'),
    id          : 'V01T011001X05'.toLowerCase(),
    title       : 'TRANSFER WIN :: V01T011001X05',
    minimizable : false,
    maximizable : false,
    autoShow    : true,
    resizable   : false,
    modal       : true,
    width       : 585,     
    layout    : {
        type: 'vbox',
        align: 'stretch'
    },
    items  : [{ 
        xtype     	: Fsl.app.getXtype('V01T011001X06'), 
        resizable 	: false,
        border    	: false,
        componentCls    : 'my-border-bottom'
    },{ 
        xtype     	: Fsl.app.getXtype('V01T011001X07'),       
        resizable 	: false,
        border    	: false
    }],
     
    buttons: [
    {
        text    : 'NEW',
        icon    : Fsl.route.getImage('NEW01001.png'),
        action  : 'newTransfer'//resetAll
    },{
        text    : 'SAVE',
        icon    :  Fsl.route.getImage('SAV01004.png'),
        action  : 'save'
    },{
        text    : 'PDF',
        icon    :  Fsl.route.getImage('PDF01001.png'),
        action  : 'pdf'
    }]
});