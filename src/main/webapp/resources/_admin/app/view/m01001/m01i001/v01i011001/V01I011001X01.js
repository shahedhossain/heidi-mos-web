Ext.define(Fsl.app.getAbsView('V01I011001X01'), {   
    extend          : 'Ext.grid.Panel',
    alias           : Fsl.app.getAlias('V01I011001X01'),
    id              : 'V01I011001X01'.toLowerCase(), 
    border          : true,
    modal           : true,
    height          : 330,
    width           : 300,
    viewConfig: {
        stripeRows    : true,
        forceFit      : true,
        emptyText     : 'No Records to display',
        listeners     : {
            viewready : function(v) {               
                var store = Fsl.store.getStore('S01I011001');
                store.clearFilter();
                store.loadPage(1);
            }
        }
    },
    initComponent: function() {
        var me      = this;
        me.store    = Fsl.app.getRelStore('S01I011001');  
        this.tbar = [{
                xtype       :'button',
                text        : 'ADD',
                icon        : Fsl.route.getIcon('TB01001/ADD01004.png'),
                action      : 'add'
            },'-',{  
                xtype       :'statusbar',
                id          : 'win-statusbar-v01i011001x01',
                border      : false,
                defaultText : '',
                statusAlign : 'right'
            }],       

        this. bbar= new Ext.PagingToolbar({
            pageSize    : 10,
            store       : Fsl.app.getRelStore('S01I011001'),
            displayInfo : true
        });

        this.columns = [{
            text      : 'SN',
            xtype     : 'rownumberer',
            width     :  40, 
            sortable  :  false
        },{
            text        : 'ID',
            dataIndex   : 'id',
            width       : 40, 
            sortable    : false
        },{
            text        : 'PayMethod Name',
            dataIndex   : 'name',
            flex        : 1
        },{
            menuDisabled    : true,
            sortable        : false,
            xtype           : 'actioncolumn',            
            width           : 22,            
            items           : [{
                icon        :  Fsl.route.getImage('DEL01005.png'),
                tooltip     : 'Delete This?',
                scope       : this,
                handler     : function(grid, rowIdx, colIdx) {
                    var record  = grid.getStore().getAt(rowIdx),
                    catId       = record.data.id,
                    payMethodId = record.data.id;
                     Ext.getCmp('WestPanel').confirmDelete(payMethodId, 'M01I011001', 'S01I011001', 'v01i011001x01');
                }
            }]
        }];       
        this.callParent(arguments);
    }
});