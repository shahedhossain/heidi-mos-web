Ext.define(Fsl.app.getAbsView('V01T010001X02'), {
    extend    : 'Ext.grid.Panel',
    alias     : Fsl.app.getAlias('V01T010001X02'),
    id        : 'V01T010001X02'.toString(),
    border    : true,
    modal     : true,
    height    : 285,
    width     : 500,
    viewConfig: {
        stripeRows    : true,
        forceFit      : true,
        emptyText     : 'No Records to display',
        listeners     : {
            viewready : function(v) {               
                var store = Fsl.store.getStore('S01T010001');
                store.clearFilter();
                store.loadPage(1);
            }
        }
    },
    initComponent: function() {
         var me     	= this;
        me.store   	= Fsl.app.getRelStore('S01T010001');      
        me.tbar = Ext.create('Ext.ux.StatusBar', {            
            topBorder           : false,
            statusAlign         : 'right',
            items     : [{
                xtype       : 'button',
                text        : 'ADD',
                icon        : Fsl.route.getIcon('TB01001/ADD01004.png'),
                action      : 'add'
            },'-',{
                xtype       : 'button',
                text        : 'Clear',
                icon        : Fsl.route.getIcon('IC01001/CAN01003.png'),
                action      : 'clear'
            },'-',{
                xtype       : 'button',
                text        : 'search',
                icon        : Fsl.route.getIcon('IC01001/search.png'),
                action      : 'search'
            },'-',{  
                xtype       : 'statusbar',
                id          : 'win-statusbar-v01t010001x02',
                border      : false,
                defaultText : '',
                statusAlign : 'right'
            }]
        });
        
        this.bbar = new Ext.PagingToolbar({
            pageSize        :  10,
            store           :  Fsl.app.getRelStore('S01T010001'),
            displayInfo     :  true
        });
            
        this.columns = [{
            text            : 'SN',
            dataIndex       : 'id',
            width           :  40, 
            sortable        :  false
        },{
            text            : 'Article',
            dataIndex       : 'articleId',
            renderer        :  this.getArticleName,
            flex            :  1
        },{
            text            : 'Start Date',
            dataIndex       : 'startDate',
            renderer        :  this.getFormatDate
        },{
            text            : 'End Date',
            dataIndex       : 'endDate',
            renderer        :  this.getFormatDate
        },{
            text            : 'Discount',
            dataIndex       : 'discount',
            width           :  65
        },{
            menuDisabled    :  true,
            sortable        :  false,
            xtype           : 'actioncolumn',            
            width           :  22,            
            items           : [{
                icon        :  Fsl.route.getImage('DEL01005.png'),
                tooltip     : 'Delete This?',
                scope       :  this,
                handler     :  function(grid, rowIdx, colIdx) {
                    var record      =   grid.getStore().getAt(rowIdx);
                    var discountId  =   record.data.id
                    Ext.getCmp('WestPanel').confirmDelete(discountId, 'M01T010001', 'S01T010001', 'v01t010001x02');
                }
            }]
        }];       
        this.callParent(arguments);
    },    
    getArticleName :function(value, metadata, record, rowIdx, colIdx, store, vie) {
        return record.getM01i006001().data.name;
    },
    getFormatDate :function(value, metadata, record, rowIdx, colIdx, store, vie){ 
        return Ext.Date.format(value, 'M d, Y')
    }
});