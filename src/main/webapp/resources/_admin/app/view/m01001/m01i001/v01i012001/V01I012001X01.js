Ext.define(Fsl.app.getAbsView('V01I012001X01'), {
    extend      	: 'Ext.form.Panel',
    alias     		: Fsl.app.getAlias('V01I012001X01'),
    id          	: 'V01I012001X01'.toLowerCase(),  
    //width               : 455,
    bodyStyle           : {
        background        : 'none'
    },
    defaults: {        
        activeRecord    : null,
        border          : true, 
        layout          : 'hbox',        
        fieldDefaults   : {
            anchor      : '100%',
            labelAlign  : 'right'
        }
    },
    
    initComponent       : function(){        
        Ext.QuickTips.init();
        Ext.form.Field.prototype.msgTarget = 'qtip';         
        var me          = this; 
        me.items    = [{
            xtype                 : 'fieldcontainer',
            combineErrors         :  true,                    
            defaults              : {
                layout            : 'fit',
                margin            : '0 5 0 5',
                hideLabel         :  false,                
                labelAlign        : 'top',
                flex              : 1
            },
            items: [{
                xtype             : 'numberfield',
                name              : 'id',
                fieldLabel        : 'Price ID:',
                //width             :  100,                        
                minValue          :  0,
                mouseWheelEnabled :  false,
                hideTrigger       :  true,
                decimalPrecision  :  0,
                keyNavEnabled     :  false                 
            },{
                xtype             : 'numberfield',
                name              : 'articleId',
                fieldLabel        : 'Article ID:',
                //width             :  100,                        
                minValue          :  0,
                mouseWheelEnabled :  false,
                hideTrigger       :  true,
                decimalPrecision  :  0,
                keyNavEnabled     :  false                
            },{
                xtype             : 'textfield',
                fieldLabel        : 'Article Name',
                emptyText         : 'Write article name',
                allowBlank        :  true,
                name              : 'articleName'
            }]
        },{ 
            xtype                 : 'fieldcontainer',
            combineErrors         :  true,                    
            defaults              : {
                layout            : 'fit',
                margin            : '0 5 0 5',
                hideLabel         :  false,                
                labelAlign        : 'top',
                flex              : 1
            },
            items: [{
                xtype             : 'numberfield',
                name              : 'price',
                fieldLabel        : 'Price:',
                minValue          :  0,
                mouseWheelEnabled :  false,
                hideTrigger       :  true,
                decimalPrecision  :  0,
                keyNavEnabled     :  false
            },{
                xtype             : 'numberfield',
                name              : 'mbcode',
                fieldLabel        : 'MBCODE:',
                minValue          :  0,
                mouseWheelEnabled :  false,
                hideTrigger       :  true,
                decimalPrecision  :  0,
                keyNavEnabled     :  false
            },{
                xtype             : 'numberfield',
                name              : 'pbcode',
                fieldLabel        : 'PBCODE:',
                minValue          :  0,
                mouseWheelEnabled :  false,
                hideTrigger       :  true,
                decimalPrecision  :  0,
                keyNavEnabled     :  false
            },{
                xtype             : 'datefield',
                name              : 'date',
                fieldLabel        : 'Date:',
                format            : 'M d, Y',
                altFormats        : 'd/m/Y|M d, Y h:i A'
            }]
        }];       
        me.callParent(arguments);
    }
});

