Ext.define(Fsl.app.getAbsView('V01I008001X02'), {   
    extend          : 'Ext.grid.Panel',
    alias           : Fsl.app.getAlias('V01I008001X02'),
    id              : 'V01I008001X02'.toLowerCase(), 
    border          : true,
    modal           : true,
    height          : 285,
    viewConfig: {
        stripeRows    : true,
        forceFit      : true,
        emptyText     : 'No Records to display',
        listeners     : {
            viewready : function(v) {               
                var store = Fsl.store.getStore('S01I008001');
                store.clearFilter();
                store.loadPage(1);
            }
        }
    },
    initComponent: function() {
        var me      = this;
        me.store    = Fsl.app.getRelStore('S01I008001');    
        me.tbar= Ext.create('Ext.ux.StatusBar', {            
            topBorder               : false,
            statusAlign             : 'right',
            items    : [{
                xtype       :'button',
                text        : 'ADD',
                icon        : Fsl.route.getIcon('TB01001/ADD01004.png'),
                action      : 'add'
            },'-',{
                xtype       :'button',
                text        : 'Clear',
                icon        : Fsl.route.getIcon('IC01001/CAN01003.png'),
                action      : 'clear'
            },'-',{
                xtype       : 'button',
                text        : 'search',
                icon        : Fsl.route.getIcon('IC01001/search.png'),
                action      : 'search'
            },'-',{  
                xtype       :'statusbar',
                id          : 'win-statusbar-v01i008001x02',
                border      : false,
                defaultText : '',
                statusAlign : 'right'
            }]
        });
        
        me. bbar= new Ext.PagingToolbar({
            pageSize    	: 10,
            store       	: Fsl.app.getRelStore('S01I008001'),
            displayInfo 	: true
        });
            
        me.columns = [{
            text      : 'SN',
            xtype     : 'rownumberer',
            width     :  40, 
            sortable  :  false
        },{
            text        : 'ID',
            dataIndex   : 'id',
            width       :  40, 
            sortable    : false
        },{
            text        : 'Start Date',
            dataIndex   : 'startDate',
            flex        : 1,
            renderer    : this.getFormatDate
        },{
            text        : 'End Date',
            dataIndex   : 'endDate',
            renderer    :  this.getFormatDate
        },{
            text        : 'Percent',
            dataIndex   : 'vatPercent',
            renderer    :  this.getPercent,
            align       : 'right',
            width       :  60 
        },{           
            xtype           : 'actioncolumn',
            renderer: function (val, metadata, record) {
                if (record.raw.flag != true) {
                    this.items[0].icon = '';
                    this.items[0].tooltip = '';
                    this.disable();
                } else if(record.raw.flag == true){
                    this.items[0].icon = Fsl.route.getImage('DEL01005.png');
                    this.items[0].tooltip = 'Delete ?';
                    metadata.style = 'cursor: pointer;';
                    this.enable(); 
                }                
                return val;
            },
            width           : 22,
            align           : 'center',
            sortable        : false,
            items           : [{               
                scope       : this,
                handler     : function(grid, rowIdx, colIdx) {
                    var record      = grid.getStore().getAt(rowIdx);
                    var grossVatId  = record.data.id;
                    Ext.getCmp('WestPanel').confirmDelete(grossVatId, 'M01I008001', 'S01I008001', 'v01i008001x02');
                }
            }]
        }];       
        this.callParent(arguments);
    },
    getFormatDate :function(value, metadata, record, rowIdx, colIdx, store, vie){ 
        return Ext.Date.format(value, 'M d, Y'); //value ? value.dateFormat('Y-M-d') : '';
    },
    getPercent    : function(value, metadata, record, rowIdx, colIdx, store, vie){
        return value +'%';
    }
});