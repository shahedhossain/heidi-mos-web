Ext.define(Fsl.app.getAbsView('V01T009001X06'), {
    extend    : 'Ext.grid.Panel',
    alias     : Fsl.app.getAlias('V01T009001X06'),
    id        : 'V01T009001X06'.toLowerCase(),
    border    : true,
    modal     : true,
    width     : 500,
    height    : 285,
    viewConfig: {
        stripeRows    : true,
        forceFit      : true,
        emptyText     : 'No Records to display',
        listeners     : {
            viewready : function(v) { 
                var store = Fsl.store.getStore('S01I015001');
                store.clearFilter();
                store.loadPage(1);
            }
        }
    },
    initComponent: function() {
       var me     	= this;
       me.store   	= Fsl.app.getRelStore('S01I015001');                   
       me.tbar = Ext.create('Ext.ux.StatusBar', {            
            topBorder           : false,
            statusAlign         : 'right',
            items     : [{
                xtype       : 'button',
                text        : 'Clear',
                icon        : Fsl.route.getIcon('IC01001/CAN01003.png'),
                action      : 'clear'
            },'-',{
                xtype       : 'button',
                text        : 'search',
                icon        : Fsl.route.getIcon('IC01001/search.png'),
                action      : 'search'
            },'-',{  
                xtype       : 'statusbar',
                id          : 'win-statusbar-v01t009001x06',
                border      : false,
                defaultText : '',
                statusAlign : 'right'
            }]
        });
        
        this.bbar = new Ext.PagingToolbar({
            pageSize        :  20,
            store           : Fsl.app.getRelStore('S01I015001'),
            displayInfo     :  true
        }); 
       
         
        this.columns = [{
            text            : 'SN',
            xtype           : 'rownumberer',
            width           :  40, 
            sortable        :  false
        },{
            text            : 'SN',
            dataIndex       : 'id',
            width           :  40, 
            sortable        :  false
        },{
            text            : 'Branch name',
            dataIndex       : 'name'
        },{
            text            : 'Location',
            dataIndex       : 'location'
        },{
            text            : 'Address',
            dataIndex       : 'address',
            flex            : 1
        }];       
        this.callParent(arguments);
    },    
    getTitle :function(value, metadata, record, rowIdx, colIdx, store, vie) {
              return record.getM01i007001().data.name;
    }
});