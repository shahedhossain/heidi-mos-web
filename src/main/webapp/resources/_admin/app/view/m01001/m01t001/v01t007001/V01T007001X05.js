//Ext.grid.GridSummary = function(config){
//    Ext.apply(this, config);
//};
Ext.define(Fsl.app.getAbsView('V01T007001X05'), {
    extend    : 'Ext.grid.Panel',
    alias     : Fsl.app.getAlias('V01T007001X05'),
    id        : 'V01T007001X05'.toLowerCase(),
    border    : true,
    modal     : true,
    height    : 230,
    width     : 560, 
    viewConfig: {
        stripeRows    : true,
        forceFit      : true,
        loading       : true,
        emptyText     : 'No Records to display',
        listeners     : {
            viewready : function(v) {               
                //var store = Fsl.store.getStore('S01T008003');
                //store.clearFilter();
                //store.loadPage(1);
                var myMask = new Ext.LoadMask('v01t007001x05', {
                    msg:"Please wait..."
                });
                myMask.show();
            }
        }
    },
    //    columnLines     : true,
    //    rowLines     : true,
    features        : [{
        ftype       : 'summary'       
    }],
    initComponent   : function() {
        var me     	= this;
        me.store   	= Fsl.app.getRelStore('S01T008003');
        
        me.columns  = [{
            text      	: 'SN',
            xtype     	: 'rownumberer',
            width     	:  40, 
            sortable  	:  false
        },{
            text        : 'ID',
            dataIndex   : 'id',
            width       :  60, 
            sortable    :  false,
            hidden      :  true
        },{
            text        : 'Article',
            dataIndex   : 'articleId',            
            renderer    : this.getArticleName,
            flex        : 1,
            field: {
                xtype   : 'numberfield'
            }
        },{
            text        : 'Rate',
            dataIndex   : 'priceId',
            align       : 'right',
            width       : 70,
            renderer    : this.getPrice,
            field: {
                xtype   : 'numberfield'
            }
        },{
            text        : 'S.QTY',
            dataIndex   : 'quantity',
            align       : 'right',
            width       : 70
        },{
            text        : 'Vat',
            dataIndex   : 'vatId',
            align       : 'right',
            width       : 50,
            renderer    : this.getVat
        },{
            text        : 'Disc',
            dataIndex   : 'discountId',
            align       : 'right',
            width       : 50,
            renderer    : this.getDiscount
        },{
            header      : 'SubTotal',
            align       : 'right',
            width       : 80,
            dataIndex   : 'total',        
            renderer    : function(value, metaData, record, rowIdx, colIdx, store, view) {                
                var subtotal  = Ext.util.Format.number(value,'0.00');         
                return subtotal+'&nbsp' + '৳';           
            },
            summaryType: 'sum',
            summaryRenderer: function(value, summaryData, dataIndex) {
                var downform   = Ext.getCmp('v01t007001x06').getForm();
                downform.findField('total').setValue(value);
                
                var upform   = Ext.getCmp('v01t007001x04').getForm(),
                invoiceId =upform.findField('invoiceId').getValue();

                var subTotal    = value, 
                
                vatPercent      = downform.findField('vatPercent').getValue(),  
                rebatePercent   = downform.findField('rebatePercent').getValue(),
                purchaseTarget  = downform.findField('purchaseTarget').getValue(),
                promotionPercent= downform.findField('percent').getValue();                
                
                var reb         = parseFloat(subTotal * parseFloat(rebatePercent/100)),
                promotion       = parseFloat(subTotal * parseFloat(promotionPercent/100)),
                total           = Math.round(subTotal-reb-promotion),
                gvat            = Math.round(total * parseFloat(vatPercent/100));

                var grossTotal
                if(subTotal >= purchaseTarget){
                    grossTotal  = parseFloat(total+gvat);
                    downform.findField('rebate').setValue(Ext.util.Format.number(reb,'0.00'));         
                    downform.findField('grossVat').setValue(Ext.util.Format.number(gvat,'0.00')); 
                    downform.findField('gtotal').setValue(Math.round(grossTotal)); 
                    var payAmt      = downform.findField('payAmount').getValue();
                    var returnAmt   = Math.round(payAmt) - Math.round(grossTotal);
                    downform.findField('return').setValue(returnAmt);
                }else{
                    grossTotal  = parseFloat(total+gvat);
                    downform.findField('rebate').setValue(Ext.util.Format.number('0.00'));         
                    downform.findField('grossVat').setValue(Ext.util.Format.number(gvat,'0.00')); 
                    downform.findField('gtotal').setValue(Math.round(grossTotal));
                    var payAmt      = downform.findField('payAmount').getValue();
                    var returnAmt   = Math.round(payAmt) - Math.round(grossTotal);
                    downform.findField('return').setValue(returnAmt);
                } 
                return Ext.String.format('{0} Tk{1}', value, value !== 1 ? '' : ''); 
            }
        },{
            menuDisabled    : true,
            sortable        : false,
            xtype           : 'actioncolumn',            
            width           : 22,            
            items           : [{
                icon        : Fsl.route.getImage('DEL01005.png'),
                tooltip     : 'Delete This?',
                scope       : this,
                handler     : function(grid, rowIdx, colIdx) {                                      
                    var record    = grid.getStore().getAt(rowIdx);                     
                    var articleId = record.getM01i006001().data.id;    
                    Ext.getCmp('WestPanel').confirmDelete(articleId, 'M01T008001', 'S01T008003', 'v01t007001x04');
                }
            }]
        }];       
        this.callParent(arguments);
    },    
    getArticleName :function(value, metadata, record, rowIdx, colIdx, store, vie) {
        return record.getM01i006001().data.name;
    }, 
    getVendorName :function(value, metadata, record, rowIdx, colIdx, store, vie) {
        return record.getM01i007001().data.name;
    }, 
    getBranchName :function(value, metadata, record, rowIdx, colIdx, store, vie) {
        return record.getM01i015001().data.name;
    },
    getFormatDate :function(value, metadata, record, rowIdx, colIdx, store, vie){
        return Ext.Date.format(value, 'M d, Y')
    },    
    getPrice :function(value, metaData, record, rowIdx, colIdx, store, view){
        var value=record.getM01i012001().data.price +'&nbsp';
        return value+'৳';
    },
    getVat : function(value, metaData, record, rowIdx, colIdx, store, view){
        var value=record.getM01i013001().data.vat +'&nbsp';
        return value+'%';
    },
    getDiscount :function(value, metaData, record, rowIdx, colIdx, store, view){
        var value=record.getM01t010001().data.discount +'&nbsp';
        return value+'%';
    }
});