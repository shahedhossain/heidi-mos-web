Ext.define(Fsl.app.getAbsView('V01I009001'), {
    extend      : 'Ext.window.Window',
    alias       : Fsl.app.getAlias('V01I009001'),
    id          : 'V01I009001'.toLowerCase(),
    title       : 'REBATE LVM :: V01I009001',
    icon        : Fsl.route.getImage('APP01009.png'),
    minimizable : false,
    maximizable : false,
    autoShow    : true,
    resizable   : false,
    modal       : true,
    
      layout    : {
            type: 'vbox',
            align: 'stretch'
        },
    items  : [{ 
        xtype       :  Fsl.app.getXtype('V01I009001X01'),
        resizable   :  false,
        border      :  false,
        componentCls: 'my-border-bottom'
    },{ 
        xtype       :  Fsl.app.getXtype('V01I009001X02'),
        resizable   :  false,
        border      :  false
    }]
});