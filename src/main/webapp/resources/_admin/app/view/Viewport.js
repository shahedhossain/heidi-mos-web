Ext.define('Fsl.admin.view.Viewport', {
    extend       : 'Ext.Viewport',
    requires     : [            
        'Fsl.admin.view.WestPanel',
		'Fsl.admin.view.CenterPanel'      
    ],
	layout       : {
		type : 'border'
    },
    defaults     : {
		split: true
    }, 
    initComponent: function(config) {
        var me = this;        
		me.items = [{
			region   : 'north',
			bodyCls  : 'fsl-banner-bg',
			html     : '<span class="fsl-banner-text">Formative Soft Ltd </span>&nbsp <span class="fsl-copyright">&copy 2013 All Right Reserved</span>',                 
			bodyStyle: {
				background : 'body',
				padding    : '5px'
			},
			height   : 35,
			margin   : '0 0 0 0',
			border   : false
		},
		{                   
			region   : 'center',
			xtype    : 'centerPanel',
			margin   : '0 5 4 0'
		},
		{
			region   : 'west',
			xtype    : 'westPanel',
			split    : true,
			collapsible :true,
			collapseMode:'mini',
			width    : 400,
			margin   : '0 0 4 5'
		}]; 
        me.callParent(arguments);
    }
});