Ext.define(Fsl.app.getAbsView('V04J002001X07'), {
    extend      : 'Ext.window.Window',
    alias       : Fsl.app.getAlias('V04J002001X07'),
    icon        : Fsl.route.getImage('APP01009.png'),
    id          : 'V04J002001X07'.toLowerCase(),
    title       : 'ROLE LVM :: V04J002001X07',
    minimizable :  false,
    maximizable :  false,
    autoShow    :  true,
    resizable   :  false,
    modal       :  true,
    width       :  550,
    height      :  375, 
    layout      :  {
        type	: 'vbox',
        align	: 'stretch'
    },
    items  : [{ 
        xtype     	: Fsl.app.getXtype('V04J002001X08'), 
        resizable 	: false,
        border    	: false,
        componentCls    : 'my-border-bottom'
    },{ 
        xtype     	: Fsl.app.getXtype('V04J002001X09'),       
        resizable 	: false,
        border    	: false
    }]
});
