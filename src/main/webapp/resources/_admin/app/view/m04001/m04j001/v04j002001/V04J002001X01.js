Ext.define(Fsl.app.getAbsView('V04J002001X01'), {
    extend      	: 'Ext.form.Panel',
    alias     		: Fsl.app.getAlias('V04J002001X01'),
    id          	: 'V04J002001X01'.toLowerCase(), 
    bodyStyle : {
        background: 'none'                
    },
    initComponent : function(){
        var me      = this;     
        this.items  =[{
            defaultType   : 'textfield',
            layout        : 'hbox',
            flex          : 1,
            border        : false,
            fieldDefaults : {
                anchor      : '100%',
                labelAlign  : 'right'               
            },
            bodyStyle     : {
                background  : 'none'                
            },
            items   : [{
                xtype             : 'fieldcontainer',
                layout            : 'vbox',
                margin            : '5 0 0 5',                        
                defaults          :  {
                    width           :  150,
                    labelAlign      : 'left',
                    labelWidth      :  55
                }, 
                items : [{
                    xtype             : 'numberfield',
                    name              : 'id',
                    fieldLabel        : 'ID',
                    minValue          :  1,                       
                    mouseWheelEnabled :  false,
                    hideTrigger       :  true,
                    decimalPrecision  :  0,
                    keyNavEnabled     :  false                        
                },{
                    xtype             : 'textfield',
                    name              : 'authority',
                    fieldLabel        : 'ROLE:'
                }]
            }]
        }];
        this.callParent(arguments);
    }
});

