﻿Ext.define(Fsl.app.getAbsView('V04J002001X06'), {
    extend    : 'Ext.grid.Panel',
    alias     : Fsl.app.getAlias('V04J002001X06'),
    id        : 'V04J002001X06'.toLowerCase(),
    border    : true,
    modal     : true,
    height    : 285,
    width     : 300,
    viewConfig: {
        stripeRows    : true,
        forceFit      : true,
        emptyText     : 'No Records to display',
        listeners     : {
            viewready : function(gridpanel, eOpts){                  
                var store = Fsl.store.getStore('S04T001003');
                store.clearFilter();
                store.loadPage(1);
            }
        }
    },
    initComponent: function() {
        var me     	= this;
        me.store   	= Fsl.app.getRelStore('S04T001003');        
        me.tbar         = Ext.create('Ext.ux.StatusBar', {            
            topBorder       : false,
            statusAlign     : 'right',
            items    : [{
                xtype       :'button',
                text        : 'Clear',
                icon        : Fsl.route.getIcon('IC01001/CAN01003.png'),
                action      : 'clear'
            },'-',{
                xtype       : 'button',
                text        : 'search',
                icon        : Fsl.route.getIcon('IC01001/search.png'),
                action      : 'search'
            },'-',{  
                xtype       :'statusbar',
                id          : 'win-statusbar-v04j002001x06',
                border      : false,
                defaultText : '',
                statusAlign : 'right'
            }]
        });
		
        me.bbar = new Ext.PagingToolbar({
            pageSize    : 10,
            id          : 'paging-v04j002001x06',
            store       : Fsl.app.getRelStore('S04T001003'),
            displayInfo : true          
        });
        me.columns = [{
            text      		: 'SN',
            xtype     		: 'rownumberer',
            width     		:  40, 
            sortable  		:  false
        },{
            text        	: 'ID',
            dataIndex   	: 'id',
            width       	:  40, 
            sortable    	:  false
        },{
            text        	: 'USER ID',
            dataIndex   	: 'userId',
            width       	:  40, 
            sortable    	:  false
        },{
            text        	: 'Category',
            dataIndex   	: 'username',
            flex        	:  1
        },{
            text        	: 'Enable/Dis.',
            dataIndex   	: 'enabled',
            align               : 'center',
            tdCls               : 'fsl-cursor-pointer',
            flex        	:  1,
            renderer            : this.getIcon
        },{
            text        	: 'Acc. Expire',
            dataIndex   	: 'accountExpired',
            align               : 'center',
            tdCls               : 'fsl-cursor-pointer',
            flex        	:  1,
            renderer            : this.getIcon
        },{
            text        	: 'Lock',
            dataIndex   	: 'accountLocked',
            align               : 'center',
            tdCls               : 'fsl-cursor-pointer',
            flex        	:  1,
            renderer            : this.getIcon
        },{
            text        	: 'Pass Expire',
            dataIndex   	: 'passwordExpired',
            align               : 'center',
            tdCls               : 'fsl-cursor-pointer',
            flex        	:  1,
            renderer            : this.getIcon
        },{
            text        	: 'Branch',
            dataIndex   	: 'branch',
            tdCls               : 'fsl-cursor-pointer',
            flex        	:  1,
            renderer            : this.getBranch
        }]; 
        me.callParent(arguments);
    },
    getIcon : function (value, meta, record, rowIndex, colIndex, store) {
        var src;
        if(value== true){
            src = Fsl.route.getIcon("lock_icon.gif");                    
        }else if(value==false){
            src = Fsl.route.getIcon("unlock_icon.gif");
        }
        return '<img src ="'+ src +'" />';
    },
    getBranch : function(value, meta, record, rowIndex, colIndex, store){
        return record.getM01t009001().getM01i015001().data.name
    }
});