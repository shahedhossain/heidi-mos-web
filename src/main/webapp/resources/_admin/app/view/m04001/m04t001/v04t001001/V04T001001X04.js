Ext.define(Fsl.app.getAbsView('V04T001001X04'), {
    extend      : 'Ext.window.Window',
    alias       : Fsl.app.getAlias('V04T001001X04'),
    icon        : Fsl.route.getImage('APP01009.png'),
    id          : 'V04T001001X04'.toLowerCase(),
    title       : 'USER LVM :: V04T001001X04',
    minimizable : false,
    maximizable : false,
    autoShow    : true,
    resizable   : false,
    modal       : true,
      layout    : {
            type: 'vbox',
            align: 'stretch'
        },
    items  : [{ 
        xtype     	: Fsl.app.getXtype('V04T001001X05'), 
        resizable 	: false,
        border    	: false,
        componentCls    : 'my-border-bottom'
    },{ 
        xtype     	: Fsl.app.getXtype('V04T001001X06'),       
        resizable 	: false,
        border    	: false
    }]
});