Ext.define(Fsl.app.getAbsView('V04T001001'), {
    extend      : 'Ext.window.Window',
    alias       : Fsl.app.getAlias('V04T001001'),
    icon        : Fsl.route.getImage('APP01009.png'),
    id          : 'V04T001001'.toLowerCase(),
    title       : 'ACCOUNT LVM :: V04T001001',
    minimizable :  false,
    maximizable :  false,
    autoShow    :  true,
    resizable   :  false,
    modal       :  true,
    width       :  630,
    height      :  405, 
    layout      :  {
        type	: 'vbox',
        align	: 'stretch'
    },
    items  : [{ 
        xtype     	: Fsl.app.getXtype('V04T001001X01'), 
        resizable 	: false,
        border    	: false,
        componentCls    : 'my-border-bottom'
    },{ 
        xtype     	: Fsl.app.getXtype('V04T001001X02'),       
        resizable 	: false,
        border    	: false
    }]
});