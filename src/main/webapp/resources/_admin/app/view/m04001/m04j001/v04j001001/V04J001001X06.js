﻿Ext.define(Fsl.app.getAbsView('V04J001001X06'), {
    extend    : 'Ext.grid.Panel',
    alias     : Fsl.app.getAlias('V04J001001X06'),
    id        : 'V04J001001X06'.toLowerCase(),
    border    : true,
    modal     : true,
    height    : 285,
    viewConfig: {
        stripeRows    : true,
        forceFit      : true,
        emptyText     : 'No Records to display',
        listeners     : {
            viewready : function(gridpanel, eOpts){                  
                var store = Fsl.store.getStore('S04T001002');
                store.clearFilter();
                store.loadPage(1);
            //this.getSelectionModel().select(0);
            }
        }
    },
    initComponent: function() {
        var me     	= this;
        me.store   	= Fsl.app.getRelStore('S04T001002');        
        me.tbar         = Ext.create('Ext.ux.StatusBar', {            
            topBorder       : false,
            statusAlign     : 'right',
            items    : [{
                xtype       :'button',
                text        : 'ADD',
                icon        : Fsl.route.getIcon('TB01001/ADD01004.png'),
                action      : 'add'
            },'-',{
                xtype       :'button',
                text        : 'Clear',
                icon        : Fsl.route.getIcon('IC01001/CAN01003.png'),
                action      : 'clear'
            },'-',{
                xtype       : 'button',
                text        : 'search',
                icon        : Fsl.route.getIcon('IC01001/search.png'),
                action      : 'search'
            },'-',{  
                xtype       :'statusbar',
                id          : 'win-statusbar-v04t003001x06',
                border      : false,
                defaultText : '',
                statusAlign : 'right'
            }]
        });
		
        me.bbar = new Ext.PagingToolbar({
            pageSize    : 10,
            id          : 'paging-v04j001001x06',
            store       : Fsl.app.getRelStore('S04T001002'),
            displayInfo : true          
        });
        me.columns = [{
            text      		: 'SN',
            xtype     		: 'rownumberer',
            width     		:  40, 
            sortable  		:  false
        },{
            text        	: 'ID',
            dataIndex   	: 'id',
            width       	:  40, 
            sortable    	:  false
        },{
            text        	: 'USER ID',
            dataIndex   	: 'userId',
            width       	:  60, 
            sortable    	:  false
        },{
            text        	: 'Category',
            dataIndex   	: 'username',
            flex        	:  1
        },{
            text        	: 'Enable/Dis.',
            dataIndex   	: 'enabled',
            tdCls               : 'fsl-cursor-pointer',
            flex        	:  1,
            renderer            : this.getIcon
        },{
            text        	: 'Acc. Expire',
            dataIndex   	: 'accountExpired',
            tdCls               : 'fsl-cursor-pointer',
            flex        	:  1,
            renderer            : this.getIcon
        },{
            text        	: 'Lock',
            dataIndex   	: 'accountLocked',
            tdCls               : 'fsl-cursor-pointer',
            flex        	:  1,
            renderer            : this.getIcon
        },{
            text        	: 'Pass Expire',
            dataIndex   	: 'passwordExpired',
            tdCls               : 'fsl-cursor-pointer',
            flex        	:  1,
            renderer            : this.getIcon
        },{
            text        	: 'Branch',
            dataIndex   	: 'branch',
            tdCls               : 'fsl-cursor-pointer',
            flex        	:  1,
            renderer            : this.getBranch
        }]; 
        me.callParent(arguments);
    },
    
    getIcon : function (value, meta, record, rowIndex, colIndex, store) {
        var src;
        if(value== true){
            src = Fsl.route.getIcon("lock_icon.gif");                    
        }else if(value==false){
            src = Fsl.route.getIcon("unlock_icon.gif");
        }
        return '<img src ="'+ src +'" />';
    },
    getBranch : function(value, meta, record, rowIndex, colIndex, store){
        return record.getM01t009001().getM01i015001().data.name
    }
});