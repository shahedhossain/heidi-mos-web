Ext.define(Fsl.app.getAbsController('C04T001001'), {
    extend      : 'Ext.app.Controller', 
    requires    : Fsl.app.getMergedRequires({
        apps: [],
        misc: []
    }),  
    views       : Fsl.view.getViews(['V04T001001', 'V04T001001X01:07']),
    
    refs        : [{
            ref       : 'activeWindow',
            selector  : 'window'
        }],
    init: function() {
        var me = this;
        me.control({
            'v04t001001x02 button[action=add]':{
                click  :  function (field, el) {
                    Ext.widget('v04t001001x03')//.show()
                }
            },
            'v04t001001x01 *':{
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        this.filter(field, el, me)
                    }
                }
            },
            'v04t001001x02 button[action=clear]':{
                click  :  function (field, el) {
                    field.up('window').down('form').getForm().reset();
                    Fsl.store.getStore('S01T004001').load();
                }
            },
            'v04t001001x02 button[action=search]':{
                click  :  function (field, el) {
                    this.filter(field, el, me)
                }
            },
            
            'v04t001001x03 button[action=save]': {
                click           : this.onSave
            },
            'v04t001001x02':{
                itemdblclick  :  this.preUpdate
            },
            'v04t001001x07 button[action=update]':{
                click  :  this.onUpdate
            },
            'v04t001001x06 button[action=search]':{
                click  :  function (field, el) {
                    this.userFilter(field, el, me)
                }
            },
            'v04t001001x06 button[action=clear]':{
                click  :  function (field, el) {
                    field.up('window').down('form').getForm().reset();
                    Fsl.store.getStore('S01T009003').load();
                }
            },
            'v04t001001x05 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        this.userFilter(field, el, me)
                    }
                }
            },
            'v04t001001x03 textfield[name=name]':{
                afterrender: function(c) {
                    c.inputEl.on('dblclick', function() {
                        Ext.widget('v04t001001x04')
                    });
                }
            },
            'v04t001001x06':{
                itemdblclick:me.setUser
            }
        });
    },
    onSave            : function(button){
        var win = button.up('window'),
        form    = win.down('form'),
        record  = form.getRecord(),
        values  = form.getValues(),
        status  = 'win-statusbar-v04t001001x02',
        statusView  = 'win-statusbar-v04t001001x03';                    

        if (form.getForm().isValid()) { 
            if (form.getForm().isDirty()) { 
                Fsl.model.save('M04T001001', values, {
                    scope   : this,
                    success : function(model, operation) {
                        form.loadRecord(model);
                        Fsl.store.getStore('S04T001001').load();
                        Fsl.severity.info(status, operation);
                        Fsl.severity.info(statusView, operation);
                    },
                    failure : function(model, operation){
                        Fsl.severity.error(status, operation);
                        Fsl.severity.error(statusView, operation);
                        Fsl.security.recheck(operation);
                    }
                });
            } else {
                var message  = 'No change occured';
                Fsl.severity.warn(status, message);
                Fsl.severity.warn(statusView, message);
            }
        } else {
            var message  = 'Necessary Field Required';
            Fsl.severity.warn(status, message);
            Fsl.severity.warn(statusView, message);
        }     
    },     
    preUpdate : function (model, records) {        
        var win = Ext.widget('v04t001001x07'),
        form    = win.down('form').loadRecord(records);
        form.findField('name').setValue(records.getM01t009001().data.name);
        if(records.data.enabled == true){
           // form.findField('enabled').setValue(true);
           Ext.getCmp('checkbox1').setValue(true);
        }
        if(records.data.accountLocked == true){
            //form.findField('accountLocked').setValue(true);
            Ext.getCmp('checkbox2').setValue(true);
        }

    },
    onUpdate : function(button){
        var win = button.up('window'),
        form    = win.down('form'),
        record  = form.getRecord(),
        values  = form.getValues(),
        status  = 'win-statusbar-v04t001001x02',
        statusView  = 'win-statusbar-v04t001001x07';                    
        if (form.getForm().isValid()) { 
            if (form.getForm().isDirty()) { 
                Fsl.model.save('M04T001001', values, {
                    scope   : this,
                    success : function(model, operation) {
                        form.loadRecord(model);
                        Fsl.store.getStore('S04T001001').load();
                        Fsl.severity.info(status, operation);
                        Fsl.severity.info(statusView, operation);
                    },
                    failure : function(model, operation){
                        Fsl.severity.error(status, operation);
                        Fsl.severity.error(statusView, operation);
                        Fsl.security.recheck(operation);
                    }
                });
            } else {
                var message  = 'No change occured';
                Fsl.severity.warn(status, message);
                Fsl.severity.warn(statusView, message);
            }
        } else {
            var message  = 'Necessary Field Required';
            Fsl.severity.warn(status, message);
            Fsl.severity.warn(statusView, message);
        }  
    },
    filter:function(field, el, me){    
        var form          =   field.up('window').down('form').getForm();
        if(form.isValid()){
            var idValue     =   form.findField('id').getValue(),
            userId          =   form.findField('userId').getValue(),
            username        =   form.findField('username').getValue(),
            enabled         =   form.findField('enabled').getValue(),
            accountExpired  =   form.findField('accountExpired').getValue(),
            accountLocked   =   form.findField('accountLocked').getValue(),
            passwordExpired =   form.findField('passwordExpired').getValue(),
            filter          =   Fsl.store.getFilter('S04T001001');
            filter.add('id', idValue).add('userId', userId).add('username', username).add('enabled', enabled).add('accountExpired', accountExpired).add('accountLocked', accountLocked).add('passwordExpired', passwordExpired).load(1);
        }
    },    
    userFilter:function(field, el, me){    
        var form          =   field.up('window').down('form').getForm();
        if(form.isValid()){
            var idValue     =   form.findField('id').getValue(),
            contact         =   form.findField('contact').getValue(),
            name            =   form.findField('name').getValue(),
            branchId        =   form.findField('branchId').getValue(),
            branchName      =   form.findField('branchName').getValue(),
            location        =   form.findField('location').getValue(),
            filter          =   Fsl.store.getFilter('S01T009003');
            filter.add('id', idValue).add('name', name).add('contact', contact).add('branchId', branchId).add('branchName', branchName).add('location', location).load(1);
        }
    },
    setUser : function(view, records){
        var form = Ext.getCmp('v04t001001x03').down('form').getForm();
        form.findField('userId').setValue(records.data.id);
        form.findField('name').setValue(records.data.name);
        view.up('window').close();
    }
});
