Ext.define(Fsl.app.getAbsController('C01I013001'), {
    extend      : 'Ext.app.Controller', 
    requires    : Fsl.app.getMergedRequires({
        apps: [],
        misc: []
    }),  
    views       : Fsl.view.getViews(['V01I013001', 'V01I013001X01:06']),
    models      : Fsl.model.getModels(['M01I013001:2']),
    stores      : Fsl.store.getStores(['S01I013001:2']),
    refs        : [{
        ref       : 'activeWindow',
        selector  : 'window'
    }],
    init: function() {
        var me = this;
        me.control({
            'v01i013001x01 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        me.filter(field, el,me);
                    }
                }
            },
            'v01i013001x02 button[action=add]':{
                click  :  function (field, el) {                      
                    var win = Ext.widget('v01i013001x03');
                    var form = win.down('form').getForm();
                    form.findField('date').setMinValue(new Date());                 
                }
            },
            'v01i013001x02 button[action=search]':{
                click  :  function (field, el) {
                    me.filter(field, el,me);
                }
            },
            'v01i013001x02 button[action=clear]':{
                click  :  function (field, el) {
                    field.up('window').down('form').getForm().reset();
                    Fsl.store.getStore('S01I013001').loadPage(1);
                }
            },
            'v01i013001x02' : {
                itemdblclick:this.preUpdate
            },
            'v01i013001x03 button[action = save]':{
                click:this.onSave
            },
            'v01i013001x03 textfield[name=articleName]':{
                afterrender: function(c) {
                    c.inputEl.on('dblclick', function() {
                        Ext.widget('v01i013001x04').show(); 
                    });
                }
            },
            'v01i013001x05 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        me.articleFilterForVat(field, el, me);
                    }
                }
            },
            'v01i013001x06 button[action=search]':{
                click  :  function (field, el) {
                    me.articleFilterForVat(field, el, me);
                }
            },
            'v01i013001x06 button[action=clear]':{
                click  :  function (field, el) {
                    field.up('window').down('form').getForm().reset();
                    Fsl.store.getStore('S01I006005').loadPage(1);
                }
            },
            'v01i013001x06':{
                itemdblclick:this.setArticleIntoVat      
            }
        });
    },
    setArticleIntoVat :function(view, records){
        var form = Ext.getCmp('v01i013001x03').down('form').getForm();  
        form.findField('articleId').setValue(records.data.id);
        form.findField('articleName').setValue(records.data.name);
        view.up('window').close();
    },
       
     preUpdate : function(model, records){
        var win     =  Ext.widget('v01i013001x03'),
        frm         =  win.down('form'),
        form        =  frm.getForm();
        win.setTitle('VAT UPDATE');
        form.loadRecord(records);
        form.findField('articleName').setValue(records.getM01i006001().data.name);
        form.findField('date').setReadOnly(false);
        form.findField('date').setMinValue(records.data.date);    
        var btn         = win.down('button[text=Save]');
        btn.setText('Update');
        btn.setIcon(Fsl.route.getIcon('TB01001/EDT01004.png'));
    },
    onSave   : function(button){
        var win     = button.up('window'),
        form        = win.down('form'),
        record      = form.getRecord(),
        values      = form.getValues(),
        status      = 'win-statusbar-v01i013001x02',
        statusView  = 'win-statusbar-v01i013001x03';                    

        if (form.getForm().isValid()) { 
            if (form.getForm().isDirty()) { 
                Fsl.model.save('M01I013001', values, {
                    scope   : this,
                    success : function(model, operation) {
                        form.loadRecord(model);
                        Fsl.severity.info(status, operation);
                        Fsl.severity.info(statusView, operation);
                        Fsl.store.getStore('S01I013001').load();
                    },
                    failure : function(model, operation){
                        Fsl.severity.error(status, operation);
                        Fsl.severity.error(statusView, operation);
                        Fsl.security.recheck(operation);
                    }
                });
            } else {
                var message  = 'No change occured';
                Fsl.severity.warn(status, message);
                Fsl.severity.warn(statusView, message);
            }
        } else {
            var message  = 'Necessary Field Required';
            Fsl.severity.warn(status, message);
            Fsl.severity.warn(statusView, message);
        }     
    },     
    filter:function(field, el,me){    
        var form          =   field.up('window').down('form').getForm();
        if(form.isValid()){
            var idValue     =   form.findField('id').getValue(),
            articleId       =   form.findField('articleId').getValue(),
            date            =   form.findField('date').getValue(),
            vat             =   form.findField('vat').getValue(),
            articleName     =   form.findField('articleName').getValue(),
            mbcode          =   form.findField('mbcode').getValue(),
            pbcode          =   form.findField('pbcode').getValue(),                        
            filter          =   Fsl.store.getFilter('S01I013001');
            filter.add('id', idValue).add('articleId', articleId).add('date', date).add('vat', vat).add('articleName', articleName).add('mbcode', mbcode).add('pbcode', pbcode).load(1);
        }
    },   
    articleFilterForVat:function(field, el, me){    
        var form            =   field.up('window').down('form').getForm();
        if(form.isValid()){
            var idValue     =   form.findField('id').getValue(),
            name            =   form.findField('name').getValue(),
            mbcode          =   form.findField('mbcode').getValue(),
            pbcode          =   form.findField('pbcode').getValue(),                        
            filter          =   Fsl.store.getFilter('S01I006005');
            filter.add('id', idValue).add('name', name).add('mbcode', mbcode).add('pbcode', pbcode).add('pbcode', pbcode).load(1);
         }
    }   
});