Ext.define(Fsl.app.getAbsController('C01I010001'), {
    extend      : 'Ext.app.Controller', 
    requires    : Fsl.app.getMergedRequires({
        apps: [],
        misc: []
    }),  
    views       : Fsl.view.getViews(['V01I010001', 'V01I010001X01:04']),
    models      : Fsl.model.getModels(['M01I010001']),
    stores      : Fsl.store.getStores(['S01I010001:2']),
    refs        : [{
        ref       : 'activeWindow',
        selector  : 'window'
    }],
    init: function() {
        var me = this;
        me.control({
            'v01i010001x01 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        me.filter(field, el, me);
                    }
                }
            },
            'v01i010001x03 button[action=save]':{
                click       : me.onSave
            },
            'v01i010001x04 button[action=save]':{
                click       : me.onSave
            },
            'v01i010001x02' : {
                itemclick   : me.infoOnStatus,
                itemdblclick: me.preUpdate
            },
            'v01i010001x02 button[action=add]':{
                click  :  function (field, el) {
                    Ext.widget('v01i010001x03').show(); 
                }
            },
            'v01i010001x02 button[action=search]':{
                click  :  function (field, el) {
                    me.filter(field, el, me);
                }
            },
            'v01i010001x02 button[action=clear]':{
                click  :  function (field, el) {
                    field.up('window').down('form').getForm().reset();
                    Fsl.store.getStore('S01I010001').load();
                }
            }   
        });
    }, 
    preUpdate:function(model, records){
        var view   = Fsl.app.getWidget('V01I010001X04');  
        view.setTitle('PROMOTION UPDATE');
        var form = view.down('form').getForm();
        form.loadRecord(records); 
        if(records.raw.flag == true){           
            form.findField('endDate').setReadOnly(false);
            form.findField('startDate').setReadOnly(false);
        }
        var btn    = view.down('button[text=Save]');
        btn.setText('Update');
        btn.setIcon(Fsl.route.getIcon('TB01001/EDT01004.png'));      
    },
    infoOnStatus:function(model, operation){
        var status  = 'win-statusbar-v01i010001x02';
        Fsl.severity.info(status, operation.data.name);      
    },      
    onSave       : function(button){
        var win     = button.up('window'),
        form        = win.down('form'),
        record      = form.getRecord(),
        values      = form.getValues(),
        status      = 'win-statusbar-v01i010001x02',
        statusView  = '';
        if(win.alias[0]== 'widget.v01i010001x04'){
            statusView  = 'win-statusbar-v01i010001x04';
        }else if(win.alias[0]== 'widget.v01i010001x03'){
            statusView  = 'win-statusbar-v01i010001x03';  
        }
        if (form.getForm().isValid()) { 
            if (form.getForm().isDirty()) { 
                Fsl.model.save('M01I010001', values, {
                    scope   : this,
                    success : function(model, operation) {
                        form.loadRecord(model);
                        Fsl.severity.info(status, operation);
                        Fsl.severity.info(statusView, operation);
                        Fsl.store.getStore('S01I010001').load();
                    },
                    failure : function(model, operation){
                        Fsl.severity.error(status, operation);
                        Fsl.severity.error(statusView, operation);
                        Fsl.security.recheck(operation);
                    }
                });
            } else {
                var message  = 'No change occured';
                Fsl.severity.warn(status, message);
                Fsl.severity.warn(statusView, message);
            }
        } else {
            var message  = 'Necessary Field Required';
            Fsl.severity.warn(status, message);
            Fsl.severity.warn(statusView, message);
        }     
    },    
    filter:function(field, el, me){    
        var form          =   field.up('window').down('form').getForm();//'sv01i01000104'
        if(form.isValid()){
            var idValue     =   form.findField('id').getValue(),
            startDate       =   form.findField('startDate').getValue(),
            endDate         =   form.findField('endDate').getValue(),
            amount          =   form.findField('amount').getValue(),
            percent         =   form.findField('percent').getValue(),                        
            filter          =   Fsl.store.getFilter('S01I010001');
            filter.add('id', idValue).add('startDate', startDate).add('endDate', endDate).add('amount', amount).add('percent', percent).load(1);
        }
    }
});