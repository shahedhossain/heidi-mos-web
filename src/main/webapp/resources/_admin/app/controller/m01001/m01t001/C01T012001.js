Ext.define(Fsl.app.getAbsController('C01T012001'), {
    extend      : 'Ext.app.Controller', 
    requires    : Fsl.app.getMergedRequires({
        apps: [],
        misc: []
    }),  
    views       : Fsl.view.getViews(['V01T012001','V01T012002','V01T012001X01:15']),
    refs        : [{
        ref       : 'activeWindow',
        selector  : 'window'
    }],
    init: function() { 
        var me = this;
        me.control({
            'v01t012001x01 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        me.filter(field, el, me);
                    }
                }
            },
            'v01t012001x02 button[action=search]':{
                click  :  function (field, el) {
                    me.filter(field, el, me);
                }
            },
            'v01t012001x02 button[action=clear]':{
                click  :  function (field, el) {
                    field.up('window').down('form').getForm().reset();
                    Fsl.store.getStore('S01T012002').load();
                }
            },
            'v01t012001x02'     :{
                itemdblclick    : this.getItems 
            },
            'v01t012001x05' :{
                itemdblclick    : this.getPandingQty 
            },
            'v01t012001x06 button[action=save]': {
                click           : this.updatePandingQty
            },
            'v01t012001x06 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER){
                        var button = field.up('window').down('button[action=save]');
                        button.fireEvent('click', button);                            
                    }
                }
            },  
            'v01t012001x03 button[action=clear]': {
                click           : this.clearPandingItem
            },
            'v01t012001x03 button[action=save]': {
                click           : this.savePandingItem
            },
            'v01t012001x09'     :{
                itemdblclick    :this.getReceiveQty 
            },
            'v01t012001x10 button[action=save]': {
                click       : this.updateReceiveQty
            },
            'v01t012001x10 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER){
                        var button = field.up('window').down('button[action=save]');
                        button.fireEvent('click', button);                            
                    }
                }
            },  
            'v01t012001x07 button[action=update]': {
                click       : this.updateReceiveItem
            },            
            'v01t012001x11 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        me.receiveFilter(field, el, me);
                    }
                }
            },
            'v01t012001x12':{
                itemdblclick    :this.getReadOnlyReceiveItem
            }
        });
    },
    getItems : function(view, record){
        var transferId  = record.data.id,
        receiveId       = record.data.receiveId;
        if(transferId > 0 && receiveId > 0){
            this.getReceiveItem(receiveId, record);
        }else if(transferId>0 && (receiveId == 0 || receiveId == '')){
            this.getPandingItem(transferId, record);
        }
    },
    getPandingItem :function(transferId, record){
        var view = Ext.widget('v01t012001x03',{
            animateTarget  : 'v01t012001x03'
        });
        var preform = view.down('form'),
        form        = preform.getForm();
        form.loadRecord(record);
        form.findField('transferId').setValue(record.data.id);
        var pandingStore = Fsl.store.getStore('S01T005004');
        if(transferId != null){
            if(transferId > 0){
                Fsl.model.load('M01T011003', {
                    id:transferId
                }, {                
                    scope   : this,
                    success : function(model, operations) {
                        if(model != null){
                            form.findField('branchId').setValue(model.getM01i015002().data.id);
                            form.findField('fromBranchId').setValue(model.getM01i015001().data.id);
                            form.findField('branchName').setValue(model.getM01i015002().data.name);
                            form.findField('fromBranchName').setValue(model.getM01i015001().data.name);
                            pandingStore.load();
                            Ext.getCmp('v01t012001x05').getEl().unmask();
                        }
                    },
                    failure: function(model, operation){
                        me.destroyPandingItemStore();
                        pandingStore.load();
                        Fsl.security.recheck(operation);
                    }
                });
            }
        }
    },
    getReceiveItem : function(receiveId, record){
        var view = Ext.widget('v01t012001x07',{
            animateTarget  : 'v01t012001x07'
        });
        var form = view.down('form').getForm(),
        receiveCartStore = Fsl.store.getStore('S01T006001');
        form.findField('transferId').setValue(record.data.id);
        form.findField('sendDate').setValue(record.data.entryDate);
        if(receiveId > 0){
            Fsl.model.load('M01T012001', {
                id:receiveId
            }, {  
                scope       : this,
                success     : function(record, operation) {
                    receiveCartStore.load();
                    form.findField('receiveId').setValue(record.data.id);
                    form.findField('fromBranchId').setValue(record.getM01i015002().data.id);
                    form.findField('fromBranchName').setValue(record.getM01i015002().data.name);
                    form.findField('entryDate').setValue(record.data.entryDate);
                },
                failure     : function(record, operation) {
                    //console.log("not");
                    Fsl.security.recheck(operation);
                }
            }); 
        }
    },
    destroyPandingItemStore : function(){
        var pandingStore= Fsl.store.getStore('S01T005004');
        var tcartItem  = Ext.create(Fsl.app.getAbsModel('M01T005003'), {
            id : 1
        });                  
        tcartItem.destroy({
            success: function(records, operation, success) { 
                pandingStore.load();
            },
            failure :function(model, operation){
                pandingStore.load();
                Fsl.security.recheck(operation);
            }
        });  
    },
    getPandingQty :function(model, records){
        var view  = Ext.widget('v01t012001x06'),
        form      = view.down('form');
        form.loadRecord(records);
        form.getForm().findField('articleId').setValue(records.getM01i006001().data.id);
        form.getForm().findField('articleName').setValue(records.getM01i006001().data.name);
    },
    updatePandingQty :function(button){
        var win = button.up('window'),
        form    = win.down('form'),
        record  = form.getRecord(),
        values  = form.getValues(),
        statusView  = 'win-statusbar-v01t012001x06',
        pandingItemStore = Fsl.store.getStore('S01T005004');
        
        if (form.getForm().isValid()) { 
            if (form.getForm().isDirty()) { 
                Fsl.model.save('M01T005003', {
                    articleId   :values.articleId,
                    quantity    :values.receiveQuantity            
                }, {
                    scope   :this,
                    success :function(model, operation){
                        pandingItemStore.load();
                        Fsl.severity.info(statusView, operation);
                    },
                    failure     : function(record, operation) {
                        Fsl.severity.error(statusView, operation.error);
                        Fsl.security.recheck(operation);
                    }
                });
            }else{
                var message  = 'No change occured';
                Fsl.severity.warn(statusView, message);
            }
        }else{
            var message  = 'Necessary Field Required';
            Fsl.severity.warn(statusView, message);
        }
    },
    savePandingItem : function(button){
        var win         = button.up('window'),
        form            = win.down('form'),
        record          = form.getRecord(),
        values          = form.getValues(),
        pandingItemStore= Fsl.store.getStore('S01T005004'),
        pandingStore    = Fsl.store.getStore('S01T012002');
        if(form.getForm().isValid()){  
            values.id   = values.receiveId;
            Fsl.model.save('M01T012002', values, {            
                scope   : this,
                success : function(model, operation){
                    var text    = operation.response.responseText,
                    json        = Ext.decode(text);
                    form.loadRecord(model);
                    form.getForm().findField('receiveId').setValue(model.data.id); 
                    form.getForm().findField('entryDate').setValue(model.data.entryDate); 
                    pandingItemStore.load();    
                    pandingStore.load();
                },                  
                failure:function(model, operation){ 
                    Fsl.security.recheck(operation);
                } 
            }); 
        }
    },
    getReceiveQty :function(model, records){
        var view  = Ext.widget('v01t012001x10'),
        form      = view.down('form');
        form.loadRecord(records);
        form.getForm().findField('articleId').setValue(records.getM01i006001().data.id);
        form.getForm().findField('articleName').setValue(records.getM01i006001().data.name);
    },
    updateReceiveQty :function(button){
        var win = button.up('window'),
        form    = win.down('form'),
        record  = form.getRecord(),
        values  = form.getValues(),
        statusView          = 'win-statusbar-v01t012001x10', 
        receiveCartStore    = Fsl.store.getStore('S01T006001'),
        pandingStore        = Fsl.store.getStore('S01T012002');
        
        if (form.getForm().isValid()) { 
            if (form.getForm().isDirty()) { 
                Fsl.model.save('M01T006001', {
                    articleId   :values.articleId,
                    quantity    :values.receiveQuantity            
                }, {
                    scope   :this,
                    success :function(model, operation){
                        receiveCartStore.load();
                        pandingStore.load();
                        Fsl.severity.info(statusView, operation);
                    },
                    failure     : function(record, operation) {
                        Fsl.security.recheck(operation);
                    }
                });
            }else{
                var message  = 'No change occured';
                Fsl.severity.warn(statusView, message);
            }
        }else{
            var message  = 'Necessary Field Required';
            Fsl.severity.warn(statusView, message);
        }
    },    
    updateReceiveItem :function(button){
        var win         = button.up('window'),
        form            = win.down('form'),
        record          = form.getRecord(),
        values          = form.getValues(),  
        statusView          = 'win-statusbar-v01t012001x09', 
        receiveCartStore    = Fsl.store.getStore('S01T006001'),
        pandingStore        = Fsl.store.getStore('S01T012002');
        
        if(form.getForm().isValid()){  
            values.id   = values.receiveId;
            Fsl.model.save('M01T012001', values, {
                scope   : this,
                success : function(model, operation){
                    var text    = operation.response.responseText,
                    json        = Ext.decode(text);
                    form.loadRecord(model);
                    form.getForm().findField('receiveId').setValue(model.data.id); 
                    form.getForm().findField('entryDate').setValue(model.data.entryDate); 
                    receiveCartStore.load();    
                    pandingStore.load();
                    /*console.log(json.message); (problem arise sometime message come with null) )*/
                    Fsl.severity.info(statusView, 'Successfully Updated');
                },                  
                failure:function(model, operation){ 
                    Fsl.security.recheck(operation);
                }
            });        
        }else{
            var message  = 'Necessary Field Required';
            Fsl.severity.warn(statusView, message);
        }
    },
   
    getReadOnlyReceiveItem :function(model, record){
        var win         = Ext.widget('v01t012001x13'),
        form            = win.down('form').getForm(),
        receiveCartStore= Fsl.store.getStore('S01T006001');
        form.findField('transferId').setValue(record.data.transferId);
        form.findField('sendDate').setValue(record.getM01t011001().data.entryDate);
        form.findField('receiveId').setValue(record.data.id);        
        form.findField('fromBranchId').setValue(record.data.fromBranchId);
        form.findField('fromBranchName').setValue(record.getM01i015002().data.name);
        form.findField('entryDate').setValue(record.data.entryDate);
        
        if(record.data.id){
            Fsl.model.load('M01T012001', {
                id:record.data.id
            }, {  
                scope       : this,
                success     : function(record, operation) {
                    receiveCartStore.load();
                },
                failure     : function(record, operation) {
                    Fsl.security.recheck(operation);
                }
            });
        }else if(!record.data.id){
            me.destroyReceiveCartStore();
        }
    },
    
    destroyReceiveCartStore : function(){
        var receiveCartStore= Fsl.store.getStore('S01T006001');
        var tcartItem  = Ext.create(Fsl.app.getAbsModel('M01T012001'), {
            id : 1
        });                  
        tcartItem.destroy({
            success: function(records, operation, success) { 
                receiveCartStore.load();
            },
            failure :function(model){
                receiveCartStore.load();
            }
        });  
    },
    filter : function(field, el, me){    
        var form            =   field.up('window').down('form').getForm();
        if(form.isValid()){
            var idValue      =   form.findField('id').getValue(),
            branchId         =   form.findField('branchId').getValue(),
            branchName       =   form.findField('branchName').getValue(),
            entryDate        =   form.findField('entryDate').getValue(),
            filter           =   Fsl.store.getFilter('S01T012002');
            filter.add('id', idValue).add('branchId', branchId).add('branchName', branchName).add('entryDate', entryDate).load(1);
        }
    },
    receiveFilter : function(field, el, me){    
        var form            =   field.up('window').down('form').getForm();
        if(form.isValid()){
            var idValue      =   form.findField('id').getValue(),
            transferId       =   form.findField('transferId').getValue(),
            fromBranchId     =   form.findField('fromBranchId').getValue(),
            branchName       =   form.findField('branchName').getValue(),
            entryDate        =   form.findField('entryDate').getValue(),
            filter           =   Fsl.store.getFilter('S01T012001');
            filter.add('id', idValue).add('transferId', transferId).add('fromBranchId', fromBranchId).add('branchName', branchName).add('entryDate', entryDate).load(1);
        }
    }
});