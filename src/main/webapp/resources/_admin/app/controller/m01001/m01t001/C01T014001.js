Ext.define(Fsl.app.getAbsController('C01T014001'), {
    extend      : 'Ext.app.Controller', 
    requires    : Fsl.app.getMergedRequires({
        apps: [],
        misc: []
    }),  
    views       : Fsl.view.getViews(['V01T014001','V01T014001X01:8']),
    refs        : [{
        ref       : 'activeWindow',
        selector  : 'window'
    }],
    init: function() { 
        var me = this;
        me.control({
            'v01t014001x01 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        me.filter(field, el, me);
                    }
                }
            },
            'v01t014001x02 button[action=new]':{
                click  :  function (field, el) {                    
                    var odrWin = Ext.widget('v01t014001x03',{
                        animateTarget:'v01t014001x03'
                    });
                    var button = odrWin.down('button[action=newOrder]');
                    button.fireEvent('click', button);      
                }
            },
            'v01t014001x02 button[action=search]':{
                click  :  function (field, el) {
                    this.filter(field, el, me)
                }
            },
            'v01t014001x02 button[action=clear]':{
                click  :  function (field, el) {
                    field.up('window').down('form').getForm().reset();
                    Fsl.store.getStore('S01T014001').load(1);
                }
            },
            'v01t014001x04  textfield[name=toBranchName]':{
                specialkey      : function(field, el){
                    if (el.getKey() == el.ENTER) {
                        Ext.widget('v01t014001x06');
                    }
                },               
                afterrender: function(c) {
                    c.inputEl.on('dblclick', function() {
                        var win = Ext.widget('v01t014001x06')
                    //                        ,
                    //                        grid    = win.down('grid'),
                    //                        store   = grid.getStore()
                    //                        console.log(store)
                    });
                }
            },
            'v01t014001x07 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        me.branchFilter(field, el, me)
                    }
                }
            },            
            'v01t014001x08 button[action=search]':{
                click  :  function (field, el) {
                    this.branchFilter(field, el, me)
                }
            },
            'v01t014001x08 button[action=clear]':{
                click  :  function (field, el) {
                    field.up('window').down('form').getForm().reset();
                    Fsl.store.getStore('S01I015001').load(1);
                }
            },   
            'v01t014001x08':{
                itemdblclick    :this.setBranch
            },
            'v01t014001x04 numberfield[name=articleId]':{
                specialkey      : function(field, el){
                    if (el.getKey() == el.ENTER) {
                        me.getArticle(field, el, me)
                    }
                }
            },                    
            'v01t014001x04 numberfield[name=quantity]':{
                specialkey      : function(field, el){
                    if (el.getKey() == el.ENTER) {
                        me.addItemInCartForOrder(field, el, me);
                    }
                },
                keyup           : function(field, el){                    
                    Fsl.C01I000000.setItemSubTotal(field, el, me);
                }
            },
            'v01t014001x03 button[action=newOrder]':{
                click           :me.newOrder
            },
            'v01t014001x03 button[action=sendOrder]':{
                click           :me.sendOrder
            },
            'v01t014001x05' :{
                itemdblclick    : me.setArticleOnField
            },   
            'v01t014001x02' :{
                itemdblclick    : me.getOrder
            },
            'v01t014001x04 numberfield[name=id]':{
                specialkey      : function(field, el){
                    if (el.getKey() == el.ENTER || el.getKey()==el.TAB) { 
                        me.getOrderItems(field, el, me)
                    }
                }/*,
                blur: function(field, event) {
                    event.keyCode = event.ENTER;
                    field.fireEvent('specialkey',field, event);
                }*/
            },
            
            
            
            'sv01t01400102':{
                itemdblclick    :this.setOrder 
            },
            'sv01t01400103 button[action = getArticleforOrder]':{
                click       :this.getArticleforOrder
            },
            'sv01t01400103 button[action=addOrderItem]':{
                click       :this.addOrderItemInCart
            },
            'sv01t01400103 button[action=sendOrder]':{
                click       :this.sendOrder
            },
            'sv01t01400103 button[action=resetAll]':{
                click       :this.resetOrder
            },
            'sv01t01400103 button[action=getOrder]':{
                click       :this.getOrder
            },
            'sv01t01400105':{
                itemdblclick:this.getArticleinfo
            },                                          /*For Branch Search*/
            'sv01t01400109 button[action=search]':{
                click  :  function (field, el) {
                    this.filter(field, el,me)
                }
            },
            'sv01t01400109 button[action=clear]':{
                click  :  function (field, el) {
                    field.up('window').down('form').getForm().reset();
                    this.filter(field, el,me)
                }
            },
            'sv01t01400108 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        this.filter(field, el,me)
                    }
                }
            },
            'v01t014001x03 button[action=pdf]' : {
                click : this. pdfreport
            }
        })
    },
    filter : function(field, el, me){    
        var form            =   field.up('window').down('form').getForm();
        if(form.isValid()){
            var idValue      =   form.findField('id').getValue(),
            toBranchId       =   form.findField('toBranchId').getValue(),
            branchName       =   form.findField('branchName').getValue(),
            entryDate        =   form.findField('entryDate').getValue(),
            filter           =   Fsl.store.getFilter('S01T014001');
            filter.add('id', idValue).add('toBranchId', toBranchId).add('branchName', branchName).add('entryDate', entryDate).load(1);
        }
    },   
    branchFilter:function(field, el,me){    
        var form        =   field.up('window').down('form').getForm();
        if(form.isValid()){
            var idValue     = form.findField('id').getValue(),
            name        = form.findField('name').getValue(),
            location    = form.findField('location').getValue(),
            filter           =   Fsl.store.getFilter('S01I015001');
            filter.add('id', idValue).add('name', name).add('location', location).load(1);
        }
    },
    setBranch : function(view, records){        
        var form = Ext.getCmp('v01t014001x04').getForm();      
        form.findField('toBranchId').setValue(records.data.id);
        form.findField('toBranchName').setValue(records.data.name);
        view.up('window').close();
    },
    getArticle : function(field, el, me){
        var preform     =   field.up('window').down('form'),
        form            =   preform.getForm(),
        values          =   form.getValues(),
        statusView      =   'win-statusbar-v01t014001x05';   
        if(values.articleId != ""){  
            Fsl.model.load('M01T013002', {
                id:values.articleId
            }, {
                scope     : this,
                success   : function(records, operation) {
                    Fsl.C01I000000.setValueInfieldContainer(records, operation, form);
                    form.findField('quantity').setValue('1').focus(true);
                                       
                    var rate = records.getM01i012001().data.price,
                    discount = records.getM01t010001().data.discount,
                    vat      = records.getM01i013001().data.vat,
                    st       = (parseFloat(rate)-parseFloat(rate * (discount/100))+parseFloat(rate * (vat/100)))*parseFloat(1);
                    form.findField('individualTotal').setValue(st);
                },
                failure :function(records, operation){
                    var containerId = '#orderCt'
                    Fsl.C01I000000.clearFieldContainer(preform, containerId);
                    Fsl.severity.error(statusView, operation.error);
                    Fsl.security.recheck(operation);
                }
            });
        }        
    }, 
    addItemInCartForOrder : function(field, el, me){
        var preform = field.up('window').down('form'),
        form        = preform.getForm(),
        values      = form.getValues(),        
        qty         = parseFloat(values.quantity),
        statusView  = 'win-statusbar-v01t014001x05';   
       
        if(form.isValid()){   
            if(values.articleId >= 1){                                              
                if(qty >= 0){ 
                    me.items_load_on_orderCart(values, preform);                  
                }             
            }else{
                var containerId = '#orderCt'
                Fsl.C01I000000.clearFieldContainer(preform, containerId);             
            }
        }else{
            var message  = 'Necessary Field Required';
            Fsl.severity.warn(statusView, message);  
        }
    },
    items_load_on_orderCart : function(values, preform){
        values.id = 0;
        var statusView  = 'win-statusbar-v01t014001x05',  
        containerId     = '#orderCt',
        orderCartStore  = Fsl.store.getStore('S01T013001');
        Fsl.model.save('M01T013001', values, {
            scope:this,
            success:function(model, operation){
                var text        = operation.response.responseText,
                json            = Ext.decode(text);                
                Fsl.C01I000000.clearFieldContainer(preform, containerId);   
                orderCartStore.load(); 
                Fsl.severity.info(statusView, operation);
            },                
            failure:function(model, operation){ 
                Fsl.severity.error(statusView, operation.error);
                Fsl.security.recheck(operation);
            } 
        });
    },
    setArticleOnField :  function(view, record){
        var preform     = view.up('window').down('form'),
        form            = preform.getForm(),
        values          = form.getValues(),
        articleId       = record.getM01i006001().data.id,
        statusView      =   'win-statusbar-v01t014001x05';   

        Fsl.model.load('M01T013002', {
            id:articleId
        }, {
            scope:this,
            success: function(records, operation, success) {
                form.findField('quantity').setValue(record.data.quantity).focus(true);
                form.findField('articleId').setValue(articleId);
                Fsl.C01I000000.setValueInfieldContainer(records, operation, form);  
                var rate = parseFloat(records.getM01i012001().data.price),
                discount = parseFloat(records.getM01t010001().data.discount),
                vat      = parseFloat(records.getM01i013001().data.vat),
                quantity = parseFloat(form.findField('quantity').getValue()),
                st       = (rate+(rate * (vat/100))-(rate * (discount/100)))*quantity;
                form.findField('individualTotal').setValue(st);
            },
            failure :function(model, operation){
                var containerId = '#orderCt'
                Fsl.C01I000000.clearFieldContainer(preform, containerId);
                Fsl.security.recheck(operation);
            }
        });
    },    
    newOrder : function(button){
        var win = button.up('window'),
        form    = win.down('form');
        form.getForm().reset();
        var orderCartStore= Fsl.store.getStore('S01T013001');
        Fsl.model.load('M01T014004', {
            id:1
        }, {
            success: function(model, operation) { 
                form.getForm().findField('branchId').setValue(model.getM01i015001().data.id);
                //form.getForm().findField('toBranchId').setValue(model.getM01i015002().data.id);
                form.getForm().findField('branchName').setValue(model.getM01i015001().data.name);
                //form.getForm().findField('toBranchName').setValue(model.getM01i015002().data.name);
                orderCartStore.load();
                var btn    = win.down('button[action=sendOrder]')
                btn.setText('SEND');
                btn.setIcon(Fsl.route.getIcon('TB01001/SAV01004.png'));    
            },
            failure :function(model, operation){
                orderCartStore.load();
                Fsl.security.recheck(operation);
            }
        });  
    },
    sendOrder : function(button){
        var win         =  button.up('window'),
        form            =  win.down('form'),
        record          =  form.getRecord(),
        values          =  form.getValues(),        
        orderCartStore  = Fsl.store.getStore('S01T013001'),
        orderStore      = Fsl.store.getStore('S01T014001'),       
        statusView      = 'win-statusbar-v01t014001x05',message;  
        button.setDisabled(true);        
        if(form.getForm().isValid()){  
            Fsl.model.save('M01T014002', values, {            
                scope   :this,
                success :function(model, operation){
                    var text    = operation.response.responseText,
                    json        = Ext.decode(text);
                    form.getForm().findField('id').setValue(model.data.id);
                    orderCartStore.load();
                    orderStore.load();
                    button.enable();
                    button.setText('Update');
                    button.setIcon(Fsl.route.getIcon('TB01001/EDT01004.png'));    
                    Fsl.severity.info(statusView, json.message);
                },                  
                failure:function(model, operation){               
                    Fsl.severity.error(statusView, operation.error);
                    button.enable();
                    Fsl.security.recheck(operation);
                } 
            });
        }else{
            button.enable();
            message  = 'Necessary Field Required';
            Fsl.severity.warn(statusView, message);              
        }    
    },
    getOrderItems : function(field, el, me){
        var win     = field.up('window'),
        form        = win.down('form').getForm(),
        values      = form.getValues(),
        orderCartStore  = Fsl.store.getStore('S01T013001');
        if(values.id > 0){  
            Fsl.model.load('M01T014001', {
                id:values.id
            }, {
                scope   : this,
                success : function(records, operation, success) {  
                    orderCartStore.load();
                    form.loadRecord(records);
                    form.findField('branchName').setValue(records.getM01i015001().data.name);  
                    form.findField('toBranchName').setValue(records.getM01i015002().data.name);  
                    form.findField('articleId').focus();  
                    var btn  = win.down('button[action=sendOrder]')
                    btn.setText('UPDATE');
                    btn.setIcon(Fsl.route.getIcon('TB01001/EDT01004.png'));      
                },
                failure :function(model, operation){
                    orderCartStore.reload();
                    Fsl.security.recheck(operation);
                }
            });
        }
    },
    getOrder : function(model, records, item, index,  event){   
        var orderId = records.data.id;
        var win =Ext.widget('v01t014001x03',{
            animateTarget  : 'v01t014001x03'
        });       
        var field = win.down('form').getForm().findField('id')
        field.setValue(orderId); 
        event.keyCode = event.ENTER;
        field.fireEvent('specialkey',field, event);        
    },
    pdfreport : function(button){
        var form    = button.up('window').down('form').getForm(),
        values      = form.getValues(),      
        orderId     = values.id,
        branchId    = values.branchId;
        if(orderId != null){
            var body = Ext.getBody();
            var frame = body.createChild({
                tag:'iframe',
                cls:'x-hidden',
                id:'iframe',
                name:'iframe'
            });
            var form = body.createChild({
                tag:'form',
                cls:'x-hidden',
                id:'form',
                method:'post',
                action:Fsl.route.getAction('c01t014001/report/'),
                target:'iframe'
            });
        
            form.createChild({
                tag:'input',
                name:'orderId',
                type:'hidden',
                value:orderId
            });
            
            form.createChild({
                tag:'input',
                name:'branchId',
                type:'hidden',
                value:branchId
            });
        
            form.dom.submit();      
        }else{
            Ext.Msg.alert("Sorry","Invoice is not created, So PDF can't be generated !!!")
        }
    }
});