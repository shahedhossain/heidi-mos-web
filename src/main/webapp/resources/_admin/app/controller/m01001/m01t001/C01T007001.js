Ext.define(Fsl.app.getAbsController('C01T007001'), {
    extend      : 'Ext.app.Controller', 
    requires    : Fsl.app.getMergedRequires({
        apps: [],
        misc: []
    }),  
    views       : Fsl.view.getViews(['V01T007001', 'V01T007001X01:06']),    
    refs        : [{
        ref       : 'activeWindow',
        selector  : 'window'
    }],
    init: function() { 
        var me = this;
        me.control({
            'v01t007001x02' :{
                itemdblclick:me.getInvoiceOngriddblClick
            },
            'v01t007001x01 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        me.filter(field, el, me);
                    }
                }
            },
            'v01t007001x02 button[action=search]':{
                click  :  function (field, el) {
                    me.filter(field, el, me);
                }
            },
            'v01t007001x02 button[action=clear]':{
                click  :  function (field, el) {
                    field.up('window').down('form').getForm().reset();
                    Fsl.store.getStore('S01T007001').load();
                }
            },
            'v01t007001x02 button[action=new]':{
                click  :  function(btn, e, eOpts){
                    var invWin =  Ext.widget('v01t007001x03',{
                        animateTarget  : 'v01t007001x03'
                    });                     
                    var button = invWin.down('button[action=newInvoice]');
                    button.fireEvent('click', button);      
                }
            },         
            'v01t007001x04 numberfield[name=customerId]':{
                specialkey      : function(field, el){
                    if (el.getKey() == el.ENTER) {
                        me.getCustomer(field, el, me);
                    }
                }
            },
            'v01t007001x04 numberfield[name=articleId]':{
                specialkey      : function(field, el){
                    if (el.getKey() == el.ENTER) {
                        me.getArticle(field, el, me);
                    }
                }
            },            
            'v01t007001x04 numberfield[name=quantity]':{
                specialkey      : function(field, el){
                    if (el.getKey() == el.ENTER) {
                        me.addItemInCartForInvoice(field, el, me);
                    }
                },
                keyup           : function(field, el){                    
                    Fsl.C01I000000.setItemSubTotal(field, el, me);
                }
            },
            'v01t007001x05' :{
                itemdblclick : me.setArticleOnField
            },        
           
            'v01t007001x03 button[action=save]':{
                click       : me.onSave
            },
            'v01t007001x03 button[action=newInvoice]':{
                click       : me.newInvoice
            },
            'v01t007001x04 numberfield[name=invoiceId]':{
                specialkey      : function(field, el){
                    if (el.getKey() == el.ENTER || el.getKey()==el.TAB) { 
                        me.getInvoice(field, el, me);
                    }
                }/*,
                blur: function(field, event) {
                    event.keyCode = event.ENTER;
                    field.fireEvent('specialkey',field, event);
                }*/
            },
            'v01t007001x06 combobox[name=payMethodId]':{                
                boxready  : function(combo, width,  height, eOpts){  
                    combo.setValue(combo.getStore().getAt(0).get(combo.valueField),true);
                    combo.fireEvent('select',combo);                            
                },
                select:  function (combo, records, eOpts) {
                    var value   = combo.getValue();  
                    var form    = combo.up('form').getForm();
                    if(value != null || value != ''){                              
                        if(value == '2' || value == '3'){
                            form.findField('return').disable();                                                    
                        }else{
                            form.findField('return').enable();
                        }
                    }
                }
            },
            'v01t007001x06 numberfield[name=payAmount]':{  
                keyup: function(field, event, eOpts){ 
                    var form  = field.up('form').getForm(),
                    values    = form.getValues(),
                    gmony     = values.gtotal,
                    paym      = values.payAmount,
                    rmony     = paym-gmony;
                    if(paym != ''){
                        var rmonydecimal=Ext.util.Format.number(rmony,'0.00');
                        form.findField('return').setValue(rmonydecimal);
                    }else{
                        form.findField('return').setValue('');
                    }
                },
                specialkey  : function(field, e){
                    if (e.getKey() == e.ENTER) {
                        var button = field.up('window').down('button[action=save]');
                        button.fireEvent('click', button); 
                    }
                }
            },
            'v01t007001x03 button[action=pdf]' : {
                click : this. pdfreport
            }
            
        });
    },
    getInvoiceOngriddblClick : function(model, records, item, index,  event){   
        var invoiceId = records.data.id;
        var win =Ext.widget('v01t007001x03',{
            animateTarget  : 'v01t007001x03'
        });       
        var field = win.down('form').getForm().findField('invoiceId');
        field.setValue(invoiceId); 
        event.keyCode = event.ENTER;
        field.fireEvent('specialkey',field, event);        
    },
    onSave   : function(button){
        var upform    = button.up('window').down('form').getForm(),
        downform      = Ext.getCmp('v01t007001x06').getForm(),
        upvalues      = upform.getValues(),
        downvalues    = downform.getValues(),        
        values        = Ext.Object.merge(upvalues, downvalues),
        statusView    = 'win-statusbar-v01t007001x04',message;  
        values.id     = values.invoiceId;               
        if(upform.isValid() && downform.isValid()){ 
            if(values.payAmount >= values.gtotal){
                if(values.rebate == 0 || values.rebate == null){
                    values.rebateId = 0;
                }
                if(values.grossVatId >0 ){
                    button.setDisabled(true);
                    Fsl.model.save('M01T007001', values, {
                        scope   : this,
                        success : function(records, operation){
                            var text    = operation.response.responseText,
                            json        = Ext.decode(text);                    
                            upform.findField('invoiceId').setValue(records.data.id);                   
                            Fsl.store.getStore('S01T008003').load();                 
                            Fsl.store.getStore('S01T007001').load();
                            button.enable();
                            button.setText('Update');
                            button.setIcon(Fsl.route.getIcon('TB01001/EDT01004.png'));    
                            Fsl.severity.info(statusView, json.message);
                        },                  
                        failure:function(model, operation){
                            message = "Unable To Save";
                            Fsl.severity.error(statusView, operation.error);
                            button.enable();
                            Fsl.security.recheck(operation);
                        } 
                    });
                }else{                    
                    message = "Gross Vat Undefined";
                    Fsl.severity.error(statusView, message);
                }
            }else{         
                downform.findField('payAmount').focus(true);
            }
        }else{         
            message  = 'Necessary Field Required';
            Fsl.severity.warn(statusView, message);
        }
    },
    filter:function(field, el, me){    
        var form            =   field.up('window').down('form').getForm();
        if(form.isValid()){
            var idValue     =   form.findField('id').getValue(),
            customerName    =   form.findField('customerName').getValue(),
            customerId      =   form.findField('customerId').getValue(),
            entryDate       =   form.findField('entryDate').getValue(),
            filter          =   Fsl.store.getFilter('S01T007001');
            filter.add('id', idValue).add('customerName', customerName).add('customerId', customerId).add('entryDate', entryDate).load(1);   
        }
    },    
    newInvoice : function(button){
        var win     = button.up('window'),
        upform      = win.down('form').getForm(),
        values      = upform.getValues(),
        downform    = Ext.getCmp('v01t007001x06').getForm(),
        iItemStore  = Fsl.store.getStore('S01T008003'); 
        downform.reset();
        upform.reset();  
        button.setDisabled(true);        
        Fsl.model.load('M01T007003', {
            id:1
        }, {
            scope   : this,
            success : function(records, operation, success) {                
                if(iItemStore.count()>0){
                    iItemStore.load();
                }
                upform.loadRecord(records);
                upform.findField('titleId').setValue(records.getM01t001001().data.id);  
                upform.findField('name').setValue(records.getM01t001001().data.name);    
                upform.findField('branchId').setValue(records.getM01i015001().data.id);    
                upform.findField('branchName').setValue(records.getM01i015001().data.name);    
                downform.loadRecord(records);
                downform.findField('vatPercent').setValue(records.getM01i008001().data.vatPercent);  
                downform.findField('rebatePercent').setValue(records.getM01i009001().data.rebatePercent);
                downform.findField('purchaseTarget').setValue(records.getM01i009001().data.purchaseTarget);
                downform.findField('percent').setValue(records.getM01i010001().data.percent);              
                button.enable();
                
                var btn  = win.down('button[action=save]');
                btn.setText('SAVE');
                btn.setIcon(Fsl.route.getImage('SAV01004.png')); 
                win.down('grid').getEl().unmask();
            },
            failure :function(model){
                if(iItemStore.count()>0){
                    iItemStore.load();
                }
                button.enable();
                win.down('grid').getEl().unmask();
            }
        });
    },
    getCustomer : function(field, el, me){
        var form     =   field.up('window').down('form').getForm(),
        values       =   form.getValues();
        if(values.customerId != ""){   
            Fsl.model.load('M01T001001', {
                id:values.customerId
            }, {
                scope   :this,
                success : function(records, operation, success) {
                    form.loadRecord(records);
                    form.findField('articleId').focus();
                },
                failure :function(model){
                    
                }
            });
        }
    },
    
    
    getArticle : function(field, el, me){
        var preform     =   field.up('window').down('form'),
        form            = preform.getForm(),
        values          =   form.getValues(),
        statusView      =   'win-statusbar-v01t007001x04';   
        if(values.articleId != ""){  
            Fsl.model.load('M01T008002', {
                id:values.articleId
            }, {
                scope     : this,
                success   : function(records, operation) {
                    Fsl.C01I000000.setValueInfieldContainer(records, operation, form);
                    form.findField('quantity').setValue('1').focus(true);
                    form.findField('dbQuantity').setValue(records.data.quantity);
                   
                    var rate = records.getM01i012001().data.price,
                    discount = records.getM01t010001().data.discount,
                    vat      = records.getM01i013001().data.vat,
                    st       = (parseFloat(rate)-parseFloat(rate * (discount/100))+parseFloat(rate * (vat/100)))*parseFloat(1);
                    form.findField('individualTotal').setValue(st);
                },
                failure :function(records, operation){
                    var containerId = '#invoiceCt';
                    Fsl.C01I000000.clearFieldContainer(preform, containerId);
                    Fsl.severity.error(statusView, operation.error);
                }
            });
        }        
    },    
            
    addItemInCartForInvoice :function(field, el, me){
        var preform = field.up('window').down('form'),
        form        = preform.getForm(),
        values      = form.getValues(),        
        qty         = parseFloat(values.quantity),
        dbqty       = parseFloat(values.dbQuantity),
        statusView  = 'win-statusbar-v01t007001x04';   
        if(form.isValid()){   
            if(values.articleId >= 1){
                if(qty <= dbqty){                
                    var gridItem       = this.gridQty(values);               
                    if(qty > 1){                    
                        this.items_load_on_grid(values, preform);
                    }else if(qty == 1){                         
                        if(gridItem == undefined){ 
                            this.items_load_on_grid(values, preform);        
                        }else if(parseFloat(gridItem.data.quantity) == dbqty){
                            Fsl.C01I000000.showMsg("Empty", form);
                        }else if(parseFloat(gridItem.data.quantity) < dbqty){
                            this.items_load_on_grid(values, preform);                      
                        }
                    }else if(qty == 0){
                        this.items_load_on_grid(values, preform);
                    }else{
                        Fsl.C01I000000.showMsg(dbqty, form);
                    }
                }else if(!qty){  
                    Fsl.C01I000000.showMsg('Not Valid', form);
                }else{
                    Fsl.C01I000000.showMsg(dbqty, form);
                }
            }else{                
                form.findField('articleId').focus(true);
                var containerId = '#invoiceCt';
                Fsl.C01I000000.clearFieldContainer(preform, containerId);                
            }     
        }else{ 
            var message  = 'Necessary Field Required';
            Fsl.severity.error(statusView, message);
        }
    },
    
    gridQty : function(values){
        var records, iItemStore  = Fsl.store.getStore('S01T008003'); 
        iItemStore.each(function(record){           
            if(record.getM01i006001().data.id == values.articleId){
                if(record.data.id >0 ){                
                    records   = record;               
                }else if(record.data.id == 0){
                    records   = record;
                }
                return  records;
            }             
        });
        return records;
    },
   
    items_load_on_grid :function(values, preform){
        var statusView  = 'win-statusbar-v01t007001x04';   
        Fsl.model.save('M01T008001', values, {       
            scope   : this,
            success : function(model, operation){
                var text     = operation.response.responseText,
                json         = Ext.decode(text),                    
                containerId = '#invoiceCt';
                Fsl.C01I000000.clearFieldContainer(preform, containerId);
                Fsl.store.getStore('S01T008003').load(); 
                Fsl.severity.info(statusView, json.message);
            },                
            failure:function(model, operation){ 
                Fsl.severity.error(statusView, operation.error);
            } 
        });
    },
       
    setArticleOnField :  function(view, record){
        var preform     = view.up('window').down('form'),
        form            = preform.getForm(),
        values          = form.getValues(),
        articleId       = record.getM01i006001().data.id,     
        invoiceItemId   = record.data.id;        
        /* if(invoiceItemId){
            console.log(invoiceItemId)
        }else{*/
        Fsl.model.load('M01T008002', {
            id:articleId
        }, {
            scope:this,
            success: function(records, operation, success) {
                form.findField('quantity').setValue(record.data.quantity).focus(true);
                form.findField('articleId').setValue(articleId);
                Fsl.C01I000000.setValueInfieldContainer(records, operation, form);
                form.findField('dbQuantity').setValue(records.data.quantity);  
                var rate = parseFloat(records.getM01i012001().data.price),
                discount = parseFloat(records.getM01t010001().data.discount),
                vat      = parseFloat(records.getM01i013001().data.vat),
                quantity = parseFloat(form.findField('quantity').getValue()),
                st       = (rate+(rate * (vat/100))-(rate * (discount/100)))*quantity;
                form.findField('individualTotal').setValue(st);
            },
            failure :function(model){
                var containerId = '#invoiceCt';
                Fsl.C01I000000.clearFieldContainer(preform, containerId);
            }
        });
    // }
    },    
    getInvoice : function(field, el, me){
        var win     = field.up('window'),
        upform      = win.down('form').getForm(),
        downform    = Ext.getCmp('v01t007001x06').getForm(),
        values      = upform.getValues(),
        iItemStore  = Fsl.store.getStore('S01T008003'); 
        if(values.invoiceId > 0){  
            Fsl.model.load('M01T007001', {
                id:values.invoiceId
            }, {
                scope   : this,
                success : function(records, operation, success) {  
                    iItemStore.load();
                    upform.loadRecord(records);
                    upform.findField('titleId').setValue(records.getM01t001001().data.id);  
                    upform.findField('name').setValue(records.getM01t001001().data.name);  
                    downform.loadRecord(records);
                    downform.findField('vatPercent').setValue(records.getM01i008001().data.vatPercent);  
                    downform.findField('rebatePercent').setValue(records.getM01i009001().data.rebatePercent);
                    downform.findField('purchaseTarget').setValue(records.getM01i009001().data.purchaseTarget);
                    downform.findField('percent').setValue(records.getM01i010001().data.percent);
                    
                    upform.findField('articleId').focus();  
                    var btn  = win.down('button[action=save]');
                    btn.setText('UPDATE');
                    btn.setIcon(Fsl.route.getIcon('TB01001/EDT01004.png')); 
                    win.down('grid').getEl().unmask();
                },
                failure :function(model){
                    iItemStore.reload();
                }
            });
        }
    },
    pdfreport : function(button){
        var form    = button.up('window').down('form').getForm(),
        values      = form.getValues(),      
        invoiceId   = values.invoiceId, 
        branchId    = values.branchId;
        if(invoiceId != null){
            var body = Ext.getBody();
            var frame = body.createChild({
                tag:'iframe',
                cls:'x-hidden',
                id:'iframe',
                name:'iframe'
            });
            var form = body.createChild({
                tag:'form',
                cls:'x-hidden',
                id:'form',
                method:'post',
                action:Fsl.route.getAction('c01t007001/report/'),
                target:'iframe'
            });
        
            form.createChild({
                tag:'input',
                name:'invoiceId',
                type:'hidden',
                value:invoiceId
            });
            form.createChild({
                tag:'input',
                name:'branchId',
                type:'hidden',
                value:branchId
            });
        
            form.dom.submit();      
        }else{
            Ext.Msg.alert("Sorry","Invoice is not created, So PDF can't be generated !!!");
        }
    }
});