Ext.define(Fsl.app.getAbsController('C01T004002'), {
    extend      : 'Ext.app.Controller', 
    requires    : Fsl.app.getMergedRequires({
        apps: [],
        misc: []
    }),  
    views       : Fsl.view.getViews(['V01T004002:3', 'V01T004002X01:02','V01T004003X01:02']),
    refs        : [{
        ref       : 'activeWindow',
        selector  : 'window'
    }],
    init: function() {
        var me = this;
        this.control({
             
        });
    }
});