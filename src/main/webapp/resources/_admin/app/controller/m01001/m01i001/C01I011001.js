Ext.define(Fsl.app.getAbsController('C01I011001'), {
    extend      : 'Ext.app.Controller', 
    requires    : Fsl.app.getMergedRequires({
        apps: [],
        misc: []
    }),  
    views       : Fsl.view.getViews(['V01I011001', 'V01I011001X01:02']),
    models      : Fsl.model.getModels(['M01I011001']),
    stores      : Fsl.store.getStores(['S01I011001:2']),
    refs        : [{
        ref       : 'activeWindow',
        selector  : 'window'
    }],
    init        : function() {  
        var me     = this;
        me.control({
            'v01i011001x01 button[action=add]':{
                click  :  function (field, el) {
                    Ext.widget('v01i011001x02').show();
                }
            },
            'v01i011001x01'     :   {
                itemclick       :   me.infoOnStatus,
                itemdblclick    :   this.preUpdate
            },
            'v01i011001x02 button[action=save]': {
                click        : this.onSave
            },
            'v01i011001x02 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        var button = field.up('window').down('button[action=save]');
                        button.fireEvent('click', button);                            
                    }
                }
            }
        });
    },
    onSave            : function(button){
        var win = button.up('window'),
        form    = win.down('form'),
        record  = form.getRecord(),
        values  = form.getValues(),
        status  = 'win-statusbar-v01i011001x01',
        statusView  = 'win-statusbar-v01i011001x02';                    

        if (form.getForm().isValid()) { 
            if (form.getForm().isDirty()) { 
                Fsl.model.save('M01I011001', values, {
                    scope   : this,
                    success : function(model, operation) {
                        form.loadRecord(model);
                        Fsl.severity.info(status, operation);
                        Fsl.severity.info(statusView, operation);
                        Fsl.store.getStore('S01I011001').load();
                    },
                    failure : function(model, operation){
                        Fsl.severity.error(status, operation);
                        Fsl.severity.error(statusView, operation);
                        Fsl.security.recheck(operation);
                    }
                });
            } else {
                var message  = 'No change occured';
                Fsl.severity.warn(status, message);
                Fsl.severity.warn(statusView, message);
            }
        } else {
            var message  = 'Necessary Field Required';
            Fsl.severity.warn(status, message);
            Fsl.severity.warn(statusView, message);
        }     
    },
    infoOnStatus      : function(model, operation){
        var status  = 'win-statusbar-v01i011001x01';
        Fsl.severity.info(status, operation.data.name);      
    },
    preUpdate         : function(model, records){
        var view   = Fsl.app.getWidget('V01I011001X02');  
        view.setTitle('PAY METHOD UPDATE');
        var form   = view.down('form').loadRecord(records); 
        var btn    = view.down('button[text=Save]');
        btn.setText('Update');
        btn.setIcon(Fsl.route.getIcon('TB01001/EDT01004.png'));         
    }
});


