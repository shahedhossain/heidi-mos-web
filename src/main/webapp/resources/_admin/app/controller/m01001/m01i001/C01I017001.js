Ext.define(Fsl.app.getAbsController('C01I017001'), {
    extend      : 'Ext.app.Controller', 
    requires    : Fsl.app.getMergedRequires({
        apps: [],
        misc: []
    }),  
    views       : Fsl.view.getViews(['V01I017001','V01I017001X01:09']), 
    models      : Fsl.model.getModels(['M01I017001']),
    stores      : Fsl.store.getStores(['S01I017001:2']),
    refs        : [{
        ref       : 'activeWindow',
        selector  : 'window'
    }],
    init: function() {
        var me = this;
        me.control({
            'v01i017001x01 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        me.filter(field, el,me);
                    }
                }
            },
            'v01i017001x02 button[action=add]':{
                click  :  function (field, el) {                      
                    Ext.widget('v01i017001x03').show();             
                }
            },
            'v01i017001x02 button[action=search]':{
                click  :  function (field, el) {
                    me.filter(field, el,me);
                }
            },
            'v01i017001x02 button[action=clear]':{
                click  :  function (field, el) {
                    field.up('window').down('form').getForm().reset();
                    Fsl.store.getStore('S01I017001').loadPage(1);
                }
            },
            'v01i017001x03 textfield[name=articleName]':{
                afterrender: function(c) {
                    c.inputEl.on('dblclick', function() {
                        Ext.widget('v01i017001x04').show();    
                    });
                }
            },
            'v01i017001x05 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        me.articleFilterForArticleLimit(field, el,me);
                    }
                }
            },           
            'v01i017001x06 button[action=search]':{
                click  :  function (field, el) {
                    me.articleFilterForArticleLimit(field, el,me);
                }
            },
            'v01i017001x06 button[action=clear]':{
                click  :  function (field, el) {
                    field.up('window').down('form').getForm().reset();
                    Fsl.store.getStore('S01I017001').loadPage(1);
                }
            },
            'v01i017001x06':{
                itemdblclick:this.setArticleIntoArticleLimit      
            },
        
            'v01i017001x03 textfield[name=branchName]':{
                afterrender: function(c) {
                    c.inputEl.on('dblclick', function() {
                        Ext.widget('v01i017001x07').show();
                    });
                }
            },
            'v01i017001x08 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        me.branchFilterForArticleLimit(field, el,me);
                    }
                }
            },           
            'v01i017001x09 button[action=search]':{
                click  :  function (field, el) {
                    me.branchFilterForArticleLimit(field, el,me);
                }
            },
            'v01i017001x09 button[action=clear]':{
                click  :  function (field, el) {
                    field.up('window').down('form').getForm().reset();
                    Fsl.store.getStore('S01I017001').loadPage(1);
                }
            },
            'v01i017001x09':{
                itemdblclick:this.setBranchIntoArticleLimit      
            },
            'v01i017001x02' : {
                itemdblclick:this.preUpdate
            },
            'v01i017001x03 button[action = save]':{
                click:this.onSave
            },
            'v01i017001x03 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER){
                        var button = field.up('window').down('button[action=save]');
                        button.fireEvent('click', button);                            
                    }
                }
            }
        });
    }, 
    setArticleIntoArticleLimit :function(view, records){
        var form = Ext.getCmp('v01i017001x03').down('form').getForm();  
        form.findField('articleId').setValue(records.data.id);
        form.findField('articleName').setValue(records.data.name);
        view.up('window').close();
    },
    
    setBranchIntoArticleLimit : function(view, records){
        var form = Ext.getCmp('v01i017001x03').down('form').getForm();  
        form.findField('branchId').setValue(records.data.id);
        form.findField('branchName').setValue(records.data.name);
        view.up('window').close();
    },
    preUpdate : function(model, records){
        var win     =  Ext.widget('v01i017001x03'),
        frm         =  win.down('form'),
        form        =  frm.getForm();
        win.setTitle('ARTICLE LIMIT UPDATE');
        form.loadRecord(records); 
        form.findField('articleName').setValue(records.getM01i006001().data.name);
        form.findField('branchName').setValue(records.getM01i015001().data.name);
        var btn     = win.down('button[text=Save]');
        btn.setText('Update');
        btn.setIcon(Fsl.route.getIcon('TB01001/EDT01004.png'));
    },
    onSave   : function(button){
        var win     = button.up('window'),
        form        = win.down('form'),
        record      = form.getRecord(),
        values      = form.getValues(),
        status      = 'win-statusbar-v01i017001x02',
        statusView  = 'win-statusbar-v01i017001x03';                    

        if (form.getForm().isValid()) { 
            if (form.getForm().isDirty()) { 
                Fsl.model.save('M01I017001', values, {
                    scope   : this,
                    success : function(model, operation) {
                        form.loadRecord(model);
                        Fsl.severity.info(status, operation);
                        Fsl.severity.info(statusView, operation);
                        Fsl.store.getStore('S01I017001').load();
                    },
                    failure : function(model, operation){
                        Fsl.severity.error(status, operation);
                        Fsl.severity.error(statusView, operation);
                        Fsl.security.recheck(operation);
                    }
                });
            } else {
                var message  = 'No change occured';
                Fsl.severity.warn(status, message);
                Fsl.severity.warn(statusView, message);
            }
        } else {
            var message  = 'Necessary Field Required';
            Fsl.severity.warn(status, message);
            Fsl.severity.warn(statusView, message);
        }     
    },    
    filter:function(field, el,me){    
        var form        =   field.up('window').down('form').getForm();
        if(form.isValid()){
            var idValue     = form.findField('id').getValue(),
            articleId       = form.findField('articleId').getValue(),
            articleName     = form.findField('articleName').getValue(),
            branchName      = form.findField('branchName').getValue(),
            mbcode          = form.findField('mbcode').getValue(),
            pbcode          = form.findField('pbcode').getValue(),   
            maxQuantity     = form.findField('maxQuantity').getValue(),
            minQuantity     = form.findField('minQuantity').getValue(), 
            entryDate       = form.findField('entryDate').getValue(), 
            filter          = Fsl.store.getFilter('S01I017001');
            filter.add('id', idValue).add('articleId', articleId).add('articleName', articleName).add('branchName', branchName).add('mbcode', mbcode).add('pbcode', pbcode).add('maxQuantity', maxQuantity).add('minQuantity', minQuantity).add('entryDate', entryDate).load(1);
        }
    },
    articleFilterForArticleLimit : function(field, el,me){    
        var form       = field.up('window').down('form').getForm();
        if(form.isValid()){
            var idValue = form.findField('id').getValue(),
            name        = form.findField('name').getValue(),
            mbcode      = form.findField('mbcode').getValue(),
            pbcode      = form.findField('pbcode').getValue(),                        
            filter      = Fsl.store.getFilter('S01I006006');
            filter.add('id', idValue).add('name', name).add('mbcode', mbcode).add('pbcode', pbcode).load(1);
        }
    },    
    branchFilterForArticleLimit : function(field, el,me){    
        var form       = field.up('window').down('form').getForm();
        if(form.isValid()){
            var idValue     = form.findField('id').getValue(),
            name        = form.findField('name').getValue(),
            location    = form.findField('location').getValue(),                        
            filter      =   Fsl.store.getFilter('S01I015003');
            filter.add('id', idValue).add('name', name).add('location', location).load(1);
        }
    }
});