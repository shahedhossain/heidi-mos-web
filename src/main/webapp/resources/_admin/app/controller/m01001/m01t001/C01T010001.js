Ext.define(Fsl.app.getAbsController('C01T010001'), {
    extend      : 'Ext.app.Controller', 
    requires    : Fsl.app.getMergedRequires({
        apps: [],
        misc: []
    }),  
    views       : Fsl.view.getViews(['V01T010001', 'V01T010001X01:06']),
    
    refs        : [{
        ref       : 'activeWindow',
        selector  : 'window'
    }],
    init: function() {
        var me = this;
        me.control({
            'v01t010001x01 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        me.filter(field, el,me);
                    }
                }
            },
            'v01t010001x02 button[action=add]':{
                click  :  function (field, el) {                        
                    var win = Ext.widget('v01t010001x03');
                    win.show(); 
                    var form = win.down('form').getForm();
                    form.findField('startDate').setMinValue(new Date());
                }
            },
            'v01t010001x02 button[action=search]':{
                click  :  function (field, el) {
                    me.filter(field, el,me);
                }
            },
            'v01t010001x02 button[action=clear]':{
                click  :  function (field, el) {
                    field.up('window').down('form').getForm().reset();
                    Fsl.store.getStore('S01T010001').loadPage(1);
                }
            },
            'v01t010001x03 textfield[name=articleName]':{
                afterrender: function(c) {
                    c.inputEl.on('dblclick', function() {
                        Ext.widget('v01t010001x04').show();     
                    });
                }
            },
            'v01t010001x06 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        me.articleFilterForDiscount(field, el,me);
                    }
                }
            },           
            'v01t010001x06 button[action=search]':{
                click  :  function (field, el) {
                    me.articleFilterForDiscount(field, el,me);
                }
            },
            'v01t010001x06 button[action=clear]':{
                click  :  function (field, el) {
                    field.up('window').down('form').getForm().reset();
                    Fsl.store.getStore('S01I006007').loadPage(1);
                }
            },
            'v01t010001x06':{
                itemdblclick:this.setArticleIntoDiscount      
            },
        
            'v01t010001x02' : {
                itemdblclick:this.preUpdate
            },
            'v01t010001x03 button[action = save]':{
                click:this.onSave
            },
            'v01t010001x03 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER){
                        var button = field.up('window').down('button[action=save]');
                        button.fireEvent('click', button);                            
                    }
                }
            }
        });
    },
    setArticleIntoDiscount :function(view, records){
        var form = Ext.getCmp('v01t010001x03').down('form').getForm();
        form.findField('articleId').setValue(records.data.id);
        form.findField('articleName').setValue(records.data.name);
        view.up('window').close();
        
    },
    preUpdate:function(model, records){
         var view   = Fsl.app.getWidget('V01T010001X03');  
        view.setTitle('DISCOUNT UPDATE');
        var form = view.down('form').loadRecord(records); 
        form.loadRecord(records);           
        form.findField('startDate').setMinValue(records.data.startDate); 
        form.findField('articleName').setValue(records.getM01i006001().data.name);
        var btn    = view.down('button[text=Save]');
        btn.setText('Update');
        btn.setIcon(Fsl.route.getIcon('TB01001/EDT01004.png'));    
    },
    onSave            : function(button){
        var win = button.up('window'),
        form    = win.down('form'),
        record  = form.getRecord(),
        values  = form.getValues(),
        status  = 'win-statusbar-v01t010001x02',
        statusView  = 'win-statusbar-v01t010001x03';                    

        if (form.getForm().isValid()) { 
            if (form.getForm().isDirty()) { 
                Fsl.model.save('M01T010001', values, {
                    scope   : this,
                    success : function(model, operation) {
                        form.loadRecord(model);
                        Fsl.severity.info(status, operation);
                        Fsl.severity.info(statusView, operation);
                        Fsl.store.getStore('S01T010001').load();
                    },
                    failure : function(model, operation){
                        Fsl.severity.error(status, operation);
                        Fsl.severity.error(statusView, operation);
                        Fsl.security.recheck(operation);
                    }
                });
            } else {
                var message  = 'No change occured';
                Fsl.severity.warn(status, message);
                Fsl.severity.warn(statusView, message);
            }
        } else {
            var message  = 'Necessary Field Required';
            Fsl.severity.warn(status, message);
            Fsl.severity.warn(statusView, message);
        }     
    },   
    filter:function(field, el,me){    
        var form          =   field.up('window').down('form').getForm();//'sv01t01000104'
        if(form.isValid()){
            var idValue     =   form.findField('id').getValue(),
            articleId       =   form.findField('articleId').getValue(),
            startDate       =   form.findField('startDate').getValue(),
            endDate         =   form.findField('endDate').getValue(),
            discount        =   form.findField('discount').getValue(),
            articleName     =   form.findField('articleName').getValue(),
            mbcode          =   form.findField('mbcode').getValue(),
            pbcode          =   form.findField('pbcode').getValue(),                        
            filter          =   Fsl.store.getFilter('S01T010001');
            filter.add('id', idValue).add('articleId', articleId).add('startDate', startDate).add('endDate', endDate).add('discount', discount).add('articleName', articleName).add('mbcode', mbcode).add('pbcode', pbcode).load(1);
        }
    },
    articleFilterForDiscount : function(field, el,me){    
        var form       = field.up('window').down('form').getForm();//'sv01i00600106'
        if(form.isValid()){
            var idValue = form.findField('id').getValue(),
            nameValue   = form.findField('name').getValue(),
            brandName   = form.findField('brandName').getValue(),
            mbcode      = form.findField('mbcode').getValue(),
            pbcode      = form.findField('pbcode').getValue(),                        
            filter          =   Fsl.store.getFilter('S01T006007');
            filter.add('id', idValue).add('name', nameValue).add('brandName', brandName).add('mbcode', mbcode).add('pbcode', pbcode).load(1);
        }
    }
});