Ext.define(Fsl.app.getAbsController('C01I016001'), {
    extend      : 'Ext.app.Controller', 
    requires    : Fsl.app.getMergedRequires({
        apps: [],
        misc: []
    }),  
    views       : Fsl.view.getViews(['V01I016001','V01I016001X01:02']), 
    models      : Fsl.model.getModels(['M01I016001']),
    stores      : Fsl.store.getStores(['S01I016001:2']),
    refs        : [{
        ref       : 'activeWindow',
        selector  : 'window'
    }],
    init: function() {
        var me = this;
        me.control({
          'v01i016001x01 button[action=add]':{
                click  :  function (field, el) {                      
                    Ext.widget('v01i016001x02').show();              
                }
            },
            'v01i016001x01' : {
                itemdblclick:this.preUpdate
            },
            'v01i016001x02 button[action = save]':{
                click:this.onSave
            },
            'v01i016001x02 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER){
                        var button = field.up('window').down('button[action=save]');
                        button.fireEvent('click', button);                            
                    }
                }
            }
        });
    },    
    preUpdate : function(model, records){
        var win     =  Ext.widget('v01i016001x02'),
        frm         =  win.down('form'),
        form        =  frm.getForm();
        win.setTitle('STATUS UPDATE');
        form.loadRecord(records);    
        var btn     = win.down('button[text=Save]');
        btn.setText('Update');
        btn.setIcon(Fsl.route.getIcon('TB01001/EDT01004.png'));
    },
    onSave   : function(button){
        var win     = button.up('window'),
        form        = win.down('form'),
        record      = form.getRecord(),
        values      = form.getValues(),
        status      = 'win-statusbar-v01i016001x01',
        statusView  = 'win-statusbar-v01i016001x02';                    

        if (form.getForm().isValid()) { 
            if (form.getForm().isDirty()) { 
                Fsl.model.save('M01I016001', values, {
                    scope   : this,
                    success : function(model, operation) {
                        form.loadRecord(model);
                        Fsl.severity.info(status, operation);
                        Fsl.severity.info(statusView, operation);
                        Fsl.store.getStore('S01I016001').load();
                    },
                    failure : function(model, operation){
                        Fsl.severity.error(status, operation);
                        Fsl.severity.error(statusView, operation);
                        Fsl.security.recheck(operation);
                    }
                });
            } else {
                var message  = 'No change occured';
                Fsl.severity.warn(status, message);
                Fsl.severity.warn(statusView, message);
            }
        } else {
            var message  = 'Necessary Field Required';
            Fsl.severity.warn(status, message);
            Fsl.severity.warn(statusView, message);
        }     
    }
});