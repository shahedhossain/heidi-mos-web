@echo off
echo           ###############################################
echo          ###############################################
echo         ###                                         ###
echo        ###  ####### ###### ####### ###### ######## ###
echo       ###  ##      ##     ##   ## ##       ###    ###
echo      ###  #####   ###### ##   ## #####    ###    ###
echo     ###  ##          ## ##   ## ##       ###    ###
echo    ###  ##   ## ###### ####### ##       ###    ###
echo   ###                                         ###
echo  ###############################################
echo ###############################################
echo:

echo:
rem Software Developer use only
rem Environment Variable and Path Setting
set "EXTJS_APP=app"
set "CURRENT_DIR=%CD%"

goto setCleanPath

:setCleanPath
rem source path setting
set "ALL_CLASSES=%CURRENT_DIR%\all-classes.js"
set "APP_JSB3=%CURRENT_DIR%\%EXTJS_APP%.jsb3"
set "APP_ALL=%CURRENT_DIR%\app-all.js"
goto clean

:clean
rem clean path 
if exist %ALL_CLASSES% del %ALL_CLASSES%
if exist %APP_JSB3% del %APP_JSB3%
if exist %APP_ALL% del %APP_ALL%
echo Successfully clean!

rem move completed
goto end

:end
@echo on