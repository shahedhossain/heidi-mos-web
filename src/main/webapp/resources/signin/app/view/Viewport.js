Ext.define('Fsl.signin.view.Viewport', {
    extend      : 'Ext.Viewport',
	requires    : [            
        'Fsl.signin.view.EastPanel',
		'Fsl.signin.view.CenterPanel',
		'Fsl.signin.view.SigninPanel'		
    ],	
    layout      : {
		type    : 'border'
    },   
    defaults    : {
        split   : true
    }, 
    initComponent: function(config) {
        var me = this;        
		me.items = [{
			region   : 'north',
			bodyCls  : 'fsl-banner-bg',
			html     : '<span class="fsl-banner-text">Formative Soft Ltd </span>&nbsp <span class="fsl-copyright">&copy 2013 All Right Reserved</span>',                 
			bodyStyle: {
				background : 'body',
				padding    : '5px'
			},
			height   : 35,
			margin   : '0 0 0 0',
			border   : false
		},
		{                   
			region   : 'center',
			xtype    : 'centerPanel',
			margin   : '0 0 4 5'
		},
		{
			region   	: 'east',
			xtype    	: 'eastPanel',
			split    	: true,
			collapsible : true,
			collapseMode: 'mini',
			width    	: 400,
			margin   	: '0 5 4 0'
		}]; 
        me.callParent(arguments);
    }
});