Ext.define('Fsl.signin.view.SigninPanel' , {
    extend       : 'Ext.panel.Panel',
    requires	 : [
        'Ext.form.Panel',
        'Ext.form.field.Checkbox',
        'Ext.form.field.Text'
    ],
    alias        : 'widget.signinPanel',
    layout       : 'fit',
    border       : true,
    frame 	 : true,
    singleExpand : true,
    height	 : 180,
    width 	 : 320,
	
    initComponent: function () {
        var me 		= this;
        me.items 	= [{
            xtype		: 'form',            
            title		: 'Signin',
            frame		:true,
            bodyPadding         : 14,
            height		: null,            
            defaultType	: 'textfield',
            defaults	: {
                anchor	: '100%' 
            },            
            items: [{
                fieldLabel	: 'User ID',
                name		: 'j_username',
                emptyText	: 'username',
                allowBlank	:false
            },
            { 
                fieldLabel	: 'Password',
                name		: 'j_password',
                inputType	: 'password',
                emptyText	: 'password',
                allowBlank	:false
            },
            { 
                xtype		: 'checkbox',
                fieldLabel	: 'Remember me',
                name		: '_spring_security_remember_me'
            }
            ],            
            buttons: [{
                text	:'Login',
                action  :'login'
            },{
                text	:'Cancel',
                action  :'cancel'
            }
            ]
        }];
        me.callParent(arguments);
    }
});