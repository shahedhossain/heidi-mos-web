/**
 * This JavaScript Library is Fsl.signin application. This application developed
 * by Extjs 4.1.x UI with client side MVC by Extjs MVC and Data Package.Such type
 * of application are cross browser supported and user friendly like as desktop
 * applications.
 *
 * @static
 * @class       signin
 * @package     Fsl.signin
 * @access      private
 * @version		1.0.0
 * @since		May 21, 2013
 *
 **/
Ext.application({
    name        : Fsl.app.getName(), 
    appFolder   : Fsl.app.getPath(),
    controllers : Fsl.controller.getControllers(['Authentication']),
	models      : Fsl.model.getModels([]),
    stores      : Fsl.store.getStores([]),
    views       : Fsl.view.getViews([]),
	launch      : function() {
		Fsl.loader.complete();
	},
    autoCreateViewport: true
});