/**
 * This JavaScript Library is Fsl.admin application. This application developed
 * by Extjs 4.1.x UI with client side MVC by Extjs MVC and Data Package.Such type
 * of application are cross browser supported and user friendly like as desktop
 * applications.
 *
 * @static
 * @class       admin
 * @package     Fsl.admin
 * @access      private
 * @version		1.0.0
 * @since		May 21, 2013
 *
 **/
Ext.application({
    name        : Fsl.app.getName(), 
    appFolder   : Fsl.app.getPath(),
    controllers : Fsl.controller.getControllers([
        'Navigation',
        'C01I000000',
        'C01I001001:2',
        'C01I002001:2',
        'C01I003001:C01I017001',
       
        
        
        'C01T001001',
        'C01T002001',
        'C01T003001',
        'C01T004001',
        'C01T004002',
        'C01T007001',
        'C01T009001',
        'C01T010001',
        'C01T011001',
        'C01T012001',
        'C01T014001',
        'C01T015001',
        'C01T017001',
         
        'C01R001001',
        'C01R002001',
        'C01R003001',
        
        'C04T001001',
        'C04T002001',
        'C04T003001',
        
        'C04J001001',
        'C04J002001',
        'C04J003001'
        ]),
    launch      : function() {
        Fsl.loader.complete();
    },
    autoCreateViewport: true
});     
