Ext.define(Fsl.app.getAbsController('C04J003001'), {
    extend      : 'Ext.app.Controller', 
    requires    : Fsl.app.getMergedRequires({
        apps: [],
        misc: []
    }),  
    views       : Fsl.view.getViews(['V04J003001', 'V04J003001X01:09']),
    
    refs        : [{
        ref       : 'activeWindow',
        selector  : 'window'
    }],
    init: function() {
        var me = this;
        me.control({
            'v04j003001x02 button[action=add]':{
                click  :  function (field, el) {
                    Ext.widget('v04j003001x03')//.show()
                }
            },
            'v04j003001x03 textfield[name=roleName]':{
                afterrender: function(c) {
                    c.inputEl.on('dblclick', function() {
                        Ext.widget('v04j003001x04')
                    });
                }
            },
            'v04j003001x03 textfield[name=authorityName]':{
                afterrender: function(c) {
                    c.inputEl.on('dblclick', function() {
                        Ext.widget('v04j003001x07')
                    });
                }
            },
            'v04j003001x06':{
                itemdblclick:me.setRole
            },
            'v04j003001x09':{
                itemdblclick:me.setAuthority
            },
            'v04j003001x03 button[action=save]': {
                click           : this.onSave
            },
            'v04j003001x03 *' :{
                specialkey : function(field, el){
                    if (el.getKey() == Ext.EventObject.ENTER){
                        var button = field.up('window').down('button[action=save]');
                        button.fireEvent('click', button);                            
                    }
                }
            },
            'v04j003001x02': {
                itemdblclick : this.preUpdate
            },
            'v04j003001x01 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        this.filter(field, el, me)
                    }
                }
            }
        });
    },
    setRole : function(view, records){
        var form = Ext.getCmp('v04j003001x03').down('form').getForm();
        form.findField('roleId').setValue(records.data.id);
        form.findField('roleName').setValue(records.data.role);
        view.up('window').close();
    },
    setAuthority : function(view, records){
        var form = Ext.getCmp('v04j003001x03').down('form').getForm();
        form.findField('authorityId').setValue(records.data.id);
        form.findField('authorityName').setValue(records.data.authority);
        view.up('window').close();
    },
    onSave : function(button){
        var win = button.up('window'),
        form    = win.down('form'),
        record  = form.getRecord(),
        values  = form.getValues(),
        status  = 'win-statusbar-v04j003001x02',
        statusView  = 'win-statusbar-v04j003001x03';                    

        if (form.getForm().isValid()) { 
            if (form.getForm().isDirty()) { 
                Fsl.model.save('M04J003001', values, {
                    scope   : this,
                    success : function(model, operation) {
                        form.loadRecord(model);
                        Fsl.store.getStore('S04J003001').load();
                        Fsl.severity.info(status, operation);
                        Fsl.severity.info(statusView, operation);
                    },
                    failure : function(model, operation){
                        Fsl.severity.error(status, operation);
                        Fsl.severity.error(statusView, operation);
                        Fsl.security.recheck(operation);
                    }
                });
            } else {
                var message  = 'No change occured';
                Fsl.severity.warn(status, message);
                Fsl.severity.warn(statusView, message);
            }
        } else {
            var message  = 'Necessary Field Required';
            Fsl.severity.warn(status, message);
            Fsl.severity.warn(statusView, message);
        }     
    },
    preUpdate : function(model, records){
        var view    = Fsl.app.getWidget('V04J003001X03');  
        view.setTitle('CHANGES ROLE');
        var form    = view.down('form').getForm()
        form.loadRecord(records);
        form.findField('authorityName').setValue(records.getM04t002001().data.authority);
    //form.findField('username').setValue(records.getM04t001001().data.username);    
    },
    
    filter : function(field, el, me){    
        var form          =   field.up('window').down('form').getForm();
        if(form.isValid()){
            var role     =   form.findField('role').getValue(),
            authority    =   form.findField('authority').getValue(),
            filter       =   Fsl.store.getFilter('S04J003001');
            filter.add('role', role).add('authority', authority).load(1);
        }
    }
});
