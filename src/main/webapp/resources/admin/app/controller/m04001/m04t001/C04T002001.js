Ext.define(Fsl.app.getAbsController('C04T002001'), {
    extend      : 'Ext.app.Controller', 
    requires    : Fsl.app.getMergedRequires({
        apps: [],
        misc: []
    }),  
    views       : Fsl.view.getViews(['V04T002001', 'V04T002001X01:03']),
    
    refs        : [{
        ref       : 'activeWindow',
        selector  : 'window'
    }],
    init: function() {
        var me = this;
        me.control({
            'v04t002001x02 button[action=add]':{
                click  :  function (field, el) {
                    Ext.widget('v04t002001x03')//.show()
                }
            }, 
            'v04t002001x03 button[action=save]': {
                click           : this.onSave
            },
            'v04t002001x02': {
                itemdblclick : this.preUpdate
            },
            'v04t002001x01 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        this.filter(field, el, me)
                    }
                }
            }
        });
    },
    onSave            : function(button){
        var win = button.up('window'),
        form    = win.down('form'),
        record  = form.getRecord(),
        values  = form.getValues(),
        status  = 'win-statusbar-v04t002001x02',
        statusView  = 'win-statusbar-v04t002001x03';                    

        if (form.getForm().isValid()) { 
            if (form.getForm().isDirty()) { 
                Fsl.model.save('M04T002001', values, {
                    scope   : this,
                    success : function(model, operation) {
                        form.loadRecord(model);
                        Fsl.store.getStore('S04T002001').load();
                        Fsl.severity.info(status, operation);
                        Fsl.severity.info(statusView, operation);
                    },
                    failure : function(model, operation){
                        Fsl.severity.error(status, operation);
                        Fsl.severity.error(statusView, operation);
                        Fsl.security.recheck(operation);
                    }
                });
            } else {
                var message  = 'No change occured';
                Fsl.severity.warn(status, message);
                Fsl.severity.warn(statusView, message);
            }
        } else {
            var message  = 'Necessary Field Required';
            Fsl.severity.warn(status, message);
            Fsl.severity.warn(statusView, message);
        }     
    },
    preUpdate : function(model, records){
        var view    = Fsl.app.getWidget('v04t002001x03');  
        var form    = view.down('form').getForm()
        form.loadRecord(records);   
    },
    
    filter : function(field, el, me){    
        var form          =   field.up('window').down('form').getForm();
        if(form.isValid()){
            var idValue     =   form.findField('id').getValue(),
            authority       =   form.findField('authority').getValue(),
            filter          =   Fsl.store.getFilter('S04T002001');
            filter.add('id', idValue).add('authority', authority).load(1);
        }
    }
});
