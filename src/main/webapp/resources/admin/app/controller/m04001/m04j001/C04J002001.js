Ext.define(Fsl.app.getAbsController('C04J002001'), {
    extend      : 'Ext.app.Controller', 
    requires    : Fsl.app.getMergedRequires({
        apps: [],
        misc: []
    }),  
    views       : Fsl.view.getViews(['V04J002001', 'V04J002001X01:09']),
    
    refs        : [{
        ref       : 'activeWindow',
        selector  : 'window'
    }],
    init: function() {
        var me = this;
        me.control({
            'v04j002001x02 button[action=add]':{
                click  :  function (field, el) {
                    Ext.widget('v04j002001x03')//.show()
                }
            },
            'v04j002001x03 textfield[name=username]':{
                afterrender: function(c) {
                    c.inputEl.on('dblclick', function() {
                        Ext.widget('v04j002001x04')
                    });
                }
            },
            'v04j002001x06':{
                itemdblclick:me.setAccount
            },
            'v04j002001x03 textfield[name=roleName]':{
                afterrender: function(c) {
                    c.inputEl.on('dblclick', function() {
                        Ext.widget('v04j002001x07')
                    });
                }
            },
            'v04j002001x09':{
                itemdblclick:me.setRole
            },
            'v04j002001x03 button[action=save]': {
                click           : this.onSave
            },
            'v04j002001x02': {
                itemdblclick : this.preUpdate
            },
            'v04j002001x05 *':{
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        this.accountfilter(field, el, me)
                    }
                }
            },
            'v04j002001x06 button[action=clear]':{
                click  :  function (field, el) {
                    field.up('window').down('form').getForm().reset();
                    Fsl.store.getStore('S01T004003').load();
                }
            },
            'v04j002001x06 button[action=search]':{
                click  :  function (field, el) {
                    this.accountfilter(field, el, me)
                }
            },
            'v04j002001x01 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        this.filter(field, el, me)
                    }
                }
            }
        });
    },
    setAccount : function(view, records){
        var form = Ext.getCmp('v04j002001x03').down('form').getForm();
        form.findField('accountId').setValue(records.data.id);
        form.findField('username').setValue(records.data.username);
        view.up('window').close();
    },
    setRole: function(view, records){
        var form = Ext.getCmp('v04j002001x03').down('form').getForm();
        form.findField('roleId').setValue(records.data.id);
        form.findField('roleName').setValue(records.data.role);
        view.up('window').close();
    },
    onSave : function(button){
        var win = button.up('window'),
        form    = win.down('form'),
        record  = form.getRecord(),
        values  = form.getValues(),
        status  = 'win-statusbar-v04j002001x02',
        statusView  = 'win-statusbar-v04j002001x03';                    

        if (form.getForm().isValid()) { 
            if (form.getForm().isDirty()) { 
                Fsl.model.save('M04J002001', values, {
                    scope   : this,
                    success : function(model, operation) {
                        form.loadRecord(model);
                        Fsl.store.getStore('S04J002001').load();
                        Fsl.severity.info(status, operation);
                        Fsl.severity.info(statusView, operation);
                    },
                    failure : function(model, operation){
                        Fsl.severity.error(status, operation);
                        Fsl.severity.error(statusView, operation);
                        Fsl.security.recheck(operation);
                    }
                });
            } else {
                var message  = 'No change occured';
                Fsl.severity.warn(status, message);
                Fsl.severity.warn(statusView, message);
            }
        } else {
            var message  = 'Necessary Field Required';
            Fsl.severity.warn(status, message);
            Fsl.severity.warn(statusView, message);
        }     
    },
    preUpdate : function(model, records){
        var view    = Fsl.app.getWidget('V04J002001X03');  
        view.setTitle('CHANGES ROLE');
        var form    = view.down('form').getForm()
        form.loadRecord(records);
        form.findField('accountId').setValue(records.getM04t001001().data.id);
        form.findField('username').setValue(records.getM04t001001().data.username);    
    },
    accountfilter:function(field, el, me){    
        var form          =   field.up('window').down('form').getForm();
        if(form.isValid()){
            var idValue     =   form.findField('id').getValue(),
            userId          =   form.findField('userId').getValue(),
            branchId        =   form.findField('branchId').getValue(),
            username        =   form.findField('username').getValue(),
            enabled         =   form.findField('enabled').getValue(),
            accountExpired  =   form.findField('accountExpired').getValue(),
            accountLocked   =   form.findField('accountLocked').getValue(),
            passwordExpired =   form.findField('passwordExpired').getValue(),
            filter          =   Fsl.store.getFilter('S04T001003');
            filter.add('id', idValue).add('userId', userId).add('branchId', branchId).add('username', username).add('enabled', enabled).add('accountExpired', accountExpired).add('accountLocked', accountLocked).add('passwordExpired', passwordExpired).load(1);
        }
    },
    
    filter : function(field, el, me){    
        var form          =   field.up('window').down('form').getForm();
        if(form.isValid()){
            var username     =   form.findField('username').getValue(),
            roleName         =   form.findField('roleName').getValue(),
            filter           =   Fsl.store.getFilter('S04J002001');
            filter.add('username', username).add('roleName', roleName).load(1);
        }
    }
});
