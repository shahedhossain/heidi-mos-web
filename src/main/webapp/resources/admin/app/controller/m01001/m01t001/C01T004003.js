Ext.define(Fsl.app.getAbsController('C01T004003'), {
    extend      : 'Ext.app.Controller', 
    requires    : Fsl.app.getMergedRequires({
        apps: [],
        misc: []
    }),  
    views       : Fsl.view.getViews(['V01T004003','V01T004003X01:02']),
    refs        : [{
        ref       : 'activeWindow',
        selector  : 'window'
    }],
    init: function() {
        var me = this;
        this.control({
            'v01t004003x01 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        me.filter(field, el, me)
                    }
                }
            },
             'v01t004003x02 button[action=search]':{
                click  :  function (field, el) {
                    me.filter(field, el, me)
                }
            },
            'v01t004003x02 button[action=clear]':{
                click  :  function (field, el) {
                    field.up('window').down('form').getForm().reset();
                    var store = Fsl.store.getStore('S01T004004')
                    store.clearFilter();
                    store.load();
                }
            }
        });
    },
    filter:function(field, el, me){    
        var form          =   field.up('window').down('form').getForm();//'sv01t00200108'
        if(form.isValid()){
            var id     =   form.findField('id').getValue(),
            articleId       =   form.findField('articleId').getValue(),
            articleName     =   form.findField('articleName').getValue(),
            mbcode          =   form.findField('mbcode').getValue(),
            pbcode          =   form.findField('pbcode').getValue(),
            price           =   form.findField('price').getValue(),
            vat             =   form.findField('vat').getValue(),
            vandorName      =   form.findField('vandorName').getValue(),
            vandorId        =   form.findField('vandorId').getValue();
            var store       =   Fsl.store.getStore('S01T004004');
            store.clearFilter();
            var grid = Ext.getCmp('v01t004003x02');
            grid.store.clearFilter();
            articleId ? store.filter('articleId', articleId): null          
            if (articleName) {
                grid.store.filter({
                    filterFn: function(record) {
                        return record.getM01i006001().data.name == articleName                      
                    }
                });
            }
            store.load();
        }
    }
});