Ext.define(Fsl.app.getAbsController('C01I007001'), {
    extend      : 'Ext.app.Controller', 
    requires    : Fsl.app.getMergedRequires({
        apps: [],
        misc: []
    }),  
    views       : Fsl.view.getViews(['V01I007001', 'V01I007001X01:03']),
    refs        : [{
        ref       : 'activeWindow',
        selector  : 'window'
    }],
    init        : function() {  
        var me          = this;
        me.control({
            'v01i007001x02 button[action=add]':{
                click  :  function (field, el) {
                    Ext.widget('v01i007001x03').show()
                }
            },
            'v01i007001x02 button[action=search]':{
                click  :  function (field, el) {
                    me.filter(field, el, me)
                }
            },
            'v01i007001x02 button[action=clear]':{
                click  :  function (field, el) {
                    field.up('window').down('form').getForm().reset();
                    Fsl.store.getStore('S01I007001').load();
                }
            },
            'v01i007001x02'     :   {
                itemclick       :   me.infoOnStatus,
                itemdblclick    :   this.preUpdate
            },
            'v01i007001x03 button[action=save]': {
                click        : this.onSave
            },
            'v01i007001x03 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        var button = field.up('window').down('button[action=save]');
                        button.fireEvent('click', button);                            
                    }
                }
            },
            'v01i007001x01 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        me.filter(field, el, me)
                    }
                }
            }
        });
    },
    onSave            : function(button){
        var win = button.up('window'),
        form    = win.down('form'),
        record  = form.getRecord(),
        values  = form.getValues(),
        status  = 'win-statusbar-v01i007001x02',
        statusView  = 'win-statusbar-v01i007001x03';                    

        if (form.getForm().isValid()) { 
            if (form.getForm().isDirty()) { 
                Fsl.model.save('M01I007001', values, {
                    scope   : this,
                    success : function(model, operation) {
                        form.loadRecord(model);
                        Fsl.severity.info(status, operation);
                        Fsl.severity.info(statusView, operation);
                        Fsl.store.getStore('S01I007001').load();
                    },
                    failure : function(model, operation){
                        Fsl.severity.error(status, operation);
                        Fsl.severity.error(statusView, operation);
                        Fsl.security.recheck(operation);
                    }
                });
            } else {
                var message  = 'No change occured';
                Fsl.severity.warn(status, message);
                Fsl.severity.warn(statusView, message);
            }
        } else {
            var message  = 'Necessary Field Required';
            Fsl.severity.warn(status, message);
            Fsl.severity.warn(statusView, message);
        }     
    },
    infoOnStatus      : function(model, operation){
        var status  = 'win-statusbar-v01i007001x02'
        Fsl.severity.info(status, operation.data.name);      
    },
    preUpdate         : function(model, records){
        var view   = Fsl.app.getWidget('V01I007001X03');  
        view.setTitle('CATEGORY UPDATE');
        var form = view.down('form').loadRecord(records); 
        var btn    = view.down('button[text=Save]')
        btn.setText('Update');
        btn.setIcon(Fsl.route.getIcon('TB01001/EDT01004.png'));        
    },    
    filter            :function(field, el, me){    
        var form            =   field.up('window').down('form').getForm();
        if(form.isValid()){
            var idValue     =   form.findField('id').getValue(),
            name            =   form.findField('name').getValue(),                        
            filter      = Fsl.store.getFilter('S01I007001');
            filter.add('id', idValue).add('name', name).load(1);
        }
    }
});


