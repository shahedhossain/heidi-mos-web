Ext.define(Fsl.app.getAbsController('C01T015001'), {
    extend      : 'Ext.app.Controller', 
    requires    : Fsl.app.getMergedRequires({
        apps: [],
        misc: []
    }),  
    views       : Fsl.view.getViews(['V01T015001','V01T015001X01:5']),
    refs        : [{
        ref       : 'activeWindow',
        selector  : 'window'
    }],
    init: function() { 
        var me = this;
        me.control({
            'v01t015001x01 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        me.filter(field, el, me)
                    }
                }
            },
            'v01t015001x02 button[action=search]':{
                click  :  function (field, el) {
                    this.filter(field, el, me)
                }
            },
            'v01t015001x02 button[action=clear]':{
                click  :  function (field, el) {
                    field.up('window').down('form').getForm().reset();
                    Fsl.store.getStore('S01T015001').load(1);
                }
            },
            'v01t015001x02' :{
                itemdblclick : me.getReturn
            },
            'v01t015001x04 textfield[name=returnId]':{
                specialkey      : function(field, el){
                    if (el.getKey() == el.ENTER) { 
                        me.getReturnItems(field, el, me)
                    }
                }
            },
            'v01t015001x03 button[action=receiveItem]':{
                click  :  me.onSave
            }
        })
    },
    filter : function(field, el, me){    
        var form            =   field.up('window').down('form').getForm();
        if(form.isValid()){
            var idValue      =   form.findField('id').getValue(),
            transferId       =   form.findField('transferId').getValue(),
            receiveId        =   form.findField('receiveId').getValue(),
            fromBranchId     =   form.findField('fromBranchId').getValue(),
            branchName       =   form.findField('branchName').getValue(),
            entryDate        =   form.findField('entryDate').getValue(),
            filter           =   Fsl.store.getFilter('S01T015001');
            filter.add('id', idValue).add('transferId', transferId).add('receiveId', receiveId).add('fromBranchId', fromBranchId).add('branchName', branchName).add('entryDate', entryDate).load(1);
        }
    },
    getReturn : function(model, records, item, index,  event){   
        var returnId = records.data.id;
        var win =Ext.widget('v01t015001x03',{
            animateTarget  : 'v01t015001x03'
        });       
        var field = win.down('form').getForm().findField('returnId')
        field.setValue(returnId); 
        event.keyCode = event.ENTER;
        field.fireEvent('specialkey',field, event);   
        
        var status = records.data.returnItem_status
        if(status =="Received"){
            var btn  = win.down('button[action=receiveItem]')
            btn.hide()
        }
    },
    getReturnItems : function(field, el, me){
        var win     = field.up('window'),
        form        = win.down('form').getForm(),        
        values      = form.getValues(),
        rItemStore  = Fsl.store.getStore('S01T016001'); 
        if(values.returnId > 0){  
            Fsl.model.load('M01T015001', {
                id:values.returnId
            }, {
                scope   : this,
                success : function(records, operation, success) {  
                    rItemStore.load();
                    form.loadRecord(records);
                    form.findField('fromBranchName').setValue(records.getM01i015002().data.name); 
                    /*var btn  = win.down('button[action=save]')
                    btn.setText('UPDATE');
                    btn.setIcon(Fsl.route.getIcon('TB01001/EDT01004.png')); */
                    win.down('grid').getEl().unmask();
                },
                failure :function(model, operation){
                    rItemStore.reload();
                    win.down('grid').getEl().unmask();
                    Fsl.security.recheck(operation);
                }
            });
        }
    },
    onSave   : function(button){
        var form      = button.up('window').down('form').getForm(),
        values        = form.getValues(),
        statusView    = 'win-statusbar-v01t015001x05',message; 
        values.id     = values.returnId
        if(form.isValid()){ 
            button.setDisabled(true);
            Fsl.model.save('M01T015001', values, {
                scope   : this,
                success : function(records, operation){
                    var text    = operation.response.responseText,
                    json        = Ext.decode(text);                    
                    Fsl.store.getStore('S01T015001').load();                 
                    Fsl.store.getStore('S01T016001').load();
                    button.hide();  
                    Fsl.severity.info(statusView, json.message);
                },                  
                failure:function(model, operation){
                    message = "Unable To Save"
                    Fsl.severity.error(statusView, operation.error);
                    button.enable();
                    Fsl.security.recheck(operation);
                } 
            });
        }else{         
            message  = 'Necessary Field Required';
            Fsl.severity.warn(statusView, message);
        }
    }
});