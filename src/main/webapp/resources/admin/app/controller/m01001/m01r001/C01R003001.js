Ext.define(Fsl.app.getAbsController('C01R003001'), {
    extend      : 'Ext.app.Controller', 
    requires    : Fsl.app.getMergedRequires({
        apps: [],
        misc: []
    }),  
    views         : Fsl.view.getViews(['V01R003001', 'V01R003001X01:03']),
    refs          : [{
        ref       : 'activeWindow',
        selector  : 'window'
    }],
    init        : function() {  
        var me          = this;
        me.control({
            'v01r003001 button[text=TODAY]':{
                click  :  function (field, el) {
                    var win = Ext.widget('v01r003001x01')
                    win.removeAll();
                    win.add({
                        xtype   :'v01r003001x03',
                        border  : false
                    });
                    win.setTitle('TODAY REPORT');   
                    var form = win.down('form').getForm();
                    form.findField('MONTH').hide();
                    var TODAYbtn  = win.down('button[action=TODAY]');
                    TODAYbtn.show();
                }
            },
            'v01r003001x01 button[action=TODAY]':{
                click  :  function (field, el) {
                    var form    = field.up('window').down('form').getForm();
                    if(form.isValid()){
                        var values  = form.getValues(),
                        today       = new Date(),
                        date        = Ext.Date.format(today,'Y-m-d H:i:s'),
                        FROM_DATE   = date.toString(),
                        TO_DATE     = date.toString(),
                        branchId    = values.branchId;
                        me.pdfreportByDate(FROM_DATE, TO_DATE, branchId)
                    }
                }
            },
            'v01r003001 button[text=ANY_DAY]':{
                click  :  function (field, el) {
                    var win = Ext.widget('v01r003001x01')
                    win.removeAll();
                    win.add({
                        xtype   :'v01r003001x03',
                        border  : false
                    });
                    win.setTitle('DAILY REPORT');                  
                    var Any_Day_btn  = win.down('button[action=ANY_DAY]');
                    Any_Day_btn.show();
                }            
            },            
            'v01r003001x01 button[action=ANY_DAY]':{
                click  :  function (field, el) {
                    var form            = field.up('window').down('form').getForm();
                    if(form.isValid()){
                        var values  = form.getValues(),
                        now                 = values.MONTH ? Ext.Date.parse(values.MONTH, 'Y-m-d H:i:s') :new Date(),
                        setValueInEmptyFld  = values.MONTH ? '' :form.findField('MONTH').setValue(Ext.Date.format(new Date(),'Y-m-d')),
                        formatInitDate       = Ext.Date.format(new Date(now), 'Y-m-d H:i:s'),
                        FROM_DATE            = formatInitDate.toString(), 
                        addwholeDay         = now.getTime()+24 * 60 * 60 * 1000-1,
                        formatendDate       = Ext.Date.format(new Date(addwholeDay), 'Y-m-d H:i:s'),
                        TO_DATE             = formatendDate.toString(),
                        branchId            = values.branchId;
                        me.pdfreportByDate(FROM_DATE, TO_DATE, branchId)
                    }
                }
            },
            'v01r003001 button[text=LAST_7_DAYS]':{
                click  :  function (field, el) {
                    var win = Ext.widget('v01r003001x01')
                    win.removeAll();
                    win.add({
                        xtype   :'v01r003001x03',
                        border  : false
                    });
                    win.setTitle('LAST 7 DAYS REPORT'); 
                    var form = win.down('form').getForm();
                    form.findField('MONTH').hide();
                    var LAST_7_DAYSbtn  = win.down('button[action=LAST_7_DAYS]');
                    LAST_7_DAYSbtn.show();
                }
            },
            'v01r003001x01 button[action=LAST_7_DAYS]':{
                click  :  function (field, el) {
                    var form    = field.up('window').down('form').getForm();
                    if(form.isValid()){
                        var values  = form.getValues(),
                        today       =  new Date(),
                        date        =  Ext.Date.format(today,'Y-m-d H:i:s'),
                        TO_DATE     =  date.toString(),
                        lastDate    =  today.setDate(today.getDate()-7),
                        date1       =  Ext.Date.format(new Date(lastDate), 'Y-m-d H:i:s'),
                        FROM_DATE   =  date1.toString(),                    
                        branchId    = values.branchId;
                        me.pdfreportByDate(FROM_DATE, TO_DATE, branchId)
                    }
                }
            },
            'v01r003001 button[text=LAST_30_DAYS]':{
                click  :  function (field, el) {
                    var win = Ext.widget('v01r003001x01')
                    win.removeAll();
                    win.add({
                        xtype   :'v01r003001x03',
                        border  : false
                    });
                    win.setTitle('LAST 7 DAYS REPORT'); 
                    var form = win.down('form').getForm();
                    form.findField('MONTH').hide();
                    var LAST_7_DAYSbtn  = win.down('button[action=LAST_30_DAYS]');
                    LAST_7_DAYSbtn.show(); 
                }
            },
            'v01r003001x01 button[action=LAST_30_DAYS]':{
                click  :  function (field, el) {
                    var form    =  field.up('window').down('form').getForm();
                    if(form.isValid()){
                        var values  = form.getValues(),
                        today       =  new Date(),
                        date        =  Ext.Date.format(today,'Y-m-d H:i:s'),
                        TO_DATE     =  date.toString(),
                        lastMonth   =  today.setMonth(today.getMonth()-1),
                        date1       =  Ext.Date.format(new Date(lastMonth), 'Y-m-d H:i:s'),
                        FROM_DATE   =  date1.toString(),
                        branchId    = values.branchId;
                        me.pdfreportByDate(FROM_DATE, TO_DATE, branchId)
                    }
                }
            },
            'v01r003001 button[text=DATE_TO_DATE]':{
                click  :  function (field, el) {
                    var win = Ext.widget('v01r003001x01');
                    var DATE_TO_DATEbtn   = win.down('button[action=DATE_TO_DATE]')
                    DATE_TO_DATEbtn.show();
                }
            },
            'v01r003001x01 button[action=DATE_TO_DATE]':{
                click  :  function (field, el) {
                    var form    = field.up('window').down('form').getForm();
                    if(form.isValid()){
                        var values  = form.getValues(),
                        branchId    = (values.branchId).toString(),                        
                        today       = new Date(),
                        formateDate = Ext.Date.format(today,'Y-m-d H:i:s');
                        if(values.FROM_DATE && values.TO_DATE){
                            var toDateObj   = Ext.Date.parse(values.TO_DATE, 'Y-m-d H:i:s'),
                            addwholeDay     = toDateObj.getTime()+24 * 60 * 60 * 1000-1,
                            formatDate      = Ext.Date.format(new Date(addwholeDay), 'Y-m-d H:i:s'),
                            FROM_DATE       = (values.FROM_DATE).toString(),
                            TO_DATE         = formatDate.toString();
                            me.pdfreportByDate(FROM_DATE, TO_DATE, branchId)
                        }else if(values.FROM_DATE){
                            FROM_DATE   = (values.FROM_DATE).toString(),
                            TO_DATE     = formateDate.toString();
                            form.findField('TO_DATE').setValue(formateDate);
                            me.pdfreportByDate(FROM_DATE, TO_DATE, branchId)
                        }else{
                            FROM_DATE   = formateDate.toString(),
                            TO_DATE     = formateDate.toString(),                            
                            form.findField('FROM_DATE').setValue(Ext.Date.format(new Date(),'Y-m-d'));
                            form.findField('TO_DATE').setValue(formateDate); 
                            me.pdfreportByDate(FROM_DATE, TO_DATE, branchId)
                        }                    
                    }
                }
            },
            'v01r003001x01 button[text=CLEAR]':{
                click  :  function (field, el) {
                    field.up('window').down('form').getForm().reset();
                }
            },
            'v01r003001 button[text=MONTHLY]':{
                click  :  function (field, el) {
                    var win = Ext.widget('v01r003001x01')
                    win.removeAll();
                    win.add({
                        xtype   :'v01r003001x03',
                        border  : false
                    });
                    win.setTitle('MONTHLY REPORT');
                    var MONTHLYbtn   = win.down('button[action=MONTHLY]')
                    MONTHLYbtn.show();
                }
            },
            'v01r003001x01 button[action=MONTHLY]':{
                click  :  function (field, el) {
                    var form  = field.up('window').down('form').getForm();
                    if(form.isValid()){
                        var values          = form.getValues(),
                        branchId            = values.branchId,
                        now                 = values.MONTH ? Ext.Date.parse(values.MONTH, 'Y-m-d H:i:s') :new Date(),
                        setValueInEmptyFld  = values.MONTH ? '' :form.findField('MONTH').setValue(Ext.Date.format(new Date(),'Y-m-d')),
                        firstDayOfTheMonth  = now.setDate(1),
                        formatFirstDate     = Ext.Date.format(new Date(firstDayOfTheMonth), 'Y-m-d H:i:s'),
                        firstDay            = formatFirstDate.toString(),                    
                        lastDayOfTheMonth   = new Date(1900+now.getYear(), now.getMonth()+1, 0),
                        addwholeDay         = lastDayOfTheMonth.getTime()+24 * 60 * 60 * 1000-1,
                        formatDate          = Ext.Date.format(new Date(addwholeDay), 'Y-m-d H:i:s'),
                        lastDay             = formatDate.toString();
                        me.pdfreportByDate(firstDay, lastDay, branchId)
                    }
                }
            },
            'v01r003001 button[text=YEARLY]':{
                click  :  function (field, el) {
                    var win = Ext.widget('v01r003001x01')
                    win.removeAll();
                    win.add({
                        xtype   :'v01r003001x03',
                        border  : false
                    });
                    win.setTitle('YEARLY REPORT');                    
                    var YEARLYbtn   = win.down('button[action=YEARLY]')
                    YEARLYbtn.show();
                }
            },
            'v01r003001x01 button[action=YEARLY]':{
                click  :  function (field, el) {
                    var form            = field.up('window').down('form').getForm();
                    if(form.isValid()){
                        var values  = form.getValues(),
                        branchId            = values.branchId,
                        now                 = values.MONTH ? Ext.Date.parse(values.MONTH, 'Y-m-d H:i:s') :new Date(),
                        setValueInEmptyFld  = values.MONTH ? '' :form.findField('MONTH').setValue(Ext.Date.format(new Date(),'Y-m-d')),
                        firstMonthOfTheYear = new Date(now.getFullYear(), 1, 0),
                        formatFirstDate     = Ext.Date.format(new Date(firstMonthOfTheYear), 'Y-m-d H:i:s'),
                        firstDay            = formatFirstDate.toString(),
                    
                        lastDayOfTheYear  = new Date(now.getFullYear(), 12, 0),
                        addwholeDay         = lastDayOfTheYear.getTime()+24 * 60 * 60 * 1000-1,
                        formatDate          = Ext.Date.format(new Date(addwholeDay), 'Y-m-d H:i:s'),
                        lastDay             = formatDate.toString();
                        me.pdfreportByDate(firstDay, lastDay, branchId)
                    }
                }
            },
             'v01r003001x03 textfield[name=branchName]':{
                afterrender: function(c) {
                    c.inputEl.on('dblclick', function() {
                        var win = Ext.widget('v01r015001x01')
                        win.removeAll();
                        win.add({
                            xtype   :'v01r015001x02',
                            border  : false
                        });
                        win.add({
                            xtype   :'v01r015001x05',
                            border  : false
                        });
                    });
                }
            },
            'v01r015001x05'     :   {                
                itemdblclick    :   this.setBranch
            },
            'v01r003001x02 textfield[name=branchName]':{
                afterrender: function(c) {
                    c.inputEl.on('dblclick', function() {
                        var win = Ext.widget('v01r015001x01')
                        win.removeAll();
                        win.add({
                            xtype   :'v01r015001x02',
                            border  : false
                        });
                        win.add({
                            xtype   :'v01r015001x08',
                            border  : false
                        });
                    });
                }
            },
            'v01r015001x08'     :   {                
                itemdblclick    :   this.setBranchForDayToDay
            }
        });
    }, 
    setBranchForDayToDay : function(view, records){
        var form = Ext.getCmp('v01r003001x02').getForm();
        form.findField('branchId').setValue(records.data.id);
        form.findField('branchName').setValue(records.data.name);
        view.up('window').close();
    },
    setBranch  : function(view, records){
        var form = Ext.getCmp('v01r003001x03').getForm();
        form.findField('branchId').setValue(records.data.id);
        form.findField('branchName').setValue(records.data.name);
        view.up('window').close();
    },
    pdfreportByDate : function(FROM_DATE, TO_DATE, branchId){      
        if(FROM_DATE != null){
            var body = Ext.getBody();
            var frame = body.createChild({
                tag:'iframe',
                cls:'x-hidden',
                id:'iframe',
                name:'iframe'
            });
            var form = body.createChild({
                tag:'form',
                cls:'x-hidden',
                id:'form',
                method:'post',
                action: Fsl.route.getAction('c01t014001/reportByDate/'),
                target:'iframe'
            });
        
            form.createChild({
                tag:'input',
                name:'FROM_DATE',
                type:'hidden',
                value:FROM_DATE
            });
            form.createChild({
                tag:'input',
                name:'TO_DATE',
                type:'hidden',
                value:TO_DATE
            });
            form.createChild({
                tag:'input',
                name:'branchId',
                type:'hidden',
                value:branchId
            });
        
            form.dom.submit();      
        }else{
            Ext.Msg.alert("Sorry","Invoice is not created, So PDF can't be generated !!!")
        }
    }
});


