Ext.define(Fsl.app.getAbsController('C01I000000'), {
    extend      : 'Ext.app.Controller', 
    requires    : Fsl.app.getMergedRequires({
        apps: [],
        misc: []
    }),  
    views       : Fsl.view.getViews([]),
    refs        : [{
        ref       : 'activeWindow',
        selector  : 'window'
    }],
    init        : function() {  
        var me          = this;
        Fsl.C01I000000  = this;
        me.control({
            
            });
    },
    setValueInfieldContainer :function(records, operation, form){  
        form.findField('articleId') .setValue(records.getM01i006001().data.id);
        form.findField('articleName') .setValue(records.getM01i006001().data.name);
        form.findField('discount')    .setValue(records.getM01t010001().data.discount);
        form.findField('price')       .setValue(records.getM01i012001().data.price);
        form.findField('vat')         .setValue(records.getM01i013001().data.vat);
                
        form.findField('priceId')     .setValue(records.getM01i012001().data.id);              
        form.findField('discountId')  .setValue(records.getM01t010001().data.id);
        form.findField('vatId')       .setValue(records.getM01i013001().data.id);
    },
    clearFieldContainer : function(preform, containerId){     
        var fieldContainer =preform.down(containerId);
        fieldContainer.items.each(function(f) {
            if (Ext.isFunction(f.reset)) {
                f.reset();
            }
        });
        preform.getForm().findField('articleId').setValue('').focus(true);
    },
    showMsg:function(dbQty, form){
        Ext.MessageBox.show({
            title       : 'Quantity',
            msg         : 'Quantity &nbsp'+dbQty,
            icon        : Ext.Msg.INFO,
            buttons     : Ext.MessageBox.YESNO,
            buttonText  :{ 
                yes: "Definitely!", 
                no : "No chance!" 
            },
            fn: function(btn, dbQty){
                setTimeout(function(){
                    if(btn == 'yes'){
                        form.findField('quantity').focus(true);                   
                    }
                }, 200);
            }            
        });
        setTimeout(function(){           
            Ext.MessageBox.hide();
            setTimeout(function(){  
                form.findField('quantity').setValue(dbQty).focus(true); 
            },200);
        }, 2000);
    },
    setItemSubTotal :function(field, el, me){
        var form    = field.up('window').down('form').getForm(),
        data        = form.getValues(), 
        rate        = parseFloat(data.price), 
        discount    = parseFloat((rate *(data.discount))/100),
        vat         = parseFloat((rate - discount)*((data.vat)/100)),
        quantity    = parseFloat(data.quantity),
        st          = parseFloat(rate-discount+vat)* quantity,
        lst         = st ? st : 0;      
        form.findField('individualTotal').setValue(lst); 
    },
    onDblClickSetItemSubTotal : function(records, operation, form){
        var rate = parseFloat(records.getM01i012001().data.price),
        discount = parseFloat((rate *(records.getM01t010001().data.discount))/100),
        vat      = parseFloat((rate - discount)*((records.getM01i013001().data.vat)/100)),
        quantity = parseFloat(form.findField('quantity').getValue()),
        st       = (rate-discount+vat)*quantity;
        form.findField('individualTotal').setValue(st);
    }
});


