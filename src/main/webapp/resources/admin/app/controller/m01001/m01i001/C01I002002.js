Ext.define(Fsl.app.getAbsController('C01I002002'), {
    extend      : 'Ext.app.Controller', 
    requires    : Fsl.app.getMergedRequires({
        apps: [],
        misc: []
    }),  
    views       : Fsl.view.getViews(['V01I002002', 'V01I002002X01:8']),
    refs        : [{
        ref       : 'activeWindow',
        selector  : 'window'
    }],
    init        : function() {  
        var me = this;
        me.control({            
            'v01i002002x02'     :   { 
                itemclick       :   me.infoOnStatus,
                itemdblclick    :   me.getCateByManu
            },
            'v01i002002x05 button[action=add]':{
                click  :  function (field, el) {
                    Fsl.app.getWidget('v01i002002x06').show()
                }
            },
            'v01i002002x01 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        me.filter(field, el, me)
                    }
                }
            },
            'v01i002002x02 button[action=search]':{
                click  :  function (field, el) {
                    me.filter(field, el, me)
                }
            },
            'v01i002002x02 button[action=clear]':{
                click  :  function (field, el) {
                    field.up('window').down('form').getForm().reset();
                    me.filter(field, el, me)
                }
            },
            'v01i002002x07 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        me.filterForCat(field, el, me)
                    }
                }
            },
            'v01i002002x08 button[action=search]':{
                click  :  function (field, el) {
                    me.filterForCat(field, el, me)
                }
            },
            'v01i002002x08 button[action=clear]':{
                click  :  function (field, el) {
                    field.up('window').down('form').getForm().reset();
                    me.filterForCat(field, el, me)
                }
            },
            
            'v01i002002x08'     :   {   
                itemclick       :   me.v01i002002x08Status,
                itemdblclick    :   me.addCateInManu           
            },
            'v01i002002x05'     :   {
                itemclick       :   me.v01i002002x05Status
            }
        });
    },
    infoOnStatus      : function(model, operation){
        var status      = 'win-statusbar-v01i002002x02'
        Fsl.severity.info(status, operation.data.name);      
    },
    v01i002002x08Status  :   function(model, operation){
        var status      = 'win-statusbar-v01i002002x08'
        Fsl.severity.info(status, operation.data.name);         
    },
    v01i002002x05Status  :   function(model, operation){
        var status      = 'win-statusbar-v01i002002x05'
        Fsl.severity.info(status, operation.data.name);      
    },
    
    getCateByManu       : function(model, records){        
        var manuId      = records.data.id,
        manuInfo        = this.getCatItemByManu(manuId, model, records);
        Fsl.app.getWidget('v01i002002x03').down('form').loadRecord(records);
    },    
    getCatItemByManu    :function(manuId, model, records){  
        Fsl.model.load('M01I002001', {
            id:manuId
        }, {        
            success: function (records, operation, success) {                
                var store = Fsl.store.getStore('S01I001002').load();
                records.m01i001001s().each(function(cat){                   
                    store.add(cat.data);
                });         
            },
            failure :function(response, options){
                //Fsl.security.recheck(response);
            }
        });
    },
    
    filter            :function(field, el, me){    
        var form            =   field.up('window').down('form').getForm();
        if(form.isValid()){
            var idValue     =   form.findField('id').getValue(),
            name            =   form.findField('name').getValue(),
            filter          =   Fsl.store.getFilter('S01I002003');
            filter.add('id', idValue).add('name', name).load(1);  
        }
    },    
    addCateInManu :function(model, records){          
        var values = Ext.getCmp('v01i002002x04').getForm().getValues(),
        manuId      = values.id,
        catId       = records.data.id;
        Ext.Ajax.request({
            scope      : this,
            url        : Fsl.route.getAction('C01i002001/catInsertInManu/'),
            params     : {
                manuId  : manuId,
                catId   : catId
            },
            timeout : 60000,
            success: function(response, options){
                var json   =  Ext.decode(response.responseText),
                status     = 'win-statusbar-v01i002002x05';
                if('data' in json && json.success == true){
                    var data    =  json.data[0].m01i001001s;                     
                    Fsl.store.getStore('S01I001002').loadData(data);
                    me.up('window').close();
                }
                Fsl.severity.info(status, response);
                Fsl.security.recheck(response);                                                             
            },
            failure :function(response, options){
                Fsl.security.recheck(response);
            }
        });
    },
    filterForCat : function(field, el, me){    
        var form            =   field.up('window').down('form').getForm();
        if(form.isValid()){
            var idValue     =   form.findField('id').getValue(),
            name            =   form.findField('name').getValue(),
            filter          =   Fsl.store.getFilter('S01I001004');
            filter.add('id', idValue).add('name', name).load(1); 
        }
    }    
});
