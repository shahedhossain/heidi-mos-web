Ext.define(Fsl.app.getAbsController('C01T009001'), {
    extend      : 'Ext.app.Controller', 
    requires    : Fsl.app.getMergedRequires({
        apps: [],
        misc: []
    }),  
    views       : Fsl.view.getViews(['V01T009001', 'V01T009001X01:06']),
    refs        : [{
        ref       : 'activeWindow',
        selector  : 'window'
    }],
    init: function() {
        var me = this;

        me.control({
            'v01t009001x03 button[action = save]':{
                click:this.onSave
            },
           
            'v01t009001x02':{
                itemdblclick:this.preUpdate
            },
            'v01t009001x02 button[action=add]':{
                click  :  function (field, el) {
                    Ext.widget('v01t009001x03').show(); 
                }
            },
            'v01t009001x02 button[action=search]':{
                click  :  function (field, el) {
                    this.filter(field, el, me)
                }
            },
            'v01t009001x02 button[action=clear]':{
                click  :  function (field, el) {
                    field.up('window').down('form').getForm().reset();
                    Fsl.store.getStore('S01T009001').load();
                }
            },
            'v01t009001x01 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        this.filter(field, el, me)
                    }
                }
            },
            'v01t009001x03 textfield[name=branchName]':{
                afterrender: function(c) {
                    c.inputEl.on('dblclick', function() {
                        Ext.widget('v01t009001x04').show()      
                    });
                }
            },
            'v01t009001x05 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        me.branchFilter(field, el,me)
                    }
                }
            },
            'v01t009001x06 button[action=search]':{
                click  :  function (field, el) {
                    me.branchFilter(field, el,me)
                }
            },
            'v01t009001x06 button[action=clear]':{
                click  :  function (field, el) {
                    field.up('window').down('form').getForm().reset();
                    me.branchFilter(field, el,me)
                }
            },
            'v01t009001x06':{
                itemdblclick:me.setBranchInUser
            }
        });
    },
    setBranchInUser :function(view, records){
        var form = Ext.getCmp('v01t009001x03').down('form').getForm();
        form.findField('branchId').setValue(records.data.id);
        form.findField('branchName').setValue(records.data.name);
        view.up('window').close();
    },
    preUpdate : function(model, records){
        var win     =  Ext.widget('v01t009001x03'),
        frm         =  win.down('form'),
        form        =  frm.getForm();
        win.setTitle('User Update');
        form.loadRecord(records);           
        form.findField('branchName').setValue(records.getM01i015001().data.name);
        var btn     = win.down('button[text=Save]');
        btn.setText('Update');
        btn.setIcon(Fsl.route.getIcon('TB01001/EDT01004.png'));
    },   
    onSave      : function(button){
        var win = button.up('window'),
        form    = win.down('form'),
        record  = form.getRecord(),
        values  = form.getValues(),
        status  = 'win-statusbar-v01t009001x02',
        statusView  = 'win-statusbar-v01t009001x03';                    

        if (form.getForm().isValid()) { 
            if (form.getForm().isDirty()) { 
                Fsl.model.save('M01T009001', values, {
                    scope   : this,
                    success : function(model, operation) {
                        form.loadRecord(model);
                        Fsl.severity.info(status, operation);
                        Fsl.severity.info(statusView, operation);
                        Fsl.store.getStore('S01T009001').load();
                    },
                    failure : function(model, operation){
                        Fsl.severity.error(status, operation);
                        Fsl.severity.error(statusView, operation);
                        Fsl.security.recheck(operation);
                    }
                });
            } else {
                var message  = 'No change occured';
                Fsl.severity.warn(status, message);
                Fsl.severity.warn(statusView, message);
            }
        } else {
            var message  = 'Necessary Field Required';
            Fsl.severity.warn(status, message);
            Fsl.severity.warn(statusView, message);
        }     
    },    
    filter:function(field, el, me){    
        var form          =   field.up('window').down('form').getForm();//'v01t009001x01'
        if(form.isValid()){
            var idValue     =   form.findField('id').getValue(),
            contact         =   form.findField('contact').getValue(),
            name            =   form.findField('name').getValue(),
            branchId        =   form.findField('branchId').getValue(),
            branchName      =   form.findField('branchName').getValue(),
            location        =   form.findField('location').getValue(),
            filter          =   Fsl.store.getFilter('S01T009001');
            filter.add('id', idValue).add('name', name).add('contact', contact).add('branchId', branchId).add('branchName', branchName).add('location', location).load(1);
        }
    },    
    branchFilter:function(field, el,me){    
        var form        =   field.up('window').down('form').getForm();//'sv01i01500104'
        if(form.isValid()){
            var idValue     = form.findField('id').getValue(),
            name        = form.findField('name').getValue(),
            location    = form.findField('location').getValue(),
            filter      =   Fsl.store.getFilter('S01I015008');                 
            filter.add('id', idValue).add('name', name).add('location', location).load(1);
        }
    } 
});