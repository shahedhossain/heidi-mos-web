Ext.define(Fsl.app.getAbsController('C01I012001'), {
    extend      : 'Ext.app.Controller', 
    requires    : Fsl.app.getMergedRequires({
        apps: [],
        misc: []
    }),  
    views       : Fsl.view.getViews(['V01I012001', 'V01I012001X01:06']),
    refs        : [{
            ref       : 'activeWindow',
            selector  : 'window'
        }],
    init: function() {
        var me = this;
        me.control({
            'v01i012001x01 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        me.filter(field, el,me)
                    }
                }
            },
            'v01i012001x02 button[action=add]':{
                click  :  function (field, el) {                      
                    var win = Ext.widget('v01i012001x03');
                    win.show(); 
                    var form = win.down('form').getForm();
                    form.findField('date').setMinValue(new Date())                    
                }
            },
            'v01i012001x02 button[action=search]':{
                click  :  function (field, el) {
                    me.filter(field, el,me)
                }
            },
            'v01i012001x02 button[action=clear]':{
                click  :  function (field, el) {
                    field.up('window').down('form').getForm().reset();
                    Fsl.store.getStore('S01I012001').loadPage(1);
                }
            },
            'v01i012001x02' : {
                itemdblclick:this.preUpdate
            },
            'v01i012001x03 button[action = save]':{
                click:this.onSave
            },
            'v01i012001x03 textfield[name=articleName]':{
                afterrender: function(c) {
                    c.inputEl.on('dblclick', function() {
                        Ext.widget('v01i012001x04').show()      
                    });
                }
            },
            'v01i012001x03 *':{//numberfield[name = purchase_price]
               keyup : this.grossTotal
//               keyup: function(c) {
//                  console.log("hhhhhhhh")
//                }
            },
            'v01i012001x05 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        me.articleFilterForPrice(field, el, me)
                    }
                }
            },
            'v01i012001x06 button[action=search]':{
                click  :  function (field, el) {
                    me.articleFilterForPrice(field, el, me)
                }
            },
            'v01i012001x06 button[action=clear]':{
                click  :  function (field, el) {
                    field.up('window').down('form').getForm().reset();
                    Fsl.store.getStore('S01I006004').loadPage(1);
                }
            },
            'v01i012001x06':{
                itemdblclick:this.setArticleIntoPrice         
            }

        });
    },
    setArticleIntoPrice :function(view, records){
        var form = Ext.getCmp('v01i012001x03').down('form').getForm();  
        form.findField('articleId').setValue(records.data.id);
        form.findField('articleName').setValue(records.data.name);
        view.up('window').close();
    },
    preUpdate : function(model, records){
        var win     =  Ext.widget('v01i012001x03'),
        frm         =  win.down('form'),
        form        =  frm.getForm();
        win.setTitle('PRICE UPDATE');
        form.loadRecord(records);
        form.findField('articleName').setValue(records.getM01i006001().data.name);
        form.findField('date').setMinValue(records.data.date);    
        var btn         = win.down('button[text=Save]');
        btn.setText('Update');
        btn.setIcon(Fsl.route.getIcon('TB01001/EDT01004.png'));
    },
   
    onSave   : function(button){
        var win     = button.up('window'),
        form        = win.down('form'),
        record      = form.getRecord(),
        values      = form.getValues(),
        status      = 'win-statusbar-v01i012001x02',
        statusView  = 'win-statusbar-v01i012001x03';                    

        if (form.getForm().isValid()) { 
            if (form.getForm().isDirty()) { 
                Fsl.model.save('M01I012001', values, {
                    scope   : this,
                    success : function(model, operation) {
                        form.loadRecord(model);
                        Fsl.severity.info(status, operation);
                        Fsl.severity.info(statusView, operation);
                        Fsl.store.getStore('S01I012001').load();
                    },
                    failure : function(model, operation){
                        Fsl.severity.error(status, operation);
                        Fsl.severity.error(statusView, operation);
                        Fsl.security.recheck(operation);
                    }
                });
            } else {
                var message  = 'No change occured';
                Fsl.severity.warn(status, message);
                Fsl.severity.warn(statusView, message);
            }
        } else {
            var message  = 'Necessary Field Required';
            Fsl.severity.warn(status, message);
            Fsl.severity.warn(statusView, message);
        }     
    },    
    filter:function(field, el,me){    
        var form          =   field.up('window').down('form').getForm();
        if(form.isValid()){
            var idValue     =   form.findField('id').getValue(),
            articleId       =   form.findField('articleId').getValue(),
            date            =   form.findField('date').getValue(),
            price           =   form.findField('price').getValue(),
            articleName     =   form.findField('articleName').getValue(),
            mbcode          =   form.findField('mbcode').getValue(),
            pbcode          =   form.findField('pbcode').getValue(),                        
            filter          =   Fsl.store.getFilter('S01I012001');
            filter.add('id', idValue).add('articleId', articleId).add('date', date).add('price', price).add('articleName', articleName).add('mbcode', mbcode).add('pbcode', pbcode).load(1);
        }
    },   
    articleFilterForPrice:function(field, el, me){    
        var form          =   field.up('window').down('form').getForm();
        if(form.isValid()){
            var idValue     =   form.findField('id').getValue(),
            name            =   form.findField('name').getValue(),
            mbcode          =   form.findField('mbcode').getValue(),
            pbcode          =   form.findField('pbcode').getValue(),                        
            filter          =   Fsl.store.getFilter('S01I006004');
            filter.add('id', idValue).add('name', name).add('mbcode', mbcode).add('pbcode', pbcode).add('pbcode', pbcode).load(1);
        }
    },
    grossTotal: function(field, EventObject, eOpts ){
        var form           =  field.up('window').down('form').getForm();
        var values         =  form.getValues(),
        fields             =  form.getFields( ), 
        parchase           = parseFloat(values.purchase_price? values.purchase_price : 0),
        unit_cost          = parseFloat(values.unit_cost ? values.unit_cost : 0),
        unit_profit        = parseFloat(values.unit_profit? values.unit_profit : 0),
        total              = parseFloat(parchase+unit_cost+unit_profit)
        form.findField('price').setValue(total);
    }
});