Ext.define(Fsl.app.getAbsController('C01R004001'), {
    extend      : 'Ext.app.Controller', 
    requires    : Fsl.app.getMergedRequires({
        apps: [],
        misc: []
    }),  
    views       : Fsl.view.getViews(['V01R004001', 'V01R004001X01:06']),
    models      : Fsl.model.getModels(['M01I006001:2']),
    init: function() {
        var me = this;
        me.control({            
            'v01r004001x01 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        me.filter(field, el,me)
                    }
                }
            },
            'v01r004001x02 button[action=add]':{
                click  :  function (field, el) {                      
                    Ext.widget('v01r004001x03').show();                     
                }
            },
            'v01r004001x02 button[action=search]':{
                click  :  function (field, el) {
                    me.filter(field, el,me)
                }
            },
            'v01r004001x02 button[action=clear]':{
                click  :  function (field, el) {
                    field.up('window').down('form').getForm().reset();
                    Fsl.store.getStore('S01I006001').loadPage(1);
                }
            },
     

            
            
             'v01r004001x05 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        me.brandFilter(field, el,me)
                    }
                }
            },
            
            'v01r004001x06 button[action=search]':{
                click  :  function (field, el) {
                    me.brandFilter(field, el,me)
                }
            },
            'v01r004001x06 button[action=clear]':{
                click  :  function (field, el) {
                    field.up('window').down('form').getForm().reset();
                    me.brandFilter(field, el,me)
                }
            },           
            'v01r004001x06':{
                itemdblclick: me.setBrandInArticle
            }
        });
    },    
    setBrandInArticle :function(view, records){
        var form = Ext.getCmp('v01r004001x03').down('form').getForm();
        form.findField('brandId').setValue(records.data.id);
        form.findField('brandName').setValue(records.data.name);
        view.up('window').close();         
    },
    filter:function(field, el,me){    
        var form       = field.up('window').down('form').getForm();//'sv01r00400106'
        if(form.isValid()){
            var idValue = form.findField('id').getValue(),
            name        = form.findField('name').getValue(),
            brandName   = form.findField('brandName').getValue(),
            mbcode      = form.findField('mbcode').getValue(),
            pbcode      = form.findField('pbcode').getValue(),                        
            filter      = Fsl.store.getFilter('S01I006001');
            filter.add('id', idValue).add('name', name).add('brandName', brandName).add('mbcode', mbcode).add('pbcode', pbcode).load(1);
        }
    },    
    brandFilter:function(field, el,me){    
        var form              = field.up('window').down('form').getForm();//sv01r00400116
        if(form.isValid()){
            var idValue         = form.findField('id').getValue(),
            name                = form.findField('name').getValue(),
            brandTypeName       = form.findField('brandTypeName').getValue(),
            manufacturerName    = form.findField('manufacturerName').getValue(),
            genericProductName  = form.findField('genericProductName').getValue(),                        
            filter      = Fsl.store.getFilter('S01I005005');
            filter.add('id', idValue).add('name', name).add('brandTypeName', brandTypeName).add('manufacturerName', manufacturerName).add('genericProductName', genericProductName).load(1);
        }
    }    
});
