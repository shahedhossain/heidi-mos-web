Ext.define(Fsl.app.getAbsController('C01T003001'), {
    extend      : 'Ext.app.Controller', 
    requires    : Fsl.app.getMergedRequires({
        apps: [],
        misc: []
    }),  
    views       : Fsl.view.getViews(['V01T003001', 'V01T003001X01:09']),
    refs        : [{
        ref       : 'activeWindow',
        selector  : 'window'
    }],
    init: function() {
        var me = this;
        me.control({
            'v01t003001x01 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        me.filter(field, el, me)
                    }
                }
            },
            'v01t003001x02 button[action=add]':{
                click  :  function (field, el) {
                    Ext.widget('v01t003001x03').show()
                }
            },
            'v01t003001x02 button[action=search]':{
                click  :  function (field, el) {
                    me.filter(field, el, me)
                }
            },
            'v01t003001x02 button[action=clear]':{
                click  :  function (field, el) {
                    field.up('window').down('form').getForm().reset();
                    Fsl.store.getStore('S01T003001').loadPage(1);
                }
            },
            'v01t003001x03 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER){
                        var button = field.up('window').down('button[action=save]');
                        button.fireEvent('click', button);                            
                    }
                }
            },
            'v01t003001x03 button[action=save]': {
                click        : this.onSave
            },
            'v01t003001x02'     :   {
                itemdblclick    :   this.preUpdate
            },
            'v01t003001x03 textfield[name=articleName]':{
                afterrender: function(c) {
                    c.inputEl.on('dblclick', function() {
                        Ext.widget('v01t003001x04').show()      
                    });
                }
            },
            'v01t003001x03 textfield[name=vendorName]':{
                afterrender: function(c) {
                    c.inputEl.on('dblclick', function() {
                        Ext.widget('v01t003001x07').show()      
                    });
                }
            },
         
            'v01t003001x06':{
                itemdblclick:this.setArticleIntoExpire         
            },
            'v01t003001x09':{
                itemdblclick:this.setVendorIntoExpire            
            },
           
            'v01t003001x05 button[action=search]':{
                click  :  function (field, el) {
                    this.articleFilterForExpire(field, el, me)
                }
            },
            'v01t003001x05 button[action=clear]':{
                click  :  function (field, el) {
                    field.up('window').down('form').getForm().reset();
                    Fsl.store.getStore('S01T006001').load();
                }
            },
            'v01t003001x05 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        this.articleFilterForExpire(field, el, me)
                    }
                }
            },
           
            'v01t003001x08 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        me.vendorFilterForExpire(field, el, me)
                    }
                }
            },
            'v01t003001x08 button[action=search]':{
                click  :  function (field, el) {
                    this.vendorFilterForExpire(field, el, me)
                }
            },
            'v01t003001x08 button[action=clear]':{
                click  :  function (field, el) {
                    field.up('window').down('form').getForm().reset();
                    this.vendorFilterForExpire(field, el, me)
                }
            }
        });
    },
    setVendorIntoExpire : function(view, records){
        var form     = Ext.getCmp('v01t003001x03').down('form').getForm();      
        form.findField('vendorId').setValue(records.data.id);
        form.findField('vendorName').setValue(records.data.name);
        view.up('window').close();
    },
    setArticleIntoExpire :function(view, records){     
        var form      = Ext.getCmp('v01t003001x03').down('form').getForm();  
        form.findField('articleId').setValue(records.data.id);
        form.findField('articleName').setValue(records.data.name);
        view.up('window').close();
    },       
    onSave            : function(button){
        var win = button.up('window'),
        form    = win.down('form'),
        record  = form.getRecord(),
        values  = form.getValues(),
        status  = 'win-statusbar-v01t003001x02',
        statusView  = 'win-statusbar-v01t003001x03';                    

        if (form.getForm().isValid()) { 
            if (form.getForm().isDirty()) { 
                Fsl.model.save('M01T003001', values, {
                    scope   : this,
                    success : function(model, operation) {
                        form.loadRecord(model);
                        Fsl.severity.info(status, operation);
                        Fsl.severity.info(statusView, operation);
                        Fsl.store.getStore('S01T003001').load();
                    },
                    failure : function(model, operation){
                        Fsl.severity.error(status, operation);
                        Fsl.severity.error(statusView, operation);
                        Fsl.security.recheck(operation);
                    }
                });
            } else {
                var message  = 'No change occured';
                Fsl.severity.warn(status, message);
                Fsl.severity.warn(statusView, message);
            }
        } else {
            var message  = 'Necessary Field Required';
            Fsl.severity.warn(status, message);
            Fsl.severity.warn(statusView, message);
        }     
    },  
    preUpdate         : function(model, records){
        var win       = Ext.widget('v01t003001x03'),       
        form          = win.down('form').getForm();
        win.setTitle('EXPIRE UPDATE');
        form.loadRecord(records);
        form.findField('articleName').setValue(records.getM01i006001().data.name);
        form.findField('vendorName').setValue(records.getM01i014001().data.name);
        var btn  = win.down('button[text=Save]');
        btn.setText('Update');
        btn.setIcon(Fsl.route.getIcon('TB01001/EDT01004.png'));  
    },
    filter:function(field, el, me){    
        var form          =   field.up('window').down('form').getForm();//'sv01t00300108'
        if(form.isValid()){
            var idValue     =   form.findField('id').getValue(),
            articleId       =   form.findField('articleId').getValue(),
            articleName     =   form.findField('articleName').getValue(),
            mbcode          =   form.findField('mbcode').getValue(),
            pbcode          =   form.findField('pbcode').getValue(),
            price           =   form.findField('price').getValue(),
            vat             =   form.findField('vat').getValue(),
            vandorName      =   form.findField('vandorName').getValue(),
            vandorId        =   form.findField('vandorId').getValue(),
            filter          =   Fsl.store.getFilter('S01T003001');
            filter.add('id', idValue).add('articleId', articleId).add('articleName', articleName).add('mbcode', mbcode).add('pbcode', pbcode).add('price', price).add('vat', vat).add('vandorName', vandorName).add('vandorId', vandorId).load(1);
        }
    },   
    articleFilterForExpire:function(field, el, me){    
        var form          =   field.up('window').down('form').getForm();
        if(form.isValid()){
            var idValue     =   form.findField('id').getValue(),
            name            =   form.findField('name').getValue(),
            mbcode          =   form.findField('mbcode').getValue(),
            pbcode          =   form.findField('pbcode').getValue(),
            filter          =   Fsl.store.getFilter('S01I006008');
            filter.add('id', idValue).add('name', name).add('mbcode', mbcode).add('pbcode', pbcode).load(1);
        }
    },
    vendorFilterForExpire  : function(field, el,me){    
        var form          =   field.up('window').down('form').getForm();
        if(form.isValid()){
            var idValue     = form.findField('id').getValue(),
            name        = form.findField('name').getValue(),
            contact     = form.findField('contact').getValue(),
            filter       =   Fsl.store.getFilter('S01I014003');
            filter.add('id', idValue).add('name', name).add('contact', contact).load(1);
        }
    } 
});