Ext.define(Fsl.app.getAbsController('C01I006001'), {
    extend      : 'Ext.app.Controller', 
    requires    : Fsl.app.getMergedRequires({
        apps: [],
        misc: []
    }),  
    views       : Fsl.view.getViews(['V01I006001', 'V01I006001X01:06']),
    init: function() {
        var me = this;
        me.control({            
            'v01i006001x01 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        me.filter(field, el,me)
                    }
                }
            },
            'v01i006001x02 button[action=add]':{
                click  :  function (field, el) {                      
                    Ext.widget('v01i006001x03').show();                     
                }
            },
            'v01i006001x02 button[action=search]':{
                click  :  function (field, el) {
                    me.filter(field, el,me)
                }
            },
            'v01i006001x02 button[action=clear]':{
                click  :  function (field, el) {
                    field.up('window').down('form').getForm().reset();
                    Fsl.store.getStore('S01I006001').loadPage(1);
                }
            },
            'v01i006001x03 button[action=save]': {
                click       : me.onSave
            },
            'v01i006001x02':{
                itemdblclick :  me.preUpdate
            },
            
            
            'v01i006001x05 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        me.brandFilter(field, el,me)
                    }
                }
            },
            
            'v01i006001x06 button[action=search]':{
                click  :  function (field, el) {
                    me.brandFilter(field, el,me)
                }
            },
            'v01i006001x06 button[action=clear]':{
                click  :  function (field, el) {
                    field.up('window').down('form').getForm().reset();
                    me.brandFilter(field, el,me)
                }
            },           
            'v01i006001x06':{
                itemdblclick: me.setBrandInArticle
            }
        });
    },    
    setBrandInArticle :function(view, records){
        var form = Ext.getCmp('v01i006001x03').down('form').getForm();
        form.findField('brandId').setValue(records.data.id);
        form.findField('brandName').setValue(records.data.name);
        view.up('window').close();         
    },
    
    preUpdate         : function(model, records){
        var view   = Fsl.app.getWidget('V01I006001X03');  
        view.setTitle('ARTICLE UPDATE');
        var form = view.down('form').getForm()
        form.loadRecord(records); 
        form.findField('brandName').setValue(records.getM01i005001().data.name);    
        var btn    = view.down('button[text=Save]')
        btn.setText('Update');
        btn.setIcon(Fsl.route.getIcon('TB01001/EDT01004.png'));        
    },
    onSave            : function(button){
        var win     = button.up('window'),
        form        = win.down('form'),
        record      = form.getRecord(),
        values      = form.getValues(),
        status      = 'win-statusbar-v01i006001x02',
        statusView  = 'win-statusbar-v01i006001x03';                    

        if (form.getForm().isValid()) { 
            if (form.getForm().isDirty()) { 
                Fsl.model.save('M01I006001', values, {
                    scope   : this,
                    success : function(model, operation) {
                        form.loadRecord(model);
                        Fsl.severity.info(status, operation);
                        Fsl.severity.info(statusView, operation);
                        Fsl.store.getStore('S01I006001').load();
                        var btn    = win.down('button[text=Save]')
                        btn.setText('Update');
                        btn.setIcon(Fsl.route.getIcon('TB01001/EDT01004.png'));     
                    },
                    failure : function(model, operation){
                        Fsl.severity.error(status, operation);
                        Fsl.severity.error(statusView, operation);
                        Fsl.security.recheck(operation);
                    }
                });
            } else {
                var message  = 'No change occured';
                Fsl.severity.warn(status, message);
                Fsl.severity.warn(statusView, message);
            }
        } else {
            var message  = 'Necessary Field Required';
            Fsl.severity.warn(status, message);
            Fsl.severity.warn(statusView, message);
        }     
    },
    filter:function(field, el,me){    
        var form       = field.up('window').down('form').getForm();//'sv01i00600106'
        if(form.isValid()){
            var idValue = form.findField('id').getValue(),
            name        = form.findField('name').getValue(),
            brandName   = form.findField('brandName').getValue(),
            mbcode      = form.findField('mbcode').getValue(),
            pbcode      = form.findField('pbcode').getValue(),                        
            filter      = Fsl.store.getFilter('S01I006001');
            filter.add('id', idValue).add('name', name).add('brandName', brandName).add('mbcode', mbcode).add('pbcode', pbcode).load(1);
        }
    },    
    brandFilter:function(field, el,me){    
        var form              = field.up('window').down('form').getForm();//sv01i00600116
        if(form.isValid()){
            var idValue         = form.findField('id').getValue(),
            name                = form.findField('name').getValue(),
            brandTypeName       = form.findField('brandTypeName').getValue(),
            manufacturerName    = form.findField('manufacturerName').getValue(),
            genericProductName  = form.findField('genericProductName').getValue(),                        
            filter      = Fsl.store.getFilter('S01I005005');
            filter.add('id', idValue).add('name', name).add('brandTypeName', brandTypeName).add('manufacturerName', manufacturerName).add('genericProductName', genericProductName).load(1);
        }
    }    
});
