Ext.define(Fsl.app.getAbsController('C01T011001'), {
    extend      : 'Ext.app.Controller', 
    requires    : Fsl.app.getMergedRequires({
        apps: [],
        misc: []
    }),  
    views       : Fsl.view.getViews(['V01T011001','V01T011002', 'V01T011001X01:10']),
    refs        : [{
        ref       : 'activeWindow',
        selector  : 'window'
    }],
    init: function() { 
        var me = this;        
        me.control({
            'v01t011001x04' :{
                itemdblclick:me.getTransferOngriddblClick
            },
            'v01t011001x02' :{
                itemdblclick:me.getEditTransferOngriddblClick
            },
            'v01t011001x01 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        me.filter(field, el, me)
                    }
                }
            },
            'v01t011001x02 button[action=new]':{
                click  :  function(btn, e, eOpts){ 
                    var view = Ext.widget('v01t011001x05',{
                        animateTarget  : 'v01t011001x05'
                    });
                    var button = view.down('button[action=newTransfer]');
                    button.fireEvent('click', button);   
                }
            },
            'v01t011001x02 button[action=search]':{
                click  :  function (field, el) {
                    me.filter(field, el, me)
                }
            },
            'v01t011001x02 button[action=clear]':{
                click  :  function (field, el) {
                    field.up('window').down('form').getForm().reset();
                    Fsl.store.getStore('S01T011002').load();
                }
            },
            
            'v01t011001x03 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        me.transferfilter(field, el, me)
                    }
                }
            },
            'v01t011001x04 button[action=new]':{
                click  :  function(btn, e, eOpts){
                    var view = Ext.widget('v01t011001x05',{
                        animateTarget  : 'v01t011001x05'
                    });
                    view.show(); 
                    var button = view.down('button[action=newTransfer]');
                    button.fireEvent('click', button);   
                }
            },
            'v01t011001x04 button[action=search]':{
                click  :  function (field, el) {
                    me.transferfilter(field, el, me)
                }
            },
            'v01t011001x04 button[action=clear]':{
                click  :  function (field, el) {
                    field.up('window').down('form').getForm().reset();
                    Fsl.store.getStore('S01T011001').load(1);
                }
            },            
            'v01t011001x06  textfield[name=toBranchName]':{
                specialkey      : function(field, el){
                    if (el.getKey() == el.ENTER) {
                        Ext.widget('v01t011001x08').show();
                    }
                },
                afterrender: function(c) {
                    c.inputEl.on('dblclick', function() {
                        Ext.widget('v01t011001x08').show()      
                    });
                }
            },
            'v01t011001x09 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        me.branchFilter(field, el,me)
                    }
                }
            },            
            'v01t011001x10 button[action=search]':{
                click  :  function (field, el) {
                    me.branchFilter(field, el,me)
                }
            },
            'v01t011001x10 button[action=clear]':{
                click  :  function (field, el) {
                    field.up('window').down('form').getForm().reset();
                    Fsl.store.getStore('S01I015004').loadPage(1);
                }
            },
            'v01t011001x10':{
                itemdblclick:   this.setBranch
            }, 
            'v01t011001x06 numberfield[name=articleId]':{
                specialkey      : function(field, el){
                    if (el.getKey() == el.ENTER) {
                        me.getArticle(field, el, me)
                    }
                }
            },
            'v01t011001x06 numberfield[name=quantity]':{
                specialkey      : function(field, el){
                    if (el.getKey() == el.ENTER) {
                        me.addItemInCartForTransfer(field, el, me);
                    }
                },
                keyup           : function(field, el){                    
                    Fsl.C01I000000.setItemSubTotal(field, el, me);
                }
            },
            'v01t011001x07' :{
                itemdblclick : me.setItemOnField
            }, 
            'v01t011001x05 button[action=newTransfer]':{
                click       : me.newTransfer
            },
            'v01t011001x05 button[action=save]':{
                click       : me.onSave
            },
            'v01t011001x05 numberfield[name=id]':{
                specialkey      : function(field, el){
                    if (el.getKey() == el.ENTER || el.getKey()==el.TAB) { 
                        me.getTransfer(field, el, me)
                    }
                }/*,
                blur: function(field, event) {
                    event.keyCode = event.ENTER;
                    field.fireEvent('specialkey',field, event);
                }*/
            },
            'v01t011001x05 button[action=pdf]' : {
                click : this. pdfreport
            }
        });
    },
    getTransferOngriddblClick :function(model, records, item, index,  event){ 
        var  win   =   Ext.widget('v01t011001x05',{
            animateTarget  : 'v01t011001x05'
        });
        var transferId = records.data.id;
        var form = win.down('form'),
        field    = form.getForm().findField('id')
        field.setValue(transferId); 
        event.keyCode = event.ENTER;
        field.fireEvent('specialkey',field, event);
        
        form.down('#transferCt').hide();
        win.setHeight(430);
        var btn  = win.down('button[action=save]')
        btn.hide(true);
    }, 
    getEditTransferOngriddblClick :function(model, records, item, index,  event){ 
        var  win   =   Ext.widget('v01t011001x05',{
            animateTarget  : 'v01t011001x05'
        });
        var transferId = records.data.id;
        var form = win.down('form'),
        field    = form.getForm().findField('id')
        field.setValue(transferId); 
        event.keyCode = event.ENTER;
        field.fireEvent('specialkey',field, event);
        var btn  = win.down('button[action=save]')
        if(!records.data.status){        
            btn.show(true);
        }else{
            win.setHeight(430);
            form.down('#transferCt').hide();
            btn.hide();
        }
        
    }, 
    filter : function(field, el, me){    
        var form            =   field.up('window').down('form').getForm();
        if(form.isValid()){
            var idValue      =   form.findField('id').getValue(),
            toBranchId       =   form.findField('toBranchId').getValue(),
            branchName       =   form.findField('branchName').getValue(),
            entryDate        =   form.findField('entryDate').getValue(),
            filter           =   Fsl.store.getFilter('S01T011002');
            filter.add('id', idValue).add('toBranchId', toBranchId).add('branchName', branchName).add('entryDate', entryDate).load(1);
        }
    },
    transferfilter : function(field, el, me){    
        var form            =   field.up('window').down('form').getForm();
        if(form.isValid()){
            var idValue      =   form.findField('id').getValue(),
            toBranchId       =   form.findField('toBranchId').getValue(),
            branchName       =   form.findField('branchName').getValue(),
            entryDate        =   form.findField('entryDate').getValue(),
            filter           =   Fsl.store.getFilter('S01T011001');
            filter.add('id', idValue).add('toBranchId', toBranchId).add('branchName', branchName).add('entryDate', entryDate).load(1);
        }
    },
    branchFilter : function(field, el,me){    
        var form        =   field.up('window').down('form').getForm();
        if(form.isValid()){
            var idValue     = form.findField('id').getValue(),
            name        = form.findField('name').getValue(),
            location    = form.findField('location').getValue(),                        
            filter      =   Fsl.store.getFilter('S01I015004');
            filter.add('id', idValue).add('name', name).add('location', location).load(1);
        }
    },
    setBranch  : function(view, records){
        var form = Ext.getCmp('v01t011001x06').getForm();
        form.findField('toBranchId').setValue(records.data.id);
        form.findField('toBranchName').setValue(records.data.name);
        view.up('window').close();
    },
    
    getArticle : function(field, el, me){
        var preform     =   field.up('window').down('form'),
        form            =   preform.getForm(),
        values          =   form.getValues(),
        statusView      =   'win-statusbar-v01t011001x07';   
        if(values.articleId != ""){  
            Fsl.model.load('M01T005002', {
                id:values.articleId
            }, {
                scope     : this,
                success   : function(records, operation) {
                    Fsl.C01I000000.setValueInfieldContainer(records, operation, form);
                    form.findField('quantity').setValue('1').focus(true);
                    form.findField('dbQuantity').setValue(records.data.quantity);
                   
                    var rate = parseFloat(records.getM01i012001().data.price),
                    discount = parseFloat((rate *(records.getM01t010001().data.discount))/100),
                    vat      = parseFloat((rate - discount)*((records.getM01i013001().data.vat)/100)),
                    st       = (rate-discount+vat)*parseFloat(1);
                    form.findField('individualTotal').setValue(st);
                },
                failure :function(records, operation){
                    var containerId = '#transferCt'
                    Fsl.C01I000000.clearFieldContainer(preform, containerId);
                    Fsl.severity.error(statusView, operation.error);
                    Fsl.security.recheck(operation);
                }
            });
        }        
    },
    addItemInCartForTransfer  :function(field, el, me){
        var preform = field.up('window').down('form'),
        form        = preform.getForm(),
        values      = form.getValues(),        
        qty         = parseFloat(values.quantity),
        dbqty       = parseFloat(values.dbQuantity),
        statusView  = 'win-statusbar-v01t011001x07';   
        if(form.isValid()){   
            if(values.articleId >= 1){
                if(qty <= dbqty){                
                    var gridItem       = this.gridQty(values);               
                    if(qty > 1){                    
                        this.items_load_on_grid(values, preform);
                    }else if(qty == 1){                         
                        if(gridItem == undefined){ 
                            this.items_load_on_grid(values, preform);        
                        }else if(parseFloat(gridItem.data.quantity) == dbqty){
                            Fsl.C01I000000.showMsg("Empty", form)
                        }else if(parseFloat(gridItem.data.quantity) < dbqty){
                            this.items_load_on_grid(values, preform);                      
                        }
                    }else if(qty == 0){
                        this.items_load_on_grid(values, preform);
                    }else{
                        Fsl.C01I000000.showMsg(dbqty, form)
                    }
                }else if(!qty){  
                    Fsl.C01I000000.showMsg('Not Valid', form);
                }else{
                    Fsl.C01I000000.showMsg(dbqty, form);
                }
            }else{                
                form.findField('articleId').focus(true);
                var containerId = '#transferCt'
                Fsl.C01I000000.clearFieldContainer(preform, containerId);                
            }     
        }else{ 
            var message  = 'Necessary Field Required';
            Fsl.severity.error(statusView, message);
        }
    },
    
    gridQty : function(values){
        var records, iItemStore  = Fsl.store.getStore('S01T005003'); 
        iItemStore.each(function(record){           
            if(record.getM01i006001().data.id == values.articleId){
                if(record.data.id >0 ){                
                    records   = record                  
                }else if(record.data.id == 0){
                    records   = record
                }
                return  records
            }             
        });
        return records;
    },
    
    items_load_on_grid :function(values, preform){
        var statusView  = 'win-statusbar-v01t011001x07';  
        values.id = 0; /* @ Here transferId name is id. so when i entry a product then it catch wrong*/
        Fsl.model.save('M01T005001', values, {       
            scope   : this,
            success : function(model, operation){
                var text     = operation.response.responseText,
                json         = Ext.decode(text),                    
                containerId = '#transferCt'
                Fsl.C01I000000.clearFieldContainer(preform, containerId);
                Fsl.store.getStore('S01T005003').load(); 
                Fsl.severity.info(statusView, json.message);
            },                
            failure:function(model, operation){ 
                Fsl.severity.error(statusView, operation.error);
                Fsl.security.recheck(operation);
            } 
        });
    },
    setItemOnField :  function(view, record){
        var preform     = view.up('window').down('form'),
        form            = preform.getForm(),
        values          = form.getValues(),
        articleId       = record.getM01i006001().data.id,     
        invoiceItemId   = record.data.id;        
        /* if(invoiceItemId){
            console.log(invoiceItemId)
        }else{*/
        Fsl.model.load('M01T005002', {
            id:articleId
        }, {
            scope:this,
            success: function(records, operation, success) {
                form.findField('quantity').setValue(record.data.quantity).focus(true);
                form.findField('articleId').setValue(articleId);
                Fsl.C01I000000.setValueInfieldContainer(records, operation, form);
                form.findField('dbQuantity').setValue(records.data.quantity);
                Fsl.C01I000000.onDblClickSetItemSubTotal(records, operation, form); 
            },
            failure :function(model, operation){
                var containerId = '#transferCt'
                Fsl.C01I000000.clearFieldContainer(preform, containerId);
                Fsl.security.recheck(operation);
            }
        });
    // }
    },
    newTransfer :  function(button){
        var win         = button.up('window'),
        preform         = win.down('form'),
        form            = preform.getForm(),
        values          = form.getValues(),
        tItemCartStore  = Fsl.store.getStore('S01T005003'); 
        form.reset();  
        button.setDisabled(true);                 
        Fsl.model.load('M01T011005', {
            id:1
        }, {
            success: function(records, operation, success) {  
                form.findField('branchId').setValue(records.getM01i015001().data.id);
                //form.findField('toBranchId').setValue(records.getM01i015002().data.id);
                form.findField('branchName').setValue(records.getM01i015001().data.name);
                //form.findField('toBranchName').setValue(records.getM01i015002().data.name);
                tItemCartStore.load();                
                var btn  = win.down('button[action=save]')
                btn.show(true);
                button.enable();
                btn.setText('SAVE');               
                btn.setIcon(Fsl.route.getImage('SAV01004.png'));  
                preform.down('#transferCt').show();
            },
            failure :function(model, operation){
                button.enable();
                tItemCartStore.load();
                Fsl.security.recheck(operation);
            }
        });       
    },
    onSave   :function(button){
        var win       = button.up('window'),
        form          = win.down('form'),
        record        = form.getRecord(),
        values        = form.getValues(),
        tItemCartStore= Fsl.store.getStore('S01T005003'),
        editTStore    = Fsl.store.getStore('S01T011001'),
        tStore        = Fsl.store.getStore('S01T011002'),
        statusView    = 'win-statusbar-v01t011001x07',message;  
        button.setDisabled(true);
        
        if(form.getForm().isValid()){ 
            Fsl.model.save('M01T011001', values, {
                scope     :   this,
                success   :   function(model, operation){
                    var text    = operation.response.responseText,
                    json        = Ext.decode(text);
                    form.getForm().findField('id').setValue(model.data.id);
                    tItemCartStore.load();
                    editTStore.load();
                    tStore.load();
                    button.enable();
                    button.setText('Update');
                    button.setIcon(Fsl.route.getIcon('TB01001/EDT01004.png'));    
                    this.send_A_Mail_To_Transfer_Branch(model, operation, values);
                    Fsl.severity.info(statusView, json.message);
                },                  
                failure:function(model, operation){
                    Fsl.severity.error(statusView, operation.error);
                    button.enable();
                    Fsl.security.recheck(operation);
                } 
            });
        }else{
            button.enable();
            message  = 'Necessary Field Required';
            Fsl.severity.warn(statusView, message);     
        }    
    },
    getTransfer : function(field, el, me){
        var win         =   field.up('window'),
        preform         =   win.down('form'),
        record          =   preform.getRecord(),
        values          =   preform.getValues(),
        form            =   preform.getForm(),
        containerId     = '#transferCt',
        statusView      = 'win-statusbar-v01t011001x07',
        tItemCartStore  = Fsl.store.getStore('S01T005003');
        if(values.id){        
            if(values.id > 0){
                Fsl.model.load('M01T011001', {
                    id:values.id
                }, {
                    scope   : this,
                    success : function(model, operation) {                       
                        form.findField('branchId').setValue(model.getM01i015001().data.id);
                        form.findField('toBranchId').setValue(model.getM01i015002().data.id);
                        form.findField('branchName').setValue(model.getM01i015001().data.name);
                        form.findField('toBranchName').setValue(model.getM01i015002().data.name);
                        tItemCartStore.load(); 
                        var btn  = win.down('button[action=save]')
                        btn.setText('UPDATE');
                        btn.setIcon(Fsl.route.getIcon('TB01001/EDT01004.png'));  
                        Fsl.C01I000000.clearFieldContainer(preform, containerId);
                    },
                    failure:  function(model, operation){
                        Fsl.security.recheck(operation);
                        Fsl.C01I000000.clearFieldContainer(preform, containerId);
                        Fsl.severity.error(statusView, operation.error);
                        tItemCartStore.load(); 
                    }
                });
            }
        }
    },
    
    destroyTransferItemStore : function(){
        var tItemCartStore= Fsl.store.getStore('S01T005003');
        var tcartItem  = Ext.create(Fsl.app.getAbsModel('M01T005003'), {
            id : 1
        });                  
        tcartItem.destroy({
            success: function(records, operation, success) { 
                tItemCartStore.load();
            },
            failure :function(model){
                tItemCartStore.load();
            }
        });  
    },
    pdfreport : function(button){
        var form    = button.up('window').down('form').getForm(),
        values      = form.getValues(),      
        transferId  = values.id, 
        branchId    = values.branchId;
        if(transferId != null){
            var body = Ext.getBody();
            var frame = body.createChild({
                tag:'iframe',
                cls:'x-hidden',
                id:'iframe',
                name:'iframe'
            });
            var form = body.createChild({
                tag:'form',
                cls:'x-hidden',
                id:'form',
                method:'post',
                action:Fsl.route.getAction('c01t011001/report/'),
                target:'iframe'
            });
        
            form.createChild({
                tag:'input',
                name:'transferId',
                type:'hidden',
                value:transferId
            });
            form.createChild({
                tag:'input',
                name:'branchId',
                type:'hidden',
                value:branchId
            });
            form.dom.submit();      
        }else{
            Ext.Msg.alert("Sorry","Transfer is not created, So PDF can't be generated !!!")
        }
    },
    
    send_A_Mail_To_Transfer_Branch : function(model, operation, values){
        
        Ext.Ajax.request({
            scope    : this,
            url      : Fsl.route.getAction('c01t011001/emailToTransferBranch/'),                          
            params   : {
                transferId      : model.data.id,
                toBranchId      : values.toBranchId,
                values          : values
            },
            success  : function(formF, action) {
                var text = formF.responseText,
                json = Ext.decode(text),
                deliver = json.message;

                Ext.Msg.show({
                    title  : 'Email Sending Report', 
                    msg    : json.message, 
                    modal  : true,
                    icon   : Ext.Msg.INFO,
                    buttons: Ext.Msg.OK
                });                        
            },
            failure: function(form, action) {
                Ext.Msg.show({
                    title  : 'Email Sending Report', 
                    msg    : action.error, 
                    modal  : true,
                    icon   : Ext.Msg.ERROR,
                    buttons: Ext.Msg.OK
                });
            }
        }) 
    }
});