Ext.define(Fsl.app.getAbsController('C01I005001'), {
    extend      : 'Ext.app.Controller', 
    requires    : Fsl.app.getMergedRequires({
        apps: [],
        misc: []
    }),  
    views       : Fsl.view.getViews(['V01I005001', 'V01I005001X01:09']),
    init: function() {
        var me = this;
        me.control({
            'v01i005001x01 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        me.filter(field, el, me)
                    }
                }
            },
            'v01i005001x02 button[action=add]':{
                click  :  function (field, el) {
                    Fsl.app.getWidget('v01i005001x03').show()
                }
            },
            'v01i005001x02 button[action=search]':{
                click  :  function (field, el) {
                    me.filter(field, el, me)
                }
            },
            'v01i005001x02 button[action=clear]':{
                click  :  function (field, el) {
                    field.up('window').down('form').getForm().reset();
                    Fsl.store.getStore('S01I005001').loadPage(1);
                }
            },
            'v01i005001x02'     :   {
                itemclick       :   me.infoOnStatus,
                itemdblclick    :   me.preUpdate
            },
            'v01i005001x03 button[action=save]': {
                click           :  me.onSave
            },
            'v01i005001x03 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        var button = field.up('window').down('button[action=save]');
                        button.fireEvent('click', button);                            
                    }
                }
            },            
            'v01i005001x05 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        me.manufacFilter(field, el, me)
                    }
                }
            },
            'v01i005001x06 button[action=search]':{
                click  :  function (field, el) {
                    me.manufacFilter(field, el, me)
                }
            },
            'v01i005001x06 button[action=clear]':{
                click  :  function (field, el) {
                    field.up('window').down('form').getForm().reset();
                    Fsl.store.getStore('S01I002006').loadPage(1);
                }
            }, 
            'v01i005001x06':{                 
                itemdblclick: me.setManufacturer
            },            
            'v01i005001x08 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        me.genericProductFilter(field, el, me)
                    }
                }
            },            
            'v01i005001x09 button[action=search]':{
                click  :  function (field, el) {
                    me.genericProductFilter(field, el, me)
                }
            },
            'v01i005001x09 button[action=clear]':{
                click  :  function (field, el) {
                    field.up('window').down('form').getForm().reset();
                    Fsl.store.getStore('S01I003004').loadPage(1);
                }
            },
            'v01i005001x09':{                 
                itemdblclick:  me.setGenericProduct
            }
        });
    },
    onSave            : function(button){
        var win = button.up('window'),
        form    = win.down('form'),
        record  = form.getRecord(),
        values  = form.getValues(),
        status  = 'win-statusbar-v01i005001x02',
        statusView  = 'win-statusbar-v01i005001x03';                    

        if (form.getForm().isValid()) { 
            if (form.getForm().isDirty()) { 
                Fsl.model.save('M01I005001', values, {
                    scope   : this,
                    success : function(model, operation) {
                        form.loadRecord(model);
                        Fsl.severity.info(status, operation);
                        Fsl.severity.info(statusView, operation);
                        Fsl.store.getStore('S01I005001').load();
                    },
                    failure : function(model, operation){
                        Fsl.severity.error(status, operation);
                        Fsl.severity.error(statusView, operation);
                        Fsl.security.recheck(operation);
                    }
                });
            } else {
                var message  = 'No change occured';
                Fsl.severity.warn(status, message);
                Fsl.severity.warn(statusView, message);
            }
        } else {
            var message  = 'Necessary Field Required';
            Fsl.severity.warn(status, message);
            Fsl.severity.warn(statusView, message);
        }     
    },
    infoOnStatus      : function(model, operation){
        var status  = 'win-statusbar-v01i005001x02'
        Fsl.severity.info(status, operation.data.name);      
    },
    preUpdate         : function(model, records){
        var view   = Fsl.app.getWidget('V01I005001X03');  
        view.setTitle('BRAND UPDATE');
        var form = view.down('form').getForm()
        form.loadRecord(records); 
        form.findField('manufacturerName').setValue(records.getM01i002001().data.name);
        form.findField('genericProductName').setValue(records.getM01i003001().data.name);
        var btn    = view.down('button[text=Save]')
        btn.setText('Update');
        btn.setIcon(Fsl.route.getIcon('TB01001/EDT01004.png'));        
    },    
    filter            :function(field, el, me){    
        var form            =   field.up('window').down('form').getForm();
        if(form.isValid()){
            var idValue         = form.findField('id').getValue(),
            name                = form.findField('name').getValue(),
            brandTypeName       = form.findField('brandTypeName').getValue(),
            manufacturerName    = form.findField('manufacturerName').getValue(),
            genericProductName  = form.findField('genericProductName').getValue(),                        
            filter              = Fsl.store.getFilter('S01I005001');
            filter.add('id', idValue).add('name', name).add('brandTypeName', brandTypeName).add('manufacturerName', manufacturerName).add('genericProductName', genericProductName).load(1);
        }
    },
    manufacFilter  :function(field, el, me){    
        var form            =   field.up('window').down('form').getForm();
        if(form.isValid()){
            var idValue     =   form.findField('id').getValue(),
            name            =   form.findField('name').getValue(),                        
            filter          =   Fsl.store.getFilter('S01I002006');
            filter.add('id', idValue).add('name', name).load(1);
        }
    },
    genericProductFilter  :function(field, el, me){    
        var form            =   field.up('window').down('form').getForm();
        if(form.isValid()){
            var idValue     =   form.findField('id').getValue(),
            name            =   form.findField('name').getValue(),
            categoryName    =   form.findField('categoryName').getValue(),
            filter          =   Fsl.store.getFilter('S01I003004');
            filter.add('id', idValue).add('name', name).add('categoryName', categoryName).load(1);
        }
    },
    setManufacturer : function(view, records){
        var win    = Ext.getCmp('v01i005001x03'),
        form   = win.down('form').getForm();          
        form.findField('manufacturerName').setValue(records.data.name).focus();          
        form.findField('manufacturerId').setValue(records.data.id);
        view.up('window').close();    
    },
    setGenericProduct : function(view, records){
        var win = Ext.getCmp('v01i005001x03'),
        form    = win.down('form').getForm();              
        form.findField('genericProductName').setValue(records.data.name).focus();            
        form.findField('genericProductId').setValue(records.data.id);
        view.up('window').close();    
    }
});
