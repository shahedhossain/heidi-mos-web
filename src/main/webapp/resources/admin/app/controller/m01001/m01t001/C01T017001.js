Ext.define(Fsl.app.getAbsController('C01T017001'), {
    extend      : 'Ext.app.Controller', 
    requires    : Fsl.app.getMergedRequires({
        apps: [],
        misc: []
    }),  
    views       : Fsl.view.getViews(['V01T017001','V01T017001X01:6']),
    refs        : [{
        ref       : 'activeWindow',
        selector  : 'window'
    }],
    init: function() { 
        var me = this;
        me.control({
            'v01t017001x01 *':{
                specialkey: function(field, el){
                    if(el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        me.filter(field, el, me)
                    }
                }
            },
            'v01t017001x02 button[action=clear]':{
                click:function (field, el) {
                    field.up('window').down('form').getForm().reset();
                    Fsl.store.getStore('S01T017001').load(1);
                }
            },
            'v01t017001x02 button[action=search]':{
                click:function (field, el) {
                   me.filter(field, el, me)
                }
            },
            'v01t017001x02' :{
                itemdblclick:me.getOrderApprovalItem
            },
            'v01t017001x04 textfield[name=orderId]':{
                specialkey      : function(field, el){
                    if (el.getKey() == el.ENTER || el.getKey()==el.TAB) { 
                        me.getOrderAppItem(field, el, me)
                    }
                }
            },
            'v01t017001x05' :{
                itemdblclick    : this.preUpdateQty 
            },
            'v01t017001x06 button[action=save]': {
                click           : this.updateApprovalQty
            },
            'v01t017001x03 button[action=save]': {
                click           : this.sendRequestItem
            }
        })
    },
    getOrderApprovalItem :function(model, records, item, index,  event){ 
        
        var win   =   Ext.widget('v01t017001x03',{
            animateTarget  : 'v01t017001x03'
        }),
        form = win.down('form'),
        field    = form.getForm()
        field.loadRecord(records)
    
        var orderAppId = records.data.id;
        field.findField('orderApprovalId').setValue(orderAppId)
        field.findField('branchName').setValue(records.getM01i015002().data.name)
        var orderfield  = field.findField('orderId')
        event.keyCode = event.ENTER;
        orderfield.fireEvent('specialkey',orderfield, event);     
        var transferId = records.getM01t011001().data.id
        if(transferId){
            win.down('button[text=TRANSFER]').disable(true)
            win.down('button[text=TRANSFER]').hide()
            win.down('grid').on('itemdblclick', function(e) {
                e.clearListeners()
            });
        }
        
    },
    getOrderAppItem : function(field, el, me){
        var win         =   field.up('window'),
        preform         =   win.down('form'),
        record          =   preform.getRecord(),
        values          =   preform.getValues(),
        form            =   preform.getForm()
        if(values.orderId){        
            if(values.orderId > 0){
                Fsl.model.load('M01T017001', {
                    id:values.orderId
                }, {
                    scope   : this,
                    success : function(model, operation) {
                        
                        Fsl.store.getStore('S01T018001').load(1);
                    },
                    failure:  function(model, operation){
                        
                    }
                });
            }
        }
    },
    
    preUpdateQty :function(model, records){
        var approvalField = model.up('window').down('form').getForm(),
        transferId  = approvalField.findField('transferId').getValue()
        if(transferId == 0 || transferId == '' || transferId == undefined){
            var view  = Ext.widget('v01t017001x06'),
            form      = view.down('form')
            form.loadRecord(records);
            form.getForm().findField('articleId').setValue(records.getM01i006001().data.id);
            form.getForm().findField('articleName').setValue(records.getM01i006001().data.name);
        }else{
            var statusView = 'win-statusbar-v01t017001x05',
            message    = "Item was sent successfully"
            Fsl.severity.warn(statusView, message);
        }
    },
    updateApprovalQty :function(button){
        var win = button.up('window'),
        form    = win.down('form'),
        record  = form.getRecord(),
        values  = form.getValues(),
        statusView  = 'win-statusbar-v01t017001x06',
        odrApprovalItemStore = Fsl.store.getStore('S01T018001');
        
        if (form.getForm().isValid()) { 
            if (form.getForm().isDirty()) { 
                Fsl.model.save('M01T018001', {
                    articleId   :values.articleId,
                    appQty      :values.appQty            
                }, {
                    scope   :this,
                    success :function(model, operation){
                        odrApprovalItemStore.load();
                        Fsl.severity.info(statusView, operation);
                    },
                    failure     : function(record, operation) {
                        Fsl.severity.error(statusView, operation.error);
                        Fsl.security.recheck(operation);
                    }
                });
            }else{
                var message  = 'No change occured';
                Fsl.severity.warn(statusView, message);
            }
        }else{
            var message  = 'Necessary Field Required';
            Fsl.severity.warn(statusView, message);
        }
    },
    sendRequestItem : function(button){
        var win         = button.up('window'),
        form            = win.down('form'),
        record          = form.getRecord(),
        values          = form.getValues(),
        orderAppItemStore= Fsl.store.getStore('S01T018001'),
        orderAppStore    = Fsl.store.getStore('S01T017001');
        if(form.getForm().isValid()){  
            values.id   = values.receiveId
            Fsl.model.save('M01T017002', values, {            
                scope   : this,
                success : function(model, operation){
                    var text    = operation.response.responseText,
                    json        = Ext.decode(text);
                    form.loadRecord(model);
                    form.getForm().findField('transferId').setValue(model.data.id);  
                    orderAppItemStore.load();    
                    orderAppStore.load();
                },                  
                failure:function(model, operation){ 
                    Fsl.security.recheck(operation);
                } 
            }); 
        }
    },
    
    filter : function(field, el, me){
        var form    =   field.up('window').down('form').getForm();
        if(form.isValid()){
            var idValue      =   form.findField('id').getValue(),
            fromBranchId     =   form.findField('fromBranchId').getValue(),
            branchName       =   form.findField('branchName').getValue(),
            orderId          =   form.findField('orderId').getValue(),
            transferId       =   form.findField('transferId').getValue(),
            entryDate        =   form.findField('entryDate').getValue(),
            filter           =   Fsl.store.getFilter('S01T017001');
            filter.add('id', idValue).add('fromBranchId', fromBranchId).add('branchName', branchName).add('entryDate', entryDate).add('orderId', orderId).add('transferId', transferId).load(1);
        }
    }
});