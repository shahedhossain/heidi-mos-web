Ext.define(Fsl.app.getAbsController('C01I001002'), {
    extend      : 'Ext.app.Controller', 
    requires    : Fsl.app.getMergedRequires({
        apps: [],
        misc: []
    }),  
    views       : Fsl.view.getViews(['V01I001002', 'V01I001002X01:8']),
    refs        : [{
        ref       : 'activeWindow',
        selector  : 'window'
    }],
    init        : function() {  
        var me = this;
        me.control({            
            'v01i001002x02'     :   { 
                itemclick       :   me.infoOnStatus,
                itemdblclick    :   me.getManuByCate
            },
            'v01i001002x05 button[action=add]':{
                click  :  function (field, el) {
                    Fsl.app.getWidget('v01i001002x06').show()
                }
            },
            'v01i001002x01 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        me.filter(field, el, me)
                    }
                }
            },
            'v01i001002x02 button[action=search]':{
                click  :  function (field, el) {
                    me.filter(field, el, me)
                }
            },
            'v01i001002x02 button[action=clear]':{
                click  :  function (field, el) {
                    field.up('window').down('form').getForm().reset();
                    me.filter(field, el, me)
                }
            },
            'v01i001002x07 *' : {
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER || el.getKey()==el.TAB){
                        me.filterForManu(field, el, me)
                    }
                }
            },
            'v01i001002x08 button[action=search]':{
                click  :  function (field, el) {
                    me.filterForManu(field, el, me)
                }
            },
            'v01i001002x08 button[action=clear]':{
                click  :  function (field, el) {
                    field.up('window').down('form').getForm().reset();
                    me.filterForManu(field, el, me)
                }
            },
            
            'v01i001002x08'     :   {   
                itemclick       :   me.v01i001002x08Status,
                itemdblclick    :   me.addManuInCate           
            },
            'v01i001002x05'     :   {
                itemclick       :   me.v01i001002x05Status
            }
        });
    },
    infoOnStatus      : function(model, operation){
        var status      = 'win-statusbar-v01i001002x02'
        Fsl.severity.info(status, operation.data.name);      
    },
    v01i001002x08Status  :   function(model, operation){
        var status      = 'win-statusbar-v01i001002x08'
        Fsl.severity.info(status, operation.data.name);         
    },
    v01i001002x05Status  :   function(model, operation){
        var status      = 'win-statusbar-v01i001002x05'
        Fsl.severity.info(status, operation.data.name);      
    },
    
    getManuByCate:function(model, records){        
        var catId    = records.data.id,
        manuInfo     = this.getManufacturerItemByCat(catId, model, records);
        Fsl.app.getWidget('v01i001002x03').down('form').loadRecord(records);
    },    
    getManufacturerItemByCat :function(catId, model, records){  
        Fsl.model.load('M01I001001', {
            id:catId
        }, {        
            success: function (records, operation, success) {      //success instead of callback          
                var store = Fsl.store.getStore('S01I002002').load();
                records.m01i002001s().each(function(manu){
                    store.add(manu.data);
                });         
            },
            failure :function(records, operation){
                Fsl.security.recheck(operation);
            }
        });
    },
    
    filter            :function(field, el, me){    
        var form            =   field.up('window').down('form').getForm();
        if(form.isValid()){
            var idValue     =   form.findField('id').getValue(),
            name            =   form.findField('name').getValue(),
            filter          =   Fsl.store.getFilter('S01I001003');
            filter.add('id', idValue).add('name', name).load(1);  
        }
    },    
    addManuInCate :function(model, records){          
        var values = Ext.getCmp('v01i001002x04').getForm().getValues();
        var catId  = values.id;
        var manuId = records.data.id;
        Ext.Ajax.request({
            scope      : this,
            url        : Fsl.route.getAction('c01i001001/manuInsertInCat/'),
            params     : {
                catId  : catId,
                manuId : manuId
            },
            timeout : 60000,
            success: function(response, options){
                var json   =  Ext.decode(response.responseText),
                status     = 'win-statusbar-v01i001002x05';
                if('data' in json && json.success == true){
                    var data    =  json.data[0].m01i001002s;                     
                    Fsl.store.getStore('S01I002002').loadData(data);
                    me.up('window').close();
                }
                Fsl.severity.info(status, response);
                Fsl.security.recheck(response);                                                             
            },
            failure :function(response, options){
                Fsl.security.recheck(response);
            }
        });
    },
    filterForManu : function(field, el, me){    
        var form            =   field.up('window').down('form').getForm();
        if(form.isValid()){
            var idValue     =   form.findField('id').getValue(),
            name            =   form.findField('name').getValue(),
            filter          =   Fsl.store.getFilter('S01I002004');
            filter.add('id', idValue).add('name', name).load(1);                
        }
    }    
});
