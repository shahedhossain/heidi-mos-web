Ext.define('Fsl.admin.controller.Navigation', {
    extend      : 'Ext.app.Controller', 
    requires    : Fsl.app.getMergedRequires({
        apps: [],
        misc: []
    }),
    views       : Fsl.view.getViews([
        'CenterPanel',
        'WestPanel',
        'V01I001001'
        ]),
    models      : Fsl.model.getModels([
        'M01I001001',
        'M01I002001',
        'M01I003001',
        'M01I004001',
        'M01I005001',
        'M01I006001:2',
        'M01I007001',
        'M01I008001',
        'M01I009001',
        'M01I010001',
        'M01I011001',
        'M01I012001:2',
        'M01I013001:2',
        'M01I014001',
        'M01I015001',
        'M01I016001',
        'M01I017001',
        
        'M01T001001',
        'M01T002001:2',
        'M01T003001:2',
        'M01T004001:3',
        'M01T005001:3',
        'M01T006001:2',
        'M01T007001:3',
        'M01T008001:2',
        'M01T009001',
        'M01T010001:2',
        'M01T011001:5',
        'M01T012001:2',
        'M01T013001:2',
        'M01T014001:4',
        'M01T015001',
        'M01T016001:2',
        'M01T017001:2',
        
        
        'M04T001001',
        'M04T001002',
        'M04T001003',
        'M04T002001',
        'M04T003001',
        'M04J001001',
        'M04J002001',
        'M04J003001'
        ]),
    stores      : Fsl.store.getStores([
        'S01I000000',        
        'S01I001002:5',        
        'S01I002002:7',
        'S01I003001:5',
        'S01I004001:2',
        'S01I005001:5',
        'S01I006001:9',
        'S01I007001:2',
        
        'S01I008001',
        'S01I009001:2',
        'S01I010001:2',
        'S01I011001:2',
        'S01I012001:2',
        'S01I013001:2',
        'S01I014001:2',
        'S01I015001:4',
        'S01I016001:2',
        'S01I017001:2',
        
        
        'S01I014003',
        'S01I014004',
        'S01I015001:8',
        /* Transation Stote */
        'S01T001001:2',
        'S01T002001:2',
        'S01T003001:2',
        'S01T004001:4',
        'S01T005003:4',
        'S01T006001:2',
        'S01T007001:2',
        'S01T008001:3',
        'S01T009001:3',
        'S01T010001:2',
        'S01T011001:2',
        'S01T012001:2',
        'S01T013001',
        'S01T014001',
        'S01T015001',
        'S01T016001',
        'S01T017001',
        'S01T018001',
        
        'S04T001001:3',
        'S04T002001:3',
        'S04T003001:3',
        'S04J001001',
        'S04J002001',
        'S04J003001'
        ]),
    

    refs        : [
    {
        ref       : 'activeWindow',
        selector  : 'window'
    }
    ],
    init        : function() { 
        var me = this;
        me.control({ 
            'westPanel'		: {
                itemclick: this.onItemClick
            }
        });
    },
    onItemClick : function(view, node){
        if(node.isLeaf()) {
            var leaf   = view.getSelectionModel().getSelection(),
            itemId     = 'id'+leaf[0].data.id,
            TabPanel   = Ext.getCmp('centerPanel'),
            activeTab  = TabPanel.child('#'+itemId),
            component  = leaf[0].data.id.substring(1);
            
            if (activeTab == null){
                switch(component){
                    /* Initialization Table */
                    case 'V01I000001':
                        alert('Meta Data' +component);
                        break;
                    case 'V01I001001':
                        Fsl.app.getWidget(component).show();
                        break;
                    case 'V01I001002':
                        Fsl.app.getWidget(component).show();
                        break;
                    case 'V01I002001':
                        Fsl.app.getWidget(component).show();
                        break;
                    case 'V01I002002':
                        Fsl.app.getWidget(component).show();
                        break;
                    case 'V01I003001':
                        Fsl.app.getWidget(component).show();
                        break;
                    case 'V01I004001':
                        Fsl.app.getWidget(component).show();
                        break;
                    case 'V01I005001':
                        Fsl.app.getWidget(component).show();
                        break;
                    case 'V01I006001':
                        Fsl.app.getWidget(component).show();
                        break;
                    case 'V01I007001':
                        Fsl.app.getWidget(component).show();
                        break;
                    case 'V01I008001':
                        Fsl.app.getWidget(component).show();
                        break;
                    case 'V01I009001':
                        Fsl.app.getWidget(component).show();
                        break;
                    case 'V01I010001':
                        Fsl.app.getWidget(component).show();
                        break;
                    case 'V01I011001':
                        Fsl.app.getWidget(component).show();
                        break;
                    case 'V01I012001':
                        Fsl.app.getWidget(component).show();
                        break;
                    case 'V01I013001':
                        Fsl.app.getWidget(component).show();
                        break;
                    case 'V01I014001':
                        Fsl.app.getWidget(component).show();
                        break;
                    case 'V01I015001':
                        Fsl.app.getWidget(component).show();
                        break;
                    case 'V01I016001':
                        Fsl.app.getWidget(component).show();
                        break;
                    case 'V01I017001':
                        Fsl.app.getWidget(component).show();
                        break;
                        
                    /* Transation table*/        
                    case 'V01T001001':
                        Fsl.app.getWidget(component).show();
                        break;                        
                    case 'V01T002001':
                        Fsl.app.getWidget(component).show();
                        break;
                    case 'V01T003001':
                        Fsl.app.getWidget(component).show();
                        break;
                    case 'V01T004001':
                        Fsl.app.getWidget(component).show();
                        break;
                    case 'V01T004002':
                        Fsl.app.getWidget(component).show();
                        break;
                    case 'V01T004003':
                        Fsl.app.getWidget(component).show();
                        break;
                    case 'V01T005001':
                        Fsl.app.getWidget(component).show();
                        break;
                    case 'V01T006001':
                        Fsl.app.getWidget(component).show();
                        break;
                    case 'V01T007001':
                        Fsl.app.getWidget(component).show();
                        break;
                    case 'V01T008001':
                        Fsl.app.getWidget(component).show();
                        break;
                    case 'V01T009001':
                        Fsl.app.getWidget(component).show();
                        break;
                    case 'V01T010001':
                        Fsl.app.getWidget(component).show();
                        break;
                    case 'V01T011001':
                        Fsl.app.getWidget(component).show();
                        break;
                    case 'V01T011002':
                        Fsl.app.getWidget(component).show();
                        break;
                    case 'V01T012001':
                        Fsl.app.getWidget(component).show();
                        break;
                    case 'V01T012002':
                        Fsl.app.getWidget(component).show();
                        break;
                    case 'V01T013001':
                        Fsl.app.getWidget(component).show();
                        break;
                    case 'V01T014001':
                        Fsl.app.getWidget(component).show();
                        break; 
                    case 'V01T017001':
                        Fsl.app.getWidget(component).show();
                        break; 
                    case 'V01T015001':
                        Fsl.app.getWidget(component).show();
                        break; 
                        
                    case 'V04T001001':
                        Fsl.app.getWidget(component).show();
                        break;  
                    case 'V04T002001':
                        Fsl.app.getWidget(component).show();
                        break;  
                    case 'V04T003001':
                        Fsl.app.getWidget(component).show();
                        break;  
                    case 'V04J001001':
                        Fsl.app.getWidget(component).show();
                        break;  
                    case 'V04J002001':
                        Fsl.app.getWidget(component).show();
                        break; 
                    case 'V04J003001':
                        Fsl.app.getWidget(component).show();
                        break;
                        
                    case 'V01R004001':
                        Fsl.app.getWidget(component).show();
                        break;
                //                    default          :
                //                        alert('Misc' +component);
                //                        break;
                }
                var title, xtype
                switch(component){                    
                    case 'V01R001001':
                        title = 'Invoice Report'
                        xtype = 'v01r001001' 
                        this.addTabPanel(TabPanel, xtype, title, itemId)
                        break;
                    case 'V01R002001':
                        title = 'Transfer Report'
                        xtype = 'v01r002001'
                        this.addTabPanel(TabPanel, xtype, title, itemId)
                        break;                        
                    case 'V01R003001':
                        title = 'Order Report'
                        xtype = 'v01r003001'
                        this.addTabPanel(TabPanel, xtype, title, itemId)
                        break;
                        
                    /*case 'V01R004001':
                        title = 'Barcode Generaror'
                        xtype = 'v01r004001'
                        this.addTabPanel(TabPanel, xtype, title, itemId)
                        break;*/
                        
                        
                    case 'V01R011001':
                        title = 'Br. Invoice Report'
                        xtype = 'v01r011001'
                        this.addTabPanel(TabPanel, xtype, title, itemId)
                        break;
                    case 'V01R012001':
                        title = 'Br. Transfer Report'
                        xtype = 'v01r012001'
                        this.addTabPanel(TabPanel, xtype, title, itemId)
                        break;                        
                    case 'V01R013001':
                        title = 'Br.Order Report'
                        xtype = 'v01r013001'
                        this.addTabPanel(TabPanel, xtype, title, itemId)
                        break;
                }
                
                switch(component){ 
                
                }
            }else{
                activeTab.tab.show();
                TabPanel.setActiveTab(activeTab);
            }
                
        }
    },
    addTabPanel : function(TabPanel, xtype , title, itemId){
        TabPanel.add({               
            layout  :'fit',
            items   :[{
                xtype   : xtype,
                border  : false
            }],
            title    : title,
            itemId   : itemId,
            closable : true
        }).show();
    }
   
});
