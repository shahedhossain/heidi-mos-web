Ext.define(Fsl.app.getAbsData('D01I001001'), {
    singleton   : true,
    config      : {
        name    : 'D01I001001'
    },
    constructor : function(options){
        this.initConfig(options);
    },
    VERSION     : 1,
    REVISION    : 2,
    info: function(){
        return this.getName();
    },
    more: function(){
        return this.info();
    }
});


