/*        api: {
            create  : '${createLink(controller:'C01t008001', action: 'create')}',
            read    : '${createLink(controller:'C01t008001', action: 'read')}',
            update  : '${createLink(controller:'C01t008001', action: 'update')}',
            destroy : '${createLink(controller:'C01t008001', action: 'removeall')}'
        },*/
       
 Ext.define(Fsl.app.getAbsModel('M01T008002'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01T008002'),
    idProperty	: 'id', 
    fields: [
        {name:'id',              mapping:'id',                type:'int'},
        {name:'articleId',       mapping:'articleId',         type:'int'},
        {name:'invoiceId',       mapping:'invoiceId',         type:'int'},
        {name:'branchId',        mapping:'branchId',          type:'int'},
        {name:'priceId',         mapping:'priceId',           type:'int'},
        {name:'vatId',           mapping:'vatId',             type:'int'},        
        {name:'discountId',      mapping:'discountId',        type:'int'},
        {name:'quantity',        mapping:'quantity',          type:'float'},
        {name:'total',           mapping:'total',             type:'float'},
        {name:'flag',            mapping:'flag',              type:'boolean'},
        {name:'entryDate',       mapping:'entryDate',         type:'date'}
    ],
    proxy       : Fsl.proxy.getAjaxApiProxy('M01T008002'),
    hasMany     :[],
     belongsTo : [
        Fsl.data.getBelongsTo('M01I006002', 'id', 'articleId',    'm01i006001'),
        Fsl.data.getBelongsTo('M01I012002', 'id', 'priceId',      'm01i012001'),
        Fsl.data.getBelongsTo('M01I013002', 'id', 'vatId',        'm01i013001'),
        Fsl.data.getBelongsTo('M01T010002', 'id', 'discountId',   'm01t010001'),
        Fsl.data.getBelongsTo('M01T007002', 'id', 'invoiceId',    'm01t007001'),
        Fsl.data.getBelongsTo('M01I015001', 'id', 'branchId',     'm01i015001')
     ]
});	 