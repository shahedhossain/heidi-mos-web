Ext.define(Fsl.app.getAbsModel('M01T013002'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01T013002'),
    idProperty: 'id', 
    fields: [
        {name:'id',                    mapping:'id',                  type:'int'},
        {name:'orderId',               mapping:'orderId',             type:'int'},
        {name:'articleId',             mapping:'articleId',           type:'int'},
        {name:'quantity',              mapping:'quantity',            type:'float'},
        {name:'approvalQty',           mapping:'approvalQty',         type:'float'},
        {name:'discountId',            mapping:'discountId',          type:'int'},
        {name:'priceId',               mapping:'priceId',             type:'int'},
        {name:'vatId',                 mapping:'vatId',               type:'int'},
        {name:'branchId',              mapping:'branchId',            type:'int'}  
    ],   
    proxy       : Fsl.proxy.getAjaxApiProxy('M01T013001'),
    hasMany     :[],
     belongsTo : [
        Fsl.data.getBelongsTo('M01I014002', 'id', 'orderId',    'm01i014001'),
	Fsl.data.getBelongsTo('M01I006002', 'id', 'articleId',  'm01i006001'),
	Fsl.data.getBelongsTo('M01I013002', 'id', 'vatId',      'm01i013001'),
	Fsl.data.getBelongsTo('M01I012002', 'id', 'priceId',    'm01i012001'),		  
	Fsl.data.getBelongsTo('M01T010002', 'id', 'discountId', 'm01t010001'),
	Fsl.data.getBelongsTo('M01I015001', 'id', 'branchId',   'm01i015001')
     ]
});
