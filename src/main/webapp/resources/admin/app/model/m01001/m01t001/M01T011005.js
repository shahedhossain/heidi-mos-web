
Ext.define(Fsl.app.getAbsModel('M01T011005'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01T011005'),
    idProperty	: 'id', 
     fields: [
        {name:'id',                   mapping:'id',                  type:'int'},
        {name:'branchId',             mapping:'branchId',            type:'int'},
        {name:'toBranchId',           mapping:'toBranchId',          type:'int'},
        {name:'statusId',             mapping:'statusId',            type:'int'},
        {name:'entryDate',            mapping:'entryDate',           type:'date'},
        {name:'receiveId',            mapping:'receiveId',           type:'string'},
        {name:'receiveDate',          mapping:'receiveDate',         type:'date'}
    ],
    proxy       : Fsl.proxy.getAjaxApiProxy('M01T011003'),
    hasMany     :[ 		
    ],
     belongsTo : [  
	Fsl.data.getBelongsTo('M01I015001', 'id', 'branchId',   'm01i015001'),
	Fsl.data.getBelongsTo('M01I015001', 'id', 'toBranchId', 'm01i015002')
     ]
}); 
