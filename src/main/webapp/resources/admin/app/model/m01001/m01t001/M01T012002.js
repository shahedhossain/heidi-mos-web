Ext.define(Fsl.app.getAbsModel('M01T012002'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01T012002'),
    idProperty: 'id', 
    fields: [
        {name:'id',              mapping:'id',                type:'int'}, 
        {name:'branchId',        mapping:'branchId',          type:'int'},
        {name:'fromBranchId',    mapping:'fromBranchId',      type:'int'},        
        {name:'transferId',      mapping:'transferId',        type:'int'},
        {name:'returnId',        mapping:'returnId',          type:'string'},
        {name:'entryDate',       mapping:'entryDate',         type:'date'}
    ],
    proxy       : Fsl.proxy.getAjaxApiProxy('M01T012002'),
    hasMany     :[ 		
       Fsl.data.getHasMany('M01T006001', 'id', 'receiveId', 'm01t006001')
    ],
     belongsTo : [
	  Fsl.data.getBelongsTo('M01I015001', 'id', 'branchId', 'm01i015001'),
	  Fsl.data.getBelongsTo('M01I015001', 'id', 'fromBranchId', 'm01i015002'),
	  Fsl.data.getBelongsTo('M01T011001', 'id', 'transferId', 'm01t011001')
     ]
});