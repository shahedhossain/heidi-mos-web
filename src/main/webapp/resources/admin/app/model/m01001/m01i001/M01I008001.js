Ext.define(Fsl.app.getAbsModel('M01I008001'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01I008001'),
    idProperty: 'id', 
    fields: [
        {name:'id',               mapping:'id',               type:'int'},
        {name:'startDate',        mapping:'startDate',        type:'date'}, // dateFormat:'Y-M-d'},   
        {name:'endDate',          mapping:'endDate',          type:'date'}, // dateFormat:'Y-M-d'},
        {name:'vatPercent',       mapping:'vatPercent',       type:'float'},
        {name:'falg',             mapping:'falg',             type:'boolean'}
    ],
    validations:[ 
        {type:'format',     name:'vatPercent',       matcher: /^\d*[0-9](\.\d*[0-9])?$/},
        {type:'presence',   name:'vatPercent'}
    ], 
    proxy       : Fsl.proxy.getAjaxApiProxy('M01I008001'),
    hasMany     :[ 
        Fsl.data.getHasMany('M01T007001', 'id', 'grossVatId', 'm01t007001s')
    ]
});