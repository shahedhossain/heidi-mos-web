Ext.define(Fsl.app.getAbsModel('M01I009001'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01I009001'),
    idProperty: 'id', 
    fields: [
        {name:'id',               mapping:'id',               type:'int'},
        {name:'startDate',        mapping:'startDate',        type:'date'},   
        {name:'endDate',          mapping:'endDate',          type:'date'},
        {name:'purchaseTarget',   mapping:'purchaseTarget',   type:'float'},
        {name:'rebatePercent',    mapping:'rebatePercent',    type:'float'},
        {name:'flag',             mapping:'flag',             type:'boolean'}
    ],
    validations:[ 
        {type:'format',     name:'purchaseTarget',    matcher: /^\d*[0-9](\.\d*[0-9])?$/},
        {type:'format',     name:'rebatePercent',     matcher: /^\d*[0-9](\.\d*[0-9])?$/}
    ], 
    proxy       : Fsl.proxy.getAjaxApiProxy('M01I009001'),
    hasMany     :[ 
        Fsl.data.getHasMany('M01T007001', 'id', 'rebateId', 'm01t007001s')
    ]
});