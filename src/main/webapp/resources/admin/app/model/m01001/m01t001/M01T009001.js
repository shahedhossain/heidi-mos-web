Ext.define(Fsl.app.getAbsModel('M01T009001'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01T009001'),
    idProperty: 'id', 
    fields: [
        {name:'id',              mapping:'id',                type:'int'},
        {name:'titleId',         mapping:'titleId',           type:'int'},
        {name:'name',            mapping:'name',              type:'string'},
        {name:'branchId',        mapping:'branchId',          type:'int'},
        {name:'contact',         mapping:'contact',           type:'string'},
        {name:'location',        mapping:'location',          type:'string'},
        {name:'address',         mapping:'address',           type:'string'}
    ],
    validations:[        
        {type:'format ',    name:'name',              matcher:/^[\w\d,.#:\-/ ]{2,45}$/},  
        {type:'length ',    name:'contact',           min:5,  max:50},
        {type:'presence ',  name:'contact'},
        {type:'length ',    name:'location',          min:5,  max:100},    
        {type:'length',     name:'address',           min:5,  max:200}   
       ],  
    proxy       : Fsl.proxy.getAjaxApiProxy('M01T009001'),
    hasMany     :[  
          Fsl.data.getHasMany('M04T001001', 'id', 'userId', 'm04t001001s'),
    ],
     belongsTo : [
          Fsl.data.getBelongsTo('M01I015001', 'id', 'branchId', 'm01i015001'),
          Fsl.data.getBelongsTo('M01I007001', 'id', 'titleId', 'm01i007001')
     ]
});