Ext.define(Fsl.app.getAbsModel('M01I003001'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01I003001'),
    idProperty: 'id', 
    fields: [
        {name:'id',              mapping:'id',                type:'int'},
        {name:'name',            mapping:'name',              type:'string'},
        {name: 'categoryId',     mapping:'categoryId',        type: 'int' }        
    ],
    validations:[       
        {type:'format', name:'name',        matcher:/^[\w\d,.#:\-/ ]{2,45}$/},
        {type:'length', name:'name',        min:2, max:45}
    ],
    proxy       : Fsl.proxy.getAjaxApiProxy('M01I003001'),
    hasMany     :[ 
        Fsl.data.getHasMany('M01I005001', 'id', 'genericProductId', 'm01i005001s')
    ],
    belongsTo   : [
        Fsl.data.getBelongsTo('M01I001001', 'id', 'categoryId', 'm01i001001')
    ]
});