Ext.define(Fsl.app.getAbsModel('M01I010001'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01I010001'),
    idProperty: 'id', 
    fields: [
        {name:'id',                   mapping:'id',                 type:'int'},
        {name:'startDate',            mapping:'startDate',          type:'date'},   
        {name:'endDate',              mapping:'endDate',            type:'date'},
        {name:'amount',               mapping:'amount',             type:'float'},
        {name:'percent',              mapping:'percent',            type:'float'},
        {name:'flag',                 mapping:'flag',               type:'boolean'}
    ],
    proxy       : Fsl.proxy.getAjaxApiProxy('M01I010001'),
    hasMany     :[ 
        Fsl.data.getHasMany('M01T007001', 'id', 'promotionId', 'm01t007001s'),
        Fsl.data.getHasMany('M01T013001', 'id', 'discountId', 'm01t013001s')
    ]
});