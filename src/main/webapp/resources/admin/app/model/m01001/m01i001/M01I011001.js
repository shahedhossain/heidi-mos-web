Ext.define(Fsl.app.getAbsModel('M01I011001'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01I011001'),
    idProperty: 'id', 
    fields: [
        {name:'id',                   mapping:'id',                 type:'int'},
        {name:'name',                 mapping:'name',               type:'string'}
    ],
    validations:[ 
        {type:'format',     name:'name',              matcher:/^[\w\d,.#:\-/ ]{2,45}$/},
        {type:'length',     name:'name',              min:3, max:50},
        {type:'presence ',  name:'name'}
    ],
    proxy       : Fsl.proxy.getAjaxApiProxy('M01I011001'),
    hasMany     :[ 
        Fsl.data.getHasMany('M01T007001', 'id', 'payMethodId', 'm01t007001s')        
    ]
});