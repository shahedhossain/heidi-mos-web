Ext.define(Fsl.app.getAbsModel('M01I005001'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01I005001'),
    idProperty: 'id', 
    fields: [
        {name:'id',                 mapping:'id',                   type:'int'},
        {name:'name',               mapping:'name',                 type:'string'},
        {name:'brandTypeId',        mapping:'brandTypeId',          type:'int'},
        {name:'manufacturerId',     mapping:'manufacturerId',       type:'int'},
        {name:'genericProductId',   mapping:'genericProductId',     type:'int'}
        
    ],
    validations:[       
        {type:'format', name:'name',              matcher:/^[\w\d,.#:\-/ ]{2,45}$/},
        {type:'length', name:'name',              min:5, max:45},
    ],
    proxy       : Fsl.proxy.getAjaxApiProxy('M01I005001'),
    hasMany     :[ 
        Fsl.data.getHasMany('M01I006001', 'id', 'articleId', 'm01i006001s')
    ],
    belongsTo   : [
        Fsl.data.getBelongsTo('M01I004001', 'id', 'brandTypeId', 'm01i004001'),
        Fsl.data.getBelongsTo('M01I002001', 'id', 'manufacturerId', 'm01i002001'),
        Fsl.data.getBelongsTo('M01I003001', 'id', 'genericProductId', 'm01i003001')
    ]
});