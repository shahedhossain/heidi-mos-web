 Ext.define(Fsl.app.getAbsModel('M01T004002'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01T004002'),
    idProperty: 'id', 
    fields: [
        {name:'id',              mapping:'id',                type:'int'},
        {name:'articleId',       mapping:'articleId',         type:'int'},
        {name:'quantity',        mapping:'quantity',          type:'float'},       
        {name:'vendorId',        mapping:'vendorId',          type:'int'},
        {name:'branchId',        mapping:'branchId',          type:'int'},
        {name:'priceId',         mapping:'priceId',           type:'int'},
        {name:'vatId',           mapping:'vatId',             type:'int'},
        {name:'date',            mapping:'date',              type:'date'}
    ],
    validations:[ 
        {type:'presence ',  name:'quantity'},
        {type:'format ',    name:'quantity',         matcher: /^\d*[0-9](\.\d*[0-9])?$/}
     ],  
    proxy       : Fsl.proxy.getAjaxApiProxy('M01T004002'),
    hasMany     : [],
    belongsTo   : [
		Fsl.data.getBelongsTo('M01I006002', 'id', 'articleId', 'm01i006001'),		
		Fsl.data.getBelongsTo('M01I012002', 'id', 'priceId', 'm01i012001'),
		Fsl.data.getBelongsTo('M01I013002', 'id', 'vatId', 'm01i013001'),
		Fsl.data.getBelongsTo('M01I014001', 'id', 'vendorId', 'm01i014001'),
		Fsl.data.getBelongsTo('M01I015001', 'id', 'branchId', 'm01i015001')
    ] 
});