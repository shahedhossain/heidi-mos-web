 Ext.define(Fsl.app.getAbsModel('M01T004003'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01T004003'),
    idProperty: 'id', 
    fields: [
        {name:'id',              mapping:'id',                type:'int'},
        {name:'articleId',       mapping:'articleId',         type:'int'},
        {name:'quantity',        mapping:'quantity',          type:'float'}
    ],
    validations:[ 
        {type:'presence ',  name:'quantity'},
        {type:'format ',    name:'quantity',         matcher: /^\d*[0-9](\.\d*[0-9])?$/}
     ],  
    proxy       : Fsl.proxy.getAjaxApiProxy('M01T004002'),
    hasMany     : [],
    belongsTo   : [
		Fsl.data.getBelongsTo('M01I006002', 'id', 'articleId', 'm01i006001')
    ] 
});