Ext.define(Fsl.app.getAbsModel('M01T010002'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01T010002'),
    idProperty: 'id', 
     fields: [
        {name:'id',           mapping:'id',          type:'int'},
        {name:'startDate',    mapping:'startDate',   type:'date'},
        {name:'endDate',      mapping:'endDate',     type:'date'},
        {name:'discount',     mapping:'discount',    type:'float'}
    ],
    validations:[ 
        {type:'presence ',  name:'vat'},
        {type:'format ',    name:'vat',              matcher: /^\d*[0-9](\.\d*[0-9])?$/},
        {type:'format',     name:'discount',         matcher: /^\d*[0-9](\.\d*[0-9])?$/}
    ],  
    proxy       : Fsl.proxy.getAjaxApiProxy('M01T010002'),
    hasMany     : [ 
		Fsl.data.getHasMany('M01T005002', 'id', 'discountId', 'm01t005002s'),
		Fsl.data.getHasMany('M01T008002', 'id', 'discountId', 'm01t008002s'),
		Fsl.data.getHasMany('M01T013002', 'id', 'discountId', 'm01t013002s')
	],
    belongsTo   : [] 
});




