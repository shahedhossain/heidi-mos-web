Ext.define(Fsl.app.getAbsModel('M01I006002'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01I006002'),
    idProperty: 'id', 
    fields: [
        {name:'id',                 mapping:'id',                   type:'int'},        
        {name:'name',               mapping:'name',                 type:'string'},
        {name:'mbcode',             mapping:'mbcode',               type:'string'},
        {name:'pbcode',             mapping:'pbcode',               type:'string'},
        {name:'details',            mapping:'details',              type:'string'}
    ],
    validations:[       
        {type:'format', name:'name',              matcher:/^[\w\d,.#:\-/ ]{2,45}$/},
        {type:'length', name:'name',              min:5, max:45},        
        {type:'length', name:'mbcode',            min:4, max:45},
        {type:'length', name:'pbcode',            min:4, max:45}
    ],
    proxy       : Fsl.proxy.getAjaxApiProxy('M01I006002'),
    hasMany     :[ 
        Fsl.data.getHasMany('M01T005002', 'id', 'articleId', 'm01t005002s'),
        Fsl.data.getHasMany('M01T008002', 'id', 'articleId', 'm01t008002s'),
        Fsl.data.getHasMany('M01T013002', 'id', 'articleId', 'm01t013002s'),
	Fsl.data.getHasMany('M01T002002', 'id', 'articleId', 'm01t002002s'),        
        Fsl.data.getHasMany('M01T003002', 'id', 'articleId', 'm01t003002s'),
        Fsl.data.getHasMany('M01T004002', 'id', 'articleId', 'm01t004002s'),        
	Fsl.data.getHasMany('M01T006002', 'id', 'articleId', 'm01t006002s')
    ]
});
