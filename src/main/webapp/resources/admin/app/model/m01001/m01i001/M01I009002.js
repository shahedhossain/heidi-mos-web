Ext.define(Fsl.app.getAbsModel('M01I009002'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01I009002'),
    idProperty: 'id', 
    fields: [
        {name:'id',               mapping:'id',               type:'int'},
        {name:'startDate',        mapping:'startDate',        type:'date'},   
        {name:'endDate',          mapping:'endDate',          type:'date'},
        {name:'purchaseTarget',   mapping:'purchaseTarget',   type:'float'},
        {name:'rebatePercent',    mapping:'rebatePercent',    type:'float'}
    ],
    validations:[ 
        {type:'format',     name:'purchaseTarget',    matcher: /^\d*[0-9](\.\d*[0-9])?$/},
        {type:'format',     name:'rebatePercent',     matcher: /^\d*[0-9](\.\d*[0-9])?$/}
    ], 
    proxy       : getAjaxUrlProxy('m01001/m01i001/c01i009001/getRebateByDate'),
    hasMany     :[ 
        Fsl.data.getHasMany('M01T007001', 'id', 'rebateId', 'm01t007001s')
    ]
});