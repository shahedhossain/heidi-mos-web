Ext.define(Fsl.app.getAbsModel('M01T010001'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01T010001'),
    idProperty: 'id', 
     fields: [
        {name:'id',           mapping:'id',          type:'int'},
        {name:'articleId',    mapping:'articleId',   type:'int'},   
        {name:'startDate',    mapping:'startDate',   type:'date'},
        {name:'endDate',      mapping:'endDate',     type:'date'},
        {name:'discount',     mapping:'discount',    type:'float'}
    ],
    validations:[ 
        {type:'presence ',  name:'vat'},
        {type:'format ',    name:'vat',              matcher: /^\d*[0-9](\.\d*[0-9])?$/},
        {type:'format',     name:'discount',         matcher: /^\d*[0-9](\.\d*[0-9])?$/}
    ],    
    proxy       : Fsl.proxy.getAjaxApiProxy('M01T010001'),
    hasMany     : [ 
	Fsl.data.getHasMany('M01T008001', 'id', 'discountId', 'm01t008001s'),
	Fsl.data.getHasMany('M01T005001', 'id', 'discountId', 'm01t005001s'),
	Fsl.data.getHasMany('M01T006001', 'id', 'discountId', 'm01t006001s'),
        /*for return item*/
        Fsl.data.getHasMany('M01T016001', 'id', 'discountId', 'm01t016001s')
	],
    belongsTo   : [		
		Fsl.data.getBelongsTo('M01I006001', 'id', 'articleId', 'm01i006001')
    ] 
});



