Ext.define(Fsl.app.getAbsModel('M01T007003'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01T007003'),
    idProperty	: 'id', 
    fields: [
        {name:'id',              mapping:'id',               type:'int'},        
        {name:'customerId',      mapping:'customerId',       type:'int'},
        {name:'grossVatId',      mapping:'grossVatId',       type:'int'},
        {name:'rebateId',        mapping:'rebateId',         type:'int'},
        {name:'promotionId',     mapping:'promotionId',      type:'int'},
        {name:'payMethodId',     mapping:'paymethodId',      type:'int'},        
        {name:'branchId',        mapping:'branchId',         type:'int'},
        {name:'entryDate',       mapping:'entryDate',        type:'date'},
        {name:'payAmount',       mapping:'payamount',        type:'float'},
        {name:'total',           mapping:'total',            type:'float'},
        {name:'rebate',          mapping:'rebate',           type:'float'},
        {name:'grossVat',        mapping:'grossVat',         type:'float'},
        {name:'promotion',       mapping:'promotion',        type:'float'},
        {name:'gtotal',          mapping:'gtotal',           type:'float'},
        {name:'dateStatus',      mapping:'dateStatus',       type:'string'}
    ],
    validations:[ 
        {type:'length',     name:'payAmount',           max:100,min:4}
    ],  
    //proxy       : Fsl.proxy.getAjaxApiProxy('M01T007003'),
    proxy       : Fsl.proxy.getAjaxUrlProxy('m01001/m01t001/c01t007002/read'),
    hasMany     :[],
     belongsTo : [
          Fsl.data.getBelongsTo('M01T001001', 'id', 'customerId', 'm01t001001'),
		  Fsl.data.getBelongsTo('M01I008001', 'id', 'grossVatId', 'm01i008001'),
		  Fsl.data.getBelongsTo('M01I009001', 'id', 'rebateId', 'm01i009001'),
		  Fsl.data.getBelongsTo('M01I010001', 'id', 'promotionId', 'm01i010001'),
		  Fsl.data.getBelongsTo('M01I011001', 'id', 'payMethodId', 'm01i011001'),
		  Fsl.data.getBelongsTo('M01I015001', 'id', 'branchId', 'm01i015001')
     ]
});	 

