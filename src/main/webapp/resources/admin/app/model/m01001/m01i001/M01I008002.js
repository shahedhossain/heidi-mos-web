Ext.define(Fsl.app.getAbsModel('M01I008002'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01I008002'),
    idProperty: 'id', 
     fields: [
        {name:'id',               mapping:'id',               type:'int'},
        {name:'startDate',        mapping:'startDate',        type:'date'},   
        {name:'endDate',          mapping:'endDate',          type:'date'},
        {name:'vatPercent',       mapping:'vatPercent',       type:'float'},
        {name:'falg',             mapping:'falg',             type:'boolean'}
    ],
    validations:[ 
        {type:'format',     name:'vatPercent',       matcher: /^\d*[0-9](\.\d*[0-9])?$/},
        {type:'presence',   name:'vatPercent'}
    ], 
    proxy       : getAjaxUrlProxy('m01001/m01i001/c01i008001/getGrossVatByDate'),
    hasMany     :[ 
       Fsl.data.getHasMany('M01T007001', 'id', 'grossVatId', 'm01t007001s')
    ]
});
