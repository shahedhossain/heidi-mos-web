Ext.define(Fsl.app.getAbsModel('M01T014002'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01T014002'),
    idProperty: 'id', 
    fields: [
        {name:'id',                   mapping:'id',                  type:'int'},
        {name:'branchId',             mapping:'branchId',            type:'int'},
        {name:'toBranchId',           mapping:'toBranchId',          type:'int'},
        {name:'entryDate',            mapping:'entryDate',           type:'date'},
        {name:'orderId',              mapping:'orderId',             type:'int'}
    ], 
    proxy       : Fsl.proxy.getAjaxApiProxy('M01T014001'),
    hasMany     :[ 		
        Fsl.data.getHasMany('M01T013002', 'id', 'orderId', 'm01t013002s')
    ],
     belongsTo : [
        Fsl.data.getBelongsTo('M01I015001', 'id', 'branchId',   'm01i015001'),
	Fsl.data.getBelongsTo('M01I015001', 'id', 'toBranchId', 'm01i015002')
     ]
});