Ext.define(Fsl.app.getAbsModel('M01T016001'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01T016001'),
    idProperty: 'id', 
    fields: [
        {name:'id',                    mapping:'id',                  type:'int'},
        {name:'returnId',              mapping:'returnId',            type:'int'},
        {name:'articleId',             mapping:'articleId',           type:'int'},
        {name:'discountId',            mapping:'discountId',          type:'int'},
        {name:'priceId',               mapping:'priceId',             type:'int'},
        {name:'vatId',                 mapping:'vatId',               type:'int'},
        {name:'branchId',              mapping:'branchId',            type:'int'},
        {name:'sendQty',               mapping:'sendQty',             type:'float'},
        {name:'returnQty',             mapping:'returnQty',           type:'float'},
        {name:'statusId',              mapping:'statusId',            type:'int'}
    ],  
    proxy       : Fsl.proxy.getAjaxApiProxy('M01T016001'),
    hasMany     :[],
     belongsTo : [
        Fsl.data.getBelongsTo('M01T015001', 'id', 'returnId',     'm01t015001'),
	Fsl.data.getBelongsTo('M01I006001', 'id', 'articleId',    'm01i006001'),
	Fsl.data.getBelongsTo('M01I013001', 'id', 'vatId',        'm01i013001'),
	Fsl.data.getBelongsTo('M01I012001', 'id', 'priceId',      'm01i012001'),		  
	Fsl.data.getBelongsTo('M01T010001', 'id', 'discountId',   'm01t010001'),
	Fsl.data.getBelongsTo('M01I015001', 'id', 'branchId',     'm01i015001'),
        Fsl.data.getBelongsTo('M01I016001', 'id', 'statusId',     'm01i016001')
     ]
});



