        /*api: {
            read    : '${createLink(controller:'C01t011002', action: 'read')}'
        },*/
Ext.define(Fsl.app.getAbsModel('M01T011003'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01T011003'),
    idProperty	: 'id', 
     fields: [
        {name:'id',                   mapping:'id',                  type:'int'},
        {name:'branchId',             mapping:'branchId',            type:'int'},
        {name:'toBranchId',           mapping:'toBranchId',          type:'int'},
        {name:'statusId',             mapping:'statusId',            type:'int'},
        {name:'entryDate',            mapping:'entryDate',           type:'date'},
        {name:'receiveId',            mapping:'receiveId',           type:'string'},
        {name:'receiveDate',          mapping:'receiveDate',         type:'date'}
    ],
    proxy       : Fsl.proxy.getAjaxApiProxy('M01T011002'),
    hasMany     :[ 		
        Fsl.data.getHasMany('M01T005001', 'id', 'transferId', 'm01t005001'),
	Fsl.data.getHasMany('M01T012001', 'id', 'transferId', 'm01t012001')
    ],
     belongsTo : [  
	Fsl.data.getBelongsTo('M01I015001', 'id', 'branchId', 'm01i015001'),
	Fsl.data.getBelongsTo('M01I015001', 'id', 'toBranchId', 'm01i015002')
     ]
}); 
