Ext.define(Fsl.app.getAbsModel('M01T017002'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M01T017002'),
    idProperty: 'id', 
    fields: [
        {name:'id',                   mapping:'id',                  type:'int'},
        {name:'branchId',             mapping:'branchId',            type:'int'},
        {name:'fromBranchId',         mapping:'fromBranchId',        type:'int'},
        {name:'status',               mapping:'status',              type:'string'},
        {name:'entryDate',            mapping:'entryDate',           type:'date'},
        {name:'orderId',              mapping:'orderId',             type:'int'},
        {name:'orderApprovalId',      mapping:'orderApprovalId',     type:'int'},
        {name:'transferId',           mapping:'transferId',          type:'int'}
    ], 
    proxy       : Fsl.proxy.getAjaxApiProxy('M01T017001'),
    hasMany     :[ 		
        Fsl.data.getHasMany('M01T018001', 'id', 'orderApproveId', 'm01t018001'),
        Fsl.data.getHasMany('M01T018002', 'id', 'orderApproveId', 'm01t018001')
    ],
     belongsTo : [
        Fsl.data.getBelongsTo('M01I015001', 'id', 'branchId',     'm01i015001'),
        Fsl.data.getBelongsTo('M01I015001', 'id', 'fromBranchId', 'm01i015002'),
        Fsl.data.getBelongsTo('M01T014003', 'id', 'orderId',      'm01t014001'),
        Fsl.data.getBelongsTo('M01T011004', 'id', 'transferId',   'm01t011001')
     ]
});