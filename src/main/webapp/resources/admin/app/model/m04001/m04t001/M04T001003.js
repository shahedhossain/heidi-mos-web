Ext.define(Fsl.app.getAbsModel('M04T001003'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M04T001003'),
    idProperty: 'id', 
    fields: [
    {name:'id',            mapping:'id',            type:'int' },
    {name:'username',      mapping:'username',      type:'string'},
    {name:'branch',         mapping:'branch',        type:'string'}
    ],
    proxy      : Fsl.proxy.getAjaxApiProxy('M04T001003'),
    hasMany     :[   ],
    belongsTo : [
        Fsl.data.getBelongsTo('M01T009001', 'id', 'userId', 'm01t009001')
    ]
});