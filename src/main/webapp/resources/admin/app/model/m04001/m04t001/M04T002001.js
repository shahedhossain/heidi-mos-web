Ext.define(Fsl.app.getAbsModel('M04T002001'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M04T002001'),
    idProperty: 'id', 
    fields: [
    {name:'id',             mapping:'id',           type:'int' },
    {name:'authority',      mapping:'authority',    type:'string'},
    {name:'note',           mapping:'note',         type:'string'}
    ],
    validations:[       
       { type:'length',        field:'authority',         min:1,        max:60} 
    ],
    proxy       : Fsl.proxy.getAjaxApiProxy('M04T002001'),
    hasMany     : [  ],
    belongsTo   : [  ]
});