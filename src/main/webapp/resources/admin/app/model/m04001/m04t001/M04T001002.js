Ext.define(Fsl.app.getAbsModel('M04T001002'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M04T001002'),
    idProperty: 'id', 
    fields: [
    {name:'id',            mapping:'id',            type:'int' },
    {name:'username',      mapping:'username',      type:'string'},
    {name:'password',      mapping:'password',      type:'string'},
    {name:'accountLocked', mapping:'accountLocked', type:'boolean'},
    {name:'enabled',       mapping:'enabled',       type:'boolean'},
    {name:'accountExpired', mapping:'accountExpired',  type:'boolean'},
    {name:'passwordExpired',mapping:'passwordExpired',type:'boolean'},
    {name:'userId',         mapping:'userId',        type:'int'},
    {name:'branch',         mapping:'branch',        type:'string'}
    ],
   /* validations:[       
    { type:'format',        field:'password',         matcher:/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/},
    { type:'length',        field:'password',         min:8,        max:60} 
    ],*/
    proxy      : Fsl.proxy.getAjaxApiProxy('M04T001001'),
    hasMany     :[   ],
    belongsTo : []
});