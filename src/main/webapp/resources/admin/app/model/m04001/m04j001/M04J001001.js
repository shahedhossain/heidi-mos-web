Ext.define(Fsl.app.getAbsModel('M04J001001'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M04J001001'),
    idProperty: 'id', 
    fields: [
    {name:'userId',         mapping:'userId',       type:'int'},
    {name:'accountId',      mapping:'accountId',    type:'int'},
    {name:'authorityId',    mapping:'authorityId',       type:'int'}
    ],
    
    proxy       : Fsl.proxy.getAjaxApiProxy('M04J001001'),
    hasMany     : [  ],
    belongsTo   : [ 
        Fsl.data.getBelongsTo('M04T001002', 'id', 'accountId',   'm04t001001'),
        Fsl.data.getBelongsTo('M04T002001', 'id', 'authorityId', 'm04t002001')
     ]
});