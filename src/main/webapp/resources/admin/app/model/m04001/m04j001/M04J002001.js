Ext.define(Fsl.app.getAbsModel('M04J002001'), {
    extend      : 'Ext.data.Model',
    alias       : Fsl.app.getAlias('M04J002001'),
    idProperty: 'id', 
    fields: [
    {name:'userId',         mapping:'userId',       type:'int'},
    {name:'accountId',      mapping:'accountId',    type:'int'},
    {name:'roleId',         mapping:'roleId',       type:'int'},
    {name:'roleName',       mapping:'roleName',     type:'string'},
    {name:'userName',       mapping:'userName',     type:'string'}
    ],
    
    proxy       : Fsl.proxy.getAjaxApiProxy('M04J002001'),
    hasMany     : [  ],
    belongsTo   : [ 
        Fsl.data.getBelongsTo('M04T001002', 'id', 'accountId',   'm04t001001'),
        Fsl.data.getBelongsTo('M04T003001', 'id', 'roleId',      'm04t003001')
     ]
});