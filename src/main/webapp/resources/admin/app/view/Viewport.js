Ext.define('Fsl.admin.view.Viewport', {
    extend       : 'Ext.Viewport',
    requires     : [            
    'Fsl.admin.view.WestPanel',
    'Fsl.admin.view.CenterPanel'      
    ],
    layout       : {
        type : 'border'
    },
    defaults     : {
        split: true
    }, 
    initComponent: function(config) {
        var me = this;  
        me.setUser();
        me.items = [{
            region   : 'north',
            bodyCls  : 'fsl-banner-bg',
            html     : '&nbsp; <a id="logout" onclick="Fsl.security.signout();">Signout</a><span class="fsl-banner-text">Formative Soft Ltd </span>&nbsp <span class="fsl-copyright">&copy 2013 All Right Reserved</span><span id="fsl-setUser"></span>',                 
            bodyStyle: {
                background : 'body',
                padding    : '5px'
            },
            height   : 35,
            margin   : '0 0 0 0',
            border   : false
        },
        {                   
            region   : 'center',
            xtype    : 'centerPanel',
            margin   : '0 5 4 0'
        },
        {
            region   : 'west',
            xtype    : 'westPanel',
            split    : true,
            collapsible :true,
            collapseMode:'mini',
            width    : 400,
            margin   : '0 0 4 5'
        }]; 
        me.callParent(arguments);
    },
    setUser :function(){
            Fsl.model.load('M04T001003', {
                id:1
            }, {
                scope   : this,
                success : function(records, operation, success) { 
                    var title       = records.getM01t009001().getM01i007001().data.name;
                    var userName    = records.getM01t009001().data.name;
                    var branch      = records.getM01t009001().getM01i015001().data.name,
                    a               = branch +" | "+title+" "+userName+ " | ";
                     Ext.DomHelper.append('fsl-setUser', a);
                }
            });
    }
});
