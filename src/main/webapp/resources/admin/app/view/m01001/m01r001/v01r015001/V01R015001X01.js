Ext.define(Fsl.app.getAbsView('V01R015001X01'), {
    extend      : 'Ext.window.Window',
    alias       : Fsl.app.getAlias('V01R015001X01'),
    icon        : Fsl.route.getImage('APP01009.png'),
    id          : 'V01R015001X01'.toString(),
    title       : 'BRANCH LVM :: V01R015001X01',
    minimizable :  false,
    maximizable :  false,
    autoShow    :  true,
    resizable   :  false,
    modal       :  true, 
    layout      :  {
        type	: 'vbox',
        align	: 'stretch'
    },
    items  : [{ 
        xtype     	: Fsl.app.getXtype('V01R015001X02'), 
        resizable 	: false,
        border    	: false,
        componentCls    : 'my-border-bottom'
    },{ 
        xtype     	: Fsl.app.getXtype('V01R015001X03'),       
        resizable 	: false,
        border    	: false
    }]
});