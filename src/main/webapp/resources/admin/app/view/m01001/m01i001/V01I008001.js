Ext.define(Fsl.app.getAbsView('V01I008001'), {
    extend      : 'Ext.window.Window',
    alias       : Fsl.app.getAlias('V01I008001'),
    id          : 'V01I008001'.toLowerCase(),
    title       : 'GROSS VAT LVM :: V01I008001',
    icon        : Fsl.route.getImage('APP01009.png'),
    minimizable : false,
    maximizable : false,
    autoShow    : true,
    resizable   : false,
    modal       : true,
    width       : 450,
    height      : 365, 
      layout    : {
            type: 'vbox',
            align: 'stretch'
        },
    items  : [{ 
        xtype       :  Fsl.app.getXtype('V01I008001X01'),
        resizable   :  false,
        border      :  false,
        componentCls: 'my-border-bottom'
    },{ 
        xtype       :  Fsl.app.getXtype('V01I008001X02'),
        resizable   :  false,
        border      :  false
    }]
});