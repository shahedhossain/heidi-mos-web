Ext.define(Fsl.app.getAbsView('V01I011001'), {
    extend      : 'Ext.window.Window',
    alias       : Fsl.app.getAlias('V01I011001'),
    id          : 'V01I011001'.toLowerCase(),
    title       : 'PAY METHOD LVM :: V01I011001',
    icon        : Fsl.route.getImage('APP01009.png'),
    minimizable : false,
    maximizable : false,
    autoShow    : true,
    resizable   : false,
    modal       : true,    
    width       : 305,
    height      : 365, 
    
      layout    : {
            type: 'vbox',
            align: 'stretch'
        },
    items  : [{ 
        xtype       :  Fsl.app.getXtype('V01I011001X01'),
        resizable   :  false,
        border      :  false
    }]
});