Ext.define(Fsl.app.getAbsView('V01I001002X08'),{
    extend          : 'Ext.grid.Panel',
    alias           : Fsl.app.getAlias('V01I001002X08'),
    id              : 'V01I001002X08'.toLowerCase(),
    border          : true,
    modal           : true,
    width           : 370,
    height          : 285,
    viewConfig  : {
        stripeRows    : true,
        forceFit      : true,
        emptyText     : 'No Records to display',
        listeners     : {
            viewready : function(v) {               
                var store = Fsl.store.getStore('S01I002004');
                store.clearFilter();
                store.loadPage(1);
            }
        }
    },
    initComponent: function() {
        var me      = this;
        me.store    = Fsl.app.getRelStore('S01I002004');    
        me.tbar= Ext.create('Ext.ux.StatusBar', {            
            topBorder       : false,
            statusAlign     : 'right',
            items    : [{
                xtype       :'button',
                text        : 'Clear',
                icon        :  Fsl.route.getIcon('IC01001/CAN01003.png'),
                action      : 'clear'
            },'-',{
                xtype       : 'button',
                text        : 'search',
                icon        :  Fsl.route.getIcon('IC01001/search.png'),
                action      : 'search'
            },'-',{  
                xtype       : 'statusbar',
                id          : 'win-statusbar-v01i001002x08',
                border      :  false,
                defaultText : '',
                statusAlign : 'right'
            }]
        });
        
        this. bbar= new Ext.PagingToolbar({
            pageSize    : 10,
            store       : Fsl.app.getRelStore('S01I002004'),
            displayInfo : true
        });
        this.columns = [{
            text      : 'SN',
            xtype     : 'rownumberer',
            width     :  40, 
            sortable  :  false
        },{
            text        : 'ID',
            dataIndex   : 'id',
            width       :  40, 
            sortable    :  false
        },{
            text        : 'Manufacturer',
            dataIndex   : 'name',
            flex        :  1
        }];
        this.callParent(arguments);
    }
});