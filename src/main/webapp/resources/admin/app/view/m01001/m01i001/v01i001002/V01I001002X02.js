Ext.define(Fsl.app.getAbsView('V01I001002X02'),{
    extend      : 'Ext.grid.Panel',
    alias       : Fsl.app.getAlias('V01I001002X02'),
    id          : 'V01I001002X02'.toLowerCase(),
    border      : true,
    modal       : true,
    width       : 370,
    height      : 285, 
    viewConfig  : {
        stripeRows    : true,
        forceFit      : true,
        emptyText     : 'No Records to display',
        listeners     : {
            viewready : function(v) {                
                var store = Fsl.store.getStore('S01I001003');
                store.clearFilter();
                store.loadPage(1);
            }
        }
    }, 
   /* dockedItems :[{
       xtype: 'pagingtoolbar' ,
       store :Fsl.app.getRelStore('S01I001003'),
       dock : 'buttom',
       displayInfo: true       
    }],*/
    initComponent: function() {
        var me      = this;
        me.store    = Fsl.app.getRelStore('S01I001003');            
        me.tbar     = Ext.create('Ext.ux.StatusBar', {           
            topBorder       : false,
            statusAlign     : 'right',
            items    : [{
                xtype       :'button',
                text        : 'Clear',
                icon        :  Fsl.route.getIcon('IC01001/CAN01003.png'),
                action      : 'clear'
            },'-',{
                xtype       : 'button',
                text        : 'search',
                icon        :  Fsl.route.getIcon('IC01001/search.png'),
                action      : 'search'
            },'-',{  
                xtype       : 'statusbar',
                id          : 'win-statusbar-v01i001002x02',
                border      :  false,
                defaultText : '',
                statusAlign : 'right'
            }]
        });
        
        this.bbar = Ext.create('Ext.PagingToolbar', {
            store           : Fsl.app.getRelStore('S01I001003'),
            displayInfo     : true,
            displayMsg      : 'Display {0} - {1} of {2}',
            emptyMsg        : "No Items"   
        });
        

        this.columns        = [{
            text            : 'SN',
            xtype           : 'rownumberer',
            width           :  40, 
            sortable        :  false
        },{
            text            : 'ID',
            dataIndex       : 'id',
            width           :  40, 
            sortable        :  false
        },{
            text            : 'Category',
            dataIndex       : 'name',
            flex            :  1
        }];
        this.callParent(arguments);
    }
});