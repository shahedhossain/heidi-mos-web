Ext.define(Fsl.app.getAbsView('V01I015001'), {
    extend      : 'Ext.window.Window',
    alias       : Fsl.app.getAlias('V01I015001'),
    icon        : Fsl.route.getImage('APP01009.png'),
    id          : 'V01I015001'.toLowerCase(),
    title       : 'BRANCH LVM :: V01I015001',
    minimizable :  false,
    maximizable :  false,
    autoShow    :  true,
    resizable   :  false,
    modal       :  true, 
    layout      :  {
        type	: 'vbox',
        align	: 'stretch'
    },
    items  : [{ 
        xtype     	: Fsl.app.getXtype('V01I015001X01'), 
        resizable 	: false,
        border    	: false,
        componentCls    : 'my-border-bottom'
    },{ 
        xtype     	: Fsl.app.getXtype('V01I015001X02'),       
        resizable 	: false,
        border    	: false
    }]
});