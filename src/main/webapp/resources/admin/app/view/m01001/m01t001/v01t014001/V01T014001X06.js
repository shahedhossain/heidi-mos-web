Ext.define(Fsl.app.getAbsView('V01T014001X06'), {
    extend          : 'Ext.window.Window',
    alias           : Fsl.app.getAlias('V01T014001X06'),
    icon            : Fsl.route.getImage('APP01009.png'),
    //id              : 'V01T014001X06'.toLowerCase(),
    title           : 'BRANCH LVM :: V01T014001X06',
    minimizable     :  false,
    maximizable     :  false,
    autoShow        :  true,
    resizable       :  false,
    modal           :  true,
    layout          :  {
        type  : 'vbox',
        align : 'stretch'
    },
    items  : [{ 
        xtype           :  Fsl.app.getXtype('V01T014001X07'),           
        resizable       :  false,
        border          :  false,
        width           :  400,
        componentCls    :  'my-border-bottom'
    },{ 
        xtype           : Fsl.app.getXtype('V01T014001X08'),           
        resizable       : false,
        border          : false
    }]
});