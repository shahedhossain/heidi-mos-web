Ext.define(Fsl.app.getAbsView('V01I012001'), {
    extend      : 'Ext.window.Window',
    alias       : Fsl.app.getAlias('V01I012001'),
    id          : 'V01I012001'.toLowerCase(),
    title       : 'PRICE LVM :: V01I012001',
    icon        : Fsl.route.getImage('APP01009.png'),
    minimizable : false,
    maximizable : false,
    autoShow    : true,
    resizable   : false,
    modal       : true,
    width       : 650,
    height      : 410, 
      layout    : {
            type: 'vbox',
            align: 'stretch'
        },
    items  : [{ 
        xtype       :  Fsl.app.getXtype('V01I012001X01'),
        resizable   :  false,
        border      :  false,
        componentCls: 'my-border-bottom'
    },{ 
        xtype       :  Fsl.app.getXtype('V01I012001X02'),
        resizable   :  false,
        border      :  false
    }]
});