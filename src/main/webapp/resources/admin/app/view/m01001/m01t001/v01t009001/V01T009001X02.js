Ext.define(Fsl.app.getAbsView('V01T009001X02'), {
    extend    : 'Ext.grid.Panel',
    alias     : Fsl.app.getAlias('V01T009001X02'),
    id        : 'V01T009001X02'.toLowerCase(),
    border    : true,
    modal     : true,
    height    : 285,
    width     : 570,
    viewConfig: {
        stripeRows    : true,
        forceFit      : true,
        emptyText     : 'No Records to display',
        listeners     : {
            viewready : function(v) {               
                var store = Fsl.store.getStore('S01T009001');
                store.clearFilter();
                store.loadPage(1);
            }
        }
    },
    columnLines: true,
    //frame: true,
    initComponent: function() {
    var me     	= this;
    me.store   	= Fsl.app.getRelStore('S01T009001');    
    me.tbar = Ext.create('Ext.ux.StatusBar', {            
            topBorder           : false,
            statusAlign         : 'right',
            items     : [{
                xtype       : 'button',
                text        : 'ADD',
                icon        : Fsl.route.getIcon('TB01001/ADD01004.png'),
                action      : 'add'
            },'-',{
                xtype       : 'button',
                text        : 'Clear',
                icon        : Fsl.route.getIcon('IC01001/CAN01003.png'),
                action      : 'clear'
            },'-',{
                xtype       : 'button',
                text        : 'search',
                icon        : Fsl.route.getIcon('IC01001/search.png'),
                action      : 'search'
            },'-',{  
                xtype       : 'statusbar',
                id          : 'win-statusbar-v01t009001x02',
                border      : false,
                defaultText : '',
                statusAlign : 'right'
            }]
        });
        
        this.bbar = new Ext.PagingToolbar({
            pageSize        :  10,
            store           : Fsl.app.getRelStore('S01T009001'),
            displayInfo     :  true
        });  

        this.columns = [{
            text            : 'ID',
            dataIndex       : 'id',
            width           : 40, 
            sortable        : false            
        },{
            text            : 'User name',
            dataIndex       : 'name',
            renderer        :  this.getTitle
        },{
            text            : 'Branch Name',
            dataIndex       : 'branchId',
            renderer        :  this.getBranchName,
            flex            :  1
        },{
            text            : 'Contact',
            dataIndex       : 'contact'
        },{
            text            : 'Location',
            dataIndex       : 'location'
        },{
            text            : 'Address',
            dataIndex       : 'address'
        },{
            menuDisabled    : true,
            sortable        : false,
            xtype           : 'actioncolumn',            
            width           : 22,            
            items           : [{
                icon        :  Fsl.route.getImage('DEL01005.png'),
                tooltip     : 'Delete This?',
                scope       : this,
                handler     : function(grid, rowIdx, colIdx) {
                    var record = grid.getStore().getAt(rowIdx);
                    var userId = record.data.id
                    Ext.getCmp('WestPanel').confirmDelete(userId, 'M01T009001', 'S01T009001', 'v01t009001x02');
                }
            }]
        }];       
        this.callParent(arguments);
    },
    getTitle:function(value, metadata, record, rowIdx, colIdx, store, vie) {
              var title = record.getM01i007001().data.name;
              return title+''+value;
    },
    getBranchName :function(value, metadata, record, rowIdx, colIdx, store, vie) {
              return record.getM01i015001().data.name;
    }
});