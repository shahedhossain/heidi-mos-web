Ext.define(Fsl.app.getAbsView('V01I014001X02'), {   
    extend          : 'Ext.grid.Panel',
    alias           : Fsl.app.getAlias('V01I014001X02'),
    id              : 'V01I014001X02'.toLowerCase(), 
    border    : true,
    modal     : true,
    height    : 285,
    width     : 455,
    viewConfig: {
        stripeRows    : true,
        forceFit      : true,
        emptyText     : 'No Records to display',
        listeners     : {
            viewready : function(v) {               
                var store = Fsl.store.getStore('S01I014001');
                store.clearFilter();
                store.loadPage(1);
            }
        }
    },
    initComponent: function() {
       var me      = this;
        me.store    = Fsl.app.getRelStore('S01I014001');    
        me.tbar = Ext.create('Ext.ux.StatusBar', {            
            topBorder           : false,
            statusAlign         : 'right',
            items     : [{
                xtype       :'button',
                text        : 'ADD',
                icon        : Fsl.route.getIcon('TB01001/ADD01004.png'),
                action      : 'add'
            },'-',{
                xtype       :'button',
                text        : 'Clear',
                icon        : Fsl.route.getIcon('IC01001/CAN01003.png'),
                action      : 'clear'
            },'-',{
                xtype       : 'button',
                text        : 'search',
                icon        : Fsl.route.getIcon('IC01001/search.png'),
                action      : 'search'
            },'-',{  
                xtype       :'statusbar',
                id          : 'win-statusbar-v01i014001x02',
                border      : false,
                defaultText : '',
                statusAlign : 'right'
            }]
        });
        
        this. bbar= new Ext.PagingToolbar({
            pageSize        : 10,
            store           : Fsl.app.getRelStore('S01I014001'),
            displayInfo     : true
        });

        this.columns = [{
            text            : 'SN',
            xtype           : 'rownumberer',
            width           :  40, 
            sortable        :  false
        },{
            text            : 'ID',
            dataIndex       : 'id',
            width           :  40, 
            sortable        :  false
        },{
            text            : 'Vendor Name',
            dataIndex       : 'name',
            flex            :  1,
            renderer        :  this.getTitle
        },{
            text            : 'Contact',
            dataIndex       : 'contact'
        },{
            text            : 'Address',
            dataIndex       : 'address'
        },{
            menuDisabled    :  true,
            sortable        :  false,
            xtype           : 'actioncolumn',            
            width           :  22,            
            items           :  [{
                icon        :  Fsl.route.getImage('DEL01005.png'),
                tooltip     : 'Delete This?',
                scope       :  this,
                handler     :  function(grid, rowIdx, colIdx) {
                    var record = grid.getStore().getAt(rowIdx);
                    var vandorId = record.data.id
                    Ext.getCmp('WestPanel').confirmDelete(vandorId, 'M01I014001', 'S01I014001', 'v01i014001x02');
                }
            }]
        }];       
        this.callParent(arguments);
    },      
    getTitle :function(value, metadata, record, rowIdx, colIdx, store, vie) {
              var title = record.getM01i007001().data.name;
              return  title+''+value
    } 
});