Ext.define(Fsl.app.getAbsView('V01I010001X02'), {   
    extend          : 'Ext.grid.Panel',
    alias           : Fsl.app.getAlias('V01I010001X02'),
    id              : 'V01I010001X02'.toLowerCase(), 
    border          : true,
    modal           : true,
    height          : 290,
    width           : 455,
    viewConfig: {
        stripeRows    : true,
        forceFit      : true,
        emptyText     : 'No Records to display',
        listeners     : {
            viewready : function(v) {               
                var store = Fsl.store.getStore('S01I010001');
                store.clearFilter();
                store.loadPage(1);
            }
        }
    },
    initComponent: function() {
        var me      = this;
        me.store    = Fsl.app.getRelStore('S01I010001');    
        me.tbar = Ext.create('Ext.ux.StatusBar', {            
            topBorder           : false,
            statusAlign         : 'right',
            items     : [{
                xtype       :'button',
                text        : 'ADD',
                icon        : Fsl.route.getIcon('TB01001/ADD01004.png'),
                action      : 'add'
            },'-',{
                xtype       :'button',
                text        : 'Clear',
                icon        : Fsl.route.getIcon('IC01001/CAN01003.png'),
                action      : 'clear'
            },'-',{
                xtype       : 'button',
                text        : 'search',
                icon        : Fsl.route.getIcon('IC01001/search.png'),
                action      : 'search'
            },'-',{  
                xtype       :'statusbar',
                id          : 'win-statusbar-v01i010001x02',
                border      : false,
                defaultText : '',
                statusAlign : 'right'
            }]
        });
        
        this. bbar= new Ext.PagingToolbar({
            pageSize    	: 10,
            store       	: Fsl.app.getRelStore('S01I010001'),
            displayInfo 	: true
        });
        
        
        this.columns = [{
            text      : 'SN',
            xtype     : 'rownumberer',
            width     :  40, 
            sortable  :  false
        },{
            text        : 'ID',
            dataIndex   : 'id',
            width       : 40, 
            sortable    : false
        },{
            text        : 'Start Date',
            dataIndex   : 'startDate',
            flex        : 1,
            renderer    : this.getFormatDate
        },{
            text        : 'End Date',
            dataIndex   : 'endDate',
            flex        : 1,
            renderer    : this.getFormatDate
        },{
            text        : 'Amount',
            dataIndex   : 'amount'
        },{
            text        : 'Promotion',
            dataIndex   : 'percent',
            width       :  65,
            renderer    :  this.getDiscount
        },{           
            xtype           : 'actioncolumn',
            renderer: function (val, metadata, record) {
                if (record.raw.flag != true) {
                    this.items[0].icon = '';
                    this.items[0].tooltip = '';
                    this.setVisible(false);
                    this.disable();
                } else if(record.raw.flag == true){
                    this.items[0].icon = Fsl.route.getImage('DEL01005.png');
                    this.items[0].tooltip = 'Delete ?';
                    metadata.style = 'cursor: pointer;';
                    this.setVisible(true);
                    this.enable(); 
                }                
                return val;
            },
            width           : 22,
            align           : 'center',
            sortable        : false,
            items           : [{               
                scope       : this,
                handler     : function(grid, rowIdx, colIdx) {
                    var record      = grid.getStore().getAt(rowIdx),
                    promotionId = record.data.id;
                    Ext.getCmp('WestPanel').confirmDelete(promotionId, 'M01I010001', 'S01I010001', 'v01i010001x02');
                }
            }]
        }];       
        this.callParent(arguments);
    },
   
    getFormatDate :function(value, metadata, record, rowIdx, colIdx, store, vie){ 
        return Ext.Date.format(value, 'M d, Y')
    },    
    getDiscount :function(value, metadata, record, rowIdx, colIdx, store, vie) {
        return value+" %";
    }
});