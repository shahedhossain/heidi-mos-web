Ext.define(Fsl.app.getAbsView('V01T015001X04'), {
    extend        : 'Ext.form.Panel',
    alias         : Fsl.app.getAlias('V01T015001X04'),
    id            : 'V01T015001X04'.toLowerCase(),  
    background    : false,
    componentCls  : 'my-border-bottom',
    bodyStyle     : {
        background: 'none'                
    },
    defaults: {        
        activeRecord  : null,
        border        : true, 
        layout        : 'hbox',        
        fieldDefaults : {
            anchor    : '100%',
            labelAlign: 'right'
        }
    },
    
    initComponent     : function(){        
        var me = this;
        me.items = [{
            xtype: 'fieldcontainer',
            combineErrors: true,                    
            defaults: {
                layout      : 'fit',
                flex        : 1,
                margin      : '5 7 0 10',
                hideLabel   : false,                
                labelAlign  : 'left'
            },
            items: [{
                xtype             : 'textfield',
                name              : 'returnId', 
                fieldLabel        : 'RETURN ID:'                       
                //readOnly          : true
            },{
                xtype             : 'textfield',
                name              : 'transferId',
                fieldLabel        : 'TRANSFER ID:',                        
                readOnly          : true
            }]
        },{
            xtype             : 'fieldcontainer',
            combineErrors     : true,             
            defaults: { 
                layout        : 'fit',
                flex          : 1,
                margin        : '0 7 0 10',
                hideLabel     : false,                
                labelAlign    : 'left'
            },
            items: [
            {
                xtype             : 'textfield',
                fieldLabel        : 'B.ID:',
                name              : 'fromBranchId', 
                readOnly          : true
            },{
                xtype             : 'textfield',
                name              : 'fromBranchName',  
                fieldLabel        : 'NAME :',
                readOnly          : true,
                anchor            : '95%',
                margin            : '0 7 0 10'
            }]
        },{
            xtype             : 'fieldcontainer',
            combineErrors     : true,             
            defaults: { 
                layout        : 'fit',
                flex          : 1,
                margin        : '0 7 0 10',
                hideLabel     : false,                
                labelAlign    : 'left'
            },
            items: [
            {
                xtype             : 'numberfield',                
                hideTrigger       : true,
                decimalPrecision  : 0,
                keyNavEnabled     : false,
                mouseWheelEnabled : false,
                fieldLabel        : 'Receive Id :',                
                name              : 'receiveId'
            },            
            {
                xtype             : 'datefield',
                fieldLabel        : 'RETURN DATE :',
                name              : 'entryDate',
                format            : 'M d, Y',
                altFormats        : 'd/m/Y|M d, Y h:i A',
                value             : '2.4.2013',
                readOnly          : true
            }]
        },{
            xtype             : 'fieldcontainer',
            combineErrors     : true,             
            defaults: { 
                layout        : 'fit',
                flex          : 1,
                margin        : '0 7 0 10',
                hideLabel     : false,                
                labelAlign    : 'left'
            },
            items: [
            {
                xtype             : 'numberfield',                
                hideTrigger       : true,
                decimalPrecision  : 0,
                keyNavEnabled     : false,
                mouseWheelEnabled : false,
                fieldLabel        : 'RETURN RECEIVE:',                
                name              : 'returnReceiveId'
            },            
            {
                xtype             : 'datefield',
                fieldLabel        : 'TODAY:',
                value             :  new Date(),
                format            : 'M d, Y',
                altFormats        : 'd/m/Y|M d, Y h:i A',
                readOnly          : true
            }]
        }];       
        me.callParent(arguments);
    }
});