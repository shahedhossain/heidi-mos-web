Ext.define(Fsl.app.getAbsView('V01I013001X02'), {   
    extend          : 'Ext.grid.Panel',
    alias           : Fsl.app.getAlias('V01I013001X02'),
    id              : 'V01I013001X02'.toLowerCase(), 
    border    		: true,
    modal     		: true,
    height    		: 285,
    width     		: 455,
    viewConfig: {
        stripeRows    : true,
        forceFit      : true,
        emptyText     : 'No Records to display',
        listeners     : {
            viewready : function(v) {               
                var store = Fsl.store.getStore('S01I013001');
                store.clearFilter();
                store.loadPage(1);
            }
        }
    },
    initComponent: function() {
        var me      = this;
        me.store    = Fsl.app.getRelStore('S01I013001');    
        me.tbar = Ext.create('Ext.ux.StatusBar', {            
            topBorder           : false,
            statusAlign         : 'right',
            items     : [{
                xtype       :'button',
                text        : 'ADD',
                icon        : Fsl.route.getIcon('TB01001/ADD01004.png'),
                action      : 'add'
            },'-',{
                xtype       :'button',
                text        : 'Clear',
                icon        : Fsl.route.getIcon('IC01001/CAN01003.png'),
                action      : 'clear'
            },'-',{
                xtype       : 'button',
                text        : 'search',
                icon        : Fsl.route.getIcon('IC01001/search.png'),
                action      : 'search'
            },'-',{  
                xtype       :'statusbar',
                id          : 'win-statusbar-v01i013001x02',
                border      : false,
                defaultText : '',
                statusAlign : 'right'
            }]
        });
        
        this.bbar = new Ext.PagingToolbar({
            pageSize        : 10,
            store           : Fsl.app.getRelStore('S01I013001'),
            displayInfo     : true
        });
        
        this.columns = [{
            text      		: 'SN',
            xtype     		: 'rownumberer',
            width     		:  40, 
            sortable  		:  false
        },{
            text            : 'ID',
            dataIndex       : 'id',
            width           :  40, 
            sortable        :  false
        },{
            text            : 'Code',
            dataIndex       : 'name',
            width           :  40, 
            renderer        :  this.getCategoryCode
        },{
            text            : 'Article',
            dataIndex       : 'articleId',
            flex            :  1,
            renderer        :  this.getArticleName
        },{
            text            : 'Vat',
            dataIndex       : 'vat',
            width           :  70,
            align           : 'right',
            renderer        :  this.getVat
        },{
            text            : 'Date',
            dataIndex       : 'date',
            renderer        :  this.getFormatDate
        },{
            menuDisabled    : true,
            sortable        : false,
            xtype           : 'actioncolumn',            
            width           :  22,            
            items           : [{
                icon        : Fsl.route.getImage('DEL01005.png'),
                tooltip     : 'Delete This?',
                scope       : this,
                handler     : function(grid, rowIdx, colIdx) {
                    var record = grid.getStore().getAt(rowIdx);
                    var  vatId = record.data.id
                    Ext.getCmp('WestPanel').confirmDelete(vatId, 'M01I013001', 'S01I013001', 'v01i013001x02');
                }
            }]
        }];       
        this.callParent(arguments);
    },    
    getArticleName :function(value, metadata, record, rowIdx, colIdx, store, vie) {
        return record.getM01i006001().data.name;
    },   
    getFormatDate :function(value, metadata, record, rowIdx, colIdx, store, vie){ 
        return Ext.Date.format(value, 'M d, Y')
    },
    getVat        :function(value, metadata, record, rowIdx, colIdx, store, vie){ 
        return value+'%'
    },
    getCategoryCode :function(value, metadata, record, rowIdx, colIdx, store, vie) {
        return record.getM01i006001().getM01i005001().getM01i003001().getM01i001001().data.code;
    }
});