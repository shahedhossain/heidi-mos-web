Ext.define(Fsl.app.getAbsView('V01T007001X02'), {
    extend    : 'Ext.grid.Panel',
    alias     : Fsl.app.getAlias('V01T007001X02'),
    id        : 'V01T007001X02'.toLowerCase(),
    border    : true,
    modal     : true,
    height    : 285,
    width     : 650,  
    cls       :'cTextAlign',    
    viewConfig: {
        stripeRows    : true,
        forceFit      : true,
        emptyText     : 'No Records to display',
        loadMask      : false,
        listeners     : {
            viewready : function(v) {               
                var store = Fsl.store.getStore('S01T007001');
                store.clearFilter();
                store.loadPage(1);
            }
        }
    },
    columnLines		: true,
    initComponent	: function() {
        var me     	= this;
        me.store   	= Fsl.app.getRelStore('S01T007001');    
        me.tbar = Ext.create('Ext.ux.StatusBar', {            
            topBorder           : false,
            statusAlign         : 'right',
            items     : [{
                xtype       : 'button',
                text        : 'NEW',
                icon        : Fsl.route.getIcon('TB01001/ADD01004.png'),
                action      : 'new'
            },'-',{
                xtype       : 'button',
                text        : 'Clear',
                icon        : Fsl.route.getIcon('IC01001/CAN01003.png'),
                action      : 'clear'
            },'-',{
                xtype       : 'button',
                text        : 'search',
                icon        : Fsl.route.getIcon('IC01001/search.png'),
                action      : 'search'
            },'-',{  
                xtype       : 'statusbar',
                id          : 'win-statusbar-v01t007001x02',
                border      : false,
                defaultText : '',
                statusAlign : 'right'
            }]
        });
        
        this. bbar= new Ext.PagingToolbar({
            pageSize    : 10,
            store      : Fsl.app.getRelStore('S01T007001'),
            displayInfo : true
        }); 
    
            
        this.columns = [{
            text        : 'SN',
            xtype       : 'rownumberer',
            width       :  40, 
            sortable    :  false
        },{
            text        : 'ID',
            dataIndex   : 'id',            
            sortable    :  true,
            width       :  65,
            align       : 'right',
            renderer    :  this.rowToolTip
        },{
            text        : 'Customer',
            dataIndex   : 'customerId',
            flex        :  1,
            renderer    :  this.getCustomerName            
        },{
            text        : 'G.vat',
            dataIndex   : 'grossVatId',
            width       :  40,
            renderer    :  this.getGrossVat
        },{
            text        : 'Rebate',
            dataIndex   : 'rebateId',
            width       :  50,
            renderer    :  this.getRebate
        },{
            text        : 'Promotion',
            dataIndex   : 'promotionId',
            width       :  60,
            renderer    :  this.getPromotion
        },{
            text        : 'Pay Method',
            dataIndex   : 'payMethodId',
            width       :  70,
            renderer    :  this.getPayMethod
        },{
            text        : 'Amount',
            dataIndex   : 'payAmount',
            align       : 'right',
            width       :  80,
            renderer    :  this.rowToolTip
        },{
            text        : 'Date',
            dataIndex   : 'entryDate',
            width       :  140,
            renderer    :  this.getFormatDate
        }];
        this.callParent(arguments);
    },
    getCustomerName :function(value, metadata, record, rowIdx, colIdx, store, vie) {
        this.rowToolTip(value, metadata, record)
        var title = record.getM01t001001().getM01i007001().data.name;
        var name  = record.getM01t001001().data.name;
        return title+''+name
    },
    getGrossVat :function(value, metadata, record, rowIdx, colIdx, store, vie) {
        this.rowToolTip(value, metadata, record)
        return record.getM01i008001().data.vatPercent+'%';
    }, 
    getRebate :function(value, metadata, record, rowIdx, colIdx, store, vie) {
        this.rowToolTip(value, metadata, record)
        return record.getM01i009001().data.rebatePercent+'%';
    }, 
    getPromotion :function(value, metadata, record, rowIdx, colIdx, store, vie) {
        this.rowToolTip(value, metadata, record)
        return record.getM01i010001().data.percent+'%';
    }, 
    getPayMethod :function(value, metadata, record, rowIdx, colIdx, store, vie) {
        this.rowToolTip(value, metadata, record)
        return record.getM01i011001().data.name;
    }, 
    getBranch :function(value, metadata, record, rowIdx, colIdx, store, vie) {
        this.rowToolTip(value, metadata, record)
        return record.getM01i015001().data.name;
    }, 
    getFormatDate :function(value, metadata, record, rowIdx, colIdx, store, vie){
        this.rowToolTip(value, metadata, record)
        return Ext.Date.format(value, 'M d, Y, g:i a')
    },
    rowToolTip : function(value, metadata, record) {
        var smarty = "<b>Invoice Information</b>";
        smarty += "<br/>Invoice Id    : {id}";
        smarty += "<br/>Customer id   : {customerId}  &nbsp;"+'Customer Name:'+record.getM01t001001().getM01i007001().data.name+''+record.getM01t001001().data.name;
        smarty += "<br/>Gross Vat id  : {grossVatId}  &nbsp;"+'Rate:'+record.getM01i008001().data.vatPercent+'%';
        smarty += "<br/>Rebate id     : {rebateId}    &nbsp;"+'Rate:'+record.getM01i009001().data.rebatePercent+'%';
        smarty += "<br/>Promotion id  : {promotionId} &nbsp;"+'Rate:'+record.getM01i010001().data.percent+'%';  
        smarty += "<br/>Branch id     : {branchId}    &nbsp;"+'Name:'+record.getM01i015001().data.name;
        smarty += "<br/>Pay Amount    : {payAmount}";
        smarty += "<br/>Money Pay Sys.: "+record.getM01i011001().data.name;
        smarty += "<br/>Date          : "+Ext.Date.format(record.data.entryDate, 'M d, Y, g:i a'); 

        var tpl = new Ext.XTemplate(smarty);
        var qtip = tpl.apply(record.data)
        metadata.tdAttr = 'data-qtip="' + qtip + '"';               

        return value
    }
});