Ext.define(Fsl.app.getAbsView('V01R001001'), {
    extend      : 'Ext.form.Panel',
    alias     	: Fsl.app.getAlias('V01R001001'),
    id          : 'V01R001001'.toLowerCase(),  
    bodyStyle   : {
        background  : 'none'
    },
    defaults: {        
        activeRecord  : null,
        layout        : 'vbox',        
        fieldDefaults : {
            anchor    : '100%',
            labelAlign: 'right'
        }
    },
    initComponent: function() {
        var me     = this;
        Ext.QuickTips.init();
        Ext.form.Field.prototype.msgTarget = 'qtip';         
        me.items = [{
            xtype           : 'fieldcontainer',
            combineErrors   : true,                    
            defaults        : {
                layout      : 'fit',
                margin      : '3 5 1 5',
                hideLabel   : false,                
                labelAlign  : 'top',
                width       : 120
            },
            items:  [
            {
                xtype   : 'button',
                text    : 'TODAY',
                icon    : Fsl.route.getImage('PDF01001.png')
            },{
                xtype   : 'button',
                text    : 'ANY_DAY',
                icon    :  Fsl.route.getImage('PDF01001.png')
            },{
                xtype   : 'button',
                text    : 'LAST_7_DAYS',
                icon    :  Fsl.route.getImage('PDF01001.png')
            },{
                xtype   : 'button',
                text    : 'LAST_30_DAYS',
                icon    :  Fsl.route.getImage('PDF01001.png')
            },{
                xtype   : 'button',
                text    : 'DATE_TO_DATE',
                icon    :  Fsl.route.getImage('PDF01001.png')
            },{
                xtype   : 'button',
                text    : 'MONTHLY',
                icon    :  Fsl.route.getImage('PDF01001.png')
            },{
                xtype   : 'button',
                text    : 'YEARLY',
                icon    :  Fsl.route.getImage('PDF01001.png')
            }]
        }];       
        me.callParent(arguments);
    }
});