Ext.define(Fsl.app.getAbsView('V01I017001X01'), {
    extend      	: 'Ext.form.Panel',
    alias     		: Fsl.app.getAlias('V01I017001X01'),
    id          	: 'V01I017001X01'.toLowerCase(), 
    bodyStyle : {
        background: 'none'                
    },
    initComponent : function(){
        var me      = this;     
        this.items  =[{
            defaultType   : 'textfield',
            layout        : 'hbox',
            flex          : 1,
            border        : false,
            fieldDefaults : {
                anchor      : '100%',
                labelAlign  : 'right'               
            },
            bodyStyle     : {
                background  : 'none'                
            },
            items   : [{
                xtype             : 'fieldcontainer',
                layout            : 'vbox',
                margin            : '5 0 0 5',                        
                defaults          :  {
                    width           :  165,
                    labelAlign      : 'left',
                    labelWidth      :  60
                }, 
                items : [{
                    xtype             : 'numberfield',
                    name              : 'id',
                    fieldLabel        : 'Limit Id:',                       
                    minValue          :  0,
                    mouseWheelEnabled :  false,
                    hideTrigger       :  true,
                    decimalPrecision  :  0,
                    keyNavEnabled     :  false                
                },{
                    xtype             : 'numberfield',
                    name              : 'articleId',
                    fieldLabel        : 'Article ID:',                       
                    minValue          :  0,
                    mouseWheelEnabled :  false,
                    hideTrigger       :  true,
                    decimalPrecision  :  0,
                    keyNavEnabled     :  false                 
                },{
                    xtype             : 'textfield',
                    fieldLabel        : 'Article',
                    emptyText         : 'Article name',
                    allowBlank        :  true,
                    name              : 'articleName'
                }]
            },                          
            {
                xtype       : 'fieldcontainer',
                layout      : 'vbox',
                margin      : '5 0 0 10',                        
                defaults          :  {
                    width           :  160,
                    labelAlign      : 'left',
                    labelWidth      : 60
                }, 
                items: [{
                    xtype             : 'textfield',
                    fieldLabel        : 'Branch',
                    emptyText         : 'Branch name',
                    allowBlank        :  true,
                    name              : 'branchName'
                },{
                    xtype             : 'textfield',
                    fieldLabel        : 'Min Qty',
                    allowBlank        :  true,
                    name              : 'minQuantity'
                },{
                    xtype             : 'textfield',
                    fieldLabel        : 'Max Qty',
                    allowBlank        :  true,
                    name              : 'maxQuantity'
                }]
            },{
                xtype               : 'fieldcontainer',
                layout              : 'vbox',
                margin              : '5 0 0 10',                        
                defaults            :  {
                    width           :  160,
                    flex            :  1,
                    labelAlign      : 'left',
                    labelWidth      : 60
                },                 
                items: [{
                    xtype             : 'numberfield',
                    name              : 'mbcode',
                    fieldLabel        : 'MBCODE:',
                    minValue          :  0,
                    mouseWheelEnabled :  false,
                    hideTrigger       :  true,
                    decimalPrecision  :  0,
                    keyNavEnabled     :  false
                },{
                    xtype             : 'numberfield',
                    name              : 'pbcode',
                    fieldLabel        : 'PBCODE:',
                    minValue          :  0,
                    mouseWheelEnabled :  false,
                    hideTrigger       :  true,
                    decimalPrecision  :  0,
                    keyNavEnabled     :  false
                },{
                    xtype             : 'datefield',
                    name              : 'entryDate',
                    fieldLabel        : 'Date:',
                    format            : 'M d, Y',
                    altFormats        : 'd/m/Y|M d, Y h:i A'
                }]
            }]
        }];
        this.callParent(arguments);
    }
});