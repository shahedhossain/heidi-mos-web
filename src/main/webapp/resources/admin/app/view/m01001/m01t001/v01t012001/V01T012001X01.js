Ext.define(Fsl.app.getAbsView('V01T012001X01'), {
    extend        : 'Ext.form.Panel',
    alias         : Fsl.app.getAlias('V01T012001X01'),
    id            : 'V01T012001X01'.toLowerCase(),  
    bodyStyle     : {
        background    : 'none'
    },
    defaults: {        
        activeRecord  : null,
        border        : true, 
        layout        : 'hbox',        
        fieldDefaults : {
            anchor    : '100%',
            labelAlign: 'right'
        }
    },
    
    initComponent     : function(){             
        var me = this;       
        me.items = [{
            xtype           : 'fieldcontainer',
            combineErrors   : true,                    
            defaults        : {
                layout      : 'fit',
                margin      : '3 5 0 5',
                hideLabel   : false,                
                labelAlign  : 'top'
            },
            items: [{
                xtype             : 'numberfield',
                name              : 'id',
                fieldLabel        : 'TRANSFER ID :',
                minValue          :  1,
                width             :  100,
                margin            : '3 0 0 5'
            },{
                xtype             : 'numberfield',
                name              : 'branchId',
                fieldLabel        : 'BRANCH ID',
                minValue          :  1,
                width             :  100,
                margin            : '3 0 0 5'
            },{
                xtype             : 'textfield',
                name              : 'branchName',
                width             :  140,
                fieldLabel        : 'Branch Name:'                      
            },{
                xtype             : 'datefield',
                name              : 'entryDate',
                width             :  130,
                fieldLabel        : 'DATE'                       
            }]
        }];       
        me.callParent(arguments);
    }
});

   