Ext.define(Fsl.app.getAbsView('V01R013001X03'), {
    extend      : 'Ext.form.Panel',
    alias     	: Fsl.app.getAlias('V01R013001X03'),
    id          : 'V01R013001X03'.toLowerCase(),  
    bodyStyle   : {
        background  : 'none'
    },
   
    initComponent: function() {
        var me     = this;
        Ext.QuickTips.init();
        Ext.form.Field.prototype.msgTarget = 'qtip';         
        me.items = [{
            xtype           : 'fieldcontainer',
            layout          : 'hbox',                  
            defaults        : {
                layout      : 'fit',
                margin      : '5 5 2 5',
                hideLabel   : false,                
                labelAlign  : 'top'
            },
            items: [{
                xtype           : 'datefield',
                name            : 'MONTH',
                width           : 135,
                emptyText       : 'Select A Month',  
                format          : 'Y-m-d H:i:s'
            }]
        }];       
        me.callParent(arguments);
    }
});