Ext.define(Fsl.app.getAbsView('V01T010001X03'),{
    extend      : 'Ext.window.Window',
    alias     	: Fsl.app.getAlias('V01T010001X03'),
    id          : 'V01T010001X03'.toLowerCase(), 
    title       : 'CATEGORY :: V01T010001X03', 
    icon        : Fsl.route.getImage('APP01003.png'),
    layout        : 'fit',
    minimizable   :  false,
    maximizable   :  false,
    autoShow      :  true,
    resizable     :  false,
    modal         :  true,
    width         :  285,
    height        :  240,
    
    initComponent: function () {
        var me = this;
        me.items = [
        {
            xtype           : 'form',
            border          :  false,
            trackResetOnLoad:  true,
            bodyStyle : {                    
                padding           : '10px'
            },
            tbar : Ext.create('Ext.ux.StatusBar', {
                id                : 'win-statusbar-v01t010001x03',
                topBorder         :  true,
                text              : 'Status',
                defaultIconCls    : 'fsl-severity-info',
                defaultText       : 'Status'
            }),
            
            items: [
            {
                xtype             : 'numberfield',
                name              : 'id',
                fieldLabel        : 'Id',
                emptyText         : 'Not need....',
                width             :  265,
                anchor            : '100%',
                readOnly          :  true,
                minValue          :  0,
                mouseWheelEnabled :  false,
                hideTrigger       :  true,
                decimalPrecision  :  0,
                keyNavEnabled     :  false
            },
            {
                xtype             : 'textfield',
                name              : 'articleName',
                fieldLabel        : 'Article',                
                allowBlank        :  false,
                readOnly          :  true,
                width             :  253,
                enableKeyEvents   :  true
            },
            {
                xtype             : 'textfield',
                name              : 'articleId',                               
                allowBlank        :  false,
                readOnly          :  true,
                hidden            :  true
            },{
                xtype             : 'datefield',
                name              : 'startDate',
                fieldLabel        : 'Start Date',
                width             :  265,
                anchor            : '100%',
                allowBlank        :  false,
                minValue          :  '',
                format            : 'M d, Y',
                altFormats        : 'd/m/Y|M d, Y h:i A',
                id                : 'startdt',
                vtype             : 'daterange',
                endDateField      : 'enddt'
            },{
                xtype             : 'datefield',
                name              : 'endDate',
                fieldLabel        : 'End Date',
                width             :  265,
                anchor            : '100%',
                allowBlank        :  false,
                format            : 'M d, Y',
                altFormats        : 'd/m/Y|M d, Y h:i A',
                id                : 'enddt',
                vtype             : 'daterange',
                startDateField    : 'startdt'
            },{
                xtype             : 'numberfield',
                fieldLabel        : 'Discount',
                emptyText         : 'Discount....',
                name              : 'discount',
                width             :  265,
                anchor            : '100%',
                allowBlank        :  false,
                minValue          :  0,
                maxValue          :  100,
                mouseWheelEnabled :  false,
                hideTrigger       :  true,
                decimalPrecision  :  0,
                keyNavEnabled     :  false
            }]
        }];
        me.buttons = [
        {
            text    : 'Save',
            icon    : Fsl.route.getImage('SAV01004.png'),
            action  : 'save'
        },
        {
            text    : 'Close',
            icon    : Fsl.route.getImage('CLS01001.png'),     
            scope   : this,
            handler : this.close
        }
        ];
        this.callParent(arguments);
    }
});
