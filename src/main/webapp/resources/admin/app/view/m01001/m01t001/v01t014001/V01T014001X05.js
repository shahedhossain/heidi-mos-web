Ext.define(Fsl.app.getAbsView('V01T014001X05'), {
    extend    : 'Ext.grid.Panel',
    alias     : Fsl.app.getAlias('V01T014001X05'),
    id        : 'V01T014001X05'.toLowerCase(),
    border    : true,
    modal     : true,
    height    : 303,
    width     : 580,    
    viewConfig: {
        stripeRows    : true,
        forceFit      : true,
        loading       : true,
        emptyText     : 'No Records to display',
        listeners     : {
            viewready : function(v) { 
                var store = Fsl.store.getStore('S01T013001');
            //                store.clearFilter();
            //                store.loadPage(1);
                
            }
        }
    },
    features  : [{
        ftype       : 'summary' ,
        showSummaryRow : true
    }],
    initComponent: function() {
        var me      = this;
        me.store    = Fsl.app.getRelStore('S01T013001');  
        me. bbar= Ext.create('Ext.ux.StatusBar', {
            id            : 'win-statusbar-v01t014001x05',
            topBorder     :    false,
            text          :   'Status',
            defaultIconCls:   'fsl-severity-info',
            defaultText   :   'Status',
            items:[{
                xtype       : 'displayfield',
                fieldLabel  : 'Total',                
                align       :'right',
                name        :'summary',
                labelStyle  : 'font-size:11px;',
                fieldStyle  :'font-size: 15px; font-weight: bold;color:blue;'                
            }]
        });
        this.columns = [{
            text      : 'SN',
            xtype     : 'rownumberer',
            width     :  40, 
            sortable  :  false
        },{
            text        : 'ID',
            dataIndex   : 'id',
            width       :  60, 
            sortable    :  false,
            hidden      : true
        },{
            text        : 'Article',
            dataIndex   : 'articleId',            
            renderer    : this.getArticleName,
            flex        : 1,
            field: {
                xtype: 'numberfield'
            }
        },{
            text        : 'Rate',
            dataIndex   : 'priceId',
            align       : 'right',
            width       : 70,
            renderer    : this.getPrice,
            field: {
                xtype: 'numberfield'
            }
        },{
            text        : 'Req.QTY',
            dataIndex   : 'quantity',
            align       : 'right',
            width       : 70,            
            summaryType : me.getSum,            
            summaryRenderer: function(value, summaryData, dataIndex) {                
                return Ext.util.Format.number(value, '0.0');  
            }
        },{
            text        : 'Approval.QTY',
            dataIndex   : 'approvalQty',
            align       : 'right',
            width       : 80,
            field: {
                xtype: 'numberfield'
            },
            renderer    : function(value, metaData, record, rowIdx, colIdx, store, view) {     
                return (value?value+'&nbsp' + '৳':0);  
            },
            summaryRenderer: function(value, summaryData, dataIndex) {                
                return parseFloat(value)
            },
            summaryType : me.getApprovalSum
        },{
            text        : 'Vat',
            dataIndex   : 'vatId',
            align       : 'right',
            width       : 50,
            renderer    : this.getVat
        },{
            text        : 'Disc',
            dataIndex   : 'discountId',
            align       : 'right',
            width       : 50,
            renderer    : this.getDiscount
        },{
            header      : 'Total',
            align       : 'right',
            width       : 80,
            sortable    : false,
            groupable   : false,           
            renderer    : function(value, metaData, record, rowIdx, colIdx, store, view) {     
                var rate = parseFloat(record.getM01i012001().data.price),
                discount = parseFloat((rate *(record.getM01t010001().data.discount))/100),
                vat      = parseFloat((rate - discount)*((record.getM01i013001().data.vat)/100)),
                st       = (rate-discount+vat)*parseFloat(record.get('quantity'));
                return Ext.util.Format.number(st, '0.000')+'&nbsp' + '৳';  
            },
            summaryType     : me.getSumForTotal,
            summaryRenderer : function(value, summaryData, record,dataIndex) {
                var total  = Ext.util.Format.number(value,'0.00')+'/='; 
                me.up().down('displayfield[name=summary]').setValue(total);
                return Ext.String.format('{0} Tk{1}', total, value !== 1 ? '' : ''); 
            }
        },{
            menuDisabled    : true,
            sortable        : false,
            xtype           : 'actioncolumn',            
            width           : 22,            
            items           : [{
                icon        : Fsl.route.getImage('DEL01005.png'),
                tooltip     : 'Delete This?',
                scope       : this,
                handler     : function(grid, rowIdx, colIdx) {                                      
                    var record = grid.getStore().getAt(rowIdx);                     
                    var articleId = record.getM01i006001().data.id;
                    Ext.getCmp('WestPanel').confirmDelete(articleId, 'M01T013001', 'S01T013001', 'v01t014001x05');
                }
            }]
        }];       
        this.callParent(arguments);
    },    
    getArticleName :function(value, metadata, record, rowIdx, colIdx, store, vie) {
        return record.getM01i006001().data.name;
    }, 
    getVendorName :function(value, metadata, record, rowIdx, colIdx, store, vie) {
        return record.getM01i014001().data.name;
    }, 
    getBranchName :function(value, metadata, record, rowIdx, colIdx, store, vie) {
        return record.getM01i015001().data.name;
    },
    getFormatDate :function(value, metadata, record, rowIdx, colIdx, store, vie){
        return Ext.Date.format(value, 'M d, Y')
    },    
    getPrice :function(value, metaData, record, rowIdx, colIdx, store, view){
        var value=record.getM01i012001().data.price +'&nbsp';
        return value+'৳';
    },
    getVat : function(value, metaData, record, rowIdx, colIdx, store, view){
        var value=record.getM01i013001().data.vat +'&nbsp';
        return value+'%';
    },
    getDiscount :function(value, metaData, record, rowIdx, colIdx, store, view){
        var value=record.getM01t010001().data.discount +'&nbsp';
        return value+'%';
    },
    getSum: function(records, field){
        var total = 0,
        i = 0,
        len = records.length;
        for (; i < len; ++i) {
            var qty = records[i].data.quantity
            total += parseFloat(qty ? qty : 0, '0.0');//records[i].get(field)
        }
        return total;
    },
    getApprovalSum: function(records, field){
        var total = 0,
        i = 0,
        len = records.length;
        for (; i < len; ++i) {
            var approvalQty = records[i].data.approvalQty
            total += parseFloat( approvalQty ? approvalQty: 0 , '0.0');//records[i].get(field)
        }
        return total;
    },
    getSumForTotal :function(records, field){
        var total = 0,
        i = 0,
        len = records.length;
        for (; i < len; ++i) {
            var rate = parseFloat(records[i].getM01i012001().data.price),
            discount = parseFloat((rate *(records[i].getM01t010001().data.discount))/100),
            vat      = parseFloat((rate - discount)*((records[i].getM01i013001().data.vat)/100)),
            st       = (rate-discount+vat)*parseFloat(records[i].get('quantity'));
            total += parseFloat(st, '0.0');//records[i].get(field)
        }
        return total;
    }
});