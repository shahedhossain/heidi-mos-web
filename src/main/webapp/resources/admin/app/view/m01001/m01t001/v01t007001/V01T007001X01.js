Ext.define(Fsl.app.getAbsView('V01T007001X01'), {
    extend              : 'Ext.form.Panel',
    alias               : Fsl.app.getAlias('V01T007001X01'),
    id                  : 'V01T007001X01'.toLowerCase(), 
    width               : 650,  
    bodyStyle           : {
      background        : 'none'
    },
    defaults: {        
        activeRecord    : null,
        border          : true, 
        layout          : 'hbox',        
        fieldDefaults   : {
            anchor      : '100%',
            labelAlign  : 'right'
        }
    },
    
    initComponent       : function(){        
        Ext.QuickTips.init();
        Ext.form.Field.prototype.msgTarget = 'qtip';         
        var me      = this; 
        me.items    = [{
            xtype                 : 'fieldcontainer',
            combineErrors         :  true,                    
            defaults              : {
                layout            : 'fit',
                margin            : '0 5 0 5',
                hideLabel         :  false,                
                labelAlign        : 'top',
                flex              : 1
            },
            items: [{
                xtype             : 'textfield',
                fieldLabel        : 'INVOICE ID',
                name              : 'id',
                minValue          :  0,
                mouseWheelEnabled :  false,
                hideTrigger       :  true,
                decimalPrecision  :  0,
                keyNavEnabled     :  false
            },{
                xtype             : 'textfield',
                fieldLabel        : 'Customer Name',
                name              : 'customerName'
            },{
                xtype             : 'textfield',
                fieldLabel        : 'Customer Id',
                name              : 'customerId'
            },{
                xtype             : 'datefield',
                fieldLabel        : 'Date',
                name              : 'entryDate',
                format            : 'M d, Y',
                altFormats        : 'd/m/Y|M d, Y h:i A'
            }]
        }]
        me.callParent(arguments);
    }
});

