Ext.define(Fsl.app.getAbsView('V01T001001X03'),{
    extend      : 'Ext.window.Window',
    alias     	: Fsl.app.getAlias('V01T001001X03'),
    id          : 'V01T001001X03'.toLowerCase(), 
    title       : 'CATEGORY :: V01T001001X03', 
    icon        : Fsl.route.getImage('APP01003.png'),
    layout      : 'fit',
    minimizable :  false,
    maximizable :  false,
    autoShow    :  true,
    resizable   :  false,
    modal       :  true,
    width       :  285,
    height      :  290,    
    initComponent: function () {    
        var me = this;        
        me.items = [
        {
            xtype     		: 'form',
            border    		:  false,
            trackResetOnLoad    :  true,
            defaults  		: {
                width   : 265
            },
            bodyStyle : {                    
                padding   : '10px'
            },
            
            tbar : Ext.create('Ext.ux.StatusBar', {
                id                : 'win-statusbar-v01t001001x03',
                topBorder         :  true,
                text              : 'Status',
                defaultIconCls    : 'fsl-severity-info',
                defaultText       : 'Status'
            }),
            
            items: [{
                xtype             : 'textfield',
                fieldLabel        : 'Id',
                emptyText         : 'Not need..',
                name              : 'id',
                anchor            : '100%',
                readOnly          : true
            }, {
                xtype             : 'combo',
                name              : 'titleId',
                fieldLabel        : 'Title',
                store             : Fsl.store.getStore('S01I007002'),
                displayField      : 'name',
                valueField        : 'id',
                queryMode         : 'remote',
                allowBlank        : false,
                editable          : false,
                width             : 253
            },
            {
                xtype             : 'textfield',
                fieldLabel        : 'Customer Name',
                emptyText         : 'Write name....',
                name              : 'name',
                anchor            : '100%',
                allowBlank        : false             
            },            
            {
                xtype             : 'textfield',
                fieldLabel        : 'Contact',
                emptyText         : 'Write contact....',
                name              : 'contact',
                anchor            : '100%',
                allowBlank        : false
            },
            {
                xtype             : 'textarea',
                fieldLabel        : 'Address',
                emptyText         : 'Write name....',
                name              : 'address',
                anchor            : '100%',
                allowBlank        : false
            }]
        }];
        this.buttons = [
        {
            text    : 'Save',
            icon    : Fsl.route.getImage('SAV01004.png'),
            action  : 'save'
        },
        {
            text    : 'Close',
            icon    : Fsl.route.getImage('CLS01001.png'),     
            scope   : this,
            handler: this.close
        }
        ];
        this.callParent(arguments);
    }
});


