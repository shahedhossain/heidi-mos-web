Ext.define(Fsl.app.getAbsView('V01T015001X01'), {
    extend      	: 'Ext.form.Panel',
    alias     		: Fsl.app.getAlias('V01T015001X01'),
    id          	: 'V01T015001X01'.toLowerCase(), 
    bodyStyle : {
        background: 'none'                
    },
    initComponent : function(){
        var me      = this;     
        this.items  =[{
            defaultType   : 'textfield',
            layout        : 'hbox',
            flex          : 1,
            border        : false,
            fieldDefaults : {
                anchor      : '100%',
                labelAlign  : 'right'               
            },
            bodyStyle     : {
                background  : 'none'                
            },
            items   : [{
                xtype             : 'fieldcontainer',
                layout            : 'vbox',
                margin            : '5 0 0 5',                        
                defaults          :  {
                    width           :  190,
                    labelAlign      : 'left',
                    labelWidth      :  80
                }, 
                items : [{
                    xtype             : 'numberfield',
                    name              : 'id',
                    fieldLabel        : 'RETURN ID:',                       
                    minValue          :  0,
                    mouseWheelEnabled :  false,
                    hideTrigger       :  true,
                    decimalPrecision  :  0,
                    keyNavEnabled     :  false                
                },{
                    xtype             : 'numberfield',
                    name              : 'transferId',
                    fieldLabel        : 'TRANSFER ID:',                       
                    minValue          :  0,
                    mouseWheelEnabled :  false,
                    hideTrigger       :  true,
                    decimalPrecision  :  0,
                    keyNavEnabled     :  false                 
                }]
            },                          
            {
                xtype       : 'fieldcontainer',
                layout      : 'vbox',
                margin      : '5 0 0 10',                        
                defaults          :  {
                    width           :  190,
                    labelAlign      : 'left',
                    labelWidth      : 85
                }, 
                items: [{
                    xtype             : 'textfield',
                    fieldLabel        : 'BRANCH NAME',
                    emptyText         : 'Branch name',
                    allowBlank        :  true,
                    name              : 'branchName'
                },{
                    xtype             : 'textfield',
                    fieldLabel        : 'BRANCH ID',
                    allowBlank        :  true,
                    name              : 'fromBranchId'
                }]
            },{
                xtype               : 'fieldcontainer',
                layout              : 'vbox',
                margin              : '5 5 0 10',                        
                defaults            :  {
                    width           :  190,
                    flex            :  1,
                    labelAlign      : 'left',
                    labelWidth      : 80
                },                 
                items: [{
                    xtype             : 'numberfield',
                    name              : 'receiveId',
                    fieldLabel        : 'RECEIVE ID:',
                    minValue          :  0,
                    mouseWheelEnabled :  false,
                    hideTrigger       :  true,
                    decimalPrecision  :  0,
                    keyNavEnabled     :  false
                },{
                    xtype             : 'datefield',
                    name              : 'entryDate',
                    fieldLabel        : 'Date:',
                    format            : 'M d, Y',
                    altFormats        : 'd/m/Y|M d, Y h:i A'
                }]
            }]
        }];
        this.callParent(arguments);
    }
});
