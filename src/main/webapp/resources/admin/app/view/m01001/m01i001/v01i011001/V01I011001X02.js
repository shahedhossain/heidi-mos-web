Ext.define(Fsl.app.getAbsView('V01I011001X02'), { 
    extend      	: 'Ext.window.Window',
    alias       	:  Fsl.app.getAlias('V01I011001X02'),
    id          	: 'V01I011001X02'.toLowerCase(),   
    title       	: 'PAY METHOD :: V01I011001X02',
    icon        	:  Fsl.route.getImage('APP01003.png'),
    layout              : 'fit',
    autoShow            : true,
    modal               : true,
    width               : 300,
    height              : 150,  
    initComponent: function () {    
        var me = this;        
        me.items = [
            {
                xtype                   : 'form',
                border                  :  false,
                trackResetOnLoad	:  true,
                bodyStyle : {                    
                    padding   : '10px'
                },
            
                tbar : Ext.create('Ext.ux.StatusBar', {
                    id                  : 'win-statusbar-v01i011001x02',
                    topBorder           : false,
                    text                : 'Status',
                    defaultIconCls      : 'fsl-severity-info',
                    defaultText         : 'Status'
                }),

                items: [{
                        xtype             : 'textfield',
                        name              : 'id',
                        fieldLabel        : 'Id',
                        emptyText         : 'Not need....',
                        width             : 265,
                        anchor            : '100%',
                        readOnly          : true
                    },
                    {
                        xtype             : 'textfield',
                        name              : 'name',
                        fieldLabel        : 'PayMethod Name',
                        emptyText         : 'PayMethod Name Here...',
                        width             : 265,
                        anchor            : '100%',
                        allowBlank        : false
                    }]
            }];
        this.buttons = [{
            text        : 'Save',
            icon        :  Fsl.route.getImage('SAV01004.png'),
            action      : 'save',
            height      :  20
        },{
            text        : 'Close',
            icon        :  Fsl.route.getImage('CLS01001.png'),   
            scope       :  this,
            handler     :  this.close,
            height      :  20
        }
        ];
        this.callParent(arguments);
    }
});

