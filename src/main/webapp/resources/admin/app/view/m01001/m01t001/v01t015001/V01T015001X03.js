Ext.define(Fsl.app.getAbsView('V01T015001X03'), {
    extend      : 'Ext.window.Window',
    alias       : Fsl.app.getAlias('V01T015001X03'),
    icon        : Fsl.route.getImage('APP01009.png'),
    id          : 'V01T015001X03'.toLowerCase(),
    title       : 'RETURN ITEMS :: V01T015001X03',
    minimizable : false,
    maximizable : false,
    autoShow    : true,
    resizable   : false,
    modal       : true,
    width       : 585,
    height      : 475, 
    
    layout    : {
        type: 'vbox',
        align: 'stretch'
    },
    items  : [{ 
        xtype     : Fsl.app.getXtype('V01T015001X04'),           
        resizable : false,
        border    : false
    },{ 
        xtype     : Fsl.app.getXtype('V01T015001X05'),           
        resizable : false,
        border    : false
    }],
     
    buttons: [ 
    {
        text    : 'RECEIVE',
        icon    :  Fsl.route.getImage('SAV01004.png'),
        action  : 'receiveItem'
    },{
        text    : 'PDF',
        icon    :  Fsl.route.getImage('PDF01001.png'),
        action  : 'pdf'
    }]
});
