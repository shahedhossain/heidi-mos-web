Ext.define(Fsl.app.getAbsView('V01I002002X04'),{
    extend        : 'Ext.form.Panel',
    alias         :  Fsl.app.getAlias('V01I002002X04'),
    id            : 'V01I002002X04'.toLowerCase(), 
    defaults: {        
        activeRecord  : null,
        margins       : '0 5 0 5', 
        layout        : 'hbox',
        fieldDefaults : {
            anchor    : '100%',
            labelAlign: 'right'
        }
    },
    initComponent: function () {
        var me = this;        
        me.items = [{
        
            xtype: 'fieldcontainer',
            combineErrors: true, 

            defaults: {
                layout        : 'fit',
                flex          : 1,
                margin        : '0 0 0 5',
                hideLabel     : false,                
                labelAlign    : 'top'
            },
            items: [
            {
                xtype         : 'textfield',
                name          : 'id',
                fieldLabel    : 'Id',
                readOnly      : true
            },{
                xtype         : 'textfield',
                name          : 'name',
                fieldLabel    : 'Name',
                readOnly      : true,
                margin        : '0 5 0 5'
            }]
        }];
        me.callParent(arguments);
    }
});

