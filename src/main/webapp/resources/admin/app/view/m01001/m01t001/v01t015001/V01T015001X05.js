Ext.define(Fsl.app.getAbsView('V01T015001X05'), {
    extend    : 'Ext.grid.Panel',
    alias     : Fsl.app.getAlias('V01T015001X05'),
    id        : 'V01T015001X05'.toLowerCase(),
    border    : true,
    modal     : true,
    height    : 303,
    width     : 570,    
    viewConfig: {
        stripeRows    : true,
        forceFit      : true,
        loading       : true,
        emptyText     : 'No Records to display',
        listeners     : {
            viewready : function(v) { 
                var myMask = new Ext.LoadMask('v01t015001x05', {
                    msg:"Please wait..."
                });
                myMask.show();               
            }
        }
    },
    features  : [{
        ftype       : 'summary'       
    }],
    initComponent: function() {
        var me      = this;
        me.store    = Fsl.app.getRelStore('S01T016001');  
        me.bbar= Ext.create('Ext.ux.StatusBar', {
            id            : 'win-statusbar-v01t015001x05',
            topBorder     : false,
            text          : 'Status',
            defaultIconCls: 'fsl-severity-info',
            defaultText   : 'Status'
        });
        this.columns = [{
            text      : 'SN',
            xtype     : 'rownumberer',
            width     :  40, 
            sortable  :  false
        },{
            text        : 'ID',
            dataIndex   : 'id',
            width       :  60, 
            sortable    :  false,
            hidden      : true
        },{
            text        : 'Article',
            dataIndex   : 'articleId',            
            renderer    : this.getArticleName,
            flex        : 1,
            field: {
                xtype: 'numberfield'
            }
        },{
            text        : 'Rate',
            dataIndex   : 'priceId',
            align       : 'right',
            width       : 70,
            renderer    : this.getPrice,
            field: {
                xtype: 'numberfield'
            }
        },{
            text        : 'S.QTY',
            dataIndex   : 'sendQty',
            align       : 'right',
            width       : 70,
            field: {
                xtype: 'numberfield'
            },            
            summaryRenderer: function(value, summaryData, dataIndex) {                
                return parseFloat(value)
            },
            summaryType : 'count'
        },{
            text        : 'R.QTY',
            dataIndex   : 'returnQty',
            align       : 'right',
            width       : 70,
            field: {
                xtype: 'numberfield'
            },            
            summaryRenderer: function(value, summaryData, dataIndex) {                
                return parseFloat(value)
            },
            summaryType : 'count'
        },{
            text        : 'Vat',
            dataIndex   : 'vatId',
            align       : 'right',
            width       : 50,
            renderer    : this.getVat
        },{
            text        : 'Disc',
            dataIndex   : 'discountId',
            align       : 'right',
            width       : 50,
            renderer    : this.getDiscount
        },{
            text            : 'Status',
            dataIndex       : 'statusId',
            align           : 'right',
            width           :  50,
            renderer        :  this.getStatus
        }];       
        this.callParent(arguments);
    },    
    getArticleName :function(value, metadata, record, rowIdx, colIdx, store, vie) {
        return record.getM01i006001().data.name;
    }, 
    getVendorName :function(value, metadata, record, rowIdx, colIdx, store, vie) {
        return record.getM01i014001().data.name;
    }, 
    getBranchName :function(value, metadata, record, rowIdx, colIdx, store, vie) {
        return record.getM01i015001().data.name;
    },
    getFormatDate :function(value, metadata, record, rowIdx, colIdx, store, vie){
        return Ext.Date.format(value, 'M d, Y')
    },    
    getPrice :function(value, metaData, record, rowIdx, colIdx, store, view){
        var value=record.getM01i012001().data.price +'&nbsp';
        return value+'৳';
    },
    getVat : function(value, metaData, record, rowIdx, colIdx, store, view){
        var value=record.getM01i013001().data.vat +'&nbsp';
        return value+'%';
    },
    getDiscount :function(value, metaData, record, rowIdx, colIdx, store, view){
        var value=record.getM01t010001().data.discount +'&nbsp';
        return value+'%';
    },
    getStatus : function(value, metaData, record, rowIdx, colIdx, store, view){ 
        var value=record.getM01i016001().data.id;
        var icon;
        if(value == 1){
            icon = Fsl.route.getIcon('IC01001/waiting_room-3.png');         
        }else if(value == 2){        
            icon = Fsl.route.getIcon('IC01001/TIC01003.png');
        }else if(value == 3){
            icon = Fsl.route.getIcon('IC01001/partially-receive-icon.png');
        } 
        return "<img src='"+icon+"'/>";
    }
});