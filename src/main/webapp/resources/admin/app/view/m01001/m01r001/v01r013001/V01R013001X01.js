Ext.define(Fsl.app.getAbsView('V01R013001X01'), {
    extend      : 'Ext.window.Window',
    alias       : Fsl.app.getAlias('V01R013001X01'),
    icon        : Fsl.route.getImage('APP01009.png'),
    id          : 'V01R013001X01'.toLowerCase(),
    title       : 'DATE TO DATE :: V01R013001X01',
    minimizable :  false,
    maximizable :  false,
    autoShow    :  true,
    resizable   :  false,
    modal       :  true,
    width       :  323,    
    layout      :  {
        type	: 'vbox',
        align	: 'stretch'
    },
    items  : [{ 
        xtype     	: Fsl.app.getXtype('V01R013001X02'),       
        resizable 	: false,
        border    	: false
    }],
    buttons: [
    {
        text    : 'CLEAR',
        icon    :  Fsl.route.getImage('NEW01001.png')
    },{
        text    : 'REPORT',
        icon    :  Fsl.route.getImage('PDF01001.png'),
        action  :  'TODAY',
        hidden  :  true
    },{
        text    : 'REPORT',
        icon    :  Fsl.route.getImage('PDF01001.png'),
        action  :  'DATE_TO_DATE',
        hidden  :  true
    },{
        text    : 'REPORT',
        icon    :  Fsl.route.getImage('PDF01001.png'),
        action  :  'LAST_7_DAYS',
        hidden  :  true
    },{
        text    : 'REPORT',
        icon    :  Fsl.route.getImage('PDF01001.png'),
        action  :  'LAST_30_DAYS',
        hidden  :  true
    },{
        text    : 'REPORT',
        icon    :  Fsl.route.getImage('PDF01001.png'),
        action  :  'MONTHLY',
        hidden  :  true
    },{
        text    : 'REPORT',
        icon    :  Fsl.route.getImage('PDF01001.png'),
        action  :  'YEARLY',
        hidden  :  true
    },{
        text    : 'REPORT',
        icon    :  Fsl.route.getImage('PDF01001.png'),
        action  :  'ANY_DAY',
        hidden  :  true
    }]
});