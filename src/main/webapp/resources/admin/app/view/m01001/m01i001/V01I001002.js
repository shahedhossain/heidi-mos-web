Ext.define(Fsl.app.getAbsView('V01I001002'), {
    extend      : 'Ext.window.Window',
    alias       : Fsl.app.getAlias('V01I001002'),
    id          : 'V01I001002'.toLowerCase(),
    title       : 'CATEGORY :: V01I001002',
    icon        : Fsl.route.getImage('APP01009.png'),
    minimizable : false,
    maximizable : false,
    autoShow    : true,
    resizable   : false,
    modal       : true,  
    layout      : {
            type: 'vbox',
            align: 'stretch'
    },
    items  : [{ 
            xtype           :  Fsl.app.getXtype('V01I001002X01'),
            resizable       :  false,
            border          :  false,
            componentCls    : 'my-border-bottom'
    },{ 
            xtype           :  Fsl.app.getXtype('V01I001002X02'),
            resizable       :  false,
            border          :  false
    }]
});