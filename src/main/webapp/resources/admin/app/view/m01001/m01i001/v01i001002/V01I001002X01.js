
Ext.define(Fsl.app.getAbsView('V01I001002X01'), {    
    extend        : 'Ext.form.Panel',
    alias         :  Fsl.app.getAlias('V01I001002X01'),
    id            : 'V01I001002X01'.toLowerCase(), 
    defaults      : {        
        border        : true, 
        layout        : 'hbox',        
        fieldDefaults : {
            anchor    : '100%',
            labelAlign: 'right'
        }
    },
    bodyStyle    : {
        background    : 'none'
    },
    initComponent     : function(){        
        Ext.QuickTips.init();
        Ext.form.Field.prototype.msgTarget = 'qtip';         
        var me = this;       
        
        me.items = [{
            xtype           : 'fieldcontainer',
            combineErrors   : true,                    
            defaults        : {
                layout      : 'fit',
                margin      : '3 5 1 5',
                hideLabel   : false,                
                labelAlign  : 'top'
            },
            items: [{
                xtype             : 'numberfield',
                name              : 'id',
                fieldLabel        : 'ID:',
                minValue          : 1,
                width             : 100,                        
                minValue          :  0,
                mouseWheelEnabled :  false,
                hideTrigger       :  true,
                decimalPrecision  :  0,
                keyNavEnabled     :  false,
                scope             :this,                
                listeners: {
                    dblclick    : {
                        element : 'el',
                        scope   : this,
                        fn: function(){
                            var form   = this.getForm(); 
                            form.findField('id').setValue('');                            
                        }
                    }
                }
            },{
                xtype             : 'textfield',
                name              : 'name',
                fieldLabel        : 'NAME:',
                flex              : 1,
                scope             : this,                
                listeners         : {                                  
                    dblclick    : {
                        element : 'el',
                        scope   : this,
                        fn: function(){
                            var form   = this.getForm(); 
                            form.findField('name').setValue('');                            
                        }
                    }
                }                  
            }]
        }];       
        me.callParent(arguments);
    }
});

