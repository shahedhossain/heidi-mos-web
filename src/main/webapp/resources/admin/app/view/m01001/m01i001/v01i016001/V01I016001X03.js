Ext.define('${pkgName}.v01i016001.SV01I01600103' , {
    extend        : 'Ext.window.Window',
    alias         : 'widget.sv01i01600103',
    title         : 'STATUS EDIT :: SV01I01600103',
    id            : 'sv01i01600103',
    layout        : 'fit',
    autoShow      : true,    
    icon          : "${resource(dir: 'images', file: 'APP01003.png')}",
    modal         : true,
    border        : false,
    width         : 300,
    height        : 150,  
    initComponent : function () {
      var me = this;
        me.items = [
        {
            xtype     : 'form',
            bodyStyle : {
                padding   : '10px',
                border    : true
            },
            tbar: Ext.create('Ext.ux.StatusBar', {
                id                : 'win-statusbar-sv01i01600103',
                topBorder         : true,
                text              : 'Status',
                defaultIconCls    : 'info',
                defaultText       : 'Status'
            }),
            items : [{
                        xtype             : 'textfield',
                        name              : 'id',
                        fieldLabel        : 'Id',
                        emptyText         : 'Not need....',
                        width             : 265,
                        anchor            : '100%',
                        readOnly          : true
                    },
                    {
                        xtype             : 'textfield',
                        name              : 'name',
                        fieldLabel        : 'Status Name',
                        emptyText         : 'Status Name Here...',
                        width             : 265,
                        anchor            : '100%',
                        allowBlank        : false
                    }]
        }];
        this.buttons = [{
            text        : 'Update',
            icon        : "${resource(dir: 'images', file: 'EDT01004.png')}",
            action      : 'update',
            height      : 20
        },{
            text        : 'Close',
            icon        : "${resource(dir: 'images', file: 'CLS01001.png')}",      
            scope       : this,
            handler     : this.close,
            height      : 20
        }];
        this.callParent(arguments);
    }
});