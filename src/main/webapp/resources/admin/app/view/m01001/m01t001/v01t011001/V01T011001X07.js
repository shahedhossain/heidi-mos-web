Ext.define(Fsl.app.getAbsView('V01T011001X07'), {
    extend    : 'Ext.grid.Panel',
    alias     : Fsl.app.getAlias('V01T011001X07'),
    id        : 'V01T011001X07'.toLowerCase(),
    border    : true,
    modal     : true,
    height    : 285,
    width     : 610,    
    viewConfig: {
        stripeRows    : true,
        forceFit      : true,
        loading       : true,
        emptyText     : 'No Records to display',
        scope         : this,
        listeners     : {
            viewready : function(v) {                  
             //   var store = Fsl.store.getStore('S01T005003');
            //store.clearFilter();
            // store.loadPage(1);
            }
        }
    },
    features  : [{
        ftype       : 'summary'       
    }],
    initComponent: function() {
        var me     	= this;
        me.store   	= Fsl.app.getRelStore('S01T005003');    
        me. bbar= Ext.create('Ext.ux.StatusBar', {
            id            :   'win-statusbar-v01t011001x07',
            topBorder     :    false,
            text          :   'Status',
            defaultIconCls:   'fsl-severity-info',
            defaultText   :   'Status',
            items:[{
                    xtype: 'displayfield',
                    align: 'right',
                    name : 'summary',
                    id   : 'summary-id-v01t011001x07'
            }]
        });
        me.columns = [{
            text            : 'SN',
            xtype           : 'rownumberer',
            width           :  40, 
            sortable        :  false
        },{
            text            : 'Article',
            dataIndex       : 'articleId',            
            renderer        :  this.getArticleName,
            flex            :  1
        },{
            text            : 'Rate',
            dataIndex       : 'priceId',
            align           : 'right',
            width           :  70,
            renderer        :  this.getPrice
        },{
            text            : 'S.QTY',
            dataIndex       : 'quantity',
            align           : 'right',
            width           :  50,           
            summaryType: 'sum',
            summaryRenderer: function(value, summaryData, dataIndex, h , j , l, s) {
                return Ext.String.format('{0} {1}', value, value !== 1 ? '' : ''); 
            }
        },{
            text            : 'R.QTY',
            dataIndex       : 'receiveQuantity',
            align           : 'right',
            width           :  50
        },{
            text            : 'Vat',
            dataIndex       : 'vatId',
            align           : 'right',
            width           :  50,
            renderer        :  this.getVat
        },{
            text            : 'Disc',
            dataIndex       : 'discountId',
            align           : 'right',
            width           :  50,
            renderer        :  this.getDiscount
        },{
            header          : 'Total',
            dataIndex       : 'total',
            align           : 'right',
            width           :  80,
            renderer    : function(value, metaData, record, rowIdx, colIdx, store, view) {                
                var subtotal  = Ext.util.Format.number(value,'0.00');         
                return subtotal+'&nbsp' + '৳';           
            },
            summaryType     : 'sum',
            summaryRenderer : function(value, summaryData, record,dataIndex) {
                var total  = Ext.util.Format.number(value,'0.00')+'/='; 
                 me.up().down('displayfield[name=summary]').setValue(total);
                 //Ext.getCmp('summary-id-v01t011001x07').setValue(total);
                return Ext.String.format('{0} Tk{1}', total, value !== 1 ? '' : ''); 
            }
        },{
            text            : 'Status',
            dataIndex       : 'statusId',
            align           : 'right',
            width           :  50,
            renderer        :  this.getStatus
        },{
            menuDisabled    : true,
            sortable        : false,
            xtype           : 'actioncolumn',            
            width           : 22,            
            items           : [{
                icon        : Fsl.route.getImage('DEL01005.png'),
                tooltip     : 'Delete This?',
                scope       : this,
                handler     : function(grid, rowIdx, colIdx) {                                      
                    var record = grid.getStore().getAt(rowIdx);                     
                    var articleId = record.getM01i006001().data.id;
                    Ext.getCmp('WestPanel').confirmDelete(articleId, 'M01T005001', 'S01T005003', 'v01t011001x07');
                }
            }]
        }];      
        this.callParent(arguments);
    },    
    getArticleName :function(value, metadata, record, rowIdx, colIdx, store, view) {
        var form = view.up('window').down('form').getForm();
        /*if(record.getM01t011001().data.id > 0 &&  form.findField('toBranchId').getValue() == ""){
            form.findField('id').setValue(record.getM01t011001().data.id);
            form.findField('branchId').setValue(record.getM01t011001().getM01i015001().data.id); 
            form.findField('toBranchId').setValue(record.getM01t011001().getM01i015002().data.id);  
            form.findField('branchName').setValue(record.getM01t011001().getM01i015001().data.name); 
            form.findField('toBranchName').setValue(record.getM01t011001().getM01i015002().data.name);
        }*/
        return record.getM01i006001().data.name;
    }, 
    getVendorName :function(value, metadata, record, rowIdx, colIdx, store, view) {
        return record.getM01i014001().data.name;
    }, 
    getBranchName :function(value, metadata, record, rowIdx, colIdx, store, view) {
        return record.getM01i015001().data.name;
    },
    getFormatDate :function(value, metadata, record, rowIdx, colIdx, store, view){
        return Ext.Date.format(value, 'M d, Y')
    },    
    getPrice :function(value, metaData, record, rowIdx, colIdx, store, view){
        var value=record.getM01i012001().data.price +'&nbsp';
        return value+'৳';
    },
    getVat : function(value, metaData, record, rowIdx, colIdx, store, view){
        var value=record.getM01i013001().data.vat +'&nbsp';
        return value+'%';
    },
    getDiscount :function(value, metaData, record, rowIdx, colIdx, store, view){
        var value=record.getM01t010001().data.discount +'&nbsp';
        return value+'%';
    },
    getStatus : function(value, metaData, record, rowIdx, colIdx, store, view){        
        var value=record.getM01i016001().data.id;
        var icon;
        if(value == 1){
            icon = Fsl.route.getIcon('IC01001/waiting_room-3.png');         
        }else if(value == 2){        
            icon = Fsl.route.getIcon('IC01001/TIC01003.png');
        }else if(value == 3){
            icon = Fsl.route.getIcon('IC01001/partially-receive-icon.png');
        } 
        return "<img src='"+icon+"'/>";
    }
});