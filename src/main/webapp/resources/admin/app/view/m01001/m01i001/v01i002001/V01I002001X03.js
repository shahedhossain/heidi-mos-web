Ext.define(Fsl.app.getAbsView('V01I002001X03'), { 
    extend      : 'Ext.window.Window',
    alias       : Fsl.app.getAlias('V01I002001X03'),
    id          : 'V01I002001X03'.toLowerCase(),   
    title       : 'MANUFACTURE :: V01I002001X03',
    icon        : Fsl.route.getImage('APP01003.png'),	
    minimizable : false,
    maximizable : false,
    autoShow    : true,
    resizable   : false,
    border      : false,
    modal       : true,
    width       : 300,
    height      : 157,
    layout    : {
        type: 'vbox',
        align: 'stretch'
    },
    initComponent : function () {
        var me = this; 
        
        this.items = [
        {
            xtype                 : 'form',
            trackResetOnLoad      :  true,
            bodyStyle             : {
                padding           : '10px',
                border            : true
            },
            tbar: Ext.create('Ext.ux.StatusBar', {
                id                : 'win-statusbar-v01i002001x03',
                topBorder         :  true,
                text              : 'Status',
                defaultIconCls    : 'fsl-severity-info',
                defaultText       : 'Status'
            }),
            items : [{
                xtype             : 'textfield',
                fieldLabel        : 'Id',
                name              : 'id',
                width             :  265,
                anchor            : '95%',
                emptyText         : 'Not need...',
                readOnly          :  true
            },{
                xtype             : 'textfield',
                fieldLabel        : 'Manufac. Name',
                emptyText         : 'Write Manufacturer name',
                allowBlank        :  false,
                name              : 'name',
                afterLabelTextTpl :  required,
                width             :  265,
                anchor            : '95%'
            }]
        }]
        this.buttons = [
        {
            text    : 'Save',
            icon    : Fsl.route.getImage('SAV01004.png'),
            action  : 'save'
        },{
            text    : 'Close',
            icon    : Fsl.route.getImage('CLS01001.png'),   
            scope   : this,
            handler : function(button){
                var win = button.up('window');
                win.close();
            }
        }]
        me.callParent(arguments);
    }
});

