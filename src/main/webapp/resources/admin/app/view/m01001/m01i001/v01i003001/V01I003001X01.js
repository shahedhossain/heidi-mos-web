Ext.define(Fsl.app.getAbsView('V01I003001X01'), {   
    extend          : 'Ext.form.Panel',
    alias           : Fsl.app.getAlias('V01I003001X01'),
    id              : 'V01I003001X01'.toLowerCase(), 
    bodyStyle       : {
    background      : 'none'
    },
    defaults: {        
        activeRecord  : null,
        border        : true, 
        layout        : 'hbox',        
        fieldDefaults : {
            anchor    : '100%',
            labelAlign: 'right'
        }
    },
    
    initComponent     : function(){        
        Ext.QuickTips.init();
        Ext.form.Field.prototype.msgTarget = 'qtip';         
        var me      = this;        
        me.items    = [{
                xtype           : 'fieldcontainer',
                combineErrors   : true,                    
                defaults        : {
                    layout      : 'fit',
                    margin      : '3 5 1 5',
                    hideLabel   : false,                
                    labelAlign  : 'top'
                },
                items: [{
                        xtype             : 'numberfield',
                        name              : 'id',
                        fieldLabel        : 'Id :',
                        minValue          : 1,
                        width             : 100,                        
                        minValue          :  0,
                        mouseWheelEnabled :  false,
                        hideTrigger       :  true,
                        decimalPrecision  :  0,
                        keyNavEnabled     :  false,
                        scope             : this,                
                        listeners: {
                            dblclick    : {
                                  element : 'el',
                                  scope   : this,
                                  fn: function(){
                                  var form   = this.getForm(); 
                                      form.findField('id').setValue('');                            
                                  }
                              }
                        }
                    },{
                        xtype             : 'textfield',
                        name              : 'name',
                        fieldLabel        : 'Name:',
                        flex              : 1,
                        scope             : this,                
                        listeners         : {
                            dblclick    : {
                                  element : 'el',
                                  scope   : this,
                                  fn: function(){
                                  var form   = this.getForm(); 
                                      form.findField('name').setValue('');                            
                                  }
                              }
                        }                  
                    },{
                        xtype             : 'textfield',
                        name              : 'categoryName',
                        fieldLabel        : 'Category Name',                        
                        width             : 100,
                        scope             : this,                
                        listeners         : {
                            dblclick    : {
                                  element : 'el',
                                  scope   : this,
                                  fn: function(){
                                  var form   = this.getForm(); 
                                      form.findField('categoryName').setValue('');                            
                                  }
                              }
                        }                  
                    },{
                        xtype             : 'textfield',
                        name              : 'categoryId',                                       
                        hidden            :  true              
                    }]
            }];       
        me.callParent(arguments);
    }
});

