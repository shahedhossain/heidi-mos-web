Ext.define(Fsl.app.getAbsView('V01I013001X03'), { 
    extend      	: 'Ext.window.Window',
    alias       	:  Fsl.app.getAlias('V01I013001X03'),
    id          	: 'V01I013001X03'.toLowerCase(),   
    title       	: 'VAT ENTRY :: V01I013001X03',
    icon        	:  Fsl.route.getImage('APP01003.png'),	
    layout              : 'fit',
    autoShow            :  true,
    modal               :  true,
    width               : 320,
    height              : 200,  
    initComponent: function () { 
        var me  =this;
        me.items = [
        {
            xtype               : 'form',
            border              :  false,
            trackResetOnLoad	:  true,
            bodyStyle :  {                    
                padding   : '10px'
            },
            
            tbar : Ext.create('Ext.ux.StatusBar', {
                id            : 'win-statusbar-v01i013001x03',
                topBorder     : false,
                text          : 'Status',
                defaultIconCls: 'fsl-severity-info',
                defaultText   : 'Status'
            }),

            items: [{
                xtype             : 'numberfield',
                name              : 'id',
                fieldLabel        : 'Id',
                emptyText         : 'Not need....',
                width             :  265,
                anchor            : '100%',
                readOnly          :  true,
                minValue          :  0,
                mouseWheelEnabled :  false,
                hideTrigger       :  true,
                decimalPrecision  :  0,
                keyNavEnabled     :  false
            },
            {
                xtype             : 'textfield',
                name              : 'articleId',                
                allowBlank        :  false,
                readOnly          :  true,
                hidden            :  true
            },
            {
                xtype             : 'textfield',
                name              : 'articleName',                
                allowBlank        :  false,
                readOnly          :  true,
                fieldLabel        : 'Article',
                width             :  287
            },
            {
                xtype             : 'numberfield',
                name              : 'vat',
                fieldLabel        : 'Vat',
                emptyText         : 'Vat Here...',
                width             :  265,
                anchor            : '100%',
                allowBlank        :  false,
                minValue          :  0,
                maxValue          :  100
            },
            {
                xtype             : 'datefield',
                name              : 'date',
                fieldLabel        : 'Date',
                emptyText         : 'Date Here...',
                width             :  265,
                anchor            : '100%',
                allowBlank        :  false,
                minValue          :  '',
                format            : 'M d, Y',
                altFormats        : 'd/m/Y|M d, Y h:i A',
                value             : '2.4.2013',
                value             : new Date(),
                readOnly          : true
            }]
        }];
        me.buttons = [{
            text        : 'Save',
            icon        :  Fsl.route.getImage('SAV01004.png'),
            action      : 'save',
            height      :  20
        },{
            text        : 'Close',
            icon        :  Fsl.route.getImage('CLS01001.png'),   
            scope       :  this,
            handler     :  this.close,
            height      :  20
        }];
        this.callParent(arguments);
    }
});

