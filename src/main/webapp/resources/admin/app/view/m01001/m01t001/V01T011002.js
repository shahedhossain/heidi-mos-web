Ext.define(Fsl.app.getAbsView('V01T011002'), {
    extend      : 'Ext.window.Window',
    alias       : Fsl.app.getAlias('V01T011002'),
    icon        : Fsl.route.getImage('APP01009.png'),
    id          : 'V01T011002'.toLowerCase(),
    title       : 'TRANSFER LVM :: V01T011002',
    minimizable : false,
    maximizable : false,
    autoShow    : true,
    resizable   : false,
    modal       : true,
    width       : 510,
    height      : 375, 
    
      layout    : {
            type: 'vbox',
            align: 'stretch'
        },
    items  : [{ 
        xtype     	: Fsl.app.getXtype('V01T011001X03'), 
        resizable 	: false,
        border    	: false,
        componentCls    : 'my-border-bottom'
    },{ 
        xtype     	: Fsl.app.getXtype('V01T011001X04'),       
        resizable 	: false,
        border    	: false
    }]
});