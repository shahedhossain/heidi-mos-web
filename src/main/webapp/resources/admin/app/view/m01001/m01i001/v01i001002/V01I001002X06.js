Ext.define(Fsl.app.getAbsView('V01I001002X06'),{
    extend      : 'Ext.window.Window',
    alias       : Fsl.app.getAlias('V01I001002X06'),
    id          : 'V01I001002X06'.toLowerCase(),
    title       : 'MANUFACTURE LVM :: V01I001002X06',
    icon        : Fsl.route.getImage('APP01009.png'),
    minimizable : false,
    maximizable : false,
    autoShow    : true,
    resizable   : false,
    modal       : true,  
      layout    : {
            type: 'vbox',
            align: 'stretch'
        },
    initComponent: function () {
       this.items = [{ 
            xtype       : Fsl.app.getXtype('V01I001002X07'),
            resizable   : false,
            border      : false,
            componentCls:'my-border-bottom'
    },{
            xtype       : Fsl.app.getXtype('V01I001002X08'),
            resizable   : false,
            border      : false
        }];
        this.callParent(arguments);        
    }    
});