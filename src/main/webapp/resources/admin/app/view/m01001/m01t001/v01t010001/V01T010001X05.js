Ext.define(Fsl.app.getAbsView('V01T010001X05'), {
    extend      : 'Ext.form.Panel',
    alias     	: Fsl.app.getAlias('V01T010001X05'),
    id          : 'V01T010001X05'.toLowerCase(), 
    bodyStyle     :  {
    background    : 'none'
    },
    height        : 95,
    defaults: {        
        activeRecord  :  null,
        border        :  true, 
        layout        : 'hbox',        
        fieldDefaults :  {
            anchor    : '100%',
            labelAlign: 'right'
        }
    },
    
    initComponent     : function(){        
        Ext.QuickTips.init();
        Ext.form.Field.prototype.msgTarget = 'qtip';         
        var me        = this;       
        
        me.items  = [{
            xtype           : 'fieldcontainer',
            combineErrors   : true,                    
            defaults        : {
                layout      : 'fit',
                flex        :  1,
                margin      : '0 5 0 5',
                hideLabel   :  false,                
                labelAlign  : 'top'
            },
            items: [{
                xtype             : 'numberfield',
                name              : 'id',
                fieldLabel        : 'Article Id :',
                minValue          :  1,
                width             :  100,                        
                mouseWheelEnabled :  false,
                hideTrigger       :  true,
                decimalPrecision  :  0,
                keyNavEnabled     :  false,
                scope             :  this,                
                listeners: {                    
                    dblclick    : {
                        element : 'el',
                        scope   : this,
                        fn: function(){
                            var form   = this.getForm(); //Must be this scoped 
                            form.findField('id').setValue('');                            
                        }
                    }
                }
            },{
                xtype             : 'textfield',
                name              : 'name',
                fieldLabel        : 'Article Name:',                        
                scope             :  this,                
                listeners         : {                    
                    dblclick    : {
                        element : 'el',
                        scope   : this,
                        fn: function(){
                            var form   = this.getForm(); //Must be this scoped 
                            form.findField('name').setValue('');                            
                        }
                    }
                }
            },{
                xtype             : 'textfield',
                name              : 'brandName',
                fieldLabel        : 'Brand Name',
                scope             :  this,
                listeners: {
                    dblclick    : {
                        element : 'el',
                        scope   : this,
                        fn: function(){
                            var form   = this.getForm(); //Must be this scoped                            
                            form.findField('brandName').setValue('');                            
                        }
                    }
                }
            }]
        },
        {
            xtype           : 'fieldcontainer',
            combineErrors   : true,                    
            defaults        : {
                layout      : 'fit',
                flex        :  1,
                margin      : '0 5 0 5',
                hideLabel   :  false,                
                labelAlign  : 'top'
            },
            items: [{
                xtype             : 'numberfield',
                name              : 'mbcode',
                fieldLabel        : 'MB CODE:',
                minValue          :  1,
                width             :  100,                        
                minValue          :  0,
                mouseWheelEnabled :  false,
                hideTrigger       :  true,
                decimalPrecision  :  0,
                keyNavEnabled     :  false,
                scope             :  this,                
                listeners: {                    
                    dblclick    : {
                        element : 'el',
                        scope   : this,
                        fn: function(){
                            var form   = this.getForm(); //Must be this scoped 
                            form.findField('mbcode').setValue('');                            
                        }
                    }
                }
            },{
                xtype             : 'numberfield',
                name              : 'pbcode',
                fieldLabel        : 'PB CODE:',
                minValue          :  1,
                width             :  100,                        
                minValue          :  0,
                mouseWheelEnabled :  false,
                hideTrigger       :  true,
                decimalPrecision  :  0,
                keyNavEnabled     :  false,
                scope             :  this,                
                listeners: {                    
                    dblclick    : {
                        element : 'el',
                        scope   : this,
                        fn: function(){
                            var form   = this.getForm(); //Must be this scoped 
                            form.findField('pbcode').setValue('');                            
                        }
                    }
                }
            }]
        }];       
        me.callParent(arguments);
    }
});

