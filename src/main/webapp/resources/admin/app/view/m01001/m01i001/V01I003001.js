Ext.define(Fsl.app.getAbsView('V01I003001'), {
    extend      : 'Ext.window.Window',
    alias       : Fsl.app.getAlias('V01I003001'),
    id          : 'V01I003001'.toLowerCase(),
    title       : 'GENERIC PRODUCT LVM :: V01I003001',
    icon        : Fsl.route.getImage('APP01009.png'),
    minimizable : false,
    maximizable : false,
    autoShow    : true,
    resizable   : false,
    modal       : true,
    width       : 380,
    height      : 367,    
    layout    : {
        type: 'vbox',
        align: 'stretch'
    },
    items  : [{ 
        xtype       :  Fsl.app.getXtype('V01I003001X01'),
        resizable   :  false,
        border      :  false,
        componentCls: 'my-border-bottom'
    },{ 
        xtype       :  Fsl.app.getXtype('V01I003001X02'),
        resizable   :  false,
        border      :  false
    }]
});