Ext.define(Fsl.app.getAbsView('V01R004001X06'), {   
    extend          : 'Ext.grid.Panel',
    alias           : Fsl.app.getAlias('V01R004001X06'),
    id              : 'V01R004001X06'.toLowerCase(), 
    border    		: true,
    modal     		: true,    
    width     		: 420,
    height    		: 285,
    viewConfig: {
        stripeRows    : true,
        forceFit      : true,
        emptyText     : 'No Records to display',
        listeners     : {
            viewready : function(v) {               
                var store = Fsl.store.getStore('S01I005005');
                store.clearFilter();
                store.loadPage(1);
            }
        }
    },
    initComponent: function() {
       var me      = this;
       me.store    = Fsl.app.getRelStore('S01I005005');    
       me.tbar= Ext.create('Ext.ux.StatusBar', {            
            topBorder           : false,
            statusAlign         : 'right',
            items    : [
			{
                xtype       :'button',
                text        : 'Clear',
                icon        : Fsl.route.getIcon('IC01001/CAN01003.png'),
                action      : 'clear'
            },'-',{
                xtype       : 'button',
                text        : 'search',
                icon        : Fsl.route.getIcon('IC01001/search.png'),
                action      : 'search'
            },'-',{  
                xtype       :'statusbar',
                id          : 'win-statusbar-v01r004001x06',
                border      : false,
                defaultText : '',
                statusAlign : 'right'
            }]
        });
        
          

        this. bbar= new Ext.PagingToolbar({
            pageSize    	: 20,
            store       	: Fsl.app.getRelStore('S01I005005'),
            displayInfo 	: true      
        });
            
      
            
        this.columns = [{
            text        : 'SN',
            dataIndex   : 'id',
            width       : 40, 
            sortable    : false
        },{
            text        : 'Brand',
            dataIndex   : 'name'
        },{
            text        : 'brand Type',
            dataIndex   : 'brandTypeId',
            renderer    : this.getBrandType,
            flex        : 1
        },{
            text        : 'Manufacture',
            dataIndex   : 'manufacturerId',
            renderer    : this.getManufacturerName,
            flex        : 1
        },{
            text        : 'Generic Product',
            dataIndex   : 'genericProductId',
            renderer    : this.getGenericProductName,
            flex        : 1
        }];       
        this.callParent(arguments);
    },
    getBrandType :function(value, metadata, record, rowIdx, colIdx, store, vie) {
        return record.getM01i004001().data.name;
    },
    getManufacturerName :function(value, metadata, record, rowIdx, colIdx, store, vie) {
         return record.getM01i002001().data.name;
    },
    getGenericProductName :function(value, metadata, record, rowIdx, colIdx, store, vie) {
         return record.getM01i003001().data.name;
    }
});