Ext.define(Fsl.app.getAbsView('V01I005001X01'), {   
    extend          : 'Ext.form.Panel',
    alias           : Fsl.app.getAlias('V01I005001X01'),
    id              : 'V01I005001X01'.toLowerCase(), 
    bodyStyle       : {
        background    : 'none'
    },
    defaults: {        
        activeRecord  :  null,
        border        :  false,        
        layout        : 'hbox',        
        fieldDefaults :  {
            anchor    : '100%',
            labelAlign: 'right'
        }
    },
    width              :  500,
    initComponent     : function(){        
        Ext.QuickTips.init();
        Ext.form.Field.prototype.msgTarget = 'qtip';         
        var me        = this; 
        me.items  =[{
            defaultType   : 'fieldcontainer',
            layout        : 'hbox',
            bodyStyle     : {
                background  : 'none'                
            },
            items  : [{
                xtype           : 'fieldcontainer',
                combineErrors   : true,                       
                defaults        : {
                    layout      : 'vbox',
                    margin      : '5 5 5 5',
                    hideLabel   :  false, 
                    width       :  230,  
                    labelAlign  : 'left'
                },
                items: [{
                    xtype             : 'numberfield',
                    name              : 'id',
                    fieldLabel        : 'Brand Id :',
                    minValue          :  1,                                                      
                    minValue          :  0,
                    mouseWheelEnabled :  false,
                    hideTrigger       :  true,
                    decimalPrecision  :  0,
                    keyNavEnabled     :  false,
                    scope             :  this,                
                    listeners: {
                        dblclick    : {
                            element : 'el',
                            scope   : this,
                            fn: function(){
                                var form   = this.getForm(); //Must be this scoped 
                                form.findField('id').setValue('');                            
                            }
                        }
                    }
                },{
                    xtype             : 'textfield',
                    name              : 'name',
                    fieldLabel        : 'Brand Name:',                        
                    scope             :  me,                
                    listeners         : {
                        dblclick    : {
                            element : 'el',
                            scope   : this,
                            fn: function(){
                                var form   = this.getForm(); //Must be this scoped 
                                form.findField('name').setValue('');                            
                            }
                        }
                    }                  
                },
                {
                    xtype             : 'textfield',
                    name              : 'brandTypeName',
                    fieldLabel        : 'BrandType Name',               
                    listeners     : {
                        'dblclick'  : {
                            scope   : this,
                            element : 'el',
                            fn   : function(evt, el, opt) {
                                var form   = this.getForm(); //Must be this scoped                                            
                                form.findField('brandTypeName').setValue('').focus();
                            }
                        } 
                    }
                }]
            },
            {
                xtype           : 'fieldcontainer',
                combineErrors   : true,                    
                defaults        : {
                    layout      : 'vbox',
                    margin      : '5 5 5 5',
                    hideLabel   :  false, 
                    labelWidth  :  80,
                    width       :  230,
                    labelAlign  : 'left'
                },
                items: [{
                    xtype             : 'textfield',
                    name              : 'manufacturerName',
                    fieldLabel        : 'Manufac. Co.',
                    emptyText         : 'Manufacturer Com. Name Here',
                    scope             :  this,
                    listeners: {
                        dblclick    : {
                            element : 'el',
                            scope   : this,
                            fn: function(){
                                var form   = this.getForm(); 
                                form.findField('manufacturerName').setValue('');                               
                            }
                        }
                    }
                },{
                    xtype             : 'textfield',
                    name              : 'genericProductName',
                    fieldLabel        : 'GP. Name',
                    emptyText         : 'Generic Product Name here',
                    scope             :  this,
                    listeners: {
                        dblclick: {
                            element: 'el',
                            scope   : this,
                            fn: function(){
                                var form   = this.getForm(); 
                                form.findField('genericProductName').setValue('');
                            }
                        }
                    }
                }]                        
            }]
        }];       
        me.callParent(arguments);
    }
});
