Ext.define(Fsl.app.getAbsView('V01T002001X03'),{
    extend      : 'Ext.window.Window',
    alias     	: Fsl.app.getAlias('V01T002001X03'),
    id          : 'V01T002001X03'.toLowerCase(), 
    title       : 'EXPIRE :: V01T002001X03', 
    icon        : Fsl.route.getImage('APP01003.png'),
    layout      : 'fit',
    autoShow    : true,
    modal       : true,
    height      : 230,
    width       : 300,
    
    initComponent: function () {
        var me = this;
        me.items = [
        {
            xtype               : 'form',
            border              : false,
            trackResetOnLoad    : true,
            bodyStyle       : {                    
                padding   : '10px'
            },
            
            tbar : Ext.create('Ext.ux.StatusBar', {
                id                : 'win-statusbar-v01t002001x03',
                topBorder         :  true,
                text              : 'Status',
                defaultIconCls    : 'fsl-severity-info',
                defaultText       : 'Status'
            }),
            
            items: [
            {
                xtype             : 'numberfield',
                name              : 'id',
                fieldLabel        : 'Id',
                emptyText         : 'Not need....',
                width             :  265,
                anchor            : '100%',
                readOnly          :  true,
                minValue          :  0,
                mouseWheelEnabled :  false,
                hideTrigger       :  true,
                decimalPrecision  :  0,
                keyNavEnabled     :  false
            },{
                xtype             :'datefield',
                name              :'date',
                fieldLabel        :'Date',
                width             : 265,
                anchor            : '100%',                 
                value             : new Date(),
                allowBlank        : false,
                format            : 'M d, Y',
                altFormats        : 'd/m/Y|M d, Y h:i A',
                readOnly          : true
            },
            {
                xtype             : 'textfield',
                name              : 'articleName',
                emptyText         : 'Double Click Here....',
                fieldLabel        : 'Article',
                readOnly          :  true,                
                width             : 265,
                anchor            : '100%',
                allowBlank        : false,
                readOnly          : true
            },
            {
                xtype             : 'hiddenfield',
                name              : 'articleId',
                readOnly          :  true                
            },{
                xtype             : 'numberfield',
                fieldLabel        : 'Quantity',
                emptyText         : 'Quantity Here....',
                name              : 'quantity',
                width             : 265,
                anchor            : '100%',
                allowBlank        : false,
                minValue          :  0,
                mouseWheelEnabled :  false,
                hideTrigger       :  true,
                decimalPrecision  :  0,
                keyNavEnabled     :  false
            },
            {
                xtype             : 'textfield',
                name              : 'vendorName',
                emptyText         : 'Double Click Here....',
                fieldLabel        : 'VENDOR',
                readOnly          :  true,                
                width             : 265,
                anchor            : '100%',
                allowBlank        : false,
                readOnly          : true
            },
            {
                xtype             : 'hiddenfield',
                name              : 'vendorId',
                readOnly          :  true                
            }]
        }];
        this.buttons = [
        {
            text    : 'Save',
            icon    : Fsl.route.getImage('SAV01004.png'),
            action  : 'save'
        },
        {
            text    : 'Close',
            icon    : Fsl.route.getImage('CLS01001.png'),     
            scope   : this,
            handler: this.close
        }
        ];
        this.callParent(arguments);
    }
});

