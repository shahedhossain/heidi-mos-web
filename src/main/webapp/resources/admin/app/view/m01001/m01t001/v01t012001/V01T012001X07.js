Ext.define(Fsl.app.getAbsView('V01T012001X07'), {
    extend      : 'Ext.window.Window',
    alias       : Fsl.app.getAlias('V01T012001X07'),
    icon        : Fsl.route.getImage('APP01009.png'),
    id          : 'V01T012001X07'.toLowerCase(),
    title       : 'RECEIVE WIN :: V01T012001X07',
    minimizable : false,
    maximizable : false,
    autoShow    : true,
    resizable   : false,
    modal       : true,
    width       : 650,
    height      : 450, 
    
      layout    : {
            type: 'vbox',
            align: 'stretch'
        },
    items  : [{ 
        xtype     : Fsl.app.getXtype('V01T012001X08'),           
        resizable : false,
        border    : false
    },{ 
        xtype     : Fsl.app.getXtype('V01T012001X09'),           
        resizable : false,
        border    : false
    }],
     
    buttons: [
    {
        text    : 'CLEAR',
        icon    : Fsl.route.getImage('NEW01001.png'),
        action  : 'clear'
    },{
        text    : 'UPDATE',
        icon    :  Fsl.route.getImage('SAV01004.png'),
        action  : 'update'
    },{
        text    : 'PDF',
        icon    :  Fsl.route.getImage('PDF01001.png'),
        action  : 'pdf'
    }/*
   {
        text    : 'CLEAR',
        icon    : "${resource(dir: 'images', file: 'NEW01001.png')}",
        id      : 'clear-ti',
        action  : 'resetAll'
    },{
        text      : 'UPDATE',
        icon      : "${resource(dir: 'images', file: 'SAV01004.png')}",
        id        : 'saveItem-ti',
        action    : 'update'
    }*/]
});