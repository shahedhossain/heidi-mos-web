Ext.define(Fsl.app.getAbsView('V01I006001X03'), { 
    extend      : 'Ext.window.Window',
    alias       :  Fsl.app.getAlias('V01I006001X03'),
    id          : 'V01I006001X03'.toLowerCase(),   
    title       : 'Article :: V01I006001X03',
    icon        :  Fsl.route.getImage('APP01003.png'),	
    width       :  300,
    height      :  230,
    layout      : 'fit',
    autoShow    : true,
    modal       : true,
    initComponent: function () {
        var me = this;       
        me.items = [
        {
            xtype               : 'form',
            border              : false,
            trackResetOnLoad  	:  true,
            bodyStyle           : {                    
                padding   : '10px'
            },
            
            tbar : Ext.create('Ext.ux.StatusBar', {
                id            		: 'win-statusbar-v01i006001x03',
                topBorder     		: false,
                text          		: 'Status',
                defaultIconCls		: 'fsl-severity-info',
                defaultText   		: 'Status'
            }),
            
            items: [{
                xtype             	: 'textfield',
                fieldLabel        	: 'ARTICLE ID',
                emptyText         	: 'Not Required...',
                name              	: 'id',
                width             	: 265,
                anchor            	: '100%',
                readOnly          	: true
            },
            {
                xtype             	: 'textfield',
                fieldLabel        	: 'ARTICLE NAME',
                emptyText         	: 'Write Article name...',
                name              	: 'name',
                width             	:  265,
                anchor            	: '100%',
                allowBlank        	: false
            },
            {
                xtype             	: 'textfield',
                fieldLabel        	: 'MBCODE',
                emptyText         	: 'Write Manufacturer code...',
                name              	: 'mbcode',
                width             	: 265,
                anchor            	: '100%',
                allowBlank        	: false
            },
            {
                xtype             	: 'textfield',
                fieldLabel        	: 'PBCODE',
                emptyText         	: 'Write Article name...',
                name              	: 'pbcode',
                width             	: 265,
                anchor            	: '100%',
                allowBlank        	: false
            },
            {
                xtype             	: 'textfield',
                name              	: 'brandName',
                fieldLabel        	: 'BRAND',
                width             	: 265,
                anchor            	: '100%',
                allowBlank        	: false,
                readOnly         	: true,                
                listeners: {
                    dblclick: {
                        element: 'el',
                        fn: function(){
                            Fsl.app.getWidget('v01i006001x04').show()
                        }
                    }
                }
            },
            {
                xtype             	: 'hiddenfield',
                name              	: 'brandId',
                allowBlank        	:  false,
                readOnly          	:  true 
            }]
        }];
        this.buttons = [
        {
            text        : 'New',
            icon        : Fsl.route.getImage('NEW01001.png'),
            scope       : this,
            handler     : function(){
                var form = this.down('form').getForm();
                form.findField('id').setValue('');
                form.findField('name').setValue('');
                form.findField('mbcode').setValue('');
                form.findField('pbcode').setValue('');
                form.findField('brandName').setValue('');
                var btn    = this.down('button[action=save]')
                btn.setText('Save');
                btn.setIcon(Fsl.route.getImage('SAV01004.png'));   
            }
        },{
            text        : 'Save',
            icon        : Fsl.route.getImage('SAV01004.png'),
            action      : 'save'
        },
        {
            text        : 'Close',
            icon        : Fsl.route.getImage('CLS01001.png'),   
            scope       : this,
            handler     : this.close
        }
        ];
        this.callParent(arguments);
    }
});

