Ext.define(Fsl.app.getAbsView('V01T009001'), {
    extend      : 'Ext.window.Window',
    alias       : Fsl.app.getAlias('V01T009001'),
    icon        : Fsl.route.getImage('APP01009.png'),
    id          : 'V01T009001'.toLowerCase(),
    title       : 'USER LVM :: V01T009001',
    minimizable : false,
    maximizable : false,
    autoShow    : true,
    resizable   : false,
    modal       : true,
    width       :  570,
    height      :  410, 
      layout    : {
            type: 'vbox',
            align: 'stretch'
        },
    items  : [{ 
        xtype     	: Fsl.app.getXtype('V01T009001X01'), 
        resizable 	: false,
        border    	: false,
        componentCls    : 'my-border-bottom'
    },{ 
        xtype     	: Fsl.app.getXtype('V01T009001X02'),       
        resizable 	: false,
        border    	: false
    }]
});