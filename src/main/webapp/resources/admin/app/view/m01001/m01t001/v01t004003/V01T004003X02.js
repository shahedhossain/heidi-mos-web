Ext.define(Fsl.app.getAbsView('V01T004003X02'), {
    extend    : 'Ext.grid.Panel',
    alias     : Fsl.app.getAlias('V01T004003X02'),
    id        : 'V01T004003X02'.toLowerCase(),
    border    : true,
    modal     : true,
    height    : 285,
    width     :  660,
    viewConfig: {
        stripeRows    : true,
        forceFit      : true,
        emptyText     : 'No Records to display',
        listeners     : {
            viewready : function(v) {               
                var store = Fsl.store.getStore('S01T004004');
                store.clearFilter();
                store.loadPage(1);
            }
        }
    },
    columnLines: true,
    initComponent: function() {
        var me     	= this;
        me.store   	= Fsl.app.getRelStore('S01T004004');     
        me.tbar = Ext.create('Ext.ux.StatusBar', {            
            topBorder           : false,
            statusAlign         : 'right',
            items     : [{
                xtype       : 'button',
                text        : 'Clear',
                icon        : Fsl.route.getIcon('IC01001/CAN01003.png'),
                action      : 'clear'
            },'-',{
                xtype       : 'button',
                text        : 'search',
                icon        : Fsl.route.getIcon('IC01001/search.png'),
                action      : 'search'
            },'-',{  
                xtype       : 'statusbar',
                id          : 'win-statusbar-v01t004001x02',
                border      : false,
                defaultText : '',
                statusAlign : 'right'
            }]
        });
        
        this.bbar = new Ext.PagingToolbar({
            pageSize        :  10,
            store           : Fsl.app.getRelStore('S01T004004'),
            displayInfo     :  true
        });        
            
        this.columns = [{
            text      : 'SN',
            xtype     : 'rownumberer',
            width     :  40, 
            sortable  :  false
        },{
            text        : 'ID',
            dataIndex   : 'id',
            width       :  40, 
            sortable    :  false,
            renderer    :  this.rowToolTip
        },{
            text        : 'Article',
            dataIndex   : 'articleId', 
            scope       :  this,
            renderer    :  this.getArticleName,
            flex        :  1,
            field: {
                xtype: 'numberfield'
            }
        },{
            text        : 'Quantity',
            dataIndex   : 'quantity',
            align       : 'right',
            width       : 80,
            renderer    : this.rowToolTip,
            field: {
                xtype: 'numberfield'
            }            
        }];       
        this.callParent(arguments);
    },    
    getArticleName :function(value, metadata, record, rowIdx, colIdx, store, vie) {
        this.rowToolTip(value, metadata, record)
        return record.getM01i006001().data.name;
    },
    rowToolTip : function(value, metadata, record) {
        metadata.style = 'color:#D10000;background:#F7E4E4;' //font-weight:bold
        var smarty = "<b>Stock Item Information</b>";
        smarty += "<br/>Product id  : {articleId}";
        smarty += "<br/>Product Name:"+record.getM01i006001().data.name;
        smarty += "<br/>Quantity    : {quantity}";  

        var tpl = new Ext.XTemplate(smarty);
        var qtip = tpl.apply(record.data)
        metadata.tdAttr = 'data-qtip="' + qtip + '"';           

        return value
    }
});