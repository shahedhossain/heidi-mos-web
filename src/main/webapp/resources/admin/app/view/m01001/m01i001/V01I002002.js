Ext.define(Fsl.app.getAbsView('V01I002002'), {
    extend      : 'Ext.window.Window',
    alias       : Fsl.app.getAlias('V01I002002'),
    id          : 'V01I002002'.toLowerCase(),
    title       : 'MANUFACTURE :: V01I002002',
    icon        : Fsl.route.getImage('APP01009.png'),
    minimizable : false,
    maximizable : false,
    autoShow    : true,
    resizable   : false,
    modal       : true,  
    layout      : {
            type: 'vbox',
            align: 'stretch'
    },
    items  : [{ 
            xtype           :  Fsl.app.getXtype('V01I002002X01'),
            resizable       :  false,
            border          :  false,
            componentCls    : 'my-border-bottom'
    },{ 
            xtype           :  Fsl.app.getXtype('V01I002002X02'),
            resizable       :  false,
            border          :  false
    }]
});