Ext.define(Fsl.app.getAbsView('V01T012001X13'), {
    extend      : 'Ext.window.Window',
    alias       : Fsl.app.getAlias('V01T012001X13'),
    icon        : Fsl.route.getImage('APP01009.png'),
    id          : 'V01T012001X13'.toLowerCase(),
    title       : 'RECEIVE WIN :: V01T012001X13',
    minimizable : false,
    maximizable : false,
    autoShow    : true,
    resizable   : false,
    modal       : true,
    width       : 650,
    height      : 405, 
    
    layout    : {
        type: 'vbox',
        align: 'stretch'
    },
    items  : [{ 
        xtype     : Fsl.app.getXtype('V01T012001X14'),           
        resizable : false,
        border    : false
    },{ 
        xtype     : Fsl.app.getXtype('V01T012001X15'),           
        resizable : false,
        border    : false
    }]
});