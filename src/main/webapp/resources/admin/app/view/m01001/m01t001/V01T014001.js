Ext.define(Fsl.app.getAbsView('V01T014001'), {
    extend      : 'Ext.window.Window',
    alias       : Fsl.app.getAlias('V01T014001'),
    icon        : Fsl.route.getImage('APP01009.png'),
    id          : 'V01T014001'.toLowerCase(),
    title       : 'ORDER LVM :: V01T014001',
    minimizable : false,
    maximizable : false,
    autoShow    : true,
    resizable   : false,
    modal       : true,
      layout    : {
            type: 'vbox',
            align: 'stretch'
        },
    items  : [{ 
            xtype       : Fsl.app.getXtype('V01T014001X01'),           
            resizable   : false,
            border      : false,
            componentCls: 'my-border-bottom'
    },{ 
            xtype       : Fsl.app.getXtype('V01T014001X02'),             
            resizable   : false,
            border      : false
    }]
});