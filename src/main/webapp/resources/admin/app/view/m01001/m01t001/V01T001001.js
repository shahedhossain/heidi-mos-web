Ext.define(Fsl.app.getAbsView('V01T001001'), {
    extend      : 'Ext.window.Window',
    alias       : Fsl.app.getAlias('V01T001001'),
    icon        : Fsl.route.getImage('APP01009.png'),
    id          : 'V01T001001'.toLowerCase(),
    title       : 'CUSTOMER LVM :: V01T001001',
    minimizable :  false,
    maximizable :  false,
    autoShow    :  true,
    resizable   :  false,
    modal       :  true,
    width       :  400,
    height      :  367, 
    layout      :  {
        type	: 'vbox',
        align	: 'stretch'
    },
    items  : [{ 
        xtype     	: Fsl.app.getXtype('V01T001001X01'), 
        resizable 	: false,
        border    	: false,
        componentCls    : 'my-border-bottom'
    },{ 
        xtype     	: Fsl.app.getXtype('V01T001001X02'),       
        resizable 	: false,
        border    	: false
    }]
});