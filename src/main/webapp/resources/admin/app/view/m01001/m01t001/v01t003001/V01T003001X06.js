Ext.define(Fsl.app.getAbsView('V01T003001X06'), {
    extend    : 'Ext.grid.Panel',
    alias     : Fsl.app.getAlias('V01T003001X06'),
    id        : 'V01T003001X06'.toLowerCase(),
    border    : true,
    modal     : true,
    width     : 500,
    height    : 285,
    viewConfig: {
        stripeRows    : true,
        forceFit      : true,
        emptyText     : 'No Records to display',
        listeners     : {
            viewready : function(v) {               
                var store = Fsl.store.getStore('S01I006008');
                store.clearFilter();
                store.loadPage(1);
            }
        }
    },
    initComponent: function() {
        var me     = this;
        me.store   = Fsl.app.getRelStore('S01I006008');  
        this.tbar  = Ext.create('Ext.ux.StatusBar', {            
            topBorder           : false,
            statusAlign         : 'right',
            items     : [{
                xtype       : 'button',
                text        : 'Clear',
                icon        : Fsl.route.getIcon('IC01001/CAN01003.png'),
                action      : 'clear'
            },'-',{
                xtype       : 'button',
                text        : 'search',
                icon        : Fsl.route.getIcon('IC01001/search.png'),
                action      : 'search'
            },'-',{  
                xtype       : 'statusbar',
                id          : 'win-statusbar-v01t003001x06',
                border      : false,
                defaultText : '',
                statusAlign : 'right'
            }]
        });
        
        this.bbar = new Ext.PagingToolbar({
            pageSize        :  20,
            store           :  Fsl.app.getRelStore('S01I006008'),
            displayInfo     :  true
        });            
        this.columns = [{
            text        : 'SN',
            dataIndex   : 'id',
            width       : 40, 
            sortable    : false
        },{
            text        : 'Article',
            dataIndex   : 'name',
            flex        : 1
        },{
            text        : 'MBCODE',
            dataIndex   : 'mbcode',
            flex        : 1
        },{
            text        : 'PBCODE',
            dataIndex   : 'pbcode',
            flex        : 1
        },{
            text        : 'Brand',
            dataIndex   : 'brandId',
            renderer    : this.getBrandName
        },{
            text        : 'Comments',
            dataIndex   : 'details',
            flex        : 1
        }];       
        this.callParent(arguments);
    },    
    getBrandName :function(value, metadata, record, rowIdx, colIdx, store, vie) {
              return record.getM01i005001().data.name;
    } 
});