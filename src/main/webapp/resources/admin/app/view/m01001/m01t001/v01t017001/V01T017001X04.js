Ext.define(Fsl.app.getAbsView('V01T017001X04'), {
    extend        : 'Ext.form.Panel',
    alias         : Fsl.app.getAlias('V01T017001X04'),
    id            : 'V01T017001X04'.toLowerCase(), 
    background    : false,
    bodyStyle     : {
        background: 'none'                
    },
    defaults: {        
        activeRecord  : null,
        border        : true, 
        layout        : 'hbox',        
        fieldDefaults : {
            anchor    : '100%',
            labelAlign: 'right',
            readOnly  :  true
        }
    },    
    initComponent     : function(){               
        var me = this;         
        me.items = [{
            xtype: 'fieldcontainer',
            combineErrors: true,                    
            defaults: {
                layout      : 'fit',
                flex        : 1,
                margin      : '5 7 0 10',
                hideLabel   : false,                
                labelAlign  : 'left'
            },
            items: [{
                xtype               : 'textfield',
                name                : 'fromBranchId',
                fieldLabel          : 'Branch Code:',
                allowBlank          :  false,
                minValue            :  1
            },{
                xtype               : 'textfield',
                name                : 'branchName',
                fieldLabel          : 'Name:',
                allowBlank          : false
            }]
        },{
            xtype                   : 'fieldcontainer',
            combineErrors           : true,             
            defaults: { 
                layout              : 'fit',
                flex                :  1,
                margin              : '0 7 0 10',
                hideLabel           :  false,                
                labelAlign          : 'left'
            },
            items: [
            {
                xtype               : 'textfield',
                fieldLabel          : 'Order ID:',
                name                : 'orderId'
            },{
                xtype               : 'textfield',
                name                : 'entryDate',
                fieldLabel          : 'Sending Date :',
                allowBlank          :  false,
                anchor              : '95%',
                margin              : '0 7 0 10'
            }]
        },{
            xtype                   : 'fieldcontainer',
            combineErrors           : true,             
            defaults: { 
                layout              : 'fit',
                margin              : '0 7 0 10',
                hideLabel           : false,                
                labelAlign          : 'left',
                flex                : 1
            },
            items: [
            {
                xtype               : 'numberfield',                
                hideTrigger         :  true,
                decimalPrecision    :  0,
                keyNavEnabled       :  false,
                mouseWheelEnabled   :  false,
                fieldLabel          : 'Order App ID.:',                
                name                : 'orderApprovalId',
                enableKeyEvents     :  true,               
                msgDisplay          : 'tooltip',
                width               :  267,
                anchor              : '100%'
            },            
            {
                xtype               : 'textfield',
                fieldLabel          : 'Transfer :',
                name                : 'transferId',
                width               : 267
            }]
        }];       
        me.callParent(arguments);
    }
});

