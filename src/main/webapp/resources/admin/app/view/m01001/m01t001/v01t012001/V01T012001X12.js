Ext.define(Fsl.app.getAbsView('V01T012001X12'), {
    extend    : 'Ext.grid.Panel',
    alias     : Fsl.app.getAlias('V01T012001X12'),
    id        : 'V01T012001X12'.toLowerCase(),
    border    : true,
    modal     : true,
    height    : 285,
    width     : 650,     
    viewConfig: {
        stripeRows    : true,
        forceFit      : true,
        emptyText     : 'No Records to display',
        loadMask      : false,
        listeners     : {
            viewready : function(v) {               
                var store = Fsl.store.getStore('S01T012001');
                store.clearFilter();
                store.loadPage(1);
            }
        }
    },
    initComponent: function() {
        var me     	= this;
        me.store   	= Fsl.app.getRelStore('S01T012001');  
        me.tbar = Ext.create('Ext.ux.StatusBar', {            
            topBorder           : false,
            statusAlign         : 'right',
            items     : [{
                xtype       : 'button',
                text        : 'Clear',
                icon        : Fsl.route.getIcon('IC01001/CAN01003.png'),
                action      : 'clear'
            },'-',{
                xtype       : 'button',
                text        : 'search',
                icon        : Fsl.route.getIcon('IC01001/search.png'),
                action      : 'search'
            },'-',{  
                xtype       : 'statusbar',
                id          : 'win-statusbar-v01t012001x12',
                border      : false,
                defaultText : '',
                statusAlign : 'right'
            }]
        });
        
        this.bbar = new Ext.PagingToolbar({
            pageSize        :  10,
            store           :  Fsl.app.getRelStore('S01T012001'),
            displayInfo     :  true
        });         
            
        this.columns = [{
            text        : 'RECEIVE ID',
            dataIndex   : 'id',            
            sortable    :  false
        },{
            text        : 'TRANSFER ID',
            dataIndex   : 'transferId'      
        },{
            text        : 'FROM BRANCH',
            dataIndex   : 'fromBranchId',
            flex        : 1,
            renderer    : this.getBranchName
            
        },{
            text        : 'DATE',
            dataIndex   : 'entryDate',
            flex        : 1,
            renderer    : this.getFormatDate
        }];
        this.callParent(arguments);
    },   
    getBranchName :function(value, metadata, record, rowIdx, colIdx, store, vie) {
        /* var form = Ext.getCmp('sv01t01200115').getForm();
              form.findField('branchId').setValue(record.getM01I015001().data.id);
              form.findField('branchName').setValue(record.getM01I015001().data.name);*/
        return record.getM01i015002().data.name;
    },
    getFormatDate :function(value, metadata, record, rowIdx, colIdx, store, vie){
        return Ext.Date.format(value, 'M d, Y')
    }
});