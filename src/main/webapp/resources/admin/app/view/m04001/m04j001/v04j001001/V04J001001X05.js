Ext.define(Fsl.app.getAbsView('V04J001001X05'), {
    extend      	: 'Ext.form.Panel',
    alias     		: Fsl.app.getAlias('V04J001001X05'),
    id          	: 'V04J001001X05'.toLowerCase(), 
    bodyStyle : {
        background: 'none'                
    },
    initComponent : function(){
        var me      = this;     
        this.items  =[{
            defaultType   : 'textfield',
            layout        : 'hbox',
            flex          : 1,
            border        : false,
            fieldDefaults : {
                anchor      : '100%',
                labelAlign  : 'right'               
            },
            bodyStyle     : {
                background  : 'none'                
            },
            items   : [{
                xtype             : 'fieldcontainer',
                layout            : 'vbox',
                margin            : '5 0 0 5',                        
                defaults          :  {
                    width           :  150,
                    labelAlign      : 'left',
                    labelWidth      :  55
                }, 
                items : [{
                    xtype             : 'numberfield',
                    name              : 'userId',
                    fieldLabel        : 'User Id',
                    minValue          :  1,                       
                    mouseWheelEnabled :  false,
                    hideTrigger       :  true,
                    decimalPrecision  :  0,
                    keyNavEnabled     :  false                        
                },{
                    xtype             : 'numberfield',
                    name              : 'id',
                    fieldLabel        : 'Acc. Id',
                    minValue          :  1,                       
                    mouseWheelEnabled :  false,
                    hideTrigger       :  true,
                    decimalPrecision  :  0,
                    keyNavEnabled     :  false       
                },{
                    xtype             : 'textfield',
                    name              : 'username',
                    fieldLabel        : 'Name'
                }]
            },                          
            {
                xtype       : 'fieldcontainer',
                layout      : 'vbox',
                margin      : '5 0 0 10',                        
                defaults          :  {
                    width           :  190,
                    labelAlign      : 'left',
                    labelWidth      : 85,
                    xtype               : 'numberfield',
                    mouseWheelEnabled   :  false,
                    hideTrigger         :  true,
                    decimalPrecision    :  0,
                    keyNavEnabled       :  false  ,
                    maxValue            : 1,
                    minValue            : 0
                }, 
                items: [{                   
                    name              : 'enabled',
                    fieldLabel        : 'Acc. Status'
                },{
                    name              : 'accountLocked',
                    fieldLabel        : 'Lock'
                }]
            },{
                xtype               : 'fieldcontainer',
                layout              : 'vbox',
                margin              : '5 5 0 10',                        
                defaults            :  {
                    //width               :  150,
                    flex                :  1,
                    labelAlign          : 'left',
                    labelWidth          : 100,
                    xtype               : 'numberfield',
                    mouseWheelEnabled   :  false,
                    hideTrigger         :  true,
                    decimalPrecision    :  0,
                    keyNavEnabled       :  false  ,
                    maxValue            : 1,
                    minValue            : 0
                },                 
                items: [{
                    name              : 'accountExpired',
                    fieldLabel        : 'Acc. Expire:'
                },{
                    name              : 'passwordExpired',
                    fieldLabel        : 'Password Expire:'
                }]
            }]
        }];
        this.callParent(arguments);
    }
});

