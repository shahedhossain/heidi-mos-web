Ext.define(Fsl.app.getAbsView('V04J002001X03'),{
    extend      : 'Ext.window.Window',
    alias     	: Fsl.app.getAlias('V04J002001X03'),
    id          : 'V04J002001X03'.toLowerCase(), 
    title       : 'ENTRY ROLE :: V04J002001X03', 
    icon        : Fsl.route.getImage('APP01003.png'),	
    minimizable : false,
    maximizable : false,
    autoShow    : true,
    resizable   : false,
    border      : false,
    modal       : true,
    width       : 300,
    height      : 150,
    
    layout    : {
        type: 'vbox',
        align: 'stretch'
    },
    initComponent : function () {    
        var me = this; 
        
        
        me.items = [
        {
            xtype           : 'form',
            trackResetOnLoad:  true,
            bodyStyle       : {
                padding     : '10px',
                border      :  true
            },
            tbar: Ext.create('Ext.ux.StatusBar', {
                id                : 'win-statusbar-v04j002001x03',
                topBorder         :  true,
                text              : 'Status',
                defaultIconCls    : 'fsl-severity-info',
                defaultText       : 'Status'
            }),
            items : [{        
                xtype             : 'fieldcontainer',
                combineErrors     : true,
                height            : 40,
                defaults: {
                    layout            : 'fit',
                    width             :  265,
                    anchor            : '90%',
                    afterLabelTextTpl :  required,
                    labelWidth        : 110
                },
                items: [{
                    xtype             : 'textfield',
                    name              : 'accountId',
                    readOnly          :  true,
                    allowBlank        :  false,
                    hidden            :  true
                },{
                    xtype             : 'textfield',
                    fieldLabel        : 'User Name',
                    name              : 'username',
                    readOnly          :  true,
                    allowBlank        :  false
                },{
                    xtype               : 'textfield',
                    fieldLabel          : 'Role',
                    name                : 'roleId',
                    hidden              : true
                },{
                    xtype               : 'textfield',
                    fieldLabel          : 'Role',
                    name                : 'roleName',
                    readOnly            :  true
                }]
            }]
        }]
        this.buttons = [
        {
            text    : 'Save',
            icon    : Fsl.route.getImage('SAV01004.png'),
            action  : 'save'
        },{
            text    : 'Close',
            icon    : Fsl.route.getImage('CLS01001.png'),     
            scope   : this,
            handler : function(button){
                var win = button.up('window');
                win.close();
            }
        }]
        this.callParent(arguments);
    }
});