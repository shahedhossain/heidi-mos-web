Ext.define(Fsl.app.getAbsView('V04T001001X09'), {
    extend      	: 'Ext.form.Panel',
    alias     		: Fsl.app.getAlias('V04T001001X09'),
    id          	: 'V04T001001X09'.toLowerCase(), 
     bodyStyle     : {
        background    : 'none'
    },
    defaults: {        
        activeRecord  : null,
        border        : true, 
        layout        : 'hbox',        
        fieldDefaults : {
            anchor    : '100%',
            labelAlign: 'right'
        }
    },
    
    initComponent     : function(){        
        Ext.QuickTips.init();
        Ext.form.Field.prototype.msgTarget = 'qtip';         
        var me = this;       
        me.items = [{
                xtype           : 'fieldcontainer',
                combineErrors   : true,                    
                defaults        : {
                    layout      : 'fit',
                    margin      : '3 5 1 5',
                    hideLabel   : false,                
                    labelAlign  : 'top'
                },
                items: [{
                        xtype             : 'numberfield',
                        name              : 'id',
                        fieldLabel        : 'Branch Id :',
                        minValue          :  1,
                        width             :  100,                        
                        mouseWheelEnabled :  false,
                        hideTrigger       :  true,
                        decimalPrecision  :  0,
                        keyNavEnabled     :  false,
                        scope             :  this,                
                        listeners: {
                            dblclick    : {
                                element : 'el',
                                scope   : this,
                                fn: function(){
                                var form   = this.getForm(); 
                                    form.findField('id').setValue('');                            
                                }
                            }
                        }
                    },{
                        xtype             : 'textfield',
                        name              : 'name',
                        fieldLabel        : 'Branch Name:',
                        flex              : 1,
                        scope             : me,                
                        listeners         : {
                            dblclick    : {
                                element : 'el',
                                scope   : this,
                                fn: function(){
                                var form   = this.getForm(); 
                                    form.findField('name').setValue('');                            
                                }
                            }
                        }                  
                    },{
                        xtype             : 'textfield',
                        name              : 'location',
                        fieldLabel        : 'location:',
                        flex              : 1,
                        scope             : this,                
                        listeners         : {
                            dblclick    : {
                                element : 'el',
                                scope   : this,
                                fn: function(){
                                var form   = this.getForm(); 
                                    form.findField('location').setValue('');                            
                                }
                            }
                        }                  
                    }]
            }];       
        me.callParent(arguments);
    }
});

