Ext.define(Fsl.app.getAbsView('V04T001001X07'),{
    extend      : 'Ext.window.Window',
    alias     	: Fsl.app.getAlias('V04T001001X07'),
    id          : 'V04T001001X07'.toLowerCase(), 
    title       : 'UPDATE ACCOUNT :: V04T001001X07', 
    icon        : Fsl.route.getImage('APP01003.png'),	
    minimizable : false,
    maximizable : false,
    autoShow    : true,
    resizable   : false,
    border      : false,
    modal       : true,
    width       : 300,
    height      : 290,
    
    layout    : {
        type: 'vbox',
        align: 'stretch'
    },
    initComponent : function () {    
        var me = this; 
        
        
        me.items = [
        {
            xtype           : 'form',
            trackResetOnLoad:  true,
            bodyStyle       : {
                padding     : '10px',
                border      :  true
            },
            tbar: Ext.create('Ext.ux.StatusBar', {
                id                : 'win-statusbar-v04t001001x07',
                topBorder         :  true,
                text              : 'Status',
                defaultIconCls    : 'fsl-severity-info',
                defaultText       : 'Status'
            }),
            items : [{        
                xtype             : 'fieldcontainer',
                combineErrors     : true,               
                defaults: {
                    layout            : 'fit',
                    width             :  265,
                    anchor            : '90%',
                    labelWidth        : 110
                },
                items: [{
                    xtype             : 'textfield',
                    name              : 'id',
                    fieldLabel        : 'ID',
                    emptyText         : 'Not Required...',
                    readOnly          :  true,
                    allowBlank        :  true
                },{
                    xtype             : 'textfield',
                    name              : 'userId',
                    allowBlank        :  false,
                    hidden            :  true
                },{
                    xtype             : 'textfield',
                    name              : 'name',
                    allowBlank        :  false,
                    emptyText         : 'Select User',
                    fieldLabel        : 'User'
                },{
                    xtype             : 'textfield',
                    name              : 'username',
                    fieldLabel        : 'User Name',
                    emptyText         : 'Write Category name',
                    allowBlank        :  false                    
                },{
                    fieldLabel        : 'New Password(opt)',
                    xtype             : 'textfield',
                    name              : 'password',
                    id                : 'passNew',
                    inputType         : 'password',
                    emptyText         : '*******************',
                    allowBlank        :   true//,
                   // vtype             :  'password'
                },
                {
                    fieldLabel        : 'Confirm Password',
                    xtype             : 'textfield',
                    name              : 'pass-cfrm',
                    vtype             : 'confirmPass',
                    initialPassField  : 'passNew',
                    inputType         : 'password',
                    emptyText         : '*******************',
                    allowBlank        :  true
                },{
                    xtype             : 'checkboxgroup',
                    fieldLabel        : 'Account Status',
                    // Arrange checkboxes into two columns, distributed vertically
                    columns           : 2,
                    vertical: true,
                    items: [
                    {
                        boxLabel: 'Acc. Enabled', 
                        name: 'enabled',        
                        inputValue: '1' ,
                        id    : 'checkbox1'
                    },
                    {
                        boxLabel: 'Acc. Lock',   
                        name: 'accountLocked',  
                        inputValue: '1',
                       id        : 'checkbox2'
                    }
                    ]
                }]
            }]
        }]
        this.buttons = [
        {
            text    : 'Update',
            icon    : Fsl.route.getImage('SAV01004.png'),
            action  : 'update'
        },{
            text    : 'Close',
            icon    : Fsl.route.getImage('CLS01001.png'),     
            scope   : this,
            handler : function(button){
                var win = button.up('window');
                win.close();
            }
        }]
        this.callParent(arguments);
    }
});