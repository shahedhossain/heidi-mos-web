Ext.define(Fsl.app.getAbsView('V04T001001X10'), {   
    extend          : 'Ext.grid.Panel',
    alias           : Fsl.app.getAlias('V04T001001X10'),
    id              : 'V04T001001X10'.toLowerCase(), 
    border    : true,
    modal     : true,
    height    : 285,
    width     : 650,
    viewConfig: {
        stripeRows    : true,
        forceFit      : true,
        emptyText     : 'No Records to display',
        listeners     : {
            viewready : function(v) {               
                var store = Fsl.store.getStore('S01I015007');
                store.clearFilter();
                store.loadPage(1);
            }
        }
    },
    initComponent: function() {
        var me      = this;
        me.store    = Fsl.app.getRelStore('S01I015007');    
        me.tbar = Ext.create('Ext.ux.StatusBar', {            
            topBorder           : false,
            statusAlign         : 'right',
            items     : [{
                xtype       :'button',
                text        : 'ADD',
                icon        : Fsl.route.getIcon('TB01001/ADD01004.png'),
                action      : 'add'
            },'-',{
                xtype       :'button',
                text        : 'Clear',
                icon        : Fsl.route.getIcon('IC01001/CAN01003.png'),
                action      : 'clear'
            },'-',{
                xtype       : 'button',
                text        : 'search',
                icon        : Fsl.route.getIcon('IC01001/search.png'),
                action      : 'search'
            },'-',{  
                xtype       :'statusbar',
                id          : 'win-statusbar-v04t001001x10',
                border      : false,
                defaultText : '',
                statusAlign : 'right'
            }]
        });
        
        this. bbar= new Ext.PagingToolbar({
            pageSize        : 10,
            store           : Fsl.app.getRelStore('S01I015007'),
            displayInfo     : true
        });
         
        this.columns = [{
            text            : 'SN',
            xtype           : 'rownumberer',
            width           :  40, 
            sortable        :  false
        },{
            text            : 'SN',
            dataIndex       : 'id',
            width           :  40, 
            sortable        :  false
        },{
            text            : 'Branch name',
            dataIndex       : 'name'
        },{
            text            : 'Mail',
            dataIndex       : 'mail',
            flex            : 1
        },{
            text            : 'Contact',
            dataIndex       : 'contact'
        },{
            text            : 'Location',
            dataIndex       : 'location'
        },{
            text            : 'Address',
            dataIndex       : 'address'
        }];       
        this.callParent(arguments);
    },    
    getTitle :function(value, metadata, record, rowIdx, colIdx, store, vie) {
        return record.getM01i007001().data.name;
    }
});