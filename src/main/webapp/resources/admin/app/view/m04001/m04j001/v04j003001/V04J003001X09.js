﻿Ext.define(Fsl.app.getAbsView('V04J003001X09'), {
    extend    : 'Ext.grid.Panel',
    alias     : Fsl.app.getAlias('V04J003001X09'),
    id        : 'V04J003001X09'.toString(),
    border    : true,
    modal     : true,
    height    : 285,
    width     : 300,
    viewConfig: {
        stripeRows    : true,
        forceFit      : true,
        emptyText     : 'No Records to display',
        listeners     : {
            viewready : function(gridpanel, eOpts){                  
                var store = Fsl.store.getStore('S04T002002');
                store.clearFilter();
                store.loadPage(1);
                //this.getSelectionModel().select(0);
            }
        }
    },
    initComponent: function() {
        var me     	= this;
        me.store   	= Fsl.app.getRelStore('S04T002002');        
        me.tbar         = Ext.create('Ext.ux.StatusBar', {            
            topBorder       : false,
            statusAlign     : 'right',
            items    : [{
                xtype       :'button',
                text        : 'Clear',
                icon        : Fsl.route.getIcon('IC01001/CAN01003.png'),
                action      : 'clear'
            },'-',{
                xtype       : 'button',
                text        : 'search',
                icon        : Fsl.route.getIcon('IC01001/search.png'),
                action      : 'search'
            },'-',{  
                xtype       :'statusbar',
                id          : 'win-statusbar-v04j003001x09',
                border      : false,
                defaultText : '',
                statusAlign : 'right'
            }]
        });
		
        me.bbar = new Ext.PagingToolbar({
            pageSize    : 10,
            id          : 'paging-v04j003001x09',
            store       : Fsl.app.getRelStore('S04T002002'),
            displayInfo : true          
        });
        me.columns = [{
            text      		: 'SN',
            xtype     		: 'rownumberer',
            width     		:  40, 
            sortable  		:  false
        },{
            text        	: 'ID',
            dataIndex   	: 'id',
            width       	:  40, 
            sortable    	:  false
        },{
            text        	: 'ROLE',
            dataIndex   	: 'authority',
            flex        	:  1
        },{
            xtype           : 'actioncolumn',
            hidden          :  true,
            menuDisabled    :  true,
            sortable        :  false,			        
            width           :  22,            
            items           : [{
                icon        : Fsl.route.getImage('DEL01005.png'),
                tooltip     : 'Delete This?',
                scope       : this,
                handler     : function(grid, rowIdx, colIdx) {
                    var record 	= grid.getStore().getAt(rowIdx),
                    catId  		= record.data.id;
                    Ext.getCmp('WestPanel').confirmDelete(catId, 'M04T002001', 'S04T002002', 'V04J003001X09');           
                }
            }
            ]
        }
        ]; 
        me.callParent(arguments);
    }
});
