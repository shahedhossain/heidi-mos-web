Ext.define(Fsl.app.getAbsView('V04J002001X01'), {
    extend      	: 'Ext.form.Panel',
    alias     		: Fsl.app.getAlias('V04J002001X01'),
    id          	: 'V04J002001X01'.toLowerCase(), 
    bodyStyle : {
        background: 'none'                
    },
    initComponent : function(){
        var me      = this;     
            me.items   = [{
                xtype             : 'fieldcontainer',
                layout            : 'hbox',
                margin            : '5 5 5 5',                        
                defaults          :  {
                    width           :  200,
                    labelAlign      : 'right',
                    labelWidth      :  100
                }, 
                items : [{
                    xtype             : 'textfield',
                    name              : 'username',
                    fieldLabel        : 'User Name'                                           
                },{
                    xtype             : 'textfield',
                    name              : 'roleName',
                    fieldLabel        : 'Role'
                }]
        }];
        this.callParent(arguments);
    }
});

