Ext.define(Fsl.app.getAbsView('V04J002001'), {
    extend      : 'Ext.window.Window',
    alias       : Fsl.app.getAlias('V04J002001'),
    icon        : Fsl.route.getImage('APP01009.png'),
    id          : 'V04J002001'.toLowerCase(),
    title       : 'USER ROLE LVM :: V04J002001',
    minimizable :  false,
    maximizable :  false,
    autoShow    :  true,
    resizable   :  false,
    modal       :  true,
    width       :  500,
    height      :  350, 
    layout      :  {
        type	: 'vbox',
        align	: 'stretch'
    },
    items  : [{ 
        xtype     	: Fsl.app.getXtype('V04J002001X01'), 
        resizable 	: false,
        border    	: false,
        componentCls    : 'my-border-bottom'
    },{ 
        xtype     	: Fsl.app.getXtype('V04J002001X02'),       
        resizable 	: false,
        border    	: false
    }]
});