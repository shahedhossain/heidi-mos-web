Ext.define(Fsl.app.getAbsView('V04J003001X05'), {
    extend      	: 'Ext.form.Panel',
    alias     		: Fsl.app.getAlias('V04J003001X05'),
    id          	: 'V04J003001X05'.toLowerCase(), 
    bodyStyle : {
        background: 'none'                
    },
    initComponent : function(){
        var me      = this;     
        me.items   = [{
            xtype             : 'fieldcontainer',
            layout            : 'hbox',
            margin            : '5 5 5 5',                        
            defaults          :  {
                width           :  150,
                labelAlign      : 'right',
                labelWidth      :  55
            }, 
            items : [{
                xtype             : 'numberfield',
                name              : 'id',
                fieldLabel        : 'ID',
                minValue          :  1,                       
                mouseWheelEnabled :  false,
                hideTrigger       :  true,
                decimalPrecision  :  0,
                keyNavEnabled     :  false                        
            },{
                xtype             : 'textfield',
                name              : 'role',
                fieldLabel        : 'ROLE:'
            }]
        }];
        this.callParent(arguments);
    }
});

