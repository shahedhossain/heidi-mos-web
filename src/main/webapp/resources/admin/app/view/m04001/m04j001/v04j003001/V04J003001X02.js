Ext.define(Fsl.app.getAbsView('V04J003001X02'), {
    extend    : 'Ext.grid.Panel',
    alias     : Fsl.app.getAlias('V04J003001X02'),
    id        : 'V04J003001X02'.toString(),
    border    : true,
    modal     : true,
    height    : 285,
    width     : 300,
    viewConfig: {
        stripeRows    : true,
        forceFit      : true,
        emptyText     : 'No Records to display',
        listeners     : {
            viewready : function(gridpanel, eOpts){                  
                var store = Fsl.store.getStore('S04J003001');
                store.clearFilter();
                store.loadPage(1);
            }
        }
    },
    initComponent: function() {
        var me     	= this;
        me.store   	= Fsl.app.getRelStore('S04J003001');        
        me.tbar         = Ext.create('Ext.ux.StatusBar', {            
            topBorder       : false,
            statusAlign     : 'right',
            items    : [{
                xtype       :'button',
                text        : 'ADD',
                icon        : Fsl.route.getIcon('TB01001/ADD01004.png'),
                action      : 'add'
            },'-',{
                xtype       :'button',
                text        : 'Clear',
                icon        : Fsl.route.getIcon('IC01001/CAN01003.png'),
                action      : 'clear'
            },'-',{
                xtype       : 'button',
                text        : 'search',
                icon        : Fsl.route.getIcon('IC01001/search.png'),
                action      : 'search'
            },'-',{  
                xtype       :'statusbar',
                id          : 'win-statusbar-v04j003001x02',
                border      : false,
                defaultText : '',
                statusAlign : 'right'
            }]
        });
		
        me.bbar = new Ext.PagingToolbar({
            pageSize    : 10,
            id          : 'paging-v04j003001x02',
            store       : Fsl.app.getRelStore('S04J003001'),
            displayInfo : true          
        });
        me.columns = [{
            text      		: 'SN',
            xtype     		: 'rownumberer',
            width     		:  60, 
            sortable  		:  false
        },{
            text        	: 'ROLE',
            dataIndex   	: 'roleId',
            renderer    	:  this.getRole,
            hidden              :  true
        },{
            text        	: 'ROLE',
            dataIndex   	: 'roleName'
        },{
            text        	: 'AUTHORITY',
            dataIndex   	: 'authorityId',
            flex           	:  1,
            renderer    	:  this.getAuthority
        },{
            xtype           : 'actioncolumn',    
            menuDisabled    :  true,
            sortable        :  false,			        
            width           :  22,            
            items           : [{
                icon        : Fsl.route.getImage('DEL01005.png'),
                tooltip     : 'Delete This?',
                scope       : this,
                handler     : function(grid, rowIdx, colIdx) {
                    var record 	= grid.getStore().getAt(rowIdx),
                    authorityId = record.data.authorityId,
                    roleId      = record.data.roleId;
                    this.confirmDelete(authorityId,roleId, 'M04J003001', 'S04J003001', 'v04j003001x02');           
                }
            }
            ]
        }
        ]; 
        me.callParent(arguments);
    },
    
    getUserId :function(value, metadata, record, rowIdx, colIdx, store, vie) {
        return record.data.userId;
    },
    getRole :function(value, metadata, record, rowIdx, colIdx, store, vie) {
       // console.log(record.getM04t003001())
        return record.getM04t003001().data.role;
    },
    getAuthority :function(value, metadata, record, rowIdx, colIdx, store, vie) {
        return record.getM04t002001().data.authority;
    },   
      confirmDelete : function(itemId, roleId, modelName, storeName, statusId){
        Ext.MessageBox.show({
            title         : 'Delete',
            msg           : 'Really want to delete ?',
            icon          :  Ext.Msg.WARNING,
            buttons       :  Ext.MessageBox.YESNO,
            buttonText    :{ 
                yes: "Delete", 
                no : "No" 
            },
            scope         : this,
            fn: function(btn){
                if(btn == 'yes'){
                    this.onDeleteClick(itemId,roleId, modelName, storeName, statusId)
                }                         
            }         
        });   
    },  
    onDeleteClick : function(id,roleId, modelName, storeName, statusId) {
        var grossVat  = Ext.create(Fsl.app.getAbsModel(modelName), {
            id          : id,
            authorityId : id,
            roleId      : roleId
        });   
        var store     =Fsl.store.getStore(storeName);                
        grossVat.destroy({
            scope       :this,
            success: function(model, operation) {                 
                if(model != null){                    
                    var text    = operation.response.responseText,
                    json        = Ext.decode(text);
                    store.reload();
                    this.statusbar(statusId, json.message, 'fsl-severity-info', 'red'); 
                }
            },
            failure: function(model, operation){
                this.statusbar(statusId, operation.error, 'fsl-severity-warn', 'red');
            }
        });       
    },
    statusbar :function(statusId, msg, icon, color){
        var status = Ext.getCmp('win-statusbar-'+statusId);
        status.setStatus({
            text    : '<p style="color: '+color+'">'+msg+'</p>',
            iconCls : icon,
            clear   : {
                wait: 6000,
                anim: false,
                useDefaults: true
            } 
        });    
    }
     
});