Ext.define(Fsl.app.getAbsView('V04T003001'), {
    extend      : 'Ext.window.Window',
    alias       : Fsl.app.getAlias('V04T003001'),
    icon        : Fsl.route.getImage('APP01009.png'),
    id          : 'V04T003001'.toLowerCase(),
    title       : 'ROLE LVM :: V04T003001',
    minimizable :  false,
    maximizable :  false,
    autoShow    :  true,
    resizable   :  false,
    modal       :  true,
    width       :  350,
    height      :  350, 
    layout      :  {
        type	: 'vbox',
        align	: 'stretch'
    },
    items  : [{ 
        xtype     	: Fsl.app.getXtype('V04T003001X01'), 
        resizable 	: false,
        border    	: false,
        componentCls    : 'my-border-bottom'
    },{ 
        xtype     	: Fsl.app.getXtype('V04T003001X02'),       
        resizable 	: false,
        border    	: false
    }]
});