Ext.define(Fsl.app.getAbsView('V04T001001X08'), {
    extend      : 'Ext.window.Window',
    alias       : Fsl.app.getAlias('V04T001001X08'),
    icon        : Fsl.route.getImage('APP01009.png'),
    id          : 'V04T001001X08'.toLowerCase(),
    title       : 'BRANCH LVM :: V01I015001',
    minimizable :  false,
    maximizable :  false,
    autoShow    :  true,
    resizable   :  false,
    modal       :  true, 
    layout      :  {
        type	: 'vbox',
        align	: 'stretch'
    },
    items  : [{ 
        xtype     	: Fsl.app.getXtype('V04T001001X09'), 
        resizable 	: false,
        border    	: false,
        componentCls    : 'my-border-bottom'
    },{ 
        xtype     	: Fsl.app.getXtype('V04T001001X10'),       
        resizable 	: false,
        border    	: false
    }]
});