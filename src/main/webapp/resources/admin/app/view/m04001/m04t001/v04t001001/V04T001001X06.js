Ext.define(Fsl.app.getAbsView('V04T001001X06'), {
    extend    : 'Ext.grid.Panel',
    alias     : Fsl.app.getAlias('V04T001001X06'),
    id        : 'V04T001001X06'.toLowerCase(),
    border    : true,
    modal     : true,
    height    : 285,
    width     : 570,
    viewConfig: {
        stripeRows    : true,
        forceFit      : true,
        emptyText     : 'No Records to display',
        listeners     : {
            viewready : function(v) {               
                var store = Fsl.store.getStore('S01T009003');
                store.clearFilter();
                store.loadPage(1);
            }
        }
    },
    columnLines: true,
    //frame: true,
    initComponent: function() {
    var me     	= this;
    me.store   	= Fsl.app.getRelStore('S01T009003');    
    me.tbar = Ext.create('Ext.ux.StatusBar', {            
            topBorder           : false,
            statusAlign         : 'right',
            items     : [{
                xtype       : 'button',
                text        : 'Clear',
                icon        : Fsl.route.getIcon('IC01001/CAN01003.png'),
                action      : 'clear'
            },'-',{
                xtype       : 'button',
                text        : 'search',
                icon        : Fsl.route.getIcon('IC01001/search.png'),
                action      : 'search'
            },'-',{  
                xtype       : 'statusbar',
                id          : 'win-statusbar-v04t001001x06',
                border      : false,
                defaultText : '',
                statusAlign : 'right'
            }]
        });
        
        this.bbar = new Ext.PagingToolbar({
            pageSize        :  10,
            store           : Fsl.app.getRelStore('S01T009003'),
            displayInfo     :  true
        });  

        this.columns = [{
            text            : 'ID',
            dataIndex       : 'id',
            width           : 40, 
            sortable        : false            
        },{
            text            : 'User name',
            dataIndex       : 'name',
            renderer        :  this.getTitle
        },{
            text            : 'Branch Name',
            dataIndex       : 'branchId',
            renderer        :  this.getBranchName,
            flex            :  1
        },{
            text            : 'Contact',
            dataIndex       : 'contact'
        },{
            text            : 'Location',
            dataIndex       : 'location'
        },{
            text            : 'Address',
            dataIndex       : 'address'
        }];       
        this.callParent(arguments);
    },
    getTitle:function(value, metadata, record, rowIdx, colIdx, store, vie) {
              var title = record.getM01i007001().data.name;
              return title+''+value;
    },
    getBranchName :function(value, metadata, record, rowIdx, colIdx, store, vie) {
              return record.getM01i015001().data.name;
    }
});