Ext.define(Fsl.app.getAbsView('V04T001001X02'), {
    extend    : 'Ext.grid.Panel',
    alias     : Fsl.app.getAlias('V04T001001X02'),
    id        : 'V04T001001X02'.toLowerCase(),
    border    : true,
    modal     : true,
    height    : 285,
    width     : 380,
    viewConfig: {
        stripeRows    : true,
        forceFit      : true,
        emptyText     : 'No Records to display',
        listeners     : {
            viewready : function(gridpanel, eOpts){                  
                var store = Fsl.store.getStore('S04T001001');
                store.clearFilter();
                store.loadPage(1);
            //this.getSelectionModel().select(0);
            }
        }
    },
    initComponent: function() {
        var me     	= this;
        me.store   	= Fsl.app.getRelStore('S04T001001');        
        me.tbar         = Ext.create('Ext.ux.StatusBar', {            
            topBorder       : false,
            statusAlign     : 'right',
            items    : [{
                xtype       :'button',
                text        : 'ADD',
                icon        : Fsl.route.getIcon('TB01001/ADD01004.png'),
                action      : 'add'
            },'-',{
                xtype       :'button',
                text        : 'Clear',
                icon        : Fsl.route.getIcon('IC01001/CAN01003.png'),
                action      : 'clear'
            },'-',{
                xtype       : 'button',
                text        : 'search',
                icon        : Fsl.route.getIcon('IC01001/search.png'),
                action      : 'search'
            },'-',{  
                xtype       :'statusbar',
                id          : 'win-statusbar-v04t001001x02',
                border      : false,
                defaultText : '',
                statusAlign : 'right'
            }]
        });
		
        me.bbar = new Ext.PagingToolbar({
            pageSize    : 10,
            id          : 'paging-v04t001001x02',
            store       : Fsl.app.getRelStore('S04T001001'),
            displayInfo : true          
        });
        me.columns = [{
            text      		: 'SN',
            xtype     		: 'rownumberer',
            width     		:  40, 
            sortable  		:  false
        },{
            text        	: 'Account Id',
            dataIndex   	: 'id',
            width       	:  60, 
            sortable    	:  false
        },{
            text        	: 'USER ID',
            dataIndex   	: 'userId',
            width       	:  60, 
            sortable    	:  false
        },{
            text        	: 'Loging Name',
            dataIndex   	: 'username'
        },{
            text        	: 'Acc. Status',
            dataIndex   	: 'enabled',
            align               : 'center',
            tdCls               : 'fsl-cursor-pointer',
            flex        	:  1,
            renderer            : this.getIcon
        },{
            text        	: 'Acc. Expire',
            dataIndex   	: 'accountExpired',
            align               : 'center',
            tdCls               : 'fsl-cursor-pointer',
            flex        	:  1,
            renderer            : this.getIcon
        },{
            text        	: 'Lock',
            dataIndex   	: 'accountLocked',
            align               : 'center',
            tdCls               : 'fsl-cursor-pointer',
            flex        	:  1,
            renderer            : this.getIcon
        },{
            text        	: 'Pass Expire',
            dataIndex   	: 'passwordExpired',
            align               : 'center',
            tdCls               : 'fsl-cursor-pointer',
            flex        	:  1,
            renderer            : this.getIcon
        },{
            text        	: 'Branch',
            dataIndex   	: 'branch',
            tdCls               : 'fsl-cursor-pointer',
            flex        	:  1,
            renderer            : this.getBranch
        },{
            xtype           : 'actioncolumn',    
            menuDisabled    :  true,
            sortable        :  false,			        
            width           :  22,            
            items           : [{
                icon        : Fsl.route.getImage('DEL01005.png'),
                tooltip     : 'Delete This?',
                scope       : this,
                handler     : function(grid, rowIdx, colIdx) {
                    var record 	= grid.getStore().getAt(rowIdx),
                    catId  		= record.data.id;
                    Ext.getCmp('WestPanel').confirmDelete(catId, 'M04T001001', 'S04T001001', 'v04t001001x02');           
                }
            }
            ]
        }
        ]; 
        me.callParent(arguments);
    },
    
    getIcon : function (value, meta, record, rowIndex, colIndex, store) {
        var src;
        if(value== true){
            src = Fsl.route.getIcon("lock_icon.gif");                    
        }else if(value==false){
            src = Fsl.route.getIcon("unlock_icon.gif");
        }
        return '<img src ="'+ src +'" />';
    },
    getBranch : function(value, meta, record, rowIndex, colIndex, store){
        return record.getM01t009001().getM01i015001().data.name
    }
});