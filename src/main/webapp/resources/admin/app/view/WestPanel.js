Ext.define('Fsl.admin.view.WestPanel' , {
    extend       : 'Ext.tree.Panel',
    alias        : 'widget.westPanel',
    store        : Fsl.app.getRelStore('S01I000000'),
    title        : 'Navigation',
    id           : 'WestPanel',
    singleExpand : true,
    rootVisible  : false,
    useArrows    : true,
    header	 : false,
    lines        : false,   
    initComponent: function() {    
        var me   = this
        statusid = 'west-panel-status';
        me.tbar  = [{
            xtype        : 'textfield',
            emptyText    : 'Enter your query here',
            fieldBodyCls : 'fsl-tree-filter',
            name         : 'search',
            flex         : 1
        }];
        me.bbar  = Fsl.bbar.getBB01I001(statusid);
        required = new Ext.XTemplate('<tpl if="allowBlank === false"><span style="color:red;">*</span></tpl>',{
            disableFormats: true
        })
        required1 = '<span style="color:blue;font-weight:bold" data-qtip="Required">*</span>';
        Ext.apply(Ext.form.VTypes, {
            daterange : function(val, field) {
                var date = field.parseDate(val);

                if(!date){
                    return;
                }
                if (field.startDateField && (!this.dateRangeMax || (date.getTime() != this.dateRangeMax.getTime()))) {
                    var start = Ext.getCmp(field.startDateField);
                    start.setMaxValue(date);
                    start.validate();
                    this.dateRangeMax = date;
                } 
                else if (field.endDateField && (!this.dateRangeMin || (date.getTime() != this.dateRangeMin.getTime()))) {
                    var end = Ext.getCmp(field.endDateField);
                    end.setMinValue(date);
                    end.validate();
                    this.dateRangeMin = date;
                }
        
                return true;
            },
            daterangeText: 'Start date must be less than end date',
            
            password: function(val, field) {
                // at least one number, one lowercase and one uppercase letter 
                // at least six characters
                var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/;
                if( re.test(val)){
                    return true;
                }
                return false;
            },

            passwordText: 'At least one number, one lowercase and one uppercase letter & at least six characters',
        
            confirmPass :function(val, field){
                if (field.initialPassField) {
                    var pwd = field.up('form').down('#' + field.initialPassField);
                    return (val == pwd.getValue());
                }
                return true;
            },
            confirmPassText: 'Confirm Password Miss mess'
        
    
        });          
        me.callParent(arguments);
    },
    statusbar :function(statusId, msg, icon, color){
        var status = Ext.getCmp('win-statusbar-'+statusId);
        status.setStatus({
            text    : '<p style="color: '+color+'">'+msg+'</p>',
            iconCls : icon,
            clear   : {
                wait: 6000,
                anim: false,
                useDefaults: true
            } 
        });    
    },
    closeWindow :function(window){
        setTimeout(function(){            
            window.close();
        }, 6000);
    },    
    confirmDelete : function(itemId, modelName, storeName, statusId){
        Ext.MessageBox.show({
            title         : 'Delete',
            msg           : 'Really want to delete ?',
            icon          :  Ext.Msg.WARNING,
            buttons       :  Ext.MessageBox.YESNO,
            buttonText    :{ 
                yes: "Delete", 
                no : "No" 
            },
            scope         : this,
            fn: function(btn){
                if(btn == 'yes'){
                    this.onDeleteClick(itemId, modelName, storeName, statusId)
                }                         
            }         
        });   
    },  
    onDeleteClick : function(id, modelName, storeName, statusId) {
        var grossVat  = Ext.create(Fsl.app.getAbsModel(modelName), {
            id : id
        });   
        var store     =Fsl.store.getStore(storeName);                
        grossVat.destroy({
            scope       :this,
            success: function(model, operation) {                 
                if(model != null){                    
                    var text    = operation.response.responseText,
                    json        = Ext.decode(text);
                    store.reload();
                    this.statusbar(statusId, json.message, 'fsl-severity-info', 'red'); 
                }
            },
            failure: function(model, operation){
                this.statusbar(statusId, operation.error, 'fsl-severity-warn', 'red');
            }
        });       
    },
    storeFilter : function(storeName, property, value){
        var store     =Fsl.store.getStore(storeName);  
        if(!value){
            store.clearFilter();
            store.load();
        }else{                                          
            store.clearFilter();
            store.filter(property, value);
            store.loadPage(1);
        }
    },
    storeFilterByMultipleProperty : function(storeName, array){
        var store     =Fsl.store.getStore(storeName);  
        store.clearFilter();
        store.loadPage(1, {
            filters  : array                                
        });
    },    
    generateBarcode : function(articleId){
        Ext.MessageBox.show({
            title         : 'Generate Barcode',
            msg           : 'Are you confirm?',
            icon          :  Ext.Msg.WARNING,
            buttons       :  Ext.MessageBox.YESNO,
            buttonText    :{ 
                yes: "Yes", 
                no : "No" 
            },
            scope         : this,
            fn: function(btn){
                if(btn == 'yes'){
                    this.barCodeExecution(articleId)
                }                         
            }         
        });   
    },
    barCodeExecution : function(articleId){
        if(articleId != null){
            var body = Ext.getBody();
            var frame = body.createChild({
                tag:'iframe',
                cls:'x-hidden',
                id:'iframe',
                name:'iframe'
            });
            var form = body.createChild({
                tag:'form',
                cls:'x-hidden',
                id:'form',
                method:'post',
                action: Fsl.route.getAction('c01i006001/createBarCode/'),
                target:'iframe'
            });
        
            form.createChild({
                tag:'input',
                name:'articleId',
                type:'hidden',
                value:articleId
            });
            form.dom.submit();      
        }else{
            Ext.Msg.alert("Sorry","Unable to Create barcode  due to error!!!")
        }
    }
});