Ext.define(Fsl.app.getAbsStore('S04T003003'), {
    extend      : 'Ext.data.Store',
    model       :  Fsl.app.getAbsModel('M04T003001'),
    idProperty  : 'id',
    autoLoad    :  false,
    autoSync    :  true,    
    remoteSort  :  false,
    pageSize    :  10,
    proxy       :  Fsl.proxy.getStoreAjaxUrlProxy('C04T003001'),
    sorters: [{
        property    : 'id', 
        direction   : 'asc'
    }]
});