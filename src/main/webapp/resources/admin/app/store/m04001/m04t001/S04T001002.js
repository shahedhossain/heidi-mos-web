Ext.define(Fsl.app.getAbsStore('S04T001002'), {
    extend      : 'Ext.data.Store',
    model       :  Fsl.app.getAbsModel('M04T001001'),
    idProperty  : 'id',
    autoLoad    :  false,
    autoSync    :  true,    
    remoteSort  :  false,
    pageSize    :  10,
    proxy       :  Fsl.proxy.getStoreAjaxUrlProxy('S04T001001'),
    sorters: [{
        property    : 'id', 
        direction   : 'asc'
    }]
});