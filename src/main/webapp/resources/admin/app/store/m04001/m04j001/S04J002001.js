Ext.define(Fsl.app.getAbsStore('S04J002001'), {
    extend      : 'Ext.data.Store',
    model       :  Fsl.app.getAbsModel('M04J002001'),
    idProperty  : 'id',
    autoLoad    :  false,
    autoSync    :  true,    
    remoteSort  :  false,
    pageSize    :  10,
    proxy       :  Fsl.proxy.getStoreAjaxUrlProxy('C04J002001'),
    sorters: [{
        property    : 'id', 
        direction   : 'asc'
    }]
});