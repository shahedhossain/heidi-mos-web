Ext.define(Fsl.app.getAbsStore('S04J003001'), {
    extend      : 'Ext.data.Store',
    model       :  Fsl.app.getAbsModel('M04J003001'),
    idProperty  : 'id',
    autoLoad    :  false,
    autoSync    :  true,    
    remoteSort  :  false,
    pageSize    :  10,
    proxy       :  Fsl.proxy.getStoreAjaxUrlProxy('C04J003001'),
    sorters: [{
        property    : 'id', 
        direction   : 'asc'
    }]
});