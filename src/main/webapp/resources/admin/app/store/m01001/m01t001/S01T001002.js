Ext.define(Fsl.app.getAbsStore('S01T001002'), {
    extend      : 'Ext.data.Store',
    model       :  Fsl.app.getAbsModel('M01T001001'),
    idProperty  : 'id',
    autoLoad    :  false,
    autoSync    :  true,    
    remoteSort  :  false,
    pageSize    :  10,
    proxy       :  Fsl.proxy.getStoreAjaxUrlProxy('S01T001001'),
    sorters: [{
        property    : 'id', 
        direction   : 'asc'
    }]
});