Ext.define(Fsl.app.getAbsStore('S01I006003'), {
    extend      : 'Ext.data.Store',
    model       :  Fsl.app.getAbsModel('M01I006001'),
    idProperty  : 'id',
    autoLoad    :  false,
    autoSync    :  true,    
    remoteSort  :  false,
    pageSize    :  20,
    proxy       :  Fsl.proxy.getStoreAjaxUrlProxy('C01I006001'),
    sorters: [{
        property: 'id', 
        direction: 'asc'
    }]
});
