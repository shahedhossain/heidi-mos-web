Ext.define(Fsl.app.getAbsStore('S01T005002'), {   
    extend      : Fsl.app.getAbsStore('S01T005001'),
    model       : Fsl.app.getAbsModel('M01T005001'),
    proxy       : {
        type    : 'memory',
        reader  :{
            type  : 'json',
            root  : undefined
        }
    },
    sorters: [{
        property: 'id', 
        direction: 'asc'
    }]
});