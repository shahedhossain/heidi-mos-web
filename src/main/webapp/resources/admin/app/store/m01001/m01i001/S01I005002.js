Ext.define(Fsl.app.getAbsStore('S01I005002'), {   
    extend      : Fsl.app.getAbsStore('S01I005001'),
    model       : Fsl.app.getAbsModel('M01I005001'),
    proxy       : {
        type    : 'memory',
        reader  :{
            type  : 'json',
            root  : undefined
        }
    },
    sorters: [{
        property: 'id', 
        direction: 'asc'
    }]
});