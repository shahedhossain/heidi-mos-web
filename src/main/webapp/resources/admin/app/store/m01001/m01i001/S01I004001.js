Ext.define(Fsl.app.getAbsStore('S01I004001'), {
    extend      : 'Ext.data.Store',
    model       :  Fsl.app.getAbsModel('M01I004001'),
    idProperty  : 'id',
    autoLoad    :  false,
    autoSync    :  true,    
    remoteSort  :  false,
    pageSize    :  10,
    proxy       :  Fsl.proxy.getStoreAjaxUrlProxy('S01I004001'),
    sorters: [{
        property: 'id', 
        direction: 'asc'
    }]
});
