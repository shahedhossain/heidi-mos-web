Ext.define(Fsl.app.getAbsStore('S01I010001'), {
    extend      : 'Ext.data.Store',
    model       :  Fsl.app.getAbsModel('M01I010001'),
    idProperty  : 'id',
    autoLoad    :  false,
    autoSync    :  true,    
    remoteSort  :  false,
    pageSize    :  10,
    proxy       :  Fsl.proxy.getStoreAjaxUrlProxy('S01I010001'),
    sorters: [{
        property: 'id', 
        direction: 'asc'
    }]
});
