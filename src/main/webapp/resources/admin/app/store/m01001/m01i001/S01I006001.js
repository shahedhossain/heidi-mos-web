Ext.define(Fsl.app.getAbsStore('S01I006001'), {
    extend      : 'Ext.data.Store',
    model       :  Fsl.app.getAbsModel('M01I006001'),
    idProperty  : 'id',
    autoLoad    :  false,
    autoSync    :  true,    
    remoteSort  :  false,
    pageSize    :  20,
    proxy       :  Fsl.proxy.getStoreAjaxUrlProxy('S01I006001'),
    sorters: [{
        property: 'id', 
        direction: 'asc'
    }]
});
