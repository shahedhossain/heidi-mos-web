Ext.define(Fsl.app.getAbsStore('S01I000000'), {
    extend      : 'Ext.data.TreeStore',
    root        : {
        expanded : true, 
        children : [{
            text     : "MCSERP",
            iconCls  : "fsl-grid-tree-node-expanded",//"fsl-tree-icon-parent",
            expanded : true,
            children :[
            { 
                id       : 'CM01I001', 
                text     : "Initialisation",
                iconCls  : "fsl-tree-icon-node",
                expanded : false,
                children : [
                            { id: 'LV01I001001', text: "Category",                leaf: true, iconCls  : "fsl-tree-icon-leaf"},
                            { id: 'LV01I002001', text: "Manufacture",    	  leaf: true, iconCls  : "fsl-tree-icon-leaf"},
                            { id: 'LV01I001002', text: "Category By Manufacture", leaf: true, iconCls  : "fsl-tree-icon-leaf"},
                            { id: 'LV01I002002', text: "Manufacture BY Category", leaf: true, iconCls  : "fsl-tree-icon-leaf"},
                            { id: 'LV01I003001', text: "Generic Product",         leaf: true, iconCls  : "fsl-tree-icon-leaf"},
                            { id: 'LV01I004001', text: "Brand Type",        	  leaf: true, iconCls  : "fsl-tree-icon-leaf"},
                            { id: 'LV01I005001', text: "Brand", 		  leaf: true, iconCls  : "fsl-tree-icon-leaf"},
                            { id: 'LV01I006001', text: "Article",                 leaf: true, iconCls  : "fsl-tree-icon-leaf"},
                            { id: 'LV01I012001', text: "Price",         	  leaf: true, iconCls  : "fsl-tree-icon-leaf"},
                            { id: 'LV01I013001', text: "Vat",                     leaf: true, iconCls  : "fsl-tree-icon-leaf"},					
                            { id: 'LV01I007001', text: "Salutation",        	  leaf: true, iconCls  : "fsl-tree-icon-leaf"},
                            { id: 'LV01I014001', text: "Vendor",         	  leaf: true, iconCls  : "fsl-tree-icon-leaf"},
                            { id: 'LV01I015001', text: "Branch",           	  leaf: true, iconCls  : "fsl-tree-icon-leaf"},
                            { id: 'LV01I008001', text: "Gross Vat",           	  leaf: true, iconCls  : "fsl-tree-icon-leaf"},
                            { id: 'LV01I009001', text: "Rebate",         	  leaf: true, iconCls  : "fsl-tree-icon-leaf"},
                            { id: 'LV01I010001', text: "Promotion",         	  leaf: true, iconCls  : "fsl-tree-icon-leaf"},
                            { id: 'LV01I016001', text: "Status",         	  leaf: true, iconCls  : "fsl-tree-icon-leaf"},
                            { id: 'LV01I011001', text: "Pay Method",         	  leaf: true, iconCls  : "fsl-tree-icon-leaf"},					
                            { id: 'LV01I017001', text: "Article Limit",           leaf: true, iconCls  : "fsl-tree-icon-leaf"}
  
            ]
            },{ 
                id       : 'CM01T001', 
                text     : "Transation",
                iconCls  : "fsl-tree-icon-node",
                expanded : false,
                children : [
                            { id: 'LV01T001001', text: "CUSTOMER",        leaf: true },
                            { id: 'LV01T010001', text: "DISCOUNT",    	  leaf: true },                     
                            { id: 'LV01T009001', text: "USER",            leaf: true },
                            { id: 'LV01T004001', text: "STOCK",        	  leaf: true },
                            { id: 'LV01T002001', text: "EXPIRE", 	  leaf: true },
                            { id: 'LV01T003001', text: "WASTAGE",         leaf: true },
                            { id: 'LV01T014001', text: "ORDER",           leaf: true },
                            { id: 'LV01T017001', text: "ORDER_APPROVAL",  leaf: true },
                            //{ id: 'LV01T011005', text: "TRANSFERITEM",    leaf: true },					
                            { id: 'LV01T011002', text: "TRANSFER",        leaf: true },
                            { id: 'LV01T011001', text: "EDIT TRANSFER",   leaf: true },
                            { id: 'LV01T012001', text: "PENDING",         leaf: true },
                            { id: 'LV01T012002', text: "RECEIVE",         leaf: true },
                            { id: 'LV01T007001', text: "INVOICE",         leaf: true },
                            { id: 'LV01T015001', text: "RETURN",          leaf: true }
            ]},{
                id       : 'CM01T002',
                text     : "QUERY NODE",
                iconCls  : "fsl-tree-icon-node",
                expanded : false,
                children : [
                    {id: 'LV01T004002', text: "STOCK STATUS",             leaf: true },
                    {id: 'LV01T004003', text: "STOCK FOR NON EMPTY QTY",             leaf: true }
                ]
            },{ 
                id       : 'CM01I003', 
                text     : "Admin Report",
                iconCls  : "fsl-tree-icon-node",
                expanded : false,
                children : [
                            { id: 'LV01R001001', text: "INVOICE_REPORT(Admin)",         leaf: true },
                            { id: 'LV01R002001', text: "TRANSFER_REPORT(Admin)",        leaf: true },
                            { id: 'LV01R003001', text: "ORDER_REPORT(Admin)",           leaf: true },
                            { id: 'LV01R004001', text: "BARCODE_GENERATE",              leaf: true }
            ]},{ 
                id       : 'CM01I004', 
                text     : "User Report",
                iconCls  : "fsl-tree-icon-node",
                expanded : false,
                children : [                            
                            { id: 'LV01R011001', text: "BR.INVOICE_REPORT",             leaf: true },
                            { id: 'LV01R012001', text: "BR.TRANSFER_REPORT",            leaf: true },
                            { id: 'LV01R013001', text: "BR.ORDER_REPORT",               leaf: true }
            ]},{ 
                id       : 'CM04T001', 
                text     : "ADMIN",
                iconCls  : "fsl-tree-icon-node",
                expanded : true,
                children : [                        
                        { id: 'LV04T001001', text: "USERS",             leaf: true },
                        { id: 'LV04T002001', text: "AUTHORITY",         leaf: true },
                        { id: 'LV04T003001', text: "ROLE",              leaf: true },
                        { id: 'LV04J003001', text: "ROLE AUTHERITIES",  leaf: true },
                        { id: 'LV04J002001', text: "USER ROLES",        leaf: true },
                        { id: 'LV04J001001', text: "USER AUTHORITIES",  leaf: true } 
                       
            ]}
            ]
        }]
    }
});