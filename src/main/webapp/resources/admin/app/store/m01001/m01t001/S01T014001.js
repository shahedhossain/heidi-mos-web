
        /*api: {
            read    : '${createLink(controller:'C01t014001', action: 'store')}',
            destroy : '${createLink(controller:'C01t014001', action: 'removeall')}'
        },*/
       


Ext.define(Fsl.app.getAbsStore('S01T014001'), {
    extend      : 'Ext.data.Store',
    model       :  Fsl.app.getAbsModel('M01T014001'),
    idProperty  : 'id',
    autoLoad    :  false,
    autoSync    :  true,    
    remoteSort  :  false,
    pageSize    :  10,
    proxy       :  Fsl.proxy.getStoreAjaxUrlProxy('S01T014001'),
    sorters: [{
        property    : 'id', 
        direction   : 'asc'
    }]
});