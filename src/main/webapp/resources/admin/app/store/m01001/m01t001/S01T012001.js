Ext.define(Fsl.app.getAbsStore('S01T012001'), {
    extend      : 'Ext.data.Store',
    model       :  Fsl.app.getAbsModel('M01T012001'),
    idProperty  : 'id',
    autoLoad    :  false,
    autoSync    :  true,    
    remoteSort  :  false,
    pageSize    :  10,
    proxy       :  Fsl.proxy.getStoreAjaxUrlProxy('M01T012001'),
    sorters: [{
        property    : 'id', 
        direction   : 'asc'
    }]
});