Ext.define(Fsl.app.getAbsStore('S01T003002'), {   
    extend      : Fsl.app.getAbsStore('S01T003001'),
    model       : Fsl.app.getAbsModel('M01T003001'),
    proxy       : {
        type    : 'memory',
        reader  :{
            type  : 'json',
            root  : undefined
        }
    },
    sorters: [{
        property: 'id', 
        direction: 'asc'
    }]
});