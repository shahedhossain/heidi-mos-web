Ext.define(Fsl.app.getAbsStore('S01I016001'), {
    extend      : 'Ext.data.Store',
    model       :  Fsl.app.getAbsModel('M01I016001'),
    idProperty  : 'id',
    autoLoad    :  false,
    autoSync    :  true,    
    remoteSort  :  false,
    pageSize    :  10,
    proxy       :  Fsl.proxy.getStoreAjaxUrlProxy('S01I016001'),
    sorters: [{
        property: 'id', 
        direction: 'asc'
    }]
});
