Ext.define(Fsl.app.getAbsStore('S01I012001'), {
    extend      : 'Ext.data.Store',
    model       :  Fsl.app.getAbsModel('M01I012001'),
    idProperty  : 'id',
    autoLoad    :  false,
    autoSync    :  true,    
    remoteSort  :  false,
    pageSize    :  10,
    proxy       :  Fsl.proxy.getStoreAjaxUrlProxy('S01I012001'),
    sorters: [{
        property: 'id', 
        direction: 'asc'
    }]
});

