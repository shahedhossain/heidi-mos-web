Ext.define(Fsl.app.getAbsStore('S01I002004'), {
    extend      : 'Ext.data.Store',
    model       :  Fsl.app.getAbsModel('M01I002001'),
    idProperty  : 'id',
    autoLoad    : false,
    autoSync    : true,
    remoteSort  : true,
    pageSize    : 10,
    proxy       : Fsl.proxy.getStoreAjaxUrlProxy('C01I002001'),
    sorters: [{
        property: 'id', 
        direction: 'asc'
    }]
});