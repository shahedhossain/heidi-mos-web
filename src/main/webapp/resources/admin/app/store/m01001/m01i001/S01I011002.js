Ext.define(Fsl.app.getAbsStore('S01I011002'), {   
    extend      : Fsl.app.getAbsStore('S01I011001'),
    model       : Fsl.app.getAbsModel('M01I011001'),
    proxy       : {
        type    : 'memory',
        reader  :{
            type  : 'json',
            root  : undefined
        }
    },
    sorters: [{
        property: 'id', 
        direction: 'asc'
    }]
});