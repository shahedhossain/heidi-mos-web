Ext.define(Fsl.app.getAbsStore('S01I016002'), {   
    extend      : Fsl.app.getAbsStore('S01I016001'),
    model       : Fsl.app.getAbsModel('M01I016001'),
    proxy       : {
        type    : 'memory',
        reader  :{
            type  : 'json',
            root  : undefined
        }
    },
    sorters: [{
        property: 'id', 
        direction: 'asc'
    }]
});