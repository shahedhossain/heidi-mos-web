Ext.define(Fsl.app.getAbsStore('S01I014003'), {
    extend      : 'Ext.data.Store',
    model       :  Fsl.app.getAbsModel('M01I014001'),
    idProperty  : 'id',
    autoLoad    :  false,
    autoSync    :  true,    
    remoteSort  :  false,
    pageSize    :  10,
    proxy       :  Fsl.proxy.getStoreAjaxUrlProxy('S01I014001'),
    sorters: [{
        property: 'id', 
        direction: 'asc'
    }]
});
