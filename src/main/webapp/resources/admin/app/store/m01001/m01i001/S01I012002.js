Ext.define(Fsl.app.getAbsStore('S01I012002'), {   
    extend      : Fsl.app.getAbsStore('S01I012001'),
    model       : Fsl.app.getAbsModel('M01I012001'),
    proxy       : {
        type    : 'memory',
        reader  :{
            type  : 'json',
            root  : undefined
        }
    },
    sorters: [{
        property: 'id', 
        direction: 'asc'
    }]
});