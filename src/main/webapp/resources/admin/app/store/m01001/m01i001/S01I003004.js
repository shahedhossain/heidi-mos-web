Ext.define(Fsl.app.getAbsStore('S01I003004'), {
    extend      : 'Ext.data.Store',
    model       :  Fsl.app.getAbsModel('M01I003001'),
    idProperty  : 'id',
    autoLoad    :  false,
    autoSync    :  true,    
    remoteSort  :  true,
    pageSize    :  10,
    proxy       :  Fsl.proxy.getStoreAjaxUrlProxy('C01I003001'),
    sorters: [{
        property    : 'id', 
        direction   : 'asc'
    }]
});