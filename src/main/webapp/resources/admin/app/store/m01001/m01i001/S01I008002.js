Ext.define(Fsl.app.getAbsStore('S01I008002'), {   
    extend      : Fsl.app.getAbsStore('S01I008001'),
    model       : Fsl.app.getAbsModel('M01I008001'),
    proxy       : {
        type    : 'memory',
        reader  :{
            type  : 'json',
            root  : undefined
        }
    },
    sorters: [{
        property: 'id', 
        direction: 'asc'
    }]
});