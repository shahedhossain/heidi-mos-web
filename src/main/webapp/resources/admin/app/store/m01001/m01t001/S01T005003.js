
/*create  : '${createLink(controller:'C01t005001', action: 'add')}',
            read    : '${createLink(controller:'C01t005001', action: 'store')}',
            update  : '${createLink(controller:'C01t005001', action: 'update')}',
            destroy : '${createLink(controller:'C01t011001', action: 'removeall')}'*/



Ext.define(Fsl.app.getAbsStore('S01T005003'), {
    extend      : 'Ext.data.Store',
    model       :  Fsl.app.getAbsModel('M01T005002'),
    idProperty  : 'id',
    autoLoad    :  false,
    autoSync    :  true,    
    remoteSort  :  false,
    pageSize    :  10,
    proxy       :  Fsl.proxy.getStoreAjaxUrlProxy('C01T005001'),
    sorters: [{
        property    : 'id', 
        direction   : 'asc'
    }]
});