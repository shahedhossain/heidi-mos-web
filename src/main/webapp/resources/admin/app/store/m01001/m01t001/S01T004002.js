Ext.define(Fsl.app.getAbsStore('S01T004002'), {   
    extend      : Fsl.app.getAbsStore('S01T004001'),
    model       : Fsl.app.getAbsModel('M01T004001'),
    proxy       : {
        type    : 'memory',
        reader  :{
            type  : 'json',
            root  : undefined
        }
    },
    sorters: [{
        property: 'id', 
        direction: 'asc'
    }]
});