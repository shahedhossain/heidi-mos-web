Ext.define(Fsl.app.getAbsStore('S01T008002'), {   
    extend      : Fsl.app.getAbsStore('S01T008001'),
    model       : Fsl.app.getAbsModel('M01T008001'),
    proxy       : {
        type    : 'memory',
        reader  :{
            type  : 'json',
            root  : undefined
        }
    },
    sorters: [{
        property: 'id', 
        direction: 'asc'
    }]
});