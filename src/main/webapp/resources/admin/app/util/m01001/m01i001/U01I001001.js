Ext.define(Fsl.app.getAbsUtil('U01I001001'), {
    singleton   : true,
    config      : {
        name    : 'U01I001001'
    },
    constructor : function(options){
        this.initConfig(options);
    },
    statics     : {
        VERSION     : 1,
        REVISION    : 2
    },
    info: function(){
        return this.getName();
    },
    more: function(){
        return this.info();
    }
});


