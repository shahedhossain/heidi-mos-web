﻿/**
 * This JavaScript Library use for initial configuration of Extjs 4.1 apps, To
 * configuare application bootstrap initially.
 *
 * @static
 * @class       cfg
 * @package     Fsl.cfg
 * @access      public
 * @version     1.0.0
 * @since       Jun 02, 2013
 *
 **/
;Fsl.cfg||(function($){
	var cfg = {
		$class      : 'cfg',
		$package    : 'Fsl.cfg',
		appName     : 'Fsl.admin',
		appFolder   : 'app',
		uxFolder    : '../ux',
		buildMode   : true
	};	
	$.cfg = cfg;
}(Fsl));
Fsl.app.init(true);