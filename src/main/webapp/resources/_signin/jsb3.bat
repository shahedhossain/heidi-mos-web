@echo off
echo           ###############################################
echo          ###############################################
echo         ###                                         ###
echo        ###  ####### ###### ####### ###### ######## ###
echo       ###  ##      ##     ##   ## ##       ###    ###
echo      ###  #####   ###### ##   ## #####    ###    ###
echo     ###  ##          ## ##   ## ##       ###    ###
echo    ###  ##   ## ###### ####### ##       ###    ###
echo   ###                                         ###
echo  ###############################################
echo ###############################################
echo:

echo:
rem Software Developer use only
rem Environment Variable and Path Setting
set "EXTJS_APP=app"
set "CURRENT_DIR=%CD%"
set "BUILD_DIR=%CURRENT_DIR%"
set "APP_JSB3=%EXTJS_APP%.jsb3"
set "APP_HTML=index.html"

if "%SENCHA_HOME%" == "" goto failureSDK
if not "%SENCHA_HOME%" == "" goto setPath

:setPath
set "SENCHA_SDK_HOME=%SENCHA_HOME%\sdk"
set "Path=%Path%;%SENCHA_SDK_HOME%"

echo JSB3 file creating . . . . .
sencha create jsb -a %APP_HTML% -p %APP_JSB3%
goto end

:failureSDK
echo Sencha SDK Home not found
echo Install Sencha SDK
goto end

:end
@echo on
@pause