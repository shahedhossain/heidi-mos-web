Ext.define('Fsl.signin.controller.Authentication', {
    extend      : 'Ext.app.Controller',  
    views       : Fsl.view.getViews(['EastPanel','CenterPanel','EastPanel']), 
    refs        : [{
        ref: 'signinPanel',
        selector: 'form'
    }], 
    init        : function() { 
        this.control({
            'signinPanel button[action=login]':{
                click: this.onSignin
            },
            'signinPanel *':{
                specialkey: function (field, el) {
                    if (el.getKey() == Ext.EventObject.ENTER){
                        var button = field.up('panel').down('button[action=login]');
                        button.fireEvent('click', button);                            
                    }
                }
            },
            'signinPanel button[action=cancel]':{
                click: this.onCancel
            }
        });
    },
    onSignin: function(button){   
        var form  = button.up('form').getForm(),
        actionUrl = 'j_spring_security_check',
        checkUrl  = Fsl.route.getSite() + actionUrl;
        
        if (form.isValid()) {
            form.submit({
                url     : checkUrl,
                waitMsg : 'Authenticating ...',
                method  : 'POST',
                success : function(form, operation) {
                    Fsl.security.check(operation);
                },
                failure : function(form, operation) {
                    var summary  = Fsl.message.init(operation).getMessage(),
                    message      = summary ? summary : 'No response';
                    Ext.Msg.alert('Failed', message);
                }
            });
        } else {
            Ext.Msg.alert( "Error!", "Your form is invalid!" );
        }
    },
    onCancel: function(button){
        button.up('form').getForm().reset();
    }
});