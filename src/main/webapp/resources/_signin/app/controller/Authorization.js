Ext.define('Fsl.signin.controller.Authorization', {
    extend      : 'Ext.app.Controller',  
    views       : Fsl.view.getViews(['EastPanel','CenterPanel','EastPanel']), 
    refs        : [{
        ref: 'signinPanel',
        selector: 'form'
    }], 
    init        : function() { 
        this.control({
            'signinPanel button[action=login]':{
                click: this.onSignin
            },
			'signinPanel button[action=cancel]':{
                click: this.onCancel
            }
        });
    },
    onSignin: function(){    
        console.log('Authorization:onSignin');
    },
	onCancel: function(){    
        console.log('Authorization:onCancel');
    }
});