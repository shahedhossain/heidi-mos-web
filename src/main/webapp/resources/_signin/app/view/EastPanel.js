Ext.define('Fsl.signin.view.EastPanel' , {
    extend       : 'Ext.panel.Panel',
	requires	 : [
        'Ext.layout.container.VBox'
    ],    
    alias        : 'widget.eastPanel',
    border       : true,
	header		 : false,
    singleExpand : true,
	overflowY	 : 'auto',
	layout		 : {
		type	: 'vbox',
		align	: 'center',
		pack	: 'center'
	},
	bodyStyle    : {
		background : 'none'
	},	
	
    initComponent: function() {   
        var me  = this;
        /*me.tbar = [{
			xtype        : 'textfield',
			emptyText    : 'Type Here',
			fieldBodyCls :  'fsl-tree-filter',
			flex         : 1
		}];
		me.bbar= Ext.create('Ext.ux.StatusBar', {
            topBorder       : true,
            text            : 'Status',
            defaultIconCls  : 'fsl-severity-info',
            defaultText     : 'Status'
        });*/
		me.items = [{ 
			xtype    : 'signinPanel'         
		}];
        me.callParent(arguments);
    }
});