Ext.define('Fsl.signin.view.CenterPanel' , {
    extend       : 'Ext.panel.Panel',
    alias        : 'widget.centerPanel',
	bodyCls      : 'fsl-content-bg',
    layout       : 'fit',
	border       : true,
    singleExpand : true,	
	
    initComponent: function () {
        var me = this;        
         me.callParent(arguments);
    }
});